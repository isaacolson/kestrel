#! /usr/bin/env python
# encoding: utf-8

import pdb

import sys
import waflib
from waflib.Errors import ConfigurationError

# these variables are mandatory ('/' are converted automatically)
top = '.'
out = 'build'

# Allow import of custom tools, preferencing local installs
sys.path = ['waftools'] + sys.path

#           name : [machine types, compiler type]
variants = {'x64':['x64', 'gnu'],
            #'arm':'arm', # TODO: add support for cross compile
            'asan':['x64', 'clang'],  ## Core Sanitizers (Address, Undefined-Behavior)
            'msan':['x64', 'clang'],  ## Memory Sanitizers
            'tsan':['x64', 'clang'],  ## Thread Sanitizer
            }

def options(ctx):
    ctx.load('compiler_c')
    ctx.load('compiler_cxx')
    ctx.add_option('-s', '--symbols', dest='symbols', default=False, action='store_true',
                   help='Leave the debugging symbols in the resulting object files')
    ctx.add_option('-d', '--debug', dest='debug', default=False, action='store_true',
                   help='Compile all C/C++ code in debug mode: no optimizations and full symbols')

def configure(ctx):
    ctx.setenv('x64')

    ctx.load('compiler_c')
    ctx.load('compiler_cxx')

    ctx.load('java')
    ctx.check_jni_headers()

    ctx.load('helpers')

    try:
        ctx.load('cxxtest')
        ctx.env.CXXTEST_VERSION = ctx.cxxtest_version((4, 3))
    except ConfigurationError:
        ctx.env.CXXTESTGEN = None

    try:
        ctx.load('zcm-gen')
        # TODO: update zcm-gen  waf tools to run this config for you
        ctx.check_cfg(package='zcm', args='--cflags --libs', uselib_store='zcm')
        ctx.check_cfg(package='zcm', variables='classpath', uselib_store='zcm')
        ctx.env.CLASSPATH_zcmjar = ctx.env.zcm_classpath
        if not ctx.env.CLASSPATH_zcmjar:
            ctx.msg('Checking for zcm.jar', None)
            raise ConfigurationError('Failed to find zcm.jar')
        else:
            ctx.msg('Checking for zcm.jar', ctx.env.CLASSPATH_zcmjar)
        ctx.check_java_class('zcm.zcm.ZCM', ctx.env.CLASSPATH_zcmjar)
    except ConfigurationError as e:
        waflib.Logs.error('ZCM not installed proplerly\n'
                          'clone from https://github.com/ZeroCM/zcm '
                          'and follow instructions there to install')
        ctx.env.ZCMGEN = None
        raise e

    try:
        ctx.load('clang-custom')
        ctx.env.CLANG_VERSION = ctx.clang_version((3,4))
    except ConfigurationError:
        ctx.env['CLANG'] = None
        ctx.env['CLANG++'] = None

    ctx.check_cfg(package='eigen3', args='--cflags --libs', uselib_store='eigen3')

    # Create a symlink to the src/cpp-c directory for includes
    link      = ctx.path.get_bld().abspath() + '/c4che/kestrel'
    cppSrcDir = ctx.path.get_src().abspath() + '/src/cpp-c/'
    ctx.symlink(cppSrcDir, link);

    ctx.recurse_all()

    ctx.setenv('asan', env=ctx.env.derive()) # sanitizers get a copy of the default environment
    ctx.setenv('msan', env=ctx.env.derive()) # sanitizers get a copy of the default environment
    ctx.setenv('tsan', env=ctx.env.derive()) # sanitizers get a copy of the default environment

def setup_environment(ctx):
    ctx.post_mode = waflib.Build.POST_LAZY
    ctx.env.VERSION='0.0.0'

    useOptimize = not waflib.Options.options.debug
    useSymbols = waflib.Options.options.debug or waflib.Options.options.symbols

    ## Set the machine type based on the variant specified by the user
    ctx.env.MACHINE = variants[ctx.variant][0]
    ctx.env.COMP_TYPE = variants[ctx.variant][1]

    WARNING_FLAGS = ['-Wall', '-Werror', '-Wno-unused-function', '-Wno-format-zero-length',
                                         '-Wno-enum-compare']
    SYM_FLAGS = ['-g']
    OPT_FLAGS = ['-O3']

    # Note: waf adds the -fPIC flag automatically when compiling a shared library
    ctx.env.CFLAGS_default    = ['-std=gnu99', '-pthread'] + WARNING_FLAGS
    ctx.env.CXXFLAGS_default  = ['-std=c++11', '-pthread'] + WARNING_FLAGS
    # c4che contains the kestrel symlink used for include paths
    ctx.env.INCLUDES_default  = [ctx.path.get_bld().abspath() + '/../c4che']
    ctx.env.DEFINES_default   = ['_LARGEFILE_SOURCE', '_FILE_OFFSET_BITS=64']
    ctx.env.LIB_default       = []
    ctx.env.LINKFLAGS_default = []

    # Note: to prevent a nasty issue with linking, the link-time pthread flag is separated from
    #       the compile time one. This requires binaries to specifically use 'pthread' while
    #       libraries should not. Otherwise, g++ will link to the version of pthread in the .so
    #       which produces a bug if the library is then not used
    # TODO: build the extraction of the -pthread flag from compiling libraries automatically
    #       so we can put it back in default
    #       Honestly ... it's been so long since I ran into this problem that I have no
    #       recollection as to what it was / how it affected compiling ...
    ctx.env.LINKFLAGS_pthread = ['-pthread']

    ctx.env.JAVACFLAGS        = ['-Xlint:unchecked']

    if useOptimize:
        ctx.env.CFLAGS_default   += OPT_FLAGS
        ctx.env.CXXFLAGS_default += OPT_FLAGS
    if useSymbols:
        ctx.env.CFLAGS_default   += SYM_FLAGS
        ctx.env.CXXFLAGS_default += SYM_FLAGS

    ctx.setup_cxxtest()

    if   ctx.env.COMP_TYPE == 'gnu':   setup_environment_gnu(ctx)
    elif ctx.env.COMP_TYPE == 'clang': setup_environment_clang(ctx)

    if   ctx.variant == 'asan': setup_environment_asan(ctx)
    elif ctx.variant == 'msan': setup_environment_msan(ctx)
    elif ctx.variant == 'tsan': setup_environment_tsan(ctx)

    ctx.env.ENVIRONMENT_SETUP = True

def setup_environment_gnu(ctx):
    FLAGS = ['-Wno-unused-local-typedefs']
    ctx.env.CFLAGS_default   += FLAGS
    ctx.env.CXXFLAGS_default += FLAGS

def setup_environment_clang(ctx):
    ctx.set_clang_compiler()
    FLAGS = ['-fcolor-diagnostics']
    if ctx.env.CLANG_VERSION >= (3,6):
        FLAGS += ['-Wno-unused-local-typedefs']
    ctx.env.CFLAGS_default    += FLAGS
    ctx.env.CXXFLAGS_default  += FLAGS
    ctx.env.LINKFLAGS_default += FLAGS

def setup_environment_asan(ctx):
    FLAGS = ['-fsanitize=address',     # AddressSanitizer, a memory error detector.
             '-fsanitize=integer',     # Enables checks for undefined or suspicious integer behavior.
             '-fsanitize=undefined' ]  # Fast and compatible undefined behavior checker.
    ctx.env.CFLAGS_default    += FLAGS
    ctx.env.CXXFLAGS_default  += FLAGS
    ctx.env.LINKFLAGS_default += FLAGS

def setup_environment_msan(ctx):
    FLAGS = ['-fsanitize=memory',              # MemorySanitizer, detector of uninitialized reads.
             '-fno-omit-frame-pointer',
             '-fno-optimize-sibling-calls',
             '-fsanitize-memory-track-origins' ]
    ctx.env.CFLAGS_default    += FLAGS
    ctx.env.CXXFLAGS_default  += FLAGS
    ctx.env.LINKFLAGS_default += FLAGS

def setup_environment_tsan(ctx):
    FLAGS = [ '-fsanitize=thread' ]  # ThreadSanitizer, a data race detector.
    ctx.env.CFLAGS_default    += FLAGS
    ctx.env.CXXFLAGS_default  += FLAGS
    ctx.env.LINKFLAGS_default += FLAGS


def build(ctx):
    if not getattr(ctx, 'variant', None): # Using x64 build variant as default
        if (ctx.cmd == 'clean'):
            ctx.fatal('clean cannot default to x64, try clean_x64')

        print 'No variant given, running default'
        waflib.Options.commands.insert(0, ctx.cmd + '_x64')
        return

    if not getattr(ctx.env, 'ENVIRONMENT_SETUP', None):
        setup_environment(ctx)

    if ctx.env.COMP_TYPE == 'clang' and (not ctx.env['CLANG'] or not ctx.env['CLANG++']):
        ctx.fatal('Cannot build sanitizers without clang installed')

    ctx.recurse_all()

def distclean(ctx):
    ctx.exec_command('rm -f waftools/*.pyc')
    waflib.Scripting.distclean(ctx)

# NOTE: The following code defines the following build variants
#   - x86-64: clean_x64, build_x64, install_x64, uninstall_x64
#   - all:    clean_all, build_all

from waflib.Build import BuildContext, CleanContext, InstallContext, UninstallContext

for x in variants:
    for y in (BuildContext, CleanContext, InstallContext, UninstallContext):
        name = y.__name__.replace('Context','').lower()
        class tmp(y):
            cmd = name + '_' + x
            variant = x

def build_all(ctx):
    for v in variants:
        waflib.Options.commands.insert(0, 'build_'+v)

def clean_all(ctx):
    for v in variants:
        waflib.Options.commands.insert(0, 'clean_'+v)
