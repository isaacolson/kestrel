Please find the Kestrel documentation on our wiki. The wiki can be accessed:

- online: https://bitbucket.org/isaacolson/kestrel/wiki/Home
- as a submodule: run `git submodule update --init --recursive` in the root
  directory of kestrel. Then look in the directory `wiki`

