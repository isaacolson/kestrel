#!/bin/bash

# TODO: some future improvements for better output and UX, pending which ones
#       people like / would find useful
# - add a valgrind option for checking test memory (note that the --error-exitcode
#   option for valgrind will be essential for catching memory problems)
# - add option to run using the clang builds

THISDIR=$(dirname "$(readlink -f "$0")")
BASEDIR=$(dirname $THISDIR)
BLD_DIR="$BASEDIR/build"
ARCH="x64"

## Create a temporary working dir
D=$(mktemp -d)
cleanup() { rm -rf $D;}
fail() { echo "$1"; cleanup; exit 1; }

## Strict mode
set -uo pipefail
command_failed () {
    errcode=$?
    echo "ERROR: Command on line ${BASH_LINENO[0]} failed ($errcode): $BASH_COMMAND"
    cleanup
    exit $errcode
}
trap command_failed ERR

function usage() {
    echo "Usage: $0 [-hv]"
    echo "    This script runs all CxxTest suites within the repo and formats the"
    echo "    output in a succinct manner."
    echo
    echo "  OPTIONS"
    echo "    -a      Use clang address sanitizer"
    echo "    -h      Show help"
    echo "    -m      Use clang memory sanitizer"
    echo "    -t      Use clang thread sanitizer"
    echo "    -v      Enable verbose test output"
    echo

    [ $# -eq 0 ] || exit $1
}

VERBOSE=false

while getopts "ahmtv" opt; do
    case $opt in
        v)
            VERBOSE=true
            ;;
        a)
            ARCH="asan"
            ;;
        m)
            ARCH="msan"
            ;;
        t)
            ARCH="tsan"
            ;;
        h)
            usage 0
            ;;
        :)
            echo "Option $OPTARG requires an argument." >&2
            usage 1
            ;;
        /?)
            usage 1
            ;;
    esac
done

BLD_DIR="$BLD_DIR/$ARCH"

# find all test runners and make sure someone didn't name something else 'runner'
TESTS=`find $BLD_DIR -name runner | grep "test/runner"`

# Note: a good post describing how to use tput: http://stackoverflow.com/a/20983251
bold=`tput bold`
yellowf=`tput setaf 3`
whitef=`tput setaf 7`
redf=`tput setaf 1`
greenf=`tput setaf 2`
bluef=`tput setaf 4`
redb=`tput setab 1`
greenb=`tput setab 2`
blueb=`tput setab 4`
reset=`tput sgr0`

newtest=${bold}${bluef}
failtest=${bold}${redf}
passtest=${bold}${greenf}

NUM_FAILED=0

parse_testname()
{
    prefix1="$BLD_DIR/"
    prefix2="src/cpp-c/"
    suffix="/test/runner"

    s=$1
    s=${s#$prefix1}
    s=${s#$prefix2}
    s=${s%$suffix}
    echo $s
}

for test in $TESTS; do
    trap - ERR  ## It's okay if tests fail, cxxtests will product failure output
    $test > $D/output 2>&1
    ret=$?
    trap command_failed ERR

    testname="${newtest}TEST: $(parse_testname $test)${reset}"
    if [ $ret -ne 0 ]; then
        result="${failtest}FAILED $ret TEST(S)${reset}"
        NUM_FAILED=$((NUM_FAILED + 1))
    else
        result="${passtest}PASSED TEST${reset}"
    fi

    printf "%-50s %s\n" "$testname" "$result"

    if [ $VERBOSE == true -a $ret -ne 0 ]; then
        cat $D/output
    fi
done

echo
if [ $NUM_FAILED -ne 0 ]; then
    echo "${newtest}RESULTS:${reset}    ${failtest}TOTAL FAILED SUITES: $NUM_FAILED${reset}"
else
    echo "${newtest}RESULTS:${reset}    ${passtest}PASSED ALL TESTS${reset}"
fi
echo

cleanup
exit $NUM_FAILED
