#!/bin/bash

# TODO: this seems outdated. Clang installation is not right and we likely
#       don't need a lot of these deps

PKGS=''

## Compilers and Basic build tools
PKGS+='build-essential ant libtool default-jre default-jdk python-dev '
## Note: not best to install clang-3.6 manually (and will have to add
##       /usr/bin/clang symlinks, but I think my packages got messed
##       up somehow and installing "clang" was resulting in the sanitizer
##       missing obvious memory leaks
PKGS+='nodejs npm clang-3.6 llvm-3.6 cxxtest '

## Core packages
PKGS+='libeigen3-dev '

## Driver and driver-like packages (some from April)
PKGS+='libusb-dev libdc1394-22-dev libgl1-mesa-dev libpng12-dev '

## Julia : http://julialang.org/downloads/platform.html
sudo add-apt-repository ppa:staticfloat/juliareleases
sudo add-apt-repository ppa:staticfloat/julia-deps
PKGS+='julia python-matplotlib '

sudo apt-get update
#sudo apt-get install -yq $PKGS
sudo apt-get install $PKGS
