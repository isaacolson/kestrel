#! /usr/bin/env python
# encoding: utf-8

import os
from waflib.Configure import conf

# Recurses to all subdirectories that contain wscripts. However, all wscripts
# must contain the function that recurse_all is called from. E.g. if a wscript
# calls recurse_all from both the configure and build methods, then every child
# wscript MUST have both the configure and build methds, even if they only contain
# a "return None" or "ctx.recurse_all()"
@conf
def recurse_all(ctx):
    dirs = next(os.walk(ctx.path.get_src().abspath()))[1];
    for d in dirs:
        # mandatory=False prevents recursion into directories without a wscript file
        ctx.recurse(d, mandatory=False)

# lists source from the current directory, excluding files containing the word "Main"
# Note, if it would be useful, could make this do recursive source as well
# TODO: consider making this recurse and also exclude Test code
@conf
def libsource(ctx):
    return ctx.path.ant_glob(['*.c', '*.cpp'], excl=['*Main*'])

# Creates an ctx.env entry called 'RPATH_name' where name is kw['name'] (or kw['target']
# if name does not exist). This entry will have an array with the string build path in it
# to serve as the rpath of any binary using that target.
# Note: kw['name'] and kw['target'] are assumed to be strings
@conf
def store_rpath(ctx, **kw):
    name = ''
    if 'name' in kw:
        name = kw['name']
    else:
        name = kw['target']

    ctx.env['RPATH_' + name] = [ctx.path.get_bld().abspath()]

# Use just like ctx.shlib, but stores an rpath variable for this build directory.
# For easy use, simply add the shlib to the 'use' list of the binary and compile using
# the provided ctx.rprogram function.
# TODO: make this happen automatically with the original ctx.shlib call by using
#       a waf task to catch shlib compiles (believe this could be done by a feature
#       based rule) and export the rpath name.
@conf
def rshlib(ctx, **kw):
    ctx.store_rpath(**kw)
    ctx.shlib(**kw)

# Searches the kw['use'] list for entries that have an 'RPATH_entry' defined in ctx.env.
# Augments kw['rpath'] with the results (in the same format as the original kw['rpath'])
# returns the augmented kw
@conf
def lookup_rpath(ctx, **kw):
    rpath = []
    if 'use' in kw:
        deps = kw['use']
        if isinstance(kw['use'], str):
            deps = kw['use'].split(' ')
        for x in deps:
            if 'RPATH_' + x in ctx.env:
                if isinstance(ctx.env['RPATH_' + x], str):
                    rpath.extend(ctx.env['RPATH_' + x].split(' '));
                else:
                    rpath.extend(ctx.env['RPATH_' + x]);

    if 'rpath' in kw:
        if isinstance(kw['rpath'], str):
            kw['rpath'] += ' ' + ' '.join(rpath)
        else:
            kw['rpath'] += rpath
    else:
        kw['rpath'] = rpath

    return kw


# Use just like ctx.program, but searches the 'use' list for entries that have
# an 'RPATH_entry' defined in ctx.env. Forwards these values to the rpath argument
# of ctx.program.
# TODO: make this happen automatically with the original ctx.program call by using
#       a waf task to catch binary compiles (believe this could be done by a feature
#       based rule) and add the rpath name
@conf
def rprogram(ctx, **kw):
    kw = ctx.lookup_rpath(**kw)
    ctx.program(**kw)

# Use to create a symlink called linkName that points to linkTo.
# Must use absolute paths for both linkTo and linkName
@conf
def symlink(ctx, linkTo, linkName):
    try:
        os.remove(linkName)
    except:
        pass

    os.symlink(linkTo, linkName)
