curdir = pwd;
addpath(fullfile(curdir, 'rotations'));
addpath(fullfile(curdir, 'kalman'));
addpath(fullfile(curdir, 'derivations'));
addpath(fullfile(curdir, 'benchmark'));
