function [xCorr, PCorr] = kfCorrect(x, P, z, R, h, H)
    S = H*P*H' + R;
    K = P*H' / S;
    xCorr = x + K*(z -h);
    PCorr = (eye(length(x)) - K*H)*P;
end
