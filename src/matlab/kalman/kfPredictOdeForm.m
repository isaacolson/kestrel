function xPdot = kfPredictOdeForm(t, xP, f, F, Q, L)
    nx = size(F, 1);
    x = xP(1:nx);
    P = reshape(xP((nx+1):end), nx, nx);

    xdot = f(t, x);
    Pdot = F * P + P * F' + L * Q * L';

    xPdot = [ xdot; reshape(Pdot, nx*nx, 1) ];
end
