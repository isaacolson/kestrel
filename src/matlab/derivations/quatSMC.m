% clear

syms qt qx qy qz qtdot qxdot qydot qzdot real;
q = [ qt; qx; qy; qz ];
qdot = [ qtdot; qxdot; qydot; qzdot ];
et = q(1);
ep = q(2:4);
qinv = quatInv(q);
qinvdot = quatInv(qdot);
[ qSubA qSubB ] = quatIdentities(q);
syms t x y z real
qDef = angAxToQuat(t, [x; y; z]);
qDefUnit = angAxToQuat(t, [x; y; sqrt(1-x^2-y^2)]);

syms wx wy wz real;
w = [ wx; wy; wz ];

syms qtd qxd qyd qzd qtddot qxddot qyddot qzddot real;
qd = [ qtd; qxd; qyd; qzd ];
qddot = [ qtddot; qxddot; qyddot; qzddot ];
etd = qd(1);
epd = qd(2:4);
[ qdSubA qdSubB ] = quatIdentities(qd);
syms td xd yd zd real;
qdDef = angAxToQuat(td, [xd; yd; zd]);
qdDefUnit = angAxToQuat(td, [xd; yd; sqrt(1-xd^2-yd^2)]);


Q1qinv = quatCompLeftMat(qinv);
Q1eqinv = Q1qinv(2:4, :);
Q1einvqinv = Q1eqinv';

Q2qd = quatCompRightMat(qd);
Q2eqd = Q2qd(2:4, :);
Q2einvqd = Q2eqd';

Fqinv = omegaToQinvdotMat(q);
Fwqinvdot = qinvdotToOmegaMat(q);

lambda = sym(sym('lambda%d%d', [3 3]), 'real');

% Base sliding surface definitions
Sq = Q2eqd * qinv;
Sqdot_base = Q1eqinv * qddot + Q2eqd * qinvdot;

% Put in synthetic control
Sqdot_synth = Q1eqinv * qddot + Q2eqd * Fqinv * w;

% Solve for synthetic control
wbard = Fwqinvdot * Q2einvqd * (-Q1eqinv * qddot - lambda*Sq);

% proving that stuff works
FqinvFwqinvdot = eye(4) - qinv*qinv';
Q2eqdFqinv = - 1/2 * (qd'*qinv*eye(3) + et*crossmat(epd) + etd*crossmat(ep) + epd*ep' + ep*epd');


tmp = rand(4,1);
qnum = angAxToQuat(tmp(1) * pi, tmp(2:4) / norm(tmp(2:4)));
tmp = rand(4,1);
qdnum = angAxToQuat(tmp(1) * pi, tmp(2:4) / norm(tmp(2:4)));
qenum = quatCompRightMat(qdnum)*quatInv(qnum);