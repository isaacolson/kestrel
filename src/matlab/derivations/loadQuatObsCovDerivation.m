clear;
syms q0 q1 q2 q3 wx wy wz Vwx Vwy Vwz dt real;

quat   = [ q0; q1; q2; q3 ];
angVel = [ wx; wy; wz ];
state  = [ quat; angVel ];
Pinit  = sym(zeros(length(state), length(state)));

quatDerivJacQuat = 0.5 * [   0 -wx -wy -wz; ...
                            wx   0  wz -wy; ...
                            wy -wz   0  wx; ...
                            wz  wy -wx   0 ];

quatDerivJacAngVel = 0.5 * [ -q1 -q2 -q3; ...
                              q0 -q3  q2; ...
                              q3  q0 -q1; ...
                             -q2  q1  q0 ];


FLong = sym(zeros(length(state), length(state)));
FLong(1:4, 1:4) = quatDerivJacQuat;
FLong(1:4, 5:7) = quatDerivJacAngVel;

Qw = [ Vwx 0 0; 0 Vwy 0; 0 0 Vwz ];

LLong = sym(zeros(length(state), size(Qw, 1)));
LLong(5:7, 1:3) = eye(3);

% long way gets too big to read or do anything with
% use abreviated form
syms q w Q33 Fqq44 Fqw43 tFqq44 tFqw34 dt real;

% use a 2x2 to represent our 7x7
% qq=4x4, qw=4x3
% wq=3x4, ww=3x3

% Fqq = quatDerivJacQuat
% Fqw = quatDerivJacAngVel
F  = [ Fqq44 Fqw43; ...
         0   0 ];
% abusing Matlab's propensity to alphabetize to get the matrix mult order to be right (mostly)
Ft = [ tFqq44 0; ...
       tFqw34 0 ];

L = sym([ 0; 1 ]);
LQLt = L * Q33 * L';

P0 = sym(zeros(2,2));

% first order integration
k1 = F * P0 + P0 * Ft + LQLt;
P1 = P0 + dt * k1;

% second order integration
P1p = P0 + dt/2 * k1;
k2  = F * P1p + P1p * Ft + LQLt;
P2  = P0 + dt * k2;

% fourth order integration
P2p = P0 + dt/2 * k2;
k3  = F * P2p + P2p * Ft + LQLt;
P3p = P0 + dt * k3;
k4  = F * P3p + P3p * Ft + LQLt;
P4  = P0 + dt/6 * (k1 + 2*k2 + 2*k3 + k4);

% results of these computations lead me to believe the the proper answer is this:
% where Q is the covariance of the angular error, not the angular velocity
% Even more interesting is that the forumation above did not depend on if we were
% using quaternions, rotation matrices, or euler angles. We basically just need
% to switch out the F that we are using
quatObsCov = Fqw43 * Q33 * tFqw34;
quatObsCovLong = FLong * LLong * Qw * LLong' * FLong';
quatObsCovLong = quatObsCovLong(1:4, 1:4);



