function Cov = rmatObsCov(rmatVec, Qang)
    F = rdotJacOmega(rmatVec);
    R = reshape(rmatVec, 3, 3);
    Cov = F * R' * Qang * R * F';
end
