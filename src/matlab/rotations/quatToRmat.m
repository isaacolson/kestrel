function R = quatToRmat(quat)
    qw2  = quat(1)*quat(1);
    qx2  = quat(2)*quat(2);
    qy2  = quat(3)*quat(3);
    qz2  = quat(4)*quat(4);
    qwqx = quat(1)*quat(2);
    qwqy = quat(1)*quat(3);
    qwqz = quat(1)*quat(4);
    qxqy = quat(2)*quat(3);
    qxqz = quat(2)*quat(4);
    qyqz = quat(3)*quat(4);
    R = [ qw2+qx2-qy2-qz2,   2*(qxqy-qwqz),   2*(qwqy+qxqz)
            2*(qwqz+qxqy), qw2-qx2+qy2-qz2,   2*(qyqz-qwqx)
            2*(qxqz-qwqy),   2*(qwqx+qyqz), qw2-qx2-qy2+qz2 ];
end
