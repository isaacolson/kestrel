function Rdot = rmatDeriv(R, omega)
    Rdot = R * crossmat(omega);
end
