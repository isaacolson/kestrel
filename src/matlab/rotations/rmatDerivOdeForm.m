function xdot = rmatDerivOdeForm(t, x)
    Rdot = rmatDeriv(reshape(x(1:9), 3, 3), x(10:12));
    xdot = [ reshape(Rdot, 9, 1); zeros(3, 1) ];
end
