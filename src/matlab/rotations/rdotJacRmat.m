function JR = rmatJacRmat(Rvec, omega)

    wx = omega(1);
    wy = omega(2);
    wz = omega(3);

    JR = [   0,   0,   0,  wz,   0,   0, -wy,   0,   0
             0,   0,   0,   0,  wz,   0,   0, -wy,   0
             0,   0,   0,   0,   0,  wz,   0,   0, -wy
           -wz,   0,   0,   0,   0,   0,  wx,   0,   0
             0, -wz,   0,   0,   0,   0,   0,  wx,   0
             0,   0, -wz,   0,   0,   0,   0,   0,  wx
            wy,   0,   0, -wx,   0,   0,   0,   0,   0
             0,  wy,   0,   0, -wx,   0,   0,   0,   0
             0,   0,  wy,   0,   0, -wx,   0,   0,   0 ];

end
