function Rnorm = svdNormalize(R)
    [U,S,V] = svd(R);
    Rnorm = U*V';
end
