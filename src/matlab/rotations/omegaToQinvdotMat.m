function Fqinv = omegaToQinvdotMat(q)
    Fqinv = 1/2 * [ -q(2:4)'
                    -q(1)*eye(3)-crossmat(q(2:4)) ];
end