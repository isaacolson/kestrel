function Jq = qdotJacQuat(quat, omega)
    Jq = [ 0      -omega'
           omega  crossmat(-omega) ];
end
