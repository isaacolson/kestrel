function xdot = quatDerivOdeForm(t, x)
    xdot = zeros(size(x));
    xdot(1:4) = quatDeriv(x(1:4), x(5:7));
end
