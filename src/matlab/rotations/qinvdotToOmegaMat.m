function Fwqinvdot = qinvdotToOmegaMat(q)
    Fwqinvdot = 2 * [ -q(2:4) -q(1)*eye(3)+crossmat(q(2:4)) ];
end