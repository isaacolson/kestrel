function xPdot = rmatPredictOdeForm(t, xP)
    F = zeros(12, 12);
    F(1:9,1:9)   = rdotJacRmat(xP(1:9), xP(10:12));
    F(1:9,10:12) = rdotJacOmega(xP(1:9));

    Q = 0.25*(0.1*0.1) * eye(3);

    L = zeros(12, 3);
    L(10:12,:) = eye(3);

    xPdot = kfPredictOdeForm(t, xP, @rmatDerivOdeForm, F, Q, L);
end
