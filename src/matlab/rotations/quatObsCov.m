function Cov = quatObsCov(quat, Qang)
    F = omegaToQdotMat(quat);
    R = quatToRmat(quat);
    Cov = F * R' * Qang * R * F';
end
