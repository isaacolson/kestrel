function Q1 = quatCompLeftMat(q, rows)
    if nargin == 1
        rows = 1:4;
    end

    Q1 = [ q(1) -q(2:4)'
           q(2:4) q(1)*eye(3)+crossmat(q(2:4)) ];

    Q1 = Q1(rows, :);
end
