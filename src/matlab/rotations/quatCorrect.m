function [xCorr, PCorr] = quatCorrect(x, P, quatObs)
    R = 0.25*(0.001*0.001)*eye(4) + quatObsCov(x(1:4), 0.25*(0.05*0.05)*eye(3));
    H = zeros(4,7);
    H(1:4,1:4) = eye(4);
    [xCorr, PCorr] = kfCorrect(x, P, quatObs, R, x(1:4), H);
end
