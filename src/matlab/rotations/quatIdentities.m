function [ subA subB ] = quatIdentities(q)
    subA = [ q'*q 
             1 - (q'*q-q(1)^2)
             1 - (q'*q-q(2)^2)
             1 - (q'*q-q(3)^2)
             1 - (q'*q-q(4)^2) 
             q'*q-q(1)^2
             q'*q-q(2)^2
             q'*q-q(3)^2
             q'*q-q(4)^2 ];
    subB = [ 1
             q(1)^2
             q(2)^2
             q(3)^2
             q(4)^2
             1 - q(1)^2
             1 - q(2)^2
             1 - q(3)^2
             1 - q(4)^2 ];
end