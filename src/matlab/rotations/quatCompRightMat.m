function Q2 = quatCompRightMat(q, rows)
    if nargin == 1
        rows = 1:4;
    end

    Q2 = [ q(1) -q(2:4)'
           q(2:4) q(1)*eye(3)-crossmat(q(2:4)) ];

    Q2 = Q2(rows, :);
end
