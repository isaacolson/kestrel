function qdot = quatDeriv(quat, omega)
    qdot = qdotJacQuat(quat, omega) * quat;
end
