function [xCorr, PCorr] = rmatCorrect(x, P, rmatVecObs)
    R = 0.25*(0.001*0.001)*eye(9) + rmatObsCov(x(1:9), 0.25*(0.05*0.05)*eye(3));
    H = zeros(9,12);
    H(1:9,1:9) = eye(9);
    [xCorr, PCorr] = kfCorrect(x, P, rmatVecObs, R, x(1:9), H);
end
