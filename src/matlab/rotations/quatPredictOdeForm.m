function xPdot = quatPredictOdeForm(t, xP)
    F = zeros(7, 7);
    F(1:4,1:4) = qdotJacQuat(xP(1:4), xP(5:7));
    F(1:4,5:7) = omegaToQdotMat(xP(1:4));

    Q = 0.25*(0.1*0.1) * eye(3);

    L = zeros(7, 3);
    L(5:7,:) = eye(3);

    xPdot = kfPredictOdeForm(t, xP, @quatDerivOdeForm, F, Q, L);
end
