opsPerLoop = 1000;
loops = 10;

dt    = 0.01;
tspan = 0:dt:((opsPerLoop-1)*dt);
options = odeset('MaxStep', dt);

omegaMin = [-3; -3; -3];
omegaMax = [ 3;  3;  3];

% Quaternions
ang = 0.4;
ax = [ 1; 2; 3 ];
ax = ax / norm(ax);
qInit = angAxToQuat(ang, ax);

if true
    tElapsed = zeros(7,1);
    for i = 1:loops
        omega = omegaMin + (omegaMax-omegaMin).*rand(size(omegaMin));
        x0 = [qInit; omega];
        P0 = 0.25*(0.1*0.1)*eye(7);
        xP0 = [x0; reshape(P0, 49, 1)];
        
        % Deriv
        tStart = tic;
        for j = 1:opsPerLoop
            quatDeriv(qInit, omega);
        end
        tElapsed(1) = tElapsed(1) + toc(tStart);


        % Integrate
        tStart = tic;
        [t, x] = ode45(@quatDerivOdeForm, tspan, x0, options); % tspan has opsPerLoop steps
        tElapsed(2) = tElapsed(2) + toc(tStart);

        tStart = tic;
        [t, x] = ode45(@quatDerivOdeForm, tspan, x0); % tspan has opsPerLoop steps
        tElapsed(3) = tElapsed(3) + toc(tStart);


        % Predict
        tStart = tic;
        [t, xP] = ode45(@quatPredictOdeForm, tspan, xP0, options); % tspan has opsPerLoop steps
        tElapsed(4) = tElapsed(4) + toc(tStart);

        tStart = tic;
        [t, xP] = ode45(@quatPredictOdeForm, tspan, xP0); % tspan has opsPerLoop steps
        tElapsed(5) = tElapsed(5) + toc(tStart);


        % Normalize
        tStart = tic;
        for j = 1:opsPerLoop
            xP(j,1:4) = xP(j,1:4) / norm(xP(j,1:4));
        end
        tElapsed(6) = tElapsed(6) + toc(tStart);


        % Correct
        xPf = xP(round(0.1/dt),:)';
        xf = xPf(1:7);
        xf(1:4) = xf(1:4) / norm(xf(1:4));
        Pf = reshape(xPf(8:end), 7, 7);
        tStart = tic;
        [xC, PC] = quatCorrect(xf, Pf, x0(1:4));
        tElapsed(7) = tElapsed(7) + toc(tStart);
    end

    tAve = tElapsed / loops;

    fprintf('\n');
    fprintf('Quaternion Derivative               per %04d ops: %f s\n', opsPerLoop, tAve(1));
    fprintf('Quaternion Integration (Fixed Step) per %04d ops: %f s\n', opsPerLoop, tAve(2));
    fprintf('Quaternion Integration (Var Step)   per %04d ops: %f s\n', opsPerLoop, tAve(3));
    fprintf('Quaternion Prediction (Fixed Step)  per %04d ops: %f s\n', opsPerLoop, tAve(4));
    fprintf('Quaternion Prediction (Var Step)    per %04d ops: %f s\n', opsPerLoop, tAve(5));
    fprintf('Quaternion Normalization            per %04d ops: %f s\n', opsPerLoop, tAve(6));
    fprintf('Quaternion Correction               per      op : %f s\n', tAve(7));
end



% Rotation Matrices

RInit = quatToRmat(qInit);

if true
    tElapsed = zeros(7,1);
    for i = 1:loops
        omega = omegaMin + (omegaMax-omegaMin).*rand(size(omegaMin));
        x0 = [reshape(RInit, 9, 1); omega];
        P0 = 0.25*(0.1*0.1)*eye(12);
        xP0 = [x0; reshape(P0, 144, 1)];
        
        % Deriv
        tStart = tic;
        for j = 1:opsPerLoop
            rmatDeriv(RInit, omega);
        end
        tElapsed(1) = tElapsed(1) + toc(tStart);


        % Integrate
        tStart = tic;
        [t, x] = ode45(@rmatDerivOdeForm, tspan, x0, options); % tspan has opsPerLoop steps
        tElapsed(2) = tElapsed(2) + toc(tStart);

        tStart = tic;
        [t, x] = ode45(@rmatDerivOdeForm, tspan, x0); % tspan has opsPerLoop steps
        tElapsed(3) = tElapsed(3) + toc(tStart);


        % Predict
        tStart = tic;
        [t, xP] = ode45(@rmatPredictOdeForm, tspan, xP0, options); % tspan has opsPerLoop steps
        tElapsed(4) = tElapsed(4) + toc(tStart);

        tStart = tic;
        [t, xP] = ode45(@rmatPredictOdeForm, tspan, xP0); % tspan has opsPerLoop steps
        tElapsed(5) = tElapsed(5) + toc(tStart);


        % Normalize
        tStart = tic;
        for j = 1:opsPerLoop
            xP(j,1:9) = reshape(svdNormalize(reshape(xP(j,1:9), 3, 3)), 1, 9);
        end
        tElapsed(6) = tElapsed(6) + toc(tStart);


        % Correct
        xPf = xP(round(0.1/dt),:)';
        xf = xPf(1:12);
        Rf = reshape(xf(1:9), 3, 3);
        Rf = svdNormalize(Rf);
        xf(1:9) = reshape(Rf, 9, 1);
        Pf = reshape(xPf(13:end), 12, 12);
        tStart = tic;
        [xC, PC] = rmatCorrect(xf, Pf, x0(1:9));
        tElapsed(7) = tElapsed(7) + toc(tStart);
    end

    tAve = tElapsed / loops;

    fprintf('\n');
    fprintf('Rot Matrix Derivative               per %04d ops: %f s\n', opsPerLoop, tAve(1));
    fprintf('Rot Matrix Integration (Fixed Step) per %04d ops: %f s\n', opsPerLoop, tAve(2));
    fprintf('Rot Matrix Integration (Var Step)   per %04d ops: %f s\n', opsPerLoop, tAve(3));
    fprintf('Rot Matrix Prediction (Fixed Step)  per %04d ops: %f s\n', opsPerLoop, tAve(4));
    fprintf('Rot Matrix Prediction (Var Step)    per %04d ops: %f s\n', opsPerLoop, tAve(5));
    fprintf('Rot Matrix Normalization            per %04d ops: %f s\n', opsPerLoop, tAve(6));
    fprintf('Rot Matrix Correction               per      op : %f s\n', tAve(7));
end
