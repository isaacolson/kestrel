#pragma once

#include <iostream>
#include <limits>
#include <string>
#include <utility>
#include <vector>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

namespace Kestrel {
namespace Sim {

typedef std::pair<f64, f64> NextTimeObsAct;

static const f64 NO_ACTION    = std::numeric_limits<f64>::max();
static const f64 NO_OBSERVE   = std::nextafter(NO_ACTION,  std::numeric_limits<f64>::lowest());
static const f64 NEXT_OBSERVE = std::nextafter(NO_OBSERVE, std::numeric_limits<f64>::lowest());

// TODO: consider making an enum macro that declares the string vector automatically

enum Timers : sz { OBS, ACT, NUM_TIMERS };
static const std::vector<std::string> TimersNames { "OBS", "ACT" };

enum Perfs : sz { PERF_HDL, PERF_OBS, PERF_PRC, PERF_ACT, NUM_PERFS };
static const std::vector<std::string> PerfsNames { "PERF_HDL", "PERF_OBS", "PERF_PRC", "PERF_ACT" };

enum Phases : sz { PHASE_CONSTRUCT,   ///< Object is not associated with a manager
                   PHASE_IN_MANAGER,  ///< Object is now owned by the manager
                   PHASE_INIT_OBS,    ///< single threaded
                   PHASE_PRE_OBS,     ///< not synced with manager
                   PHASE_OBS_TRG_CHK, ///< manager waiting on object
                   PHASE_OBS_TRG_SLP, ///< not synced with manager
                   PHASE_OBS_HANDLE,  ///< manager waiting on object
                   PHASE_OBS,         ///< manager waiting on object
                   PHASE_PRC,         ///< not synced with manager
                   PHASE_ACT,         ///< manager waiting on object
                   PHASE_COMPLETE,    ///< object being stopped / destructed
                   NUM_PHASES };
static const std::vector<std::string> PhasesNames { "PHASE_CONSTRUCT",
                                                    "PHASE_IN_MANAGER",
                                                    "PHASE_INIT_OBS",
                                                    "PHASE_PRE_OBS",
                                                    "PHASE_OBS_TRG_CHK",
                                                    "PHASE_OBS_TRG_SLP",
                                                    "PHASE_OBS_HANDLE",
                                                    "PHASE_OBS",
                                                    "PHASE_PRC",
                                                    "PHASE_ACT",
                                                    "PHASE_COMPLETE",
                                                    "NUM_PHASES" };

struct ObjPerf {
    std::string        name;
    MathUtil::NormDist exptimes[NUM_TIMERS];
    MathUtil::NormDist runtimes[NUM_PERFS];

    inline void print(std::ostream& os = std::cout) const
    {
        os << name << ":\n";
        os<< "    Exptimes:\n";
        for (sz i = 0; i < NUM_TIMERS; ++i) {
            os << "        " << TimersNames[i] << ": " << exptimes[i] << "\n";
        }
        os<< "    Runtimes:\n";
        for (sz i = 0; i < NUM_PERFS; ++i) {
            os << "        " << PerfsNames[i] << ": " << runtimes[i] << "\n";
        }
    }

    friend inline std::ostream& operator<<(std::ostream& os, const ObjPerf& op)
    {
        op.print(os);
        return os;
    }
};

static void __KESTREL_SIM_CONSTS_DONT_COMPLAIN__()
{
    (void) NO_ACTION;
    (void) NO_OBSERVE;
    (void) NEXT_OBSERVE;
    (void) TimersNames;
    (void) PerfsNames;
    (void) PhasesNames;
}

} /* Sim */
} /* Kestrel */
