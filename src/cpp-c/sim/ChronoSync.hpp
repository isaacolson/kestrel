#pragma once

#include <utility>
#include <initializer_list>
#include <atomic>
#include <chrono>
#include <limits>
#include <mutex>
#include <condition_variable>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/PriorityQueue.hpp"

namespace Kestrel {
namespace Sim {

template <typename Scalar, sz num=1>
class ChronoSync {
  public:
    class Waiter {
      private:
        sz                              idx;
        Scalar                          t;
        bool                            excl;
        mutable std::condition_variable cv;

        /// Limits wait on the mutex to a timed wait in microseconds. Value of 0 signals no limit.
        u64                             waitForUs;

      public:
        /// Creates an object that allows for waiting on this timer until the specified timer
        /// is updated to be >= the specified value. Force exclusive > comparison with bool arg
        Waiter(sz idx, Scalar t, bool excl = false,
               u64 waitForUs = std::numeric_limits<u64>::max()) :
            idx(idx), t(t), excl(excl), waitForUs(waitForUs)
        { Assert(idx < num, "update idx out of bounds"); }

        bool operator==(const Waiter& o) const
        { return (idx == o.idx) && (t == o.t) && (excl == o.excl); }

        bool operator< (const Waiter& o) const
        { return (t < o.t) || (t == o.t && !excl && o.excl); }

        // Note: std::less creates a MAX heap, std::greater creates a MinHeap
        struct PtrMinHeap {
            bool operator()(const Waiter* a, const Waiter* b) { return *b < *a; }
        };

        Scalar getWakeTime()         const { return t; }
        bool   readyToWake(Scalar t) const { return (t > this->t) || (!excl && t == this->t); }

        // No time savings from swaps. `cv` not copied.
        Waiter(const Waiter& o) : Waiter(o.idx, o.t, o.excl) {}
        Waiter(Waiter&& o) : Waiter(o.idx, o.t, o.excl) {}
        Waiter& operator=(Waiter o) { idx = o.idx; t = o.t; excl = o.excl; return *this; }

        friend ChronoSync;
    };

    struct ScalarWithFlag {
        Scalar val;
        bool   flag;
        operator Scalar() const { return val; }
        // Trivial construction, copy, and swap default defined
    };

  private:
    typedef std::mutex LockType;

    // Note: the only things protected by the constness of this class are the values stored
    //       within the timers and the invalid flag of the class as a whole. Waiters may be
    //       added or removed (via being notified) from the class without breaking its
    //       const assumptions
    Scalar                                                            t[num] {};
    std::atomic_bool                                                  invalid {false};
    mutable LockType                                                  lk[num];
    mutable PriorityQueue<const Waiter*, typename Waiter::PtrMinHeap> q[num];

  public:
    /// Ctor
    ChronoSync() { static_assert(num > 0, "Must have at least 1 chrono"); }
    /// Initializer List Ctor
    ChronoSync(std::initializer_list<Scalar> vals) : ChronoSync()
    {
        Assert(vals.size() <= num, "to many values to initialize");
        sz j = 0;
        for (auto i = vals.begin(); i < vals.end(); ++i, ++j) t[j] = *i;
    }
    /// Dtor
    virtual ~ChronoSync()
    {
        // TODO: it is not safe to be waiting on a timer while it is destroyed (even though the
        //       thread will be notified, the lock will be gone). Might adjust this later to
        //       keep track of the number of threads waiting on this and wait for all of them
        //       to wake up before finishing destructor.
        forceWake();
    }

    /// Thread safe access element of t
    Scalar get(sz idx) const
    {
        Assert(idx < num, "update idx out of bounds");
        std::unique_lock<LockType> lock(lk[idx]);
        return t[idx];
    }

    /// Thread safe update element of t, signals on completion
    void set(sz idx, Scalar val)
    {
        Assert(idx < num, "update idx out of bounds");
        {
            std::unique_lock<LockType> lock(lk[idx]);
            t[idx] = val;
            notify(idx, std::move(lock));
        }
    }

    /// Thread safe increment element of t, signals on completion
    Scalar increment(sz idx, Scalar inc)
    {
        Scalar ret;
        Assert(idx < num, "update idx out of bounds");
        {
            std::unique_lock<LockType> lock(lk[idx]);
            t[idx] += inc;
            ret = t[idx];
            notify(idx, std::move(lock));
        }

        return ret;
    }

    // TODO: we are currently allowing the user to hang onto the actual Waiter object while
    //       only using its pointer when waiting / notifying. We could change this to
    //       dynamically allocate the object internally (maybe a shared_pointer) to ensure
    //       that there is no way for the pointer to be invalid when we come around to signal
    //       the object. I haven't found a use case that necessitates the later yet, but
    //       if we do we could change over to that method.

    /// Constructs a Waiter object that can be used to put this thread to sleep
    /// See Waiter() for argument descriptions
    template <typename... Args>
    Waiter getWaiter(Args&&... args) const { return Waiter(std::forward<Args>(args)...); }

    /// Put this thread to sleep using a waiter pointer. Thread will wake when condition
    /// specified by the waiter is met.
    /// Returns the time that triggered the wakeup along with a bool that signals whether
    /// the wait condition was successful achieved (true) or if the wait ran out of time (false)
    /// Note that the return type can cast down to a Scalar, so calling `Scalar t = wait(...)`
    /// is perfectly valid if you don't have a maximum wait time specified.
    ScalarWithFlag wait(const Waiter* w) const
    {
        std::unique_lock<LockType> lock(lk[w->idx]);

        auto wakeup = [&](){ return wakeCond(w); };

        // if value is ready return it before pushing to the pqueue
        if (wakeup()) return {t[w->idx], true};

        q[w->idx].push(w);

        if (w->waitForUs == 0) {
            return {t[w->idx], false};
        } else if (w->waitForUs == std::numeric_limits<u64>::max()) {
            w->cv.wait(lock, wakeup);
            return {t[w->idx], true};
        } else {
            bool succ = w->cv.wait_for(lock, std::chrono::microseconds(w->waitForUs), wakeup);
            return {t[w->idx], succ};
        }
    }

    /// Have to declare this specifically so that passing a non-const pointer
    /// doesn't try to use the argument forwarding version of this function
    ScalarWithFlag wait(Waiter* w) const { return wait((const Waiter*) w); }

    /// Constructs a waiter and immediately puts this thread to sleep on it
    /// See Waiter() for argument descriptions
    template <typename... Args>
    ScalarWithFlag wait(Args&&... args) const
    {
        Waiter w = getWaiter(std::forward<Args>(args)...);
        return wait(&w);
    }

    /// Alters an existing waiter to use a new target time. Notifies threads sleeping on
    /// the waiter if the wakeup condition is now true.
    void changeWaitTime(Waiter* w, Scalar atLeast) const
    {
        std::unique_lock<LockType> lock(lk[w->idx]);

        w->t = atLeast;
        q[w->idx].reheapify();
        notify(w->idx, std::move(lock));
    }

    /// Force all threads waiting on timers to wake up. This call basically invalidates the
    /// timers, as the forced wakeup must remain set forever to ensure all threads wake up.
    void forceWake()
    {
        for (sz i = 0; i < num; ++i) lk[i].lock();

        invalid = true;
        notifyAll();

        for (sz i = 0; i < num; ++i) lk[i].unlock();
    }

    /// Check that timer is still in a valid state. Should only be used to check if you returned
    /// from a wait because this became invalid. Do not use this to synchronize yourself.
    bool isValid() const { return !invalid; }

  private:

    // Note: Required to own lk[w->idx] prior to calling
    bool wakeCond(const Waiter* w) const
    { return invalid || w->readyToWake(t[w->idx]); }

    /// Wakes up threads that are ready for processing. MUST already own lk[idx] through lock.
    /// lock will be unlocked upon return
    void notify(sz idx, std::unique_lock<LockType>&& lock) const
    {
        Assert(lock.owns_lock(), "Must own lock before notify");

        while (!q[idx].empty()) {
            const Waiter* w = q[idx].top();
            if (wakeCond(w)) {
                w->cv.notify_all();
                q[idx].pop();
            } else {
                break;
            }
        }
    }

    /// Wakes up ALL threads waiting on timers. MUST already own all lk[idx] and be invalid
    void notifyAll() const
    {
        Assert(invalid, "Must be invalid to wake all");

        for (sz i = 0; i < num; ++i) {
            std::vector<const Waiter*> v = q[i].clear();
            for (sz j = 0; j < v.size(); ++j) v[j]->cv.notify_all();
        }
    }

  public:

    // Note: Copy and swap behavior is disallowed for ChronoSync objects that have
    //       any Waiters in their queue.

    // TODO: Because of how we are handling the wakeup conditions in the wait functions,
    //       we can't safely swap ChronoSync objects while threads are waiting on them
    //       and maintain a logical wakeup behavior. Similarly, it isn't clear what it
    //       would mean to copy a ChronoSync that has threads waiting on it (regarding
    //       how the copy should interact with the waiting threads). If we find a use
    //       case for either of these in the future, we can revisit this idea.

    /// Swap
    inline void swap(ChronoSync& o)
    {
        for (sz i = 0; i < num; ++i) {
            lk[i].lock();
            o.lk[i].lock();
        }

        for (sz i = 0; i < num; ++i) {
            std::swap(t[i], o.t[i]);
            Assert(q[i].empty() && o.q[i].empty(), "Not allowed to swap ChronoSyncs with Waiters");
        }
        decltype(invalid) tmpInvalid {invalid.load()};
        invalid = o.invalid.load();
        o.invalid = tmpInvalid.load();

        for (sz i = 0; i < num; ++i) {
            lk[i].unlock();
            o.lk[i].unlock();
        }
    }
    /// Copy Ctor
    ChronoSync(const ChronoSync& o)
    {
        for (sz i = 0; i < num; ++i) {
            lk[i].lock();
            o.lk[i].lock();
        }

        for (sz i = 0; i < num; ++i) {
            t[i] = o.t[i];
            Assert(q[i].empty() && o.q[i].empty(), "Not allowed to swap ChronoSyncs with Waiters");
        }
        invalid = o.invalid.load();

        for (sz i = 0; i < num; ++i) {
            lk[i].unlock();
            o.lk[i].unlock();
        }
    }
    /// Move Ctor
    ChronoSync(ChronoSync&& o) { swap(o); }
    /// Unifying Assignment
    ChronoSync& operator=(ChronoSync o) { swap(o); return *this; }
};

} /* Sim */
} /* Kestrel */
