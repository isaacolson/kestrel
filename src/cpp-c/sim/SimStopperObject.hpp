#pragma once

#include <iostream>

#include "kestrel/sim/Object.hpp"
#include "kestrel/sim/Manager.hpp"

namespace Kestrel {
namespace Sim {

class SimStopperObject: public Object {
  public:
    typedef Sim::Object ParentType;

  public:
    f64      stopTime; ///< will terminate sim as soon as t > stopTime
    Manager* man;      ///< pointer to manager to stop
    bool     print;

  public:
    /// Ctor
    SimStopperObject(f64 stopTime, Manager* man, bool print = false) :
        stopTime(stopTime), man(man), print(print) { setName("SimStopper"); }
    /// Dtor
    virtual ~SimStopperObject() {}

    virtual NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world)
    { return NextTimeObsAct(NO_OBSERVE, stopTime); }

    virtual bool process(f64 tObs, f64 tObsExpect, f64 tAct) { return true; }

    virtual bool act(f64 t, f64 tExpect)
    {
        if (print) std::cout << "\nStopping: " << t << ", expected: " << tExpect
                             << ", intended: " << stopTime << std::endl;
        man->stop();
        Assert(man->isDone(), "should have stopped man");
        return true;
    }

    virtual Object* clone() const { return new SimStopperObject(*this); }

    /// Swap
    inline void swap(SimStopperObject& o)
    {
        ParentType::swap(o);
        std::swap(stopTime, o.stopTime);
        std::swap(man, o.man);
        std::swap(print, o.print);
    }
    /// Copy Ctor
    SimStopperObject(const SimStopperObject& o) : ParentType(o),
                                                  stopTime(o.stopTime),
                                                  man(o.man),
                                                  print(o.print) {}
    /// Move Ctor
    SimStopperObject(SimStopperObject&& o) { swap(o); }
    /// Unifying Assignment
    SimStopperObject& operator=(SimStopperObject o) { swap(o); return *this; }
};

} /* Sim */
} /* Kestrel */
