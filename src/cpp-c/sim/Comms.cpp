#include "Comms.hpp"

#include "kestrel/util/PriorityQueue.hpp"

using namespace std;
using namespace Kestrel::Sim;

// Note: Possible concurrent calls (based on how Sim uses Comms)
//
//  Function               | permissible phases         | Modified Internals
// ==============================================================================
//  publish                | Object act                 | transport
//                         |                            | extTrans
//                         |                            | pubChanToUid
// ------------------------------------------------------------------------------
//  subscribe              | Object initObs             | uidToSubData
//  setTrigger             | Object act                 | transport
//  hasTriggerSubscription |                            |
// ------------------------------------------------------------------------------
//  unsubscribe            | Object initObs             | uidToSubData
//                         | Object act                 | transport
//                         | Object destructor          |
// ------------------------------------------------------------------------------
//  flushIncoming          | Manager observe            | Msg queues (all uids)
//                         | (single thread before      | transport
//                         |  Objects observe)          | currTime
// ------------------------------------------------------------------------------
//  handleMe               | Object observe handle      | Msg queues (single uid)
//                         | (just before observe user) |
//
// Therefore, we only need guards on publish, subscribe, setTrigger, hasTriggerSubscription,
// and unsubscribe. `flushIncoming` is only called in 1 thread and completely independently
// of the other functions. `handleMe` calls happen concurrently, but affect separate data
// due to the different uids. Because we rely on how Sim uses this class to avoid redundant
// locking, it is very important to only use this within Sim.

Comms::Comms() :
    transport("nonblock-inproc"), extTrans(nullptr), externalZcmUrl("")
{ init(); }

Comms::Comms(const string& externalZcmUrl) :
    transport("nonblock-inproc"), extTrans(new zcm::ZCM(externalZcmUrl)),
    externalZcmUrl(externalZcmUrl)
{ init(); }

Comms::~Comms()
{ deinit(); }

/// Must own locks before calling
void Comms::init()
{
    Assert(extTrans == nullptr || extTrans->good(), "Initialized external zcm is not good");
    Assert(transport.good(), "Initialized sim zcm transport is not good");
}

/// Must own locks before calling
void Comms::deinit()
{
    for (auto& uidData : uidToSubData) {
        for (auto& subData : uidData.second) {
            cerr << "Leftover sub " << *subData << endl;
            subData->unsubscribe(&transport);
            delete subData;
        }
        uidData.second.clear();
    }
    uidToSubData.clear();
}

bool        Comms::good()            const { return transport.good(); }
int         Comms::err()             const { return transport.err(); }
const char* Comms::strerror()        const { return transport.strerror(); }
const char* Comms::strerrno(int err) const { return transport.strerrno(err); }

int Comms::publish(void* uid, const std::string& channel, const uint8_t* data, uint32_t len)
{
    std::unique_lock<decltype(dataLock)> lk(dataLock);

    // Note: we don't necessarily need to force uniqueness of publishers, but since
    //       zcm ipc transport does, we're going to keep this for now
    auto it = pubChanToUid.find(channel);
    if (it == pubChanToUid.end()) {
        pubChanToUid[channel] = uid;
    } else {
        if (it->second != uid) Assert(false, "publish channel collision");
    }

    if (extTrans) extTrans->publish(channel, data, len);
    return transport.publish(channel, data, len);
}

void* Comms::subscribe(void* uid, const string& channel, zcm::MsgHandler cb, void* usr,
                       TriggerCb triggerCb, void* triggerUsr)
{
    unique_lock<decltype(dataLock)> lk(dataLock);

    SubData* subData = new SubData(subNumber++, uid, channel,
                                   currTime, incomingMsgCache, cacheIdx,
                                   cb, usr, triggerCb, triggerUsr);

    auto uidIt = uidToSubData.find(uid);
    if (uidIt == uidToSubData.end()) uidToSubData[uid] = {};
    auto uidRes = uidToSubData[uid].insert(subData);
    Assert(uidRes.second, "SubData UID insertion failed due to duplicate addr");

    subData->subscribe(&transport);

    return subData;
}

void Comms::setTrigger(void* uid, void* subscription, TriggerCb triggerCb, void* triggerUsr)
{
    SubData* subData = (SubData*) subscription;

    unique_lock<decltype(dataLock)> lk(dataLock);

    // We don't actually need the subscription in the lookup table, but we want
    // to make sure we were given a valid pointer
    auto uidIt = uidToSubData.find(uid);
    Assert(uidIt != uidToSubData.end(), "Unable to find UID in sub map");
    auto subDataIt = uidIt->second.find(subData);
    Assert(subDataIt != uidIt->second.end(), "Unable to find sub in UID in sub set");

    subData->setTrigger(triggerCb, triggerUsr);
}

bool Comms::hasTriggerSubscription(void* uid) const
{
    unique_lock<decltype(dataLock)> lk(dataLock);

    auto uidIt = uidToSubData.find(uid);
    if (uidIt == uidToSubData.end()) return false;
    for (auto& subData : uidIt->second) {
        if (subData->isTrigger()) return true;
    }
    return false;
}

void Comms::unsubscribe(void* uid, void* subscription)
{
    SubData* subData = (SubData*) subscription;

    unique_lock<decltype(dataLock)> lk(dataLock);

    auto uidIt = uidToSubData.find(uid);
    Assert(uidIt != uidToSubData.end(), "Unable to find UID in sub map");
    auto subDataIt = uidIt->second.find(subData);
    Assert(subDataIt != uidIt->second.end(), "Unable to find sub in UID in sub set");
    uidIt->second.erase(subDataIt);
    if (uidIt->second.empty()) uidToSubData.erase(uidIt);

    subData->unsubscribe(&transport);

    // Note: we need to handle the deletion of the actual memory after we finish bookkeeping
    //       in the lookup tables (because the subData object is the same in both places)
    delete subData;
}

void Comms::flushIncoming(double t)
{
    currTime = t;

    int status;
    while ((status = transport.handleNonblock()) == ZCM_EOK) { ++cacheIdx; }
    if (status != ZCM_EAGAIN) {
        cerr << "ERR: ZCM sim transport handleNonblock() status = " << status << endl;
    }

    stable_sort(incomingMsgCache.begin(), incomingMsgCache.end(), compMsgWithSubs);

    for (auto& msgWithSubs : incomingMsgCache) {
        auto& msg = msgWithSubs.first;
        for (auto& sub : msgWithSubs.second) {
            sub->pushCachedMsg(msg);
        }
    }
    incomingMsgCache.clear();
    cacheIdx = 0;
}


void Comms::handleMe(void *uid)
{
    deque<MsgWithSub> msgSequence;

    // Rip the message data out of the queues within this object's subscription map
    // Note that we are stealing the dynamic memory and will clean it up ourselves
    auto uidIt = uidToSubData.find(uid);
    if (uidIt == uidToSubData.end()) return;

    PriorityQueue<SubData*, SubData::SubDataPtrMinHeap> pq;

    for (auto& subData : uidIt->second)
        if (!subData->empty())
            pq.push(subData);

    while (!pq.empty()) {
        SubData* curr = pq.top(); pq.pop();

        msgSequence.emplace_back(curr->popFrontMsg(), curr);

        if (pq.empty()) {
            while (!curr->empty()) {
                msgSequence.emplace_back(curr->popFrontMsg(), curr);
            }
        } else {
            while (!curr->empty() && (*curr < *pq.top())) {
                msgSequence.emplace_back(curr->popFrontMsg(), curr);
            }
        }

        if (!curr->empty()) pq.push(curr);
    }

    for (auto& msg: msgSequence) msg.second->dispatchMsg(msg.first.get());
}

/// Swap
void Comms::swap(Comms& o)
{
    Assert(  uidToSubData.empty() &&   pubChanToUid.empty() &&
           o.uidToSubData.empty() && o.pubChanToUid.empty(),
           "Swapping Sim::Comms is not allowed once it is in use");

    // Note: this is the only data that could be different given neither object is in use
    extTrans.swap(o.extTrans);
    externalZcmUrl.swap(o.externalZcmUrl);
}

/// Copy Ctor
Comms::Comms(const Comms& o) :
    transport("nonblock-inproc"), extTrans(nullptr), externalZcmUrl("")
{
    Assert(  uidToSubData.empty() &&   pubChanToUid.empty() &&
           o.uidToSubData.empty() && o.pubChanToUid.empty(),
           "Copying Sim::Comms is not allowed once it is in use");

    // Note: this is the only data that could be different given neither object is in use
    if (o.extTrans != nullptr) {
        extTrans.reset(new zcm::ZCM(o.externalZcmUrl));
        externalZcmUrl = o.externalZcmUrl;
    }

    init();
}
