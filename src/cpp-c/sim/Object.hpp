#pragma once

#include <limits>
#include <memory>
#include <utility>
#include <vector>
#include <string>
#include <mutex>

#include <zcm/zcm-cpp.hpp>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/ThreadUtil.hpp"
#include "kestrel/util/TimeUtil.hpp"

#include "kestrel/sim/SimConsts.hpp"
#include "kestrel/sim/ChronoSync.hpp"
#include "kestrel/sim/Comms.hpp"

namespace Kestrel {
namespace Sim {

class Manager;

class Object: public ThreadUtil::Thread
{
  public:
    typedef ThreadUtil::Thread ParentType;

    class ZCM : public zcm::ZCM
    {
      private:
        Object* obj;
        ZCM(Object* obj) : zcm::ZCM(nullptr), obj(obj) {}

        struct CachedSub {
            // Note: we need trivially constructable objects in here to actually
            //       put it in a vector, so changed types slighly from function signature
            void**             rawSub;
            std::string        channel;
            zcm::MsgHandler    cb;
            void*              usr;

            CachedSub(void*& rawSub, const std::string& channel, zcm::MsgHandler cb, void* usr) :
                rawSub(&rawSub), channel(channel), cb(cb), usr(usr) {}
        };
        std::vector<CachedSub> cachedSubs;

      public:
        virtual ~ZCM() {};

        bool        good()            const override
        { return (obj->comms != nullptr) && obj->comms->good(); }
        int         err()             const override
        { return (obj->comms == nullptr) ? ZCM_ECONNECT
                                         : obj->comms->err(); }
        const char* strerror()        const override
        { return (obj->comms == nullptr) ? zcm::ZCM::strerrno(ZCM_ECONNECT)
                                         : obj->comms->strerror(); }
        const char* strerrno(int err) const override
        { return (obj->comms == nullptr) ? zcm::ZCM::strerrno(err)
                                         : obj->comms->strerrno(err); }

        void run()           override { AssertMsg(false, "Invalid sim op from ", obj->getName()); }
        void start()         override { AssertMsg(false, "Invalid sim op from ", obj->getName()); }
        void stop()          override { AssertMsg(false, "Invalid sim op from ", obj->getName()); }
        int handle()         override { AssertMsg(false, "Invalid sim op from ", obj->getName());
                                        return ZCM_EINVALID; }
        int handleNonblock() override { AssertMsg(false, "Invalid sim op from ", obj->getName());
                                        return ZCM_EINVALID; }
        void flush()         override { AssertMsg(false, "Invalid sim op from ", obj->getName()); }

        int publishRaw(const std::string& channel, const uint8_t *data, uint32_t len) override
        {
            // Note: could return ZCM_EINVALID if we want to instead of asserting
            AssertMsg(obj->phase == PHASE_ACT, "Can only publish during act()\n",
                      "Inv pub from ", obj->getName(), " during ", PhasesNames[obj->phase]);
            return obj->comms->publish(obj->getUid(), channel, data, len);
        }

        // Note: use Sim::Object::setTriggerSubs() to make a subscription trigger observe
        inline void subscribeRaw(void*& rawSub, const std::string& channel,
                                 zcm::MsgHandler cb, void* usr) override
        {
            if (obj->phase == PHASE_CONSTRUCT) {
                cachedSubs.emplace_back(rawSub, channel, cb, usr);
                return;
            }

            AssertMsg(obj->phase == PHASE_INIT_OBS || obj->phase == PHASE_ACT,
                      "Can only subscribe during initObserve() or act()\n",
                      "Inv sub from ", obj->getName(), " during ", PhasesNames[obj->phase]);
            rawSub = obj->comms->subscribe(obj->getUid(), channel, cb, usr, nullptr, this);
        }

        inline void unsubscribeRaw(void*& rawSub) override
        {
            if (obj->phase == PHASE_CONSTRUCT) {
                for (auto it = cachedSubs.begin(); it != cachedSubs.end(); ++it) {
                    if (it->rawSub == &rawSub) {
                        cachedSubs.erase(it);
                        return;
                    }
                }
            }

            AssertMsg(obj->phase == PHASE_INIT_OBS || obj->phase == PHASE_ACT ||
                      obj->phase == PHASE_COMPLETE,
                      "Can only unsubscribe during initObserve(), act(), or destructor\n",
                      "Inv unsub from ", obj->getName(), " during ", PhasesNames[obj->phase]);
            obj->comms->unsubscribe(obj->getUid(), rawSub);
            rawSub = nullptr;
        }

        zcm_t* getUnderlyingZCM() override
        { AssertMsg(false, "Invalid sim op from ", obj->getName()); }

        friend Object;
    };
    friend ZCM;

    friend Manager;

  private:
    // TODO: consider renaming some of the variables in this class
    ChronoSync<f64, NUM_TIMERS>        timers;            ///< Timers tracking internal state
    const std::vector<const Object*>*  world   = nullptr; ///< list of objects in the world
    const ChronoSync<f64, NUM_TIMERS>* arbiter = nullptr; ///< Manager's timers used to synchronize
    f64                                arbDt;             ///< Manager's dt

    std::string name    = "";    ///< Name of this object
    bool        isChild = false; ///< if this object is a child, Manager use only

    Phases phase; ///< Which phase this object is in

    Comms* comms; ///< Underlying Comms comms, from Manager (who owns it)
    ZCM      zcm; ///< Wrapped zcm::ZCM instance to interface with Sim::Comms

    ChronoSync<f64, NUM_TIMERS>::Waiter* waiterPtr     = nullptr; ///< Waiter in use by Object
    bool                                 hasTriggerSub = false;   ///< Sub can wake Object
    bool                                 hasTriggerMsg = false;   ///< Msg waiting to wake Object

    MathUtil::NormDist  expTiming[NUM_TIMERS]; ///< Loop timings reported by observe()
    TimeUtil::StatTimer perf[NUM_PERFS];       ///< Real-time performance measurements

  protected:

  public:
    /// Ctor
    Object() : timers{std::numeric_limits<f64>::lowest(), NO_ACTION}, phase{PHASE_CONSTRUCT},
               zcm(this), expTiming{{0, 0, 0}, {0, 0, 0}}, perf{{false}, {false}, {false}} {}
    /// Dtor
    virtual ~Object() {}

    // XXX: add a way to report the required sim dt to run this object something like
    //      abs(tAct - tObs). If manager fails the check, print a nice big warning. Also
    //      allow manager to get a flag that let's it auto adjust it's dt.

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Children must override these interface functions
    ///////////////////////////////////////////////////////////////////////////////////////////

    // TODO: for better virtualization for inheritors that have multiple parents, might want
    //       to take the output as an argument
    //
    //       virtual void cloneInto(Object*& copyTo) const
    //
    /// Returns a new deep copy of this. MUST call the Object copy constructor.
    virtual Object* clone() const
    { AssertMsg(false, "Non-permissible code path in ", getName()); return new Object(*this); }

  protected:
    /// Initialize oject from the static world vector
    virtual void initObserve(const std::vector<const Object*>* world) { }

    /// Gather data from world, returns {next obs time, next act time}.
    /** Avoid doing heavy processing in this function, copy data to use locally for object's
     *  "process" call.
     *
     *  Input t is the sim time on wakeup, tExpect was the intended wakeup time. If frequency
     *  is controlled via a desired period, more consistent timing will be achieved by adding
     *  the period to tExpect to avoid cumulative precision errors.
     *
     *  Note that during an observe, the accessible data of the world is GUARANTEED to be in
     *  a read-only state. Therefore:
     *  - observe cannot change data that might be accessed by other sim objects
     *  - no locking is necessary on the observed data
     *  - This function should basically just be copying accessible data from other objects
     *    into a local non-accessible copy which will then be used in the process step
     *    (where the read-only state will not be guaranteed)
     *
     *  If this object has subscriptions through Sim::Comms, handler functions for new messages
     *  will *before* the observe function when the object wakes up. Triggered objects are
     *  guaranteed to wake up when a new message on any of their triggered subscriptions
     *  is received *unless* they are in a processing phase, in which case they will wake
     *  up immediately after their act phase.
     *
     *  TODO: this next statement isn't necessarily valid once we implement the objects that
     *        queue up actions as desribed in the  comments in the .cpp file
     *
     *  Return value {tObs, tAct} has some special interpretations and restrictions
     *  - (tObs >  t)            : normal operation
     *  - (tObs <= t)            : invalid, program will assert
     *  - (tObs == NEXT_OBSERVE) : object must wake up for observation on the next available
     *                             sim timestep. If this step also reported a necessary action,
     *                             that next available sim timestep will be the one immediately
     *                             following that action. Else it will be the very next sim
     *                             timestep period.
     *  - (tObs == NO_OBSERVE)   : object will never wake up for observation unless triggered
     *                             by a trigger-enabled message subscription
     *  - (tAct >= t &&
     *     tAct < tObs)          : normal operation
     *  - (tAct < tObs &&
     *     tObs < tAct + arbDt)  : normal operation BUT the next observation will be forced
     *                             onto the next timestep AFTER tAct. The tExpect value passed
     *                             to the next observation will be the same as the user asked
     *                             for, but the value of t will likely be 1 timestep later than
     *                             it otherwise should have been.
     *  - (tAct <  t)            : invalid, program will assert
     *  - (tAct == NO_ACTION)    : object will skip its next act phase
     */
    virtual NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world)
    { AssertMsg(false, "Non-permissible code path in ", getName()); return NextTimeObsAct(0, 0); }

    /// Process data gathered from observation in preparation for the next action
    /** Do the "heavy lifting" for your object here. Nothing else in the sim will block on
     *  this being done until the time reaches the specified act time
     *
     *  Arguments are the time when the observation happened, time when the observation was
     *  expected to happen, and the expected time of the action. Depending on the application,
     *  it can be important to check if tAct == NO_ACTION before actually processing data.
     *
     *  return : whether or not to apply this call of the function to the performance statistics
     *           (ie if the object essentially "skipped" its normal processing duties, it may
     *            be beneficial to return false to avoid corrupting the mean/var of normal use)
     */
    virtual bool process(f64 tObs, f64 tObsExpect, f64 tAct)
    { AssertMsg(false, "Non-permissible code path in ", getName()); return false; }

    /// Act on observed data by changing internal state
    /** Avoid doing heavy processing in this function, copy results of object's "process"
     *  call to a accessible location.
     *
     *  Input t is the sim time on wakeup, tExpect was the intended wakeup time
     *
     *  Note that during an act, the whole simulation is in a self-write-only state. It is
     *  GUARANTEED that no other objects will have access to any part of this object during
     *  the call. Therefore:
     *  - act should update the accessible data of this object so others may observe it later
     *  - no locking is necessary on the modified data
     *  - This function should basically just be copying a computed result from a local
     *    non-accessible copy into the accessible location that will be used in the next
     *    observe step
     *
     *  return : whether or not to apply this call of the function to the performance statistics
     *           (ie if the object essentially "skipped" its normal acting duties, it may
     *            be beneficial to return false to avoid corrupting the mean/var of normal use)
     */
    virtual bool act(f64 t, f64 tExpect)
    { AssertMsg(false, "Non-permissible code path in ", getName()); return false; }

  public:
    /// Set the object's name (useful for diagnostic printing). Usually a good idea to call
    /// Object::setName() from inside overriders to preserve copy/swap transfer of `name`
    virtual void setName(const std::string& name) { this->name = name; }
    /// Get the object's name (useful for diagnostic printing). Usually a good idea to call
    /// Object::getName() from inside overriders to preserve copy/swap transfer of `name`
    virtual const std::string& getName() const { return name; }

    /// Return a list of dynamically allocated objects to be added to manager (optional override)
    virtual std::vector<Object*> getChildObjects() const { return std::vector<Object*>{}; }

    ///////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Non-override interface functions
    ///////////////////////////////////////////////////////////////////////////////////////////

  protected:
    /// User access to the internal zcm pointer for zcm-style communications in sim
    inline ZCM* getZcm() { return &zcm; }

    /// Changes whether the given subscription is a "trigger" subs (triggering observe on msg)
    void setTriggerSubs(const zcm::Subscription* sub, bool isTrigger = true)
    {
        AssertMsg(phase == PHASE_INIT_OBS || phase == PHASE_ACT,
                  "Can only change sub trigger during initObserve() or act()\n",
                  "Inv trigger change from ", getName(), " during ", PhasesNames[phase]);

        comms->setTrigger(getUid(), sub->getRawSub(), isTrigger ? triggerCb : nullptr, this);
        hasTriggerSub = comms->hasTriggerSubscription(getUid());
    }

  public:
    /// Resets the object's performance statistics
    void resetPerf();

    ///////////////////////////////////////////////////////////////////////////////////////////

  private:
    // XXX: We don't actually want manager to have to give the dt to objects, but to
    //      prevent a rather nasty potential deadlock situation where objects place the
    //      next observe and act time in the same timestep even though nextObs > nextAct.
    //      Until we figure out a better way to prevent this issue, we'll just use the
    //      manager's dt to force a minimium delta between the two
    /// Setup data from sim manager. Only called by the manager when object is added to simulation
    void setManager(const std::vector<const Object*>* world, const ChronoSync<f64, 2>* arbiter,
                    f64 arbDt, Comms *comms = nullptr);

    /// Returns a new deep copy of this after it's been added to a manager. Only used by
    /// Manager to clone an object out of an unstarted Manager into a new, identical one
    /// that will then call setManager on the new Object
    Object* cloneToNewManager() const
    {
        AssertMsg(phase == PHASE_IN_MANAGER, "Obj must be in an unstarted Manager\n",
                  "Invalid cloneToNewManager from ", getName(), " during ", PhasesNames[phase]);
        const_cast<Object*>(this)->phase = PHASE_CONSTRUCT;
        Object* ret = clone();
        const_cast<Object*>(this)->phase = PHASE_IN_MANAGER;
        return ret;
    }

    /// Manager triggers the initial observe
    void managerInitObserve();

    /// Using the address of the object as the unique identifier. For now, we don't want to
    /// expose this at an api level, but it might become the case that we move this to protected
    void* getUid() { return this; };

    /// Callback used by Sim::Comms to trigger wake ups via trigger-subscriptions
    static void triggerCb(f64 t, void* usr);

    /// Make observations and actions when allowed by manager (user should not override this)
    void runFunc() override;

  protected:
    /// Force wakeups on any waiting threads (user should not override this in 99% of cases,
    /// and in the 1%, it is important to call Object::stopFunc() from the overriding defn)
    void stopFunc() override;

  public:

    // Note: Copy / Swap only allowed when Object is in the PHASE_CONSTRUCT to ensure there
    //       are not lingering pointers to manager timers / hanging subscriptions to zcm.
    //       This in general means that some of the things we are copying/swapping are
    //       guaranteed to be their initial values (and therefore ==), but we'll still
    //       leave them in the functions just to easier demonstrate completeness.

    /// Swap
    inline void swap(Object& o)
    {
        AssertMsg(phase == PHASE_CONSTRUCT && o.phase == PHASE_CONSTRUCT,
                  "Swapping Object after adding to Manager is not allowed,",
                  "Invalid swap from ", getName(), " during ", PhasesNames[phase],
                  " with ", o.getName(), " during ", PhasesNames[o.phase]);

        ParentType::swap(o);
        timers.swap(o.timers);
        std::swap(world, o.world);
        std::swap(arbiter, o.arbiter);
        name.swap(o.name);
        std::swap(isChild, o.isChild);
        std::swap(phase, o.phase);
        std::swap(comms, comms);

        // Intentionally do not swap zcm because it only operates through it's object
        AssertMsg(zcm.cachedSubs.empty() && o.zcm.cachedSubs.empty(),
                  "Swapping Object after subscribing is not allowed,",
                  " from ", getName(), " and ", o.getName());

        std::swap(waiterPtr, o.waiterPtr);
        std::swap(hasTriggerSub, o.hasTriggerSub);
        std::swap(hasTriggerMsg, o.hasTriggerMsg);
        for (sz i = 0; i < NUM_TIMERS; ++i) std::swap(expTiming[i], o.expTiming[i]);
        for (sz i = 0; i < NUM_PERFS;  ++i) perf[i].swap(o.perf[i]);
    }
    /// Copy Ctor
    Object(const Object& o) : ParentType(o),
                              timers(o.timers),
                              world(o.world),
                              arbiter(o.arbiter),
                              name(o.name),
                              isChild(o.isChild),
                              phase(o.phase),
                              comms(o.comms),
                              zcm(this),
                              waiterPtr(o.waiterPtr),
                              hasTriggerSub(o.hasTriggerSub),
                              hasTriggerMsg(o.hasTriggerMsg)
    {
        AssertMsg(phase == PHASE_CONSTRUCT && o.phase == PHASE_CONSTRUCT,
                  "Copying Object after adding to Manager is not allowed,",
                  "Invalid copy from ", getName(), " during ", PhasesNames[phase],
                  " with ", o.getName(), " during ", PhasesNames[o.phase]);

        AssertMsg(zcm.cachedSubs.empty() && o.zcm.cachedSubs.empty(),
                  "Copying Object after subscribing is not allowed,",
                  " from ", getName(), " and ", o.getName());

        for (sz i = 0; i < NUM_TIMERS; ++i) expTiming[i] = o.expTiming[i];
        for (sz i = 0; i < NUM_PERFS;  ++i) perf[i] = o.perf[i];
    }
    /// Move Ctor
    Object(Object&& o) : zcm(this) { swap(o); }
    /// Unifying Assignment
    Object& operator=(Object o) { swap(o); return *this; }
};

} /* Sim */
} /* Kestrel */
