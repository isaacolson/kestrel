#include <cmath>

#include <iostream>

#include "kestrel/util/types.hpp"

#include "Object.hpp"

using namespace Kestrel;
using namespace Sim;

void Object::resetPerf()
{
    for (sz i = 0; i < NUM_TIMERS; ++i) {
        expTiming[i] = MathUtil::NormDist{0, 0, 0};
    }
    for (sz i = 0; i < NUM_PERFS; ++i) {
        perf[i].resetStats();
    }
}

void Object::setManager(const std::vector<const Object*>* world,
                        const ChronoSync<f64, 2>* arbiter, f64 arbDt, Comms *comms)
{
    phase = PHASE_IN_MANAGER;

    this->world   = world;
    this->arbiter = arbiter;
    this->arbDt   = arbDt;
    this->comms   = comms;

    timers.set(OBS, arbiter->get(OBS));
}

void Object::managerInitObserve()
{
    phase = PHASE_INIT_OBS;
    for (auto& cachedSub : zcm.cachedSubs)
        zcm.subscribeRaw(*cachedSub.rawSub, cachedSub.channel, cachedSub.cb, cachedSub.usr);
    zcm.cachedSubs.clear();
    initObserve(world);
}

void Object::triggerCb(f64 t, void* usr)
{
    Object* obj = (Object*) usr;

    // In this case, obj already has a trigger message waiting for it the next time it wakes
    if (!obj->hasTriggerMsg) {
        obj->hasTriggerMsg = true;
        // Determine if the object is waiting in it's trigger state or still processing
        if (obj->waiterPtr != nullptr) {
            obj->timers.set(OBS, t);
            obj->arbiter->changeWaitTime(obj->waiterPtr, t);
        }
    }
}

void Object::runFunc()
{
    AssertMsg(arbiter != nullptr, "Cannot run object", getName(), ", no arbiter");
    AssertMsg(world   != nullptr, "Cannot run object", getName(), ", no world");

    // Note: Track the last time we woke up for an observe to keep accurate metrics
    //       on the rate the Object is waking up for observations.
    f64 lastTObs = std::numeric_limits<f64>::lowest();

    // Note: triggered objects MUST wake up on the timestep after any observe / act to
    //       see if messages that would trigger them were published on that step. So even
    //       if the user tries to sleep until time tNext, we need to wake up at tCurr + dt
    //       and check if messages were published before going to sleep until tNext. This
    //       is allowed to be NO_ACTION. Note that even in the case where the user actually
    //       asked for the next timestep, the second wait will still be valid because it will
    //       wake immediately.
    f64 untriggeredTExp = timers.get(OBS);

    while (!isDone()) {

        phase = PHASE_PRE_OBS;

        // ---- Synchronized observation ----
        f64 tExp = timers.get(OBS);
        f64 tObs = arbiter->wait(OBS, tExp);
        if (!arbiter->isValid()) break;

        phase = PHASE_OBS_TRG_CHK;

        if (hasTriggerSub && !hasTriggerMsg && untriggeredTExp != NEXT_OBSERVE) {

            auto waiter = arbiter->getWaiter(OBS, untriggeredTExp);
            waiterPtr = &waiter;

            phase = PHASE_OBS_TRG_SLP;

            // see comment on declaration of untriggeredTExp for explanation
            timers.set(OBS, untriggeredTExp);
            tObs = arbiter->wait(&waiter);
            if (!arbiter->isValid()) break;

            tExp = waiter.getWakeTime();
            waiterPtr = nullptr;
        }

        phase = PHASE_OBS_HANDLE;

        perf[PERF_HDL].start();
        if (comms) comms->handleMe(getUid());
        hasTriggerMsg = false;
        perf[PERF_HDL].stop();

        phase = PHASE_OBS;

        perf[PERF_OBS].start();
        NextTimeObsAct tNext = observe(tObs, tExp, world);
        perf[PERF_OBS].stop();

        // Record observe frequency (for comparison to actual performance)
        if (lastTObs != std::numeric_limits<f64>::lowest())
            expTiming[OBS] = MathUtil::normDist(expTiming[OBS], tObs - lastTObs);
        lastTObs = tObs;

        AssertMsg(tNext.first  >  tObs, "Next observation time may not be in past\n",
                  getName(), " requested observe at ", tNext.first, " when t = ", tObs);
        AssertMsg(tNext.second >= tObs, "Next action time may not be in the past\n",
                  getName(), " requested action at ", tNext.second, " when t = ", tObs);

        // XXX: this is bad for things that want to observe while processing. Example: scan
        //      matcher basically keeps a 1 message buffer of the most recent message received
        //      since it started processing a scan. As soon as it finishes processing it moves
        //      right on to the next message. We 100% need to support sensors like this, but
        //      it will require a reimagining of how this synchronization works.
        // TODO: likely going to have to make a child of this class that has much more complicated
        //       inner workings to create this type of performance. Plan would be to implement
        //       a 2 threaded object that processes outside the obs-act thread as described below
        //       Actually, the child should have 1 obs-act thread and n process threads

        // force object to wake on next manager loop
        if (tNext.first == NEXT_OBSERVE && tNext.second == NO_ACTION)
            tNext.first = std::nextafter(tObs, std::numeric_limits<f64>::max());

        AssertMsg(tNext.second < tNext.first || tNext.second == NO_ACTION,
                  "Must act before next observation unless no action required\n",
                  getName(), " observe returned: ", tNext.first, ", ", tNext.second);

        // Record expected act delay (for comparison to actual performance)
        if (tNext.second != NO_ACTION)
            expTiming[ACT] = MathUtil::normDist(expTiming[ACT], tNext.second - tObs);

        if (hasTriggerSub) {
            // see comment on declaration of untriggeredTExp for explanation
            untriggeredTExp = tNext.first;
            tNext.first = (tNext.second == NO_ACTION)
                          ? std::nextafter(tObs, std::numeric_limits<f64>::max())
                          : NEXT_OBSERVE;
        }

        // Note: that even if tNext.second == NO_ACTION, we still need to move the timer
        //       to allow manager to wake up
        timers.set(ACT, tNext.second); // Have to set act time first to avoid race condition
        timers.set(OBS, tNext.first);  // This will signal the manager to wake up

        if (tNext.second != NO_ACTION) {
            // Note: We can't wake up if the user for an observe if it will happen on the same
            //       timestep as their action. We have to fudge it over to the next timestep
            //       but still have their expected wakeup be set properly.
            if (tNext.first < tNext.second + arbDt) timers.set(OBS, NEXT_OBSERVE);

            // ---- Process while manager continues ----
            phase = PHASE_PRC;
            // TODO: may need to move this into its own thread which would let the obs-act thread
            //       run without blocking on the process and simply shunt data over for the process
            //       to handle in its own time. Hard bit would be waiting for the process thread
            //       to finish before applying the act.
            perf[PERF_PRC].start();
            bool useStats = process(tObs, tExp, tNext.second);
            if (useStats) perf[PERF_PRC].stop();
            else          perf[PERF_PRC].stopNoRecord();
            // TODO: maybe it would be better to keep track of a number of different distributions
            //       (ie to model multi modal behavior) by returning a size_t from process and act
            //       and applying the time to the designated distribution.

            // ---- Synchronized action ----
            f64 tAct = arbiter->wait(ACT, tNext.second);
            if (!arbiter->isValid()) break;

            phase = PHASE_ACT;

            perf[PERF_ACT].start();
            useStats = act(tAct, tNext.second);
            if (useStats) perf[PERF_ACT].stop();
            else          perf[PERF_ACT].stopNoRecord();

            // User asked for the next observe after their action, so we need to reset their timer
            if (tNext.first == NEXT_OBSERVE)
                tNext.first = std::nextafter(tAct, std::numeric_limits<f64>::max());
            timers.set(OBS, tNext.first);

            // TODO: change this to pull the next action time from a queue once the changes
            //       above are implemented

            // Don't know when to act next until obs tells us
            timers.set(ACT, NO_ACTION);
        }
    }

    phase = PHASE_COMPLETE;
}

void Object::stopFunc()
{
    timers.forceWake();
}
