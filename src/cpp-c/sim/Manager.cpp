#include "kestrel/util/TimeUtil.hpp"

#include "Manager.hpp"

using namespace std;
using namespace Kestrel;
using namespace Sim;

void Manager::addObject(Object* obj)
{
    // TODO: add this feature
    Assert(!isRunning(), "adding objects while running not yet supported");

    obj->setManager(&constWorld, &timers, dt, comms.get());
    world.emplace_back(obj);
    constWorld.emplace_back(obj);

    for (Object* child : obj->getChildObjects()) {
        child->isChild = true;
        addObject(child);
    }
}

std::vector<ObjPerf> Manager::getPerf() const
{
    std::unique_lock<decltype(pauseMutex)> lk(pauseMutex);

    if (isRunning() && !paused) return std::vector<ObjPerf>();

    sz n = world.size();
    std::vector<ObjPerf> ret(n);

    for (sz i = 0; i < n; ++i) {
        ret[i].name = world[i]->getName();
        for (sz j = 0; j < NUM_TIMERS; ++j) {
            ret[i].exptimes[j] = world[i]->expTiming[j];
            if (speed > 0) ret[i].exptimes[j].scale(1.0/speed);
        }
        for (sz j = 0; j < NUM_PERFS; ++j) {
            ret[i].runtimes[j] = world[i]->perf[j].stats();
        }
    }

    return ret;
}

bool Manager::resetPerf(const std::string& name)
{
    std::unique_lock<decltype(pauseMutex)> lk(pauseMutex);

    // Note: It's possible to make this function...function in the case where
    //       sim isn't paused by pausing inside of here, resetting, and then
    //       unpausing
    if (isRunning() && !paused) return false;

    if (name.empty()) for (auto& obj : world)                             obj->resetPerf();
    else              for (auto& obj : world) if (name == obj->getName()) obj->resetPerf();

    return true;
}

void Manager::runFunc()
{
    // decrement once so first loop starts at t0
    timers.increment(OBS, -dt);
    timers.increment(ACT, -dt);

    initObs();
    startObjects();

    u64 realTime[2] {TimeUtil::utime(), 0};
    while (!isDone()) {

        {
            unique_lock<decltype(pauseMutex)> lk(pauseMutex);
            // need to do paused callback here to avoid race conditions
            if (paused && pauseCb) pauseCb(this);
            pauseCond.wait(lk, [&]{ return !paused || isDone(); });
            if (isDone()) break;
        }

        syncObs(dt);

        // Note: real time is modeled as passing during the "process" step

        realTime[1] = TimeUtil::utime();
        if (speed > 0)
        {
            // TODO: could probably do a little better than a blind sleep here, but should be ok
            //       maybe consider a tolerance on the comparison?
            u64 wakeup = realTime[0] + (1e6 * dt) / speed;
            if (wakeup > realTime[1]) {
                TimeUtil::usleep(wakeup - realTime[1]);
                // Note: this isn't *exactly* when we woke up, but making another call to utime()
                //       could result in compounding drift of loop timing
                realTime[1] = wakeup;
            }
        }
        realTime[0] = realTime[1];

        syncAct(dt);
    }

    for (auto& obj : world) { obj->join(); }
}

void Manager::stopFunc()
{
    for (auto& obj : world) { obj->stop(); }
    timers.forceWake();

    pauseCond.notify_all();
}

void Manager::initObs()
{ for (auto& obj : world) obj->managerInitObserve(); }

void Manager::startObjects()
{ for (auto& obj : world) obj->start(); }

f64 Manager::syncObs(f64 dt)
{
    f64 t = timers.get(OBS) + dt;
    if (comms) comms->flushIncoming(t);
    timers.set(OBS, t);

    for (auto& obj : world) {
        auto ret = obj->timers.wait(OBS, t, true, maxWaitBeforeAssertUs);
        if (!ret.flag) {
            printMutualWaitDeadlock(TimersNames[OBS], *obj);
            Assert(ret.flag, "Suspected deadlock due to maxWaitBeforeAssertUs");
        }
    }
    return t;
}

f64 Manager::syncAct(f64 dt)
{
    f64 t = timers.increment(ACT, dt);

    for (auto& obj : world) {
        auto ret = obj->timers.wait(ACT, t, true, maxWaitBeforeAssertUs);
        if (!ret.flag) {
            printMutualWaitDeadlock(TimersNames[ACT], *obj);
            Assert(ret.flag, "Suspected deadlock due to maxWaitBeforeAssertUs");
        }
    }

    return t;
}

void Manager::printMutualWaitDeadlock(const std::string& timerName, const Object& obj)
{
    cerr << "Suspected deadlock from Obj '" << obj.getName() << "' during " << timerName << "\n"
         << "  Manager (dt = " << dt << "):\n";
    for (sz i = 0; i < NUM_TIMERS; ++i) {
        cerr << "    " << TimersNames[i] << ": " << timers.get(i) << "\n";
    }
    cerr << "  Object:\n";
    for (sz i = 0; i < NUM_TIMERS; ++i) {
        cerr << "    " << TimersNames[i] << ": " << obj.timers.get(i) << "\n";
    }
    cerr << endl;
}
