#pragma once

#include <memory>
#include <utility>
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <mutex>
#include <condition_variable>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/ThreadUtil.hpp"

#include "kestrel/sim/SimConsts.hpp"
#include "kestrel/sim/ChronoSync.hpp"
#include "kestrel/sim/Object.hpp"
#include "kestrel/sim/Comms.hpp"

class ManagerTest;
class SimpleObjectTest;

namespace Kestrel {
namespace Sim {

// TODO: change name to Simulator
class Manager : public ThreadUtil::Thread {
  public:
    typedef ThreadUtil::Thread            ParentType;
    typedef std::function<void(Manager*)> PauseCb;

  protected:
    ChronoSync<f64, NUM_TIMERS>          timers;     ///< Simualtor time (OBS, ACT)
    std::vector<std::unique_ptr<Object>> world;      ///< Objects in the simulation
    std::vector<const Object*>           constWorld; ///< Const version of world for external access

    // XXX: until we stop giving dt to objects (see comment in Object.hpp : setManager()),
    //      dt must be treated as constant after construction.
    f64 dt;    ///< timestep of simulation (should be <= update period of objects in the sim)
    f64 speed; ///< factor of real-time to run (0 = as fast as possible)

    std::unique_ptr<Comms> comms; ///< Sim messaging system

    bool                         paused;     ///< Indicates if the simulation is paused
    PauseCb                      pauseCb;    ///< User callback when the simulation is paused
    mutable std::recursive_mutex pauseMutex; ///< Lock on paused state
    std::condition_variable_any  pauseCond;  ///< Notifies threads that paused has changed

    /// Triggers an assert if any object takes more than the given amount of microseconds
    /// to complete it's observations / actions while manager is waiting on them. Ignored if == 0.
    u64 maxWaitBeforeAssertUs;

    // XXX: instead of using an Object to stop the manager, we should build the stop-time
    //      into the manager itself so that you can deterministically stop after an act.

  public:
    /// Ctor with Sim::Comms pointer (user forfeits memory), nullptr to disable Sim::Comms
    Manager(f64 dt, f64 speed, Comms* comms = new Comms(), f64 t0 = 0.0,
            const PauseCb& pauseCb = PauseCb(),
            u64 maxWaitBeforeAssertUs = std::numeric_limits<u64>::max()) :
        timers{t0, t0},
        world(),
        constWorld(),
        dt(dt),
        speed(speed),
        comms(comms),
        paused(false),
        pauseCb(pauseCb),
        maxWaitBeforeAssertUs(maxWaitBeforeAssertUs) {}
    /// Ctor with zcm url to forward to Sim::Comms for external visibility of sim messages
    Manager(f64 dt, f64 speed, const std::string& externalZcmUrl, f64 t0 = 0.0,
            const PauseCb& pauseCb = PauseCb(),
            u64 maxWaitBeforeAssertUs = std::numeric_limits<u64>::max()) :
        Manager(dt, speed, new Comms(externalZcmUrl), t0, pauseCb, maxWaitBeforeAssertUs) {}
    /// Dtor, force Objects to clean up before Manager
    virtual ~Manager() { world.clear(); }

    /// Adds an object to the world. Pass in a new dyn obj, this class will handle memory
    void addObject(Object* obj);

    // TODO: consider a way to remove objects from list

    // TODO: consider functions to get and set the time, would need to ensure that sim is paused
    //       and would need to propagate down to objects

    void pause(bool p)
    {
        std::unique_lock<decltype(pauseMutex)> lk(pauseMutex);
        paused = p;
        pauseCond.notify_all();
    }

    bool isPaused() const
    {
        std::unique_lock<decltype(pauseMutex)> lk(pauseMutex);
        return paused;
    }

    void onPause(const PauseCb& pauseCb = PauseCb())
    {
        Assert(!isRunning(), "cannot change pause callback while running");
        this->pauseCb = pauseCb;
    }

    void setMaxWait(u64 maxWaitBeforeAssertUs)
    {
        Assert(!isRunning(), "cannot change max wait time while running");
        this->maxWaitBeforeAssertUs = maxWaitBeforeAssertUs;
    }

    /// Returns the const vector of objects.
    /// IMPORTANT: Don't use the output of this function while sim is running
    const std::vector<const Object*>& getConstWorld() { return constWorld; }

    /// Returns the performance statistics of each object. Only valid if manager paused or stopped
    std::vector<ObjPerf> getPerf() const;
    /// Resets performance statistics on all objects (or a specified object if name is provided)
    /// Returns if successful (will not reset unless paused or stopped)
    bool resetPerf(const std::string& name = "");

  private:
    // Note: could change these to protected if there is ever a reason to inherit from Manager
    // Note: see kestrel/util/ThreadUtil.hpp, and look at ParentType for api

    /// Have this thread become the manager thread
    void runFunc() override;
    /// Ensure no threads are waiting on internal timers
    void stopFunc() override;

    /// Trigger initial observations on objects
    void initObs();
    /// Start all objects run loops
    void startObjects();
    /// Increment the observation timer and triggers all objects to observe (returns new time)
    f64 syncObs(f64 dt);
    /// Increment the action timer and trigger all objects to act (returns new time)
    f64 syncAct(f64 dt);

    /// Print information about an object that may have caused a deadlock
    void printMutualWaitDeadlock(const std::string& timerName, const Object& obj);

  public:

    // Note: ParentType will prevent copy/swap once object thread is running
    // TODO: maybe paused is ok? Won't work with how Thread works right now

    /// Swap
    inline void swap(Manager& o)
    {
        ParentType::swap(o);
        timers.swap(o.timers);
        world.swap(o.world);
        constWorld.swap(o.constWorld);
        std::swap(dt, o.dt);
        std::swap(speed, o.speed);
        comms->swap(*o.comms); // Using Sim::Comms::swap to take advantage of its protective asserts
        std::swap(paused, o.paused);
        pauseCb.swap(pauseCb);
        std::swap(maxWaitBeforeAssertUs, o.maxWaitBeforeAssertUs);

        // Note: Both sets of world vectors are guaranteed to contain Objects in the
        //       PHASE_IN_MANAGER because we haven't started the Manager thread yet.
        //       Therefore, we can go through and call setManager on them again.
        for (auto& obj : world) {
            obj->setManager(&constWorld, &timers, dt, comms.get());
        }
        for (auto& obj : o.world) {
            obj->setManager(&o.constWorld, &o.timers, o.dt, o.comms.get());
        }
    }
    /// Copy Ctor
    Manager(const Manager& o) : ParentType(o),
                                timers(o.timers),
                                world(),
                                constWorld(),
                                dt(o.dt),
                                speed(o.speed),
                                paused(o.paused),
                                pauseCb(o.pauseCb),
                                maxWaitBeforeAssertUs(o.maxWaitBeforeAssertUs)
    {
        if (o.comms != nullptr) comms.reset(new Comms(*o.comms));

        // need to deep copy the unique pointers
        for (auto& obj : o.world)
            if (!obj->isChild)
                addObject(obj->cloneToNewManager());
    }
    /// Move Ctor
    Manager(Manager&& o) { swap(o); }
    /// Unifying Assignment
    Manager& operator=(Manager o) { swap(o); return *this; }

    // Tests friended only to allow manual step-through of the sections of the runFunc
    friend ManagerTest;
    friend SimpleObjectTest;
};

} /* Sim */
} /* Kestrel */
