#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <deque>

#include "kestrel/sim/Object.hpp"

#include "zcmtypes/simtime_t.hpp"

class SimZcmObjectTest;

namespace Kestrel {
namespace Sim {
namespace Test {

class SimZcmSubObject: public Object
{
  public:
    typedef Object ParentType;
    friend SimZcmObjectTest;

  protected:
    f64 obsPeriod;
    f64 actDelay;
    sz  printLvl;

    // Need to track this so we can make a subId persist into the handler
    struct SubData {
        zcm::Subscription* sub = nullptr;
        SimZcmSubObject*   usr;
        sz                 subId;

        SubData(SimZcmSubObject* usr, sz subId) : usr(usr), subId(subId) {}
    };
    std::vector<std::string> chans;
    std::vector<SubData*>    subs;

    // Sorting of received messages should be by time, subId, then channel
    struct RecvMsg {
        f64         t;
        sz          subId;
        std::string channel;

        // Had to define this to make emplace_back work
        RecvMsg(f64 t, sz subId, const std::string& channel) :
            t(t), subId(subId), channel(channel) {}

        bool operator<(const RecvMsg& o) const
        {
            if (t == o.t) {
                if (subId == o.subId) {
                    return channel < o.channel;
                }
                return subId < o.subId;
            }
            return t < o.t;
        }
    };
    std::deque<RecvMsg> recvMsgs;
    sz lastSz    = 0;
    sz nextSubId = 0;

    sz numObs = 0;
    sz numPrc = 0;
    sz numAct = 0;

    void subAll(std::vector<SubData*>& subVec, const std::vector<std::string>& chanVec)
    {
        for (auto& chan : chanVec) {
            subVec.push_back(new SubData(this, nextSubId++));
            subVec.back()->sub = getZcm()->subscribe(chan, handleMsg, subVec.back());
        }
    }

    void unsubAll(std::vector<SubData*>& subVec)
    {
        for (auto& subData : subVec) {
            getZcm()->unsubscribe(subData->sub);
            delete subData;
        }
        subVec.clear();
    }

  public:
    /// Ctor
    SimZcmSubObject(const std::vector<std::string>& chans = {"SIM_TIME"}, f64 obsPeriod = 0.01,
                    f64 actDelay = 0.005, sz printLvl = 0) :
        obsPeriod(obsPeriod), actDelay(actDelay), printLvl(printLvl), chans(chans)
    { setName("SimZcmSub"); if (printLvl) std::cout << std::endl; }
    /// Dtor
    virtual ~SimZcmSubObject()
    { unsubAll(subs); }

    void printNewRecv(f64 t)
    {
        for (sz i = lastSz; i < recvMsgs.size(); ++i) {
            std::cout << "t = " << t
                      << " received t = " << recvMsgs[i].t
                      << " on channel = " << recvMsgs[i].channel
                      << " from subId = " << recvMsgs[i].subId << std::endl;
        }
    }

    bool recvProperOrder()
    {
        if (recvMsgs.size() < 2) return true;
        for (sz i = 0; i < recvMsgs.size() - 1; ++i) {
            sz j = i + 1;
            if (recvMsgs[j] < recvMsgs[i]) return false;
        }
        return true;
    }

    static void handleMsg(const zcm::ReceiveBuffer* rbuf, const std::string& channel, void* usr)
    {
        SubData* subData = (SubData*) usr;

        simtime_t st {};
        TS_ASSERT(st.decode(rbuf->data, 0, rbuf->data_size) == (int) rbuf->data_size);

        subData->usr->recvMsgs.emplace_back(st.t, subData->subId, channel);
    }

    void initObserve(const std::vector<const Object*>* world) override
    { subAll(subs, chans); }

    NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world) override
    {
        f64 nextObs = tExpect + obsPeriod;
        f64 nextAct;
        if (lastSz != recvMsgs.size()) {
            nextAct = std::max(tExpect + actDelay, t);
            if (nextObs <= nextAct) nextObs = NEXT_OBSERVE;
        } else {
            nextAct = NO_ACTION;
            if (nextObs <= t) nextObs = NEXT_OBSERVE;
        }

        ++numObs;
        if (printLvl > 1) std::cout << "Obs #" << numObs << " at " << t
                                    << " expected at " << tExpect << std::endl;
        if (printLvl > 0) printNewRecv(t);
        lastSz = recvMsgs.size();
        return NextTimeObsAct(nextObs, nextAct);
    }

    bool process(f64 tObs, f64 tObsExpect, f64 tAct) override
    {
        ++numPrc;
        if (printLvl > 2)
            std::cout << "Prc #" << numPrc << " at " << tObs << " for " << tAct << std::endl;
        return true;
    }

    bool act(f64 t, f64 tExpect) override
    {
        ++numAct;
        if (printLvl > 1) std::cout << "Act #" << numAct << " at " << t << std::endl;
        return true;
    }

    Object* clone() const override { return new SimZcmSubObject(*this); }

    /// Swap
    inline void swap(SimZcmSubObject& o)
    {
        ParentType::swap(o);
        std::swap(obsPeriod, o.obsPeriod);
        std::swap(actDelay, o.actDelay);
        std::swap(printLvl, o.printLvl);
        chans.swap(o.chans);
        Assert(subs.empty() && o.subs.empty(), "Cannot swap once subscribed");
        recvMsgs.swap(o.recvMsgs);
        std::swap(lastSz, o.lastSz);
        std::swap(nextSubId, o.nextSubId);
        std::swap(numObs, o.numObs);
        std::swap(numPrc, o.numPrc);
        std::swap(numAct, o.numAct);
    }
    /// Copy Ctor
    SimZcmSubObject(const SimZcmSubObject& o) :
        ParentType(o),
        obsPeriod(o.obsPeriod),
        actDelay(o.actDelay),
        printLvl(o.printLvl),
        chans(o.chans),
        recvMsgs(o.recvMsgs),
        lastSz(o.lastSz),
        nextSubId(o.nextSubId),
        numObs(o.numObs),
        numPrc(o.numPrc),
        numAct(o.numAct)
    {
        Assert(subs.empty() && o.subs.empty(), "Cannot copy once subscribed");
    }
    /// Move Ctor
    SimZcmSubObject(SimZcmSubObject&& o) { swap(o); }
    /// Unifying Assignment
    SimZcmSubObject& operator=(SimZcmSubObject o) { swap(o); return *this; }
};

} /* Test */
} /* Sim */
} /* Kestrel */
