#pragma once

#include <iostream>

#include "kestrel/sim/Object.hpp"
#include "kestrel/sim/Manager.hpp"

namespace Kestrel {
namespace Sim {
namespace Test {

class SimPauserObject: public Object {
  public:
    typedef Object ParentType;

  public:
    f64      pauseTime; ///< will pause sim as soon as t > pauseTime
    Manager* man;       ///< pointer to manager to pause
    bool     print;

  public:
    /// Ctor
    SimPauserObject(f64 pauseTime, Manager* man, bool print = false) :
        pauseTime(pauseTime), man(man), print(print) { setName("SimPauser"); }
    /// Dtor
    virtual ~SimPauserObject() {}

    virtual NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world)
    { return NextTimeObsAct(NO_OBSERVE, pauseTime); }

    virtual bool process(f64 tObs, f64 tObsExpect, f64 tAct) { return true; }

    virtual bool act(f64 t, f64 tExpect)
    {
        if (print) std::cout << "\nPausing: " << t << ", expected: " << tExpect
                             << ", intended: " << pauseTime << std::endl;
        man->pause(true);
        return true;
    }

    virtual Object* clone() const { return new SimPauserObject(*this); }

    /// Swap
    inline void swap(SimPauserObject& o)
    {
        ParentType::swap(o);
        std::swap(pauseTime, o.pauseTime);
        std::swap(man, o.man);
        std::swap(print, o.print);
    }
    /// Copy Ctor
    SimPauserObject(const SimPauserObject& o) : ParentType(o),
                                                  pauseTime(o.pauseTime),
                                                  man(o.man),
                                                  print(o.print) {}
    /// Move Ctor
    SimPauserObject(SimPauserObject&& o) { swap(o); }
    /// Unifying Assignment
    SimPauserObject& operator=(SimPauserObject o) { swap(o); return *this; }
};

} /* Test */
} /* Sim */
} /* Kestrel */
