#pragma once

#include <iostream>
#include <cstdio>
#include <cmath>

#include <cxxtest/TestSuite.h>

#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TimeUtil.hpp"

#include "kestrel/sim/Object.hpp"
#include "kestrel/sim/Manager.hpp"
#include "kestrel/sim/SimStopperObject.hpp"

#include "SimpleObject.hpp"
#include "SimBadStopperObject.hpp"
#include "SimPauserObject.hpp"
#include "DeadlockObject.hpp"
#include "DeadlockHardObject.hpp"

using namespace std;
using namespace Kestrel;
using namespace MathUtil;
using namespace TimeUtil;
using namespace Sim;
using namespace Sim::Test;

class SimpleObjectTest: public CxxTest::TestSuite {
  private:

  public:
    void setUp() override {}
    void tearDown() override {}

    void testManual()
    {
        f64 dt = 0.001;
        Manager man(dt, 0.0, nullptr);

        SimpleObject* o = new SimpleObject();
        man.addObject(o);

        // manually do parts of the manager.runFunc() call from here down:
        man.timers.increment(OBS, -dt);
        man.timers.increment(ACT, -dt);

        man.initObs();
        man.startObjects();

        // Objects should not start until manager does
        usleep(1000);
        TS_ASSERT_EQUALS(o->numObs, 0);

        f64 tAct = 0;
        while (tAct < o->actDelay) { // first action triggers `delay` after initial timestep
            man.syncObs(dt);
            tAct = man.syncAct(dt);
        }

        TS_ASSERT(o->properInitObs);
        TS_ASSERT_EQUALS(o->numObs, 1);
        TS_ASSERT_EQUALS(o->numPrc, 1);
        TS_ASSERT_EQUALS(o->numAct, 1);

        man.stop(); // need to invalidate manager timer

        o->stop();
        o->join();
    }

    void testManualTwo()
    {
        f64 dt = 0.001;
        Manager man(dt, 0.0, nullptr);

        SimpleObject* o1 = new SimpleObject();
        SimpleObject* o2 = new SimpleObject();
        man.addObject(o1);
        man.addObject(o2);

        // manually do parts of the manager.runFunc() call from here down:
        man.timers.increment(OBS, -dt);
        man.timers.increment(ACT, -dt);

        man.initObs();
        man.startObjects();

        // Objects should not start until manager does
        usleep(1000);
        TS_ASSERT_EQUALS(o1->numObs, 0);
        TS_ASSERT_EQUALS(o2->numObs, 0);

        f64 tAct = 0;
        while (tAct < o1->actDelay) { // first action triggers `delay` after initial timestep
            man.syncObs(dt);
            tAct = man.syncAct(dt);
        }

        TS_ASSERT(o1->properInitObs);
        TS_ASSERT(o2->properInitObs);
        TS_ASSERT_EQUALS(o1->numObs, 1);
        TS_ASSERT_EQUALS(o2->numObs, 1);
        TS_ASSERT_EQUALS(o1->numPrc, 1);
        TS_ASSERT_EQUALS(o2->numPrc, 1);
        TS_ASSERT_EQUALS(o1->numAct, 1);
        TS_ASSERT_EQUALS(o2->numAct, 1);

        man.stop(); // need to invalidate manager timer

        o1->stop();
        o2->stop();
        o1->join();
        o2->join();
    }

    void testStopper()
    {
        f64 dt = 0.001;
        Manager man(dt, 0.0, nullptr);
        man.addObject(new SimStopperObject(10, &man, false));

        man.run();

        // Stopper should trigger on the loop immediately after t=10
        TS_ASSERT_DELTA(man.timers.get(OBS), 10+dt, TINY);
        TS_ASSERT_DELTA(man.timers.get(ACT), 10+dt, TINY);
    }

    void testBadStopper()
    {
        f64 dt = 0.001;
        Manager man(dt, 0.0, nullptr);
        man.addObject(new SimBadStopperObject(10, &man));

        man.run();

        // NO assert, the stopper is built to threadlock if the manager is exiting incorrectly
        // however it does not accurately limit the max time the manager will get up to so
        // returning is considered success
    }

    void testDrift()
    {
        f64 dt = 0.01;
        f64 t  = 0;
        for (sz i = 0; i < 1000; ++i) { t += dt; }
        TS_ASSERT_DELTA(t, 10, TINY);
        t = 10 - TINY; // allow stopper to wake up on the right tic

        Manager man(dt, 0.0, nullptr);
        SimpleObject* o = new SimpleObject(dt, 0, false);
        man.addObject(o);
        man.addObject(new SimStopperObject(t, &man));

        man.start();
        man.join();

        // Note: the reason we need to add 0 or 1 here:
        // - sim dt and object dt are ==, so object will trigger every sim loop
        // - t starts at 0 inclusive (add 1)
        // - numerical precision and nondeterministic action order mean that you may get
        //   one fewer iteration than expected. Numerical precision affects the obs and proc,
        //   act order can cause manager to be shut down before final action occurs (if
        //   objects line up with the stop command.
        sz iters = std::round(t / dt) + 1;
        TS_ASSERT(o->properInitObs);
        TS_ASSERT(o->numObs == iters || o->numObs == iters-1);
        TS_ASSERT(o->numPrc == iters || o->numPrc == iters-1);
        TS_ASSERT(o->numAct == iters || o->numAct == iters-1);
    }

    void testMultiObjRun()
    {
        f64 dt  = 0.01;
        f64 odt = 0.1;
        f64 t   = 10 + 2 * dt - TINY; // allow stopper to wake up on the right tic

        Manager man(dt, 0.0, nullptr);
        man.setMaxWait(1000000);

        constexpr sz  n = 5;
        SimpleObject* o[n];
        o[0] = new SimpleObject(odt, 0, false);
        man.addObject(o[0]);
        for (sz i = 1; i < n; ++i) {
            o[i] = new SimpleObject(odt, 0);
            man.addObject(o[i]);
        }
        man.addObject(new SimStopperObject(t, &man, false));

        man.start();
        man.join();

        TS_ASSERT_DELTA(man.timers.get(OBS), t, TINY);
        TS_ASSERT_DELTA(man.timers.get(ACT), t, TINY);

        // Note: have to add 1 here because objects run on t == 0
        sz iters = std::round(t / odt) + 1;
        for (sz i = 0; i < n; ++i) {
            constexpr sz bufsize = 10;
            char buf[bufsize] {0};
            snprintf(buf, bufsize, "i=%zu", i);
            TSM_ASSERT(buf, o[i]->properInitObs);
            TSM_ASSERT_EQUALS(buf, o[i]->numObs, iters);
            TSM_ASSERT_EQUALS(buf, o[i]->numPrc, iters);
            TSM_ASSERT_EQUALS(buf, o[i]->numAct, iters);
        }
    }

    void testObjChildren()
    {
        f64 dt  = 0.01;
        f64 odt = 0.1;
        f64 t   = 10 + 2 * dt - TINY; // allow stopper to wake up on the right tic

        Manager man(dt, 0.0, nullptr);
        man.setMaxWait(1000000);

        constexpr sz  n = 5;
        SimpleObject* o = new SimpleObject(odt, 0, false, n-1);
        man.addObject(o);
        man.addObject(new SimStopperObject(t, &man, false));

        TS_ASSERT_EQUALS(man.world.size(), n+1);

        man.start();
        man.join();

        // Note: see other tests for comments on the +1
        sz iters = std::round(t / odt) + 1;
        for (sz i = 0; i < n; ++i) {
            constexpr sz bufsize = 10;
            char buf[bufsize] {0};
            snprintf(buf, bufsize, "i=%zu", i);
            TSM_ASSERT(buf, ((SimpleObject*)man.world[i].get())->properInitObs);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man.world[i].get())->numObs, iters);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man.world[i].get())->numPrc, iters);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man.world[i].get())->numAct, iters);
        }
    }

    void testRealTime()
    {
        f64 t   = 0.100;
        f64 dt  = 0.01;
        f64 odt = 0.01;

        Manager man(dt, 1.0, nullptr);
        man.setMaxWait(1000000);

        SimpleObject* o = new SimpleObject(odt, 0, false);
        man.addObject(o);
        man.addObject(new SimStopperObject(t, &man, false));

        u64 tStart = utime();
        man.start();
        man.join();
        u64 tStop  = utime();

        cout << endl << "tDiff = " << tStop - tStart << endl;

        TSM_ASSERT_LESS_THAN("Real Time Exec too fast", (u64)(0.95e6*t), tStop - tStart);
        TSM_ASSERT_LESS_THAN("Real Time Exec too slow (may be false alarm if using a sanitizer)",
                             tStop - tStart, (u64)(1.5e6*t));
    }

    void testInstantWakeup()
    {
        f64 dt  = 0.01;
        f64 odt = 0;
        f64 t   = 10 + 2 * dt - TINY; // allow stopper to wake up on the right tic

        Manager man(dt, 0.0, nullptr);
        man.setMaxWait(1000000);

        constexpr sz  n = 5;
        SimpleObject* o[n];
        o[0] = new SimpleObject(odt, 0, false);
        man.addObject(o[0]);
        for (sz i = 1; i < n; ++i) {
            o[i] = new SimpleObject(odt, 0);
            man.addObject(o[i]);
        }
        man.addObject(new SimStopperObject(t, &man, false));

        man.start();
        man.join();

        // Note: see other tests for comments on the +1
        sz iters = std::round(t / dt) + 1;
        for (sz i = 0; i < n; ++i) {
            constexpr sz bufsize = 10;
            char buf[bufsize] {0};
            snprintf(buf, bufsize, "i=%zu", i);
            TSM_ASSERT(buf, o[i]->properInitObs);
            TSM_ASSERT(buf, o[i]->numObs == iters);
            // Note: act order is not deterministic, so we can't guarantee that the objects
            //       were able to act on the last timestep of the sim (ie when SimStopper
            //       stops the Manager).
            TSM_ASSERT(buf, o[i]->numPrc == iters || o[i]->numPrc == iters-1);
            TSM_ASSERT(buf, o[i]->numAct == iters || o[i]->numAct == iters-1);
        }
    }

    void testPause()
    {
        f64 dt     = 0.01;
        f64 odt    = 0;
        f64 tPause = 5 + 2 * dt - TINY;  // allow pauser to wake up on the right tic
        f64 tStop  = 10 + 2 * dt - TINY; // allow stopper to wake up on the right tic

        mutex lock;
        condition_variable cv;
        bool callbackTriggered = false;

        Manager man(dt, 0.0, nullptr, 0.0, [&](Manager* m){
                unique_lock<mutex> lk(lock);
                callbackTriggered = true;

                auto perfStats = m->getPerf();
                for (auto& stat : perfStats) cout << stat << endl;

                cv.notify_all();
            });
        SimpleObject* o = new SimpleObject(odt, 0, false);
        man.addObject(o);
        man.addObject(new SimPauserObject(tPause, &man));
        man.addObject(new SimStopperObject(tStop, &man));

        man.start();

        // Note: see other tests for comments on the +1
        sz iters = std::round(tPause / dt) + 1;
        {
            unique_lock<mutex> lk(lock);
            cv.wait(lk, [&](){ return callbackTriggered; });
        }

        TS_ASSERT(o->properInitObs);
        TS_ASSERT_EQUALS(o->numObs, iters);
        TS_ASSERT_EQUALS(o->numPrc, iters);
        TS_ASSERT_EQUALS(o->numAct, iters);

        TS_ASSERT(callbackTriggered);

        man.pause(false);
        man.join();

        // Note: see other tests for comments on the +1
        iters = std::round(tStop / dt) + 1;
        TS_ASSERT(o->properInitObs);
        TS_ASSERT(o->numObs == iters);
        TS_ASSERT(o->numPrc == iters || o->numPrc == iters-1);
        TS_ASSERT(o->numAct == iters || o->numAct == iters-1);
    }

    void testPauseStop()
    {
        f64 dt     = 0.01;
        f64 odt    = 0;
        f64 tPause = 5 + 2 * dt - TINY;  // allow pauser to wake up on the right tic
        f64 tStop  = 10 + 2 * dt - TINY; // allow stopper to wake up on the right tic

        mutex lock;
        condition_variable cv;
        bool callbackTriggered = false;

        Manager man(dt, 0.0, nullptr);
        man.onPause([&](Manager* m){
                unique_lock<mutex> lk(lock);
                callbackTriggered = true;

                auto perfStats = m->getPerf();
                for (auto& stat : perfStats) cout << stat << endl;

                cv.notify_all();
            });

        SimpleObject* o = new SimpleObject(odt, 0, false);
        man.addObject(o);
        man.addObject(new SimPauserObject(tPause, &man));
        man.addObject(new SimStopperObject(tStop, &man));

        man.start();

        // Note: see other tests for comments on the +1
        sz iters = std::round(tPause / dt) + 1;
        {
            unique_lock<mutex> lk(lock);
            cv.wait(lk, [&](){ return callbackTriggered; });
        }

        TS_ASSERT(o->properInitObs);
        TS_ASSERT_EQUALS(o->numObs, iters);
        TS_ASSERT_EQUALS(o->numPrc, iters);
        TS_ASSERT_EQUALS(o->numAct, iters);

        TS_ASSERT(callbackTriggered);

        man.stop(); // this should not cause a deadlock
        man.join();
    }

    void testManagerSwap()
    {
        f64 dt  = 0.01;
        f64 odt = 0.1;
        f64 t   = 10 + 2 * dt - TINY; // allow stopper to wake up on the right tic

        Manager man1(dt, 0.0);
        Manager man2(dt, 0.0);

        constexpr sz  n = 5;

        SimpleObject* o1[n];
        o1[0] = new SimpleObject(odt, 0, false);
        man1.addObject(o1[0]);
        for (sz i = 1; i < n; ++i) {
            o1[i] = new SimpleObject(odt, 0);
            man1.addObject(o1[i]);
        }

        SimpleObject* o2[n];
        o2[0] = new SimpleObject(odt, 0, false);
        man2.addObject(o2[0]);
        for (sz i = 1; i < n; ++i) {
            o2[i] = new SimpleObject(odt, 0);
            man2.addObject(o2[i]);
        }

        // Note: due to a weird thing in tsan, this test is actually throwing a lock order
        //       error against the copy test if you do man1.swap(man2). I don't see how
        //       this is possible due to them being completely separate tests, but doing
        //       the swap in this order makes the locks be aquired the same way as the
        //       copy constructor, which makes tsan happy.
        man2.swap(man1);

        // Copy / Swap won't work with the stopper objects because they actually
        // need a pointer to the manager itself
        man1.addObject(new SimStopperObject(t, &man1, false));
        man2.addObject(new SimStopperObject(t, &man2, false));

        man1.run();

        TS_ASSERT_DELTA(man1.timers.get(OBS), t, TINY);
        TS_ASSERT_DELTA(man1.timers.get(ACT), t, TINY);
        TS_ASSERT_DELTA(man2.timers.get(OBS), 0, TINY);
        TS_ASSERT_DELTA(man2.timers.get(ACT), 0, TINY);

        // Note: have to add 1 here because objects run on t == 0
        sz iters = std::round(t / odt) + 1;
        for (sz i = 0; i < n; ++i) {
            constexpr sz bufsize = 10;
            char buf[bufsize] {0};
            snprintf(buf, bufsize, "i=%zu", i);
            TSM_ASSERT(buf, o2[i]->properInitObs);
            TSM_ASSERT_EQUALS(buf, o2[i]->numObs, iters);
            TSM_ASSERT_EQUALS(buf, o2[i]->numPrc, iters);
            TSM_ASSERT_EQUALS(buf, o2[i]->numAct, iters);
            TSM_ASSERT(buf, !(o1[i]->properInitObs));
            TSM_ASSERT_EQUALS(buf, o1[i]->numObs, 0);
            TSM_ASSERT_EQUALS(buf, o1[i]->numPrc, 0);
            TSM_ASSERT_EQUALS(buf, o1[i]->numAct, 0);
        }

        man2.run();

        TS_ASSERT_DELTA(man2.timers.get(OBS), t, TINY);
        TS_ASSERT_DELTA(man2.timers.get(ACT), t, TINY);

        // Note: have to add 1 here because objects run on t == 0
        for (sz i = 0; i < n; ++i) {
            constexpr sz bufsize = 10;
            char buf[bufsize] {0};
            snprintf(buf, bufsize, "i=%zu", i);
            TSM_ASSERT(buf, o1[i]->properInitObs);
            TSM_ASSERT_EQUALS(buf, o1[i]->numObs, iters);
            TSM_ASSERT_EQUALS(buf, o1[i]->numPrc, iters);
            TSM_ASSERT_EQUALS(buf, o1[i]->numAct, iters);
        }
    }

    void testManagerCopy()
    {
        f64 dt  = 0.01;
        f64 odt = 0.1;
        f64 t   = 10 + 2 * dt - TINY; // allow stopper to wake up on the right tic

        Manager man1(dt, 0.0);

        constexpr sz  n = 5;
        SimpleObject* o = new SimpleObject(odt, 0, false, n-1);
        man1.addObject(o);

        Manager man2(man1);

        TS_ASSERT_EQUALS(man1.world.size(), n);
        TS_ASSERT_EQUALS(man2.world.size(), n);

        // Copy / Swap won't work with the stopper objects because they actually
        // need a pointer to the manager itself
        man1.addObject(new SimStopperObject(t, &man1, false));
        man2.addObject(new SimStopperObject(t, &man2, false));

        man1.run();

        TS_ASSERT_DELTA(man1.timers.get(OBS), t, TINY);
        TS_ASSERT_DELTA(man1.timers.get(ACT), t, TINY);
        TS_ASSERT_DELTA(man2.timers.get(OBS), 0, TINY);
        TS_ASSERT_DELTA(man2.timers.get(ACT), 0, TINY);

        // Note: see other tests for comments on the +1
        sz iters = std::round(t / odt) + 1;
        for (sz i = 0; i < n; ++i) {
            constexpr sz bufsize = 10;
            char buf[bufsize] {0};
            snprintf(buf, bufsize, "i=%zu", i);
            TSM_ASSERT(buf, ((SimpleObject*)man1.world[i].get())->properInitObs);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man1.world[i].get())->numObs, iters);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man1.world[i].get())->numPrc, iters);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man1.world[i].get())->numAct, iters);
            TSM_ASSERT(buf, (!((SimpleObject*)man2.world[i].get())->properInitObs));
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man2.world[i].get())->numObs, 0);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man2.world[i].get())->numPrc, 0);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man2.world[i].get())->numAct, 0);
        }

        man2.run();

        TS_ASSERT_DELTA(man2.timers.get(OBS), t, TINY);
        TS_ASSERT_DELTA(man2.timers.get(ACT), t, TINY);

        // Note: see other tests for comments on the +1
        for (sz i = 0; i < n; ++i) {
            constexpr sz bufsize = 10;
            char buf[bufsize] {0};
            snprintf(buf, bufsize, "i=%zu", i);
            TSM_ASSERT(buf, ((SimpleObject*)man2.world[i].get())->properInitObs);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man2.world[i].get())->numObs, iters);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man2.world[i].get())->numPrc, iters);
            TSM_ASSERT_EQUALS(buf, ((SimpleObject*)man2.world[i].get())->numAct, iters);
        }
    }

    void testPreventDeadlock()
    {
        f64 dt  = 0.01;
        f64 tLock = 0.5;
        f64 tStop = 1 + 2 * dt - TINY; // allow stopper to wake up on the right tic

        Manager man(dt, 0.0, nullptr, 0.0, nullptr, 1000000);

        man.addObject(new DeadlockObject(dt, tLock));
        man.addObject(new SimStopperObject(tStop, &man, false));

        man.run(); // Should NOT assert due to deadlock detection logic
    }

    // Note: this test is built to assert in Manager because Manager will detect the deadlock
    void testCatchDeadlock()
    {
        return;

        f64 dt  = 0.01;
        f64 tLock = 0.5;
        f64 tStop = 1 + 2 * dt - TINY; // allow stopper to wake up on the right tic

        Manager man(dt, 0.0, nullptr, 0.0, nullptr, 1000000);

        man.addObject(new DeadlockHardObject(tLock));
        man.addObject(new SimStopperObject(tStop, &man, false));

        man.run(); // SHOULD assert due to deadlock detection logic
    }
};
