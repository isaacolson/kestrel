#pragma once

#include <vector>

#include "SimZcmSubObject.hpp"

namespace Kestrel {
namespace Sim {
namespace Test {

class SimZcmInConstructorObject: public SimZcmSubObject
{
  public:
    typedef SimZcmSubObject ParentType;

  private:
    std::vector<bool> unsubs;

  public:
    /// Ctor
    SimZcmInConstructorObject(const std::vector<std::string>& chans = {"SIM_TIME"},
                              const std::vector<       bool>& unsubs = {false},
                              f64 obsPeriod = 0.01, f64 actDelay = 0.005, sz printLvl = 0) :
        SimZcmSubObject(chans, obsPeriod, actDelay, printLvl), unsubs(unsubs)
    {
        setName("SimZcmInConstructor");

        Assert(chans.size() == unsubs.size(), "inputs must be same size");

        subAll(subs, chans);

        decltype(subs) newSubs;
        for (size_t i = 0; i < unsubs.size(); ++i) {
            if (unsubs[i]) {
                getZcm()->unsubscribe(subs[i]->sub);
                delete subs[i];
            } else {
                newSubs.push_back(subs[i]);
            }
        }
        subs = newSubs;
    }
    /// Dtor
    virtual ~SimZcmInConstructorObject() {}

    void initObserve(const std::vector<const Object*>* world) override { }

    Object* clone() const override { return new SimZcmInConstructorObject(*this); }

    /// Swap
    inline void swap(SimZcmInConstructorObject& o)
    {
        ParentType::swap(o);
        std::swap(unsubs, o.unsubs);
    }
    /// Copy Ctor
    SimZcmInConstructorObject(const SimZcmInConstructorObject& o) :
        ParentType(o),
        unsubs(o.unsubs) {}
    /// Move Ctor
    SimZcmInConstructorObject(SimZcmInConstructorObject&& o) { swap(o); }
    /// Unifying Assignment
    SimZcmInConstructorObject& operator=(SimZcmInConstructorObject o) { swap(o); return *this; }
};

} /* Test */
} /* Sim */
} /* Kestrel */
