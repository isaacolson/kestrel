#pragma once

#include "kestrel/sim/Object.hpp"

namespace Kestrel {
namespace Sim {
namespace Test {

class DeadlockObject: public Object {
  public:
    typedef Object ParentType;

  public:
    f64  managerDt;
    f64  deadlockTime;

  public:
    /// Ctor
    DeadlockObject(f64 managerDt, f64 deadlockTime = 1.0) :
        managerDt(managerDt), deadlockTime(deadlockTime)
    { setName("Deadlock"); }
    /// Dtor
    virtual ~DeadlockObject() {}

    NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world) override
    {
        if (t < deadlockTime) {
            return NextTimeObsAct(NEXT_OBSERVE, NO_ACTION);
        } else {
            // Force a deadlock by forcing the observe and action steps into the
            // same timestep with the ACT coming first.
            return NextTimeObsAct(t + managerDt / 2, t + managerDt / 3);
        }
    }

    bool process(f64 tObs, f64 tObsExpect, f64 tAct) override { return true; }

    bool act(f64 t, f64 tExpect) override { return true; }

    Object* clone() const override { return new DeadlockObject(*this); }

  public:
    /// Swap
    inline void swap(DeadlockObject& o)
    {
        ParentType::swap(o);
        std::swap(managerDt, o.managerDt);
        std::swap(deadlockTime, o.deadlockTime);
    }
    /// Copy Ctor
    DeadlockObject(const DeadlockObject& o) : ParentType(o),
                                          managerDt(o.managerDt),
                                          deadlockTime(o.deadlockTime) {}

    /// Move Ctor
    DeadlockObject(DeadlockObject&& o) { swap(o); }
    /// Unifying Assignment
    DeadlockObject& operator=(DeadlockObject o) { swap(o); return *this; }

    friend SimpleObjectTest;
};

} /* Test */
} /* Sim */
} /* Kestrel */
