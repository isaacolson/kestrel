#pragma once

#include <cxxtest/TestSuite.h>

#include <unistd.h>
#include <iostream>
#include <cstdio>
#include <thread>
#include <vector>

#include "kestrel/util/types.hpp"
#include "kestrel/util/TimeUtil.hpp"

#include "kestrel/sim/ChronoSync.hpp"

using namespace std;
using namespace Kestrel;
using namespace TimeUtil;
using namespace Sim;

template <typename Scalar, sz num>
void incrementerFunc(ChronoSync<Scalar, num>* c, sz idxInc, sz idxWait,
                     Scalar incVal, Scalar firstVal, Scalar stopVal, bool excWait)
{
    Scalar nextVal = firstVal;

    while (nextVal < stopVal) {
        c->wait(idxWait, nextVal, excWait);
        if (!c->isValid()) break;

        nextVal = c->increment(idxInc, incVal);
    }
}

template <typename Scalar, sz num>
void counterFunc(ChronoSync<Scalar, num> * c, sz waitIdx, Scalar waitPeriod, sz waitCounts)
{
    sz count = 0;
    Scalar nextVal = waitPeriod;

    while (count < waitCounts) {
        c->wait(waitIdx, nextVal);
        if (!c->isValid()) break;

        nextVal += waitPeriod;
        ++count;
    }
}

template <typename Scalar, sz num>
void waiterIncrFunc(ChronoSync<Scalar, num>* c, sz idxInc, sz idxWait,
                    Scalar incVal, Scalar firstVal, Scalar stopVal, bool excWait,
                    typename ChronoSync<Scalar, num>::Waiter** wExtPtr,
                    mutex* lock, condition_variable* cv)
{
    Scalar currVal = firstVal;

    while (currVal < stopVal) {
        auto w = c->getWaiter(idxWait, stopVal, excWait);

        {
            unique_lock<mutex> lk(*lock);
            *wExtPtr = &w;
            cv->notify_all();
        }

        c->wait(&w);
        if (!c->isValid()) break;

        currVal = c->increment(idxInc, incVal);
    }
}

template <typename Scalar, sz num>
void waiterSwitchFunc(ChronoSync<Scalar, num>* c, sz idxWait, Scalar waitVal, bool excWait,
                      bool* hasTriggered,
                      typename ChronoSync<Scalar, num>::Waiter** wExtPtr,
                      mutex* lock, condition_variable* cv)
{
    auto w = c->getWaiter(idxWait, waitVal, excWait);

    {
        unique_lock<mutex> lk(*lock);
        *wExtPtr = &w;
        cv->notify_all();
    }

    c->wait(&w);
    if (!c->isValid()) return;

    {
        unique_lock<mutex> lk(*lock);
        *hasTriggered = true;
        cv->notify_all();
    }
}

class ChronoSyncTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testApi()
    {
        ChronoSync<u64> cs;
        TS_ASSERT_EQUALS(cs.get(0), 0);
        TS_ASSERT_EQUALS(cs.increment(0, 10), 10);
        cs.increment(0, 10);
        TS_ASSERT_EQUALS(cs.get(0), 20);
        cs.set(0, 30);
        TS_ASSERT_EQUALS(cs.get(0), 30);
        TS_ASSERT_EQUALS(cs.wait(0, 10), 30);
    }

    void testNoDeadlock()
    {
        ChronoSync<u64, 2> cs;

        thread t1(incrementerFunc<u64, 2>, &cs, 0, 1, 10, 1, 100, false);
        thread t2(incrementerFunc<u64, 2>, &cs, 1, 0, 10, 1, 100, false);

        cs.set(0, 1);

        t1.join();
        t2.join();

        TS_ASSERT_EQUALS(cs.get(0), 101);
        TS_ASSERT_EQUALS(cs.get(1), 100);
    }

    void testExclWait()
    {
        ChronoSync<u64, 2> cs;

        thread t1(incrementerFunc<u64, 2>, &cs, 0, 1, 10, 0, 100, true);
        thread t2(incrementerFunc<u64, 2>, &cs, 1, 0, 10, 0, 100, true);

        // threads shouldn't start because of exclusive compare
        usleep(100);
        TS_ASSERT_EQUALS(cs.get(0), 0);
        TS_ASSERT_EQUALS(cs.get(1), 0);

        cs.set(0,1);

        t1.join();
        t2.join();

        TS_ASSERT_EQUALS(cs.get(0), 101);
        TS_ASSERT_EQUALS(cs.get(1), 100);
    }

    void testExclWaitSymmetry()
    {
        //ChronoSync<f64, 2> cs;
        // TODO: need to check that you can't have a < b and b <= a at the same time
    }

    void testCopy()
    {
        ChronoSync<u64, 2> cs1;
        cs1.set(0, 1);
        cs1.set(1, 2);
        ChronoSync<u64, 2> cs2(cs1);
        ChronoSync<u64, 2> cs3; cs3 = cs1;
        TS_ASSERT_EQUALS(cs1.get(0), cs2.get(0));
        TS_ASSERT_EQUALS(cs1.get(1), cs2.get(1));
        TS_ASSERT_EQUALS(cs1.get(0), cs3.get(0));
        TS_ASSERT_EQUALS(cs1.get(1), cs3.get(1));
    }

    void testSwap()
    {
        ChronoSync<u64, 2> cs1;
        cs1.set(0, 1);
        cs1.set(1, 2);

        ChronoSync<u64, 2> cs2;
        cs2.set(0, 3);
        cs2.set(1, 4);

        ChronoSync<u64, 2> cs3(cs1);
        ChronoSync<u64, 2> cs4(cs2);

        cs1.swap(cs2);

        TS_ASSERT_EQUALS(cs1.get(0), cs4.get(0));
        TS_ASSERT_EQUALS(cs1.get(1), cs4.get(1));
        TS_ASSERT_EQUALS(cs2.get(0), cs3.get(0));
        TS_ASSERT_EQUALS(cs2.get(1), cs3.get(1));
    }

    void testInitList()
    {
        ChronoSync<u64, 3> cs{1, 2, 3};
        TS_ASSERT_EQUALS(cs.get(0), 1);
        TS_ASSERT_EQUALS(cs.get(1), 2);
        TS_ASSERT_EQUALS(cs.get(2), 3);
    }

    void testConst()
    {
        ChronoSync<u64, 3> cs{1, 2, 3};
        const ChronoSync<u64, 3>* csptr = &cs;
        TS_ASSERT_EQUALS(csptr->get(0), cs.get(0));
        TS_ASSERT_EQUALS(csptr->get(1), cs.get(1));
        TS_ASSERT_EQUALS(csptr->get(2), cs.get(2));
    }

    void testForceWake()
    {
        ChronoSync<u64, 2> cs;

        thread t1(incrementerFunc<u64, 2>, &cs, 0, 1, 10, 10, 100, false);
        thread t2(incrementerFunc<u64, 2>, &cs, 1, 0, 10, 10, 100, false);

        cs.forceWake();

        t1.join();
        t2.join();

        TS_ASSERT_EQUALS(cs.get(0), 0);
        TS_ASSERT_EQUALS(cs.get(1), 0);
    }

    void testSpuriousSignaling()
    {
        ChronoSync<u64, 1> cs;

        constexpr sz n = 100;
        u64 period = 10000;
        u64 counts = 5;

        thread t[n];
        for (sz i = 0; i < n; ++i) {
            t[i] = thread(counterFunc<u64, 1>, &cs, 0, period, counts);
        }

        u64 start = utime();

        for (u64 i = 0; i < period*counts; ++i) {
            cs.increment(0, 1);
        }

        u64 stop = utime();

        if (stop > start + 1e6) {
            TSM_ASSERT("Spurious Signalling test took too long, suspect problem", false);
        }

        for (sz i = 0; i < n; ++i) {
            t[i].join();
        }
    }

    void testPqSort()
    {
        ChronoSync<u64, 1>::Waiter w[] { {0, 1, false},
                                         {0, 1, true },
                                         {0, 2, false},
                                         {0, 2, true },
                                         {0, 3, false},
                                         {0, 3, true },
                                         {0, 4, false},
                                         {0, 4, true },
                                         {0, 5, false},
                                         {0, 5, true },
                                         {0, 6, false},
                                         {0, 6, true },
                                         {0, 7, false},
                                         {0, 7, true } };

        PriorityQueue<ChronoSync<u64, 1>::Waiter*, ChronoSync<u64, 1>::Waiter::PtrMinHeap> q;

        auto pushIntoQ = [&](){
            // Tried to get a decently random order here
            q.push(&w[12]);
            q.push(&w[ 1]);
            q.push(&w[ 3]);
            q.push(&w[ 7]);
            q.push(&w[ 4]);
            q.push(&w[13]);
            q.push(&w[ 5]);
            q.push(&w[ 8]);
            q.push(&w[ 0]);
            q.push(&w[ 6]);
            q.push(&w[10]);
            q.push(&w[ 2]);
            q.push(&w[ 9]);
            q.push(&w[11]);
        };

        pushIntoQ();
        for (sz i = 0; i < 14; ++i) {
            ChronoSync<u64, 1>::Waiter* tmp = q.top();

            constexpr sz bufsize = 10;
            char buf[bufsize] {};
            snprintf(buf, bufsize, "i=%zu", i);
            TSM_ASSERT_EQUALS(buf, *tmp, w[i]);

            q.pop();
        }
        TS_ASSERT(q.empty());

        pushIntoQ();
        vector<ChronoSync<u64, 1>::Waiter*> v = q.clear();
        TS_ASSERT(q.empty());
        TS_ASSERT_EQUALS(v.size(), 14);
        // Note: The result of q.clear() does NOT produce a sorted vector (for efficiency)

        pushIntoQ();
        // Reset queue after changing order using pointer, first element should migrate to the end
        sz numRotate = 10;
        for (sz i = 0; i < numRotate; ++i) {
            w[i] = ChronoSync<u64, 1>::Waiter(0, w[i].getWakeTime() + w[13].getWakeTime(),
                                              (i%2 == 1));
        }
        q.reheapify();

        for (sz i = numRotate; i < 14 + numRotate; ++i) {
            ChronoSync<u64, 1>::Waiter* tmp = q.top();

            constexpr sz bufsize = 10;
            char buf[bufsize] {};
            snprintf(buf, bufsize, "i=%zu", i);
            TSM_ASSERT_EQUALS(buf, *tmp, w[i%14]);

            q.pop();
        }
        TS_ASSERT(q.empty());
    }

    void testChangeWaitTime()
    {
        ChronoSync<u64, 2> cs;

        decltype(cs)::Waiter* w = nullptr;
        mutex                 lock;
        condition_variable    cv;

        thread t1(incrementerFunc<u64, 2>, &cs, 0, 1, 10, 10, 100, false);
        thread t2( waiterIncrFunc<u64, 2>, &cs, 1, 0, 10, 10, 100, false, &w, &lock, &cv);

        cs.set(0, 1);

        usleep(100);
        TS_ASSERT_EQUALS(cs.get(0), 1);
        TS_ASSERT_EQUALS(cs.get(1), 0);

        for (sz i = 0; i < 10; ++i) {
            unique_lock<mutex> lk(lock);
            cv.wait(lk, [&](){ return w != nullptr; });

            cs.changeWaitTime(w, 10*i);

            w = nullptr;
        }

        t1.join();
        t2.join();

        TS_ASSERT_EQUALS(cs.get(0), 101);
        TS_ASSERT_EQUALS(cs.get(1), 100);
    }

    void testMultipleChangeWaitTime()
    {
        ChronoSync<u64, 1> cs;

        static constexpr sz n = 4;
        sz mixedIdx[] { 2, 1, 0, 3 };

        decltype(cs)::Waiter* w[n];
        bool                  flag[n];
        bool                  corr[n];
        mutex                 lock;
        condition_variable    cv;
        vector<thread>        t;

        constexpr sz bufsize = 10;
        char bufExt[bufsize] {};
        char bufInt[bufsize] {};

        auto checkFlags = [&]() {
            bool ret = true;
            for (sz i = 0; i < n; ++i) {
                snprintf(bufInt, bufsize, "flag i=%zu", i);
                TSM_ASSERT_EQUALS(bufInt, flag[i], corr[i]);
                ret = ret && (corr[i] == flag[i]);
            }
            return ret;
        };

        auto idxToTime = [](sz idx) { return 2 * idx + 2; };

        auto initialize = [&]() {
            cs.set(0, 0);
            t.clear();
            t.reserve(n);
            for (sz i = 0; i < n; ++i) {
                w[i] = nullptr;
                flag[i] = false;
                corr[i] = false;
                t.emplace_back(waiterSwitchFunc<u64, 1>, &cs, 0, idxToTime(i), false,
                               &flag[i], &w[i], &lock, &cv);
            }

            for (sz i = 0; i < n; ++i) {
                unique_lock<mutex> lk(lock);
                cv.wait(lk, [&](){ return w[i] != nullptr; });
            }

            return checkFlags();
        };

        // Check waking them up in order via timer sets
        TS_ASSERT(initialize());
        for (sz i = 0; i < n; ++i) {
            unique_lock<mutex> lk(lock);

            cs.set(0, idxToTime(i));

            corr[i] = true;
            cv.wait(lk, [&](){ return flag[i]; });

            snprintf(bufExt, bufsize, "woke i=%zu", i);
            TSM_ASSERT(bufExt, checkFlags());
        }
        for (sz i = 0; i < n; ++i) { t[i].join(); }

        // Check waking them up in order via waiter resets
        TS_ASSERT(initialize());
        for (sz i = 0; i < n; ++i) {
            unique_lock<mutex> lk(lock);

            cs.changeWaitTime(w[i], 0);

            corr[i] = true;
            cv.wait(lk, [&](){ return flag[i]; });

            snprintf(bufExt, bufsize, "woke i=%zu", i);
            TSM_ASSERT(bufExt, checkFlags());
        }
        for (sz i = 0; i < n; ++i) { t[i].join(); }

        // Check waking them up out of order via waiter resets
        TS_ASSERT(initialize());
        for (sz i = 0; i < n; ++i) {
            unique_lock<mutex> lk(lock);

            cs.changeWaitTime(w[mixedIdx[i]], 0);

            corr[mixedIdx[i]] = true;
            cv.wait(lk, [&](){ return flag[mixedIdx[i]]; });

            snprintf(bufExt, bufsize, "woke i=%zu", i);
            TSM_ASSERT(bufExt, checkFlags());
        }
        for (sz i = 0; i < n; ++i) { t[i].join(); }

        // Check waking them out of order using both waiter resets and timer sets
        TS_ASSERT(initialize());
        {
            unique_lock<mutex> lk(lock);
            sz i;

            i = 2;
            cs.changeWaitTime(w[i], 0);
            corr[i] = true;
            cv.wait(lk, [&](){ return flag[i]; });

            snprintf(bufExt, bufsize, "woke i=%zu", i);
            TSM_ASSERT(bufExt, checkFlags());

            i = 0;
            cs.set(0, idxToTime(i));
            corr[i] = true;
            cv.wait(lk, [&](){ return flag[i]; });

            snprintf(bufExt, bufsize, "woke i=%zu", i);
            TSM_ASSERT(bufExt, checkFlags());

            i = 1;
            cs.changeWaitTime(w[i], 0);
            corr[i] = true;
            cv.wait(lk, [&](){ return flag[i]; });

            snprintf(bufExt, bufsize, "woke i=%zu", i);
            TSM_ASSERT(bufExt, checkFlags());

            i = 3;
            cs.set(0, idxToTime(i));
            corr[i] = true;
            cv.wait(lk, [&](){ return flag[i]; });

            snprintf(bufExt, bufsize, "woke i=%zu", i);
            TSM_ASSERT(bufExt, checkFlags());
        }
        for (sz i = 0; i < n; ++i) { t[i].join(); }

        // Check waking up at a different time than originally planned due to timer set
        TS_ASSERT(initialize());
        for (sz i = 0; i < n; ++i) {
            unique_lock<mutex> lk(lock);

            cs.changeWaitTime(w[i], idxToTime(i) - 1);

            lk.unlock();
            usleep(100);
            lk.lock();

            snprintf(bufExt, bufsize, "woke i=%zu", i);
            TSM_ASSERT(bufExt, checkFlags());

            cs.set(0, idxToTime(i) - 1);
            corr[i] = true;
            cv.wait(lk, [&](){ return flag[i]; });

            snprintf(bufExt, bufsize, "woke i=%zu", i);
            TSM_ASSERT(bufExt, checkFlags());
        }
        for (sz i = 0; i < n; ++i) { t[i].join(); }
    }
};
