#pragma once

#include <iostream>

#include <cxxtest/TestSuite.h>

#include "kestrel/sim/Manager.hpp"

using namespace std;
using namespace Kestrel;
using namespace Sim;

class ManagerTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testBasic()
    {
        Manager man(0.1, 0.0);

        // These should not block as there are no objects in man
        man.syncObs(0.1);
        man.syncAct(0.1);

        // These should not leak memory (because it's managed by man)
        for (int i = 0; i < 10; ++i) { man.addObject(new Object()); }
    }
};
