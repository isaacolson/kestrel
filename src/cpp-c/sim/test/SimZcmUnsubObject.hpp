#pragma once

#include "SimZcmSubObject.hpp"

namespace Kestrel {
namespace Sim {
namespace Test {

class SimZcmUnsubObject: public SimZcmSubObject
{
  public:
    typedef SimZcmSubObject ParentType;

  private:
    f64 subAt;
    sz  unsubAfter;

  public:
    /// Ctor
    SimZcmUnsubObject(f64 subAt = 1.0, sz unsubAfter = 5,
                      const std::vector<std::string>& chans = {"SIM_TIME"},
                      f64 obsPeriod = 0.01, f64 actDelay = 0.005, sz printLvl = 0) :
        SimZcmSubObject(chans, obsPeriod, actDelay, printLvl),
        subAt(subAt), unsubAfter(unsubAfter) { setName("SimZcmUnsub"); }
    /// Dtor
    virtual ~SimZcmUnsubObject() {}

    void initObserve(const std::vector<const Object*>* world) override { }

    NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world) override
    {
        auto ret = ParentType::observe(t, tExpect, world);
        if (t < subAt) {
            return NextTimeObsAct(subAt + obsPeriod, subAt);
        } else {
            return ret;
        }
    }

    bool act(f64 t, f64 tExpect) override
    {
        if (numAct == 0) {
            subAll(subs, chans);
        } else if (recvMsgs.size() >= unsubAfter && !subs.empty()) {
            unsubAll(subs);
        }
        return ParentType::act(t, tExpect);
    }

    Object* clone() const override { return new SimZcmUnsubObject(*this); }

    /// Swap
    inline void swap(SimZcmUnsubObject& o)
    {
        ParentType::swap(o);
        std::swap(subAt, o.subAt);
        std::swap(unsubAfter, o.unsubAfter);
    }
    /// Copy Ctor
    SimZcmUnsubObject(const SimZcmUnsubObject& o) :
        ParentType(o),
        subAt(o.subAt),
        unsubAfter(o.unsubAfter) {}
    /// Move Ctor
    SimZcmUnsubObject(SimZcmUnsubObject&& o) { swap(o); }
    /// Unifying Assignment
    SimZcmUnsubObject& operator=(SimZcmUnsubObject o) { swap(o); return *this; }
};

} /* Test */
} /* Sim */
} /* Kestrel */
