#pragma once

#include "kestrel/util/MathUtil.hpp"

#include "kestrel/sim/Object.hpp"
#include "kestrel/sim/Manager.hpp"

namespace Kestrel {
namespace Sim {
namespace Test {

class SimBadStopperObject: public Object {
  public:
    typedef Object ParentType;

  public:
    f64      stopTime;
    Manager* man;

  public:
    /// Ctor
    SimBadStopperObject(f64 stopTime, Manager* man) :
        stopTime(stopTime), man(man) { setName("SimBadStopper"); }
    /// Dtor
    virtual ~SimBadStopperObject() {}

    virtual NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world)
    {
        if (t < stopTime) {
            return NextTimeObsAct(stopTime, NO_ACTION);
        } else {
            return NextTimeObsAct(NO_OBSERVE, stopTime+1);
        }
    }

    virtual bool process(f64 tObs, f64 tObsExpect, f64 tAct)
    {
        if (tObsExpect >= stopTime) {
            man->stop();
        }
        return true;
    }

    virtual bool act(f64 t, f64 tExpect) { return true; }

    virtual Object* clone() const { return new SimBadStopperObject(*this); }

    /// Swap
    inline void swap(SimBadStopperObject& o)
    {
        ParentType::swap(o);
        std::swap(stopTime, o.stopTime);
        std::swap(man, o.man);
    }
    /// Copy Ctor
    SimBadStopperObject(const SimBadStopperObject& o) : ParentType(o),
                                                        stopTime(o.stopTime),
                                                        man(o.man) {}
    /// Move Ctor
    SimBadStopperObject(SimBadStopperObject&& o) { swap(o); }
    /// Unifying Assignment
    SimBadStopperObject& operator=(SimBadStopperObject o) { swap(o); return *this; }
};

} /* Test */
} /* Sim */
} /* Kestrel */
