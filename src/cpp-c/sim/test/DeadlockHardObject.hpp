#pragma once

#include <mutex>
#include <condition_variable>

#include "kestrel/sim/Object.hpp"

namespace Kestrel {
namespace Sim {
namespace Test {

class DeadlockHardObject: public Object {
  public:
    typedef Object ParentType;

  public:
    f64 deadlockTime;
    mutable std::mutex lock;
    mutable std::condition_variable cv;

  public:
    /// Ctor
    DeadlockHardObject(f64 deadlockTime = 1.0) :
        deadlockTime(deadlockTime)
    { setName("DeadlockHard"); }
    /// Dtor
    virtual ~DeadlockHardObject() {}

    NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world) override
    {
        if (t < deadlockTime) {
            return NextTimeObsAct(NEXT_OBSERVE, NO_ACTION);
        } else {
            // Force a deadlock by going to sleep forever
            std::unique_lock<std::mutex> lk(lock);
            cv.wait(lk, [](){ return false; });
            return NextTimeObsAct(NO_OBSERVE, NO_ACTION);
        }
    }

    bool process(f64 tObs, f64 tObsExpect, f64 tAct) override { return true; }

    bool act(f64 t, f64 tExpect) override { return true; }

    Object* clone() const override { return new DeadlockHardObject(*this); }

  public:
    /// Swap
    inline void swap(DeadlockHardObject& o)
    {
        ParentType::swap(o);
        std::swap(deadlockTime, o.deadlockTime);
    }
    /// Copy Ctor
    DeadlockHardObject(const DeadlockHardObject& o) : ParentType(o),
                                          deadlockTime(o.deadlockTime) {}

    /// Move Ctor
    DeadlockHardObject(DeadlockHardObject&& o) { swap(o); }
    /// Unifying Assignment
    DeadlockHardObject& operator=(DeadlockHardObject o) { swap(o); return *this; }

    friend SimpleObjectTest;
};

} /* Test */
} /* Sim */
} /* Kestrel */
