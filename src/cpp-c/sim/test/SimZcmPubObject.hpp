#pragma once

#include <iostream>
#include <string>
#include <vector>

#include "kestrel/sim/Object.hpp"

#include "zcmtypes/simtime_t.hpp"

class SimZcmObjectTest;

namespace Kestrel {
namespace Sim {
namespace Test {

class SimZcmPubObject: public Object
{
  public:
    typedef Object ParentType;
    friend SimZcmObjectTest;

  protected:
    std::vector<std::string> chans;
    f64 obsPeriod;
    f64 actDelay;
    sz  printLvl;

  public:
    /// Ctor
    SimZcmPubObject(const std::vector<std::string>& chans = {"SIM_TIME"}, f64 obsPeriod = 0.01,
                    f64 actDelay = 0.005, sz printLvl = 0) :
        chans(chans), obsPeriod(obsPeriod), actDelay(actDelay), printLvl(printLvl)
    { setName("SimZcmPub"); if (printLvl) std::cout << std::endl; }
    /// Dtor
    virtual ~SimZcmPubObject() {}

    void initObserve(const std::vector<const Object*>* world) override { }

    NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world) override
    {
        return NextTimeObsAct((tExpect + obsPeriod > t) ? tExpect + obsPeriod
                                                        : NEXT_OBSERVE,
                              std::max(tExpect + actDelay, t));
    }

    bool process(f64 tObs, f64 tObsExpect, f64 tAct) override
    {
        return true;
    }

    bool act(f64 t, f64 tExpect) override
    {
        simtime_t st;
        st.t = t;

        for (auto& chan : chans) {
            if (printLvl > 0) std::cout << chan << " pub t = " << st.t << std::endl;
            getZcm()->publish(chan, &st);
        }

        return true;
    }

    Object* clone() const override { return new SimZcmPubObject(*this); }

    /// Swap
    inline void swap(SimZcmPubObject& o)
    {
        ParentType::swap(o);
        chans.swap(o.chans);
        std::swap(obsPeriod, o.obsPeriod);
        std::swap(actDelay, o.actDelay);
        std::swap(printLvl, o.printLvl);
    }
    /// Copy Ctor
    SimZcmPubObject(const SimZcmPubObject& o) :
        ParentType(o),
        chans(o.chans),
        obsPeriod(o.obsPeriod),
        actDelay(o.actDelay),
        printLvl(o.printLvl)
    {}
    /// Move Ctor
    SimZcmPubObject(SimZcmPubObject&& o) { swap(o); }
    /// Unifying Assignment
    SimZcmPubObject& operator=(SimZcmPubObject o) { swap(o); return *this; }
};

} /* Test */
} /* Sim */
} /* Kestrel */
