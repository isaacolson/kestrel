#pragma once

#include <cxxtest/TestSuite.h>

#include <iostream>

#include "kestrel/sim/Manager.hpp"
#include "kestrel/sim/SimStopperObject.hpp"

#include "SimZcmPubObject.hpp"
#include "SimZcmSubObject.hpp"
#include "SimZcmSubTgrObject.hpp"
#include "SimZcmUnsubObject.hpp"
#include "SimZcmInConstructorObject.hpp"

using namespace std;
using namespace Kestrel;
using namespace Sim;
using namespace Sim::Test;

class SimZcmObjectTest: public CxxTest::TestSuite
{
  public:
    void setUp() override { }
    void tearDown() override { }

    void testPubSub()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pubObj = new SimZcmPubObject({"SIM_TIME"}, 1, 0.000, 0);
        SimZcmSubObject* subObj = new SimZcmSubObject({"SIM_TIME"}, 2, 0.000, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pubObj);
        man.addObject(subObj);

        man.run();

        TS_ASSERT(subObj->recvProperOrder());
        TS_ASSERT_EQUALS(subObj->recvMsgs.size(), 10);
        TS_ASSERT_EQUALS(subObj->numObs, 6); // one extra for obs at time 0
        TS_ASSERT_EQUALS(subObj->numPrc, 5);
        TS_ASSERT_EQUALS(subObj->numAct, 5);
    }

    void testSubTrigger()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pubObj = new SimZcmPubObject({"SIM_TIME"}, 1, 0.000, 0);
        SimZcmSubObject* subObj = new SimZcmSubTgrObject({"SIM_TIME"}, {}, -1, 0.000, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pubObj);
        man.addObject(subObj);

        man.run();

        TS_ASSERT(subObj->recvProperOrder());
        TS_ASSERT_EQUALS(subObj->recvMsgs.size(), 11);
        // One observe per message + one observe at t == 0
        TS_ASSERT_EQUALS(subObj->numObs, 12);
        TS_ASSERT_EQUALS(subObj->numPrc, 11);
        TS_ASSERT_EQUALS(subObj->numAct, 11);
    }

    void testMultiSub()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pubObj  = new SimZcmPubObject({"SIM_TIME"}, 1, 0.000, 0);
        SimZcmSubObject* sub1Obj = new SimZcmSubObject({"SIM_TIME"}, 2, 0.000, 0);
        SimZcmSubObject* sub2Obj = new SimZcmSubObject({"SIM_TIME"}, 3, 0.000, 0);
        SimZcmSubObject* sub3Obj = new SimZcmSubTgrObject({"SIM_TIME"}, {}, -1, 0.000, 0);
        SimZcmSubObject* sub4Obj = new SimZcmSubTgrObject({"SIM_TIME"}, {}, -1, 2.100, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pubObj);
        man.addObject(sub1Obj);
        man.addObject(sub2Obj);
        man.addObject(sub3Obj);
        man.addObject(sub4Obj);

        man.run();

        // First Sub will receive all but 1 message over 5 periods
        TS_ASSERT(sub1Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub1Obj->recvMsgs.size(), 10);
        TS_ASSERT_EQUALS(sub1Obj->numObs, 6); // one extra for obs at time 0
        TS_ASSERT_EQUALS(sub1Obj->numPrc, 5);
        TS_ASSERT_EQUALS(sub1Obj->numAct, 5);

        // Second Sub will receive all but 2 message over 3 periods
        TS_ASSERT(sub2Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub2Obj->recvMsgs.size(), 9);
        TS_ASSERT_EQUALS(sub2Obj->numObs, 4); // one extra for obs at time 0
        TS_ASSERT_EQUALS(sub2Obj->numPrc, 3);
        TS_ASSERT_EQUALS(sub2Obj->numAct, 3);

        // Third Sub will receive all messages (triggered, so 1 per period)
        TS_ASSERT(sub3Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub3Obj->recvMsgs.size(), 11);
        TS_ASSERT_EQUALS(sub3Obj->numObs, 12); // one extra for obs at time 0
        TS_ASSERT_EQUALS(sub3Obj->numPrc, 11);
        TS_ASSERT_EQUALS(sub3Obj->numAct, 11);

        // Fourth Sub will receive all but 2 message (triggered, but delay causes enough
        // rollover that you are getting 2 msgs per obs, miss the last 2 msgs, and don't even
        // finish the act of the last obs that it performs
        TS_ASSERT(sub4Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub4Obj->recvMsgs.size(), 9);
        TS_ASSERT_EQUALS(sub4Obj->numObs, 6); // one extra for obs at time 0
        // Not checking numPrc here because I think it is a race condition because we for sure
        // won't hit the final act (so you can't be sure process completed)
        TS_ASSERT_EQUALS(sub4Obj->numAct, 4);
    }

    void testMultiChannel()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pub1Obj = new SimZcmPubObject({"ST_1", "ST_2"}, 1.0, 0.000, 0);
        SimZcmPubObject* pub2Obj = new SimZcmPubObject({"ST_3"}, 0.5, 0.000, 0);
        SimZcmSubObject* sub1Obj = new SimZcmSubObject({"ST_1", "ST_2", "ST_3"}, 5, 0.000, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pub1Obj);
        man.addObject(pub2Obj);
        man.addObject(sub1Obj);

        man.run();

        TS_ASSERT(sub1Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub1Obj->recvMsgs.size(), 40); // misses messages published at t == 10
        TS_ASSERT_EQUALS(sub1Obj->numObs, 3); // one extra for obs at t == 0
        TS_ASSERT_EQUALS(sub1Obj->numPrc, 2);
        TS_ASSERT_EQUALS(sub1Obj->numAct, 2);
    }

    void testMultiChannelSingleTrigger()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pub1Obj = new SimZcmPubObject({"ST_1", "ST_2"}, 1.0, 0.000, 0);
        SimZcmPubObject* pub2Obj = new SimZcmPubObject({"ST_3"}, 0.5, 0.000, 0);
        SimZcmSubObject* sub1Obj = new SimZcmSubTgrObject({"ST_1"}, {"ST_2", "ST_3"}, -1, 0.000, 0);
        SimZcmSubObject* sub2Obj = new SimZcmSubTgrObject({"ST_3"}, {"ST_1", "ST_2"}, -1, 0.000, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pub1Obj);
        man.addObject(pub2Obj);
        man.addObject(sub1Obj);
        man.addObject(sub2Obj);

        man.run();

        TS_ASSERT(sub1Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub1Obj->recvMsgs.size(), 43);
        TS_ASSERT_EQUALS(sub1Obj->numObs, 12); // One extra for obs at t == 0
        TS_ASSERT_EQUALS(sub1Obj->numPrc, 11);
        TS_ASSERT_EQUALS(sub1Obj->numAct, 11);

        TS_ASSERT(sub2Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub2Obj->recvMsgs.size(), 43);
        TS_ASSERT_EQUALS(sub2Obj->numObs, 22); // One extra for obs at t == 0
        TS_ASSERT_EQUALS(sub2Obj->numPrc, 21);
        TS_ASSERT_EQUALS(sub2Obj->numAct, 21);
    }

    void testMultiTrigger()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pub1Obj = new SimZcmPubObject({"ST_1"}, 1.00, 0.000, 0);
        SimZcmPubObject* pub2Obj = new SimZcmPubObject({"ST_2"}, 0.50, 0.000, 0);
        SimZcmPubObject* pub3Obj = new SimZcmPubObject({"ST_3"}, 0.98, 0.000, 0);
        SimZcmSubObject* sub1Obj = new SimZcmSubTgrObject({"ST_1", "ST_2"}, {}, -1, 0.000, 0);
        SimZcmSubObject* sub2Obj = new SimZcmSubTgrObject({"ST_1", "ST_3"}, {}, -1, 0.000, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pub1Obj);
        man.addObject(pub2Obj);
        man.addObject(pub3Obj);
        man.addObject(sub1Obj);
        man.addObject(sub2Obj);

        man.run();

        // This object will have some timesteps where it receives both ST_1 and ST_2 at once
        TS_ASSERT(sub1Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub1Obj->recvMsgs.size(), 32);
        TS_ASSERT_EQUALS(sub1Obj->numObs, 22); // One extra for obs at t == 0
        TS_ASSERT_EQUALS(sub1Obj->numPrc, 21);
        TS_ASSERT_EQUALS(sub1Obj->numAct, 21);

        // This object should only receive 1 message at a time
        TS_ASSERT(sub2Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub2Obj->recvMsgs.size(), 22);
        TS_ASSERT_EQUALS(sub2Obj->numObs, 22); // One extra for obs at t == 0
        TS_ASSERT_EQUALS(sub2Obj->numPrc, 21);
        TS_ASSERT_EQUALS(sub2Obj->numAct, 21);
    }

    void testTgrFreqWakeup()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pub1Obj = new SimZcmPubObject({"ST_1"}, 1.00, 0.000, 0);
        SimZcmSubObject* sub1Obj = new SimZcmSubTgrObject({"ST_1"}, {}, 0.6, 0.000, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pub1Obj);
        man.addObject(sub1Obj);

        man.run();

        // This object will have some timesteps where it receives both ST_1 and ST_2 at once
        TS_ASSERT(sub1Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub1Obj->recvMsgs.size(), 11);
        TS_ASSERT_EQUALS(sub1Obj->numObs, 22); // extra observes based on freq wakeup
        TS_ASSERT_EQUALS(sub1Obj->numPrc, 11); // no proc / act when we don't get a message
        TS_ASSERT_EQUALS(sub1Obj->numAct, 11);
    }

    void testSubUnsubDuringAct()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pub1Obj = new SimZcmPubObject({"ST_1"}, 1.00, 0.000, 0);
        SimZcmSubObject* sub1Obj = new SimZcmUnsubObject(2.00, 5, {"ST_1"}, 1.0, 0.000, 0);
        SimZcmSubObject* sub2Obj = new SimZcmUnsubObject(2.00, 5, {"ST_1"}, 1.0, 0.000, 0);
        SimZcmSubObject* sub3Obj = new SimZcmUnsubObject(2.01, 5, {"ST_1"}, 1.0, 0.000, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pub1Obj);
        man.addObject(sub1Obj);
        man.addObject(sub2Obj);
        man.addObject(sub3Obj);

        man.run();

        TS_ASSERT(sub1Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub1Obj->recvMsgs.size(), 5);
        // We should receive the message published at the same act step as our subscribe because
        // messages are flushed durinbg observe
        TS_ASSERT_LESS_THAN(sub1Obj->recvMsgs[0].t, 3);
        TS_ASSERT_EQUALS(sub1Obj->numObs, 9); // One at 0, then 1 per second after 2 seconds
        TS_ASSERT_EQUALS(sub1Obj->numPrc, 6); // One to set up subs, then 5 messages
        TS_ASSERT_EQUALS(sub1Obj->numAct, 6);

        TS_ASSERT(sub2Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub2Obj->recvMsgs.size(), 5);
        // We should receive the message published at the same act step as our subscribe because
        // messages are flushed durinbg observe
        TS_ASSERT_LESS_THAN(sub2Obj->recvMsgs[0].t, 3);
        TS_ASSERT_EQUALS(sub2Obj->numObs, 9); // One at 0, then 1 per second after 2 seconds
        TS_ASSERT_EQUALS(sub2Obj->numPrc, 6); // One to set up subs, then 5 messages
        TS_ASSERT_EQUALS(sub2Obj->numAct, 6);

        TS_ASSERT(sub3Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub3Obj->recvMsgs.size(), 5);
        TS_ASSERT_LESS_THAN_EQUALS(3, sub3Obj->recvMsgs[0].t);
        TS_ASSERT_EQUALS(sub3Obj->numObs, 9); // One at 0, then 1 per second after 2.01 seconds
        TS_ASSERT_EQUALS(sub3Obj->numPrc, 6); // One to set up subs, then 5 messages
        TS_ASSERT_EQUALS(sub3Obj->numAct, 6);
    }

    void testRegexSub()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pub1Obj = new SimZcmPubObject({"ST_1"}, 1.00, 0.000, 0);
        SimZcmPubObject* pub2Obj = new SimZcmPubObject({"ST_2"}, 0.98, 0.000, 0);
        SimZcmPubObject* pub3Obj = new SimZcmPubObject({"TS_3"}, 0.50, 0.000, 0);
        SimZcmSubObject* sub1Obj = new SimZcmSubTgrObject({"ST_.*"}, {}, -1.0, 0.000, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pub1Obj);
        man.addObject(pub2Obj);
        man.addObject(pub3Obj);
        man.addObject(sub1Obj);

        man.run();

        // This object should only receive 1 message at a time
        TS_ASSERT(sub1Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub1Obj->recvMsgs.size(), 22);
        TS_ASSERT_EQUALS(sub1Obj->numObs, 22); // One extra for obs at t == 0
        TS_ASSERT_EQUALS(sub1Obj->numPrc, 21);
        TS_ASSERT_EQUALS(sub1Obj->numAct, 21);
    }

    void testHardMsgSorting()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pub1Obj = new SimZcmPubObject({"ST_1"}, 0.50, 0.000, 0);
        SimZcmPubObject* pub2Obj = new SimZcmPubObject({"ST_2"}, 0.50, 0.000, 0);
        SimZcmPubObject* pub3Obj = new SimZcmPubObject({"ST_3"}, 0.50, 0.000, 0);
        SimZcmPubObject* pub4Obj = new SimZcmPubObject({"ST_4"}, 0.50, 0.000, 0);
        SimZcmSubObject* sub1Obj = new SimZcmSubTgrObject({"ST_.*", ".*"}, {}, -1.0, 0.000, 0);
        SimZcmSubObject* sub2Obj = new SimZcmSubTgrObject({".*", "ST_.*"}, {}, -1.0, 0.000, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pub1Obj);
        man.addObject(pub2Obj);
        man.addObject(pub3Obj);
        man.addObject(pub4Obj);
        man.addObject(sub1Obj);
        man.addObject(sub2Obj);

        man.run();

        TS_ASSERT(sub1Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub1Obj->recvMsgs.size(), 168); // 21 msgs on each of 2 subs per 4 chans
        TS_ASSERT_EQUALS(sub1Obj->numObs, 22); // One extra for obs at t == 0
        TS_ASSERT_EQUALS(sub1Obj->numPrc, 21);
        TS_ASSERT_EQUALS(sub1Obj->numAct, 21);

        // Note that the ordering of the messages is actually different for each object
        TS_ASSERT(sub2Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub2Obj->recvMsgs.size(), 168); // 21 msgs on each of 2 subs per 4 chans
        TS_ASSERT_EQUALS(sub2Obj->numObs, 22); // One extra for obs at t == 0
        TS_ASSERT_EQUALS(sub2Obj->numPrc, 21);
        TS_ASSERT_EQUALS(sub2Obj->numAct, 21);
    }

    void testWakeSameTimeAsMessageCheck()
    {
        f64 dt = 1;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pub1Obj = new SimZcmPubObject({"ST_1"}, 10.00, 0.000, 0);
        SimZcmSubObject* sub1Obj = new SimZcmSubTgrObject({"ST_1"}, {}, 0.0, 2.9, 0);

        man.addObject(new SimStopperObject(3.9, &man));
        man.addObject(pub1Obj);
        man.addObject(sub1Obj);

        man.run();

        // what happens in this sim:
        //  t  |  event
        // -------------------------
        //  0  |  sub1Obj observes, doesn't have a msg, and doesn't trigger an act
        //  0  |  pub1Obj publishes message
        //  1  |  sub1Obj receives message and observes
        //  3  |  sub1Obj acts. Note that it's at 3 instead of 4 because the actDelay
        //        for these particular objects is added to the tExpect, which will be
        //        close to 0 due to the object asking for "NEXT_OBSERVE" after 0.
        //  4  |  sub1Obj wakes up to check for triggered messages but doesn't find any
        //  4  |  sub1Obj wakes up because that's when the user asked it to wake up
        //  4  |  sub1Obj performs an observe but doesn't ask for an act b/c no msg for it
        //  4  |  SimStopper stops sim

        // This object should only receive 1 message at a time
        TS_ASSERT(sub1Obj->recvProperOrder());
        TS_ASSERT_EQUALS(sub1Obj->recvMsgs.size(), 1);
        TS_ASSERT_EQUALS(sub1Obj->numObs, 3);
        TS_ASSERT_EQUALS(sub1Obj->numPrc, 1);
        TS_ASSERT_EQUALS(sub1Obj->numAct, 1);
    }

    void testZcmInConstructor()
    {
        f64 dt = 0.001;

        Manager man(dt, 0.0);
        man.setMaxWait(1000000);

        SimZcmPubObject* pubObj  = new SimZcmPubObject({"SIM_TIME"}, 1, 0.000, 0);
        SimZcmSubObject* subObj1 = new SimZcmInConstructorObject({"SIM_TIME"}, {false}, 2, 0.000, 0);
        SimZcmSubObject* subObj2 = new SimZcmInConstructorObject({"SIM_TIME"}, {true}, 2, 0.000, 0);

        man.addObject(new SimStopperObject(10.1, &man));
        man.addObject(pubObj);
        man.addObject(subObj1);
        man.addObject(subObj2);

        man.run();

        TS_ASSERT(subObj1->recvProperOrder());
        TS_ASSERT_EQUALS(subObj1->recvMsgs.size(), 10);
        TS_ASSERT_EQUALS(subObj1->numObs, 6); // one extra for obs at time 0
        TS_ASSERT_EQUALS(subObj1->numPrc, 5);
        TS_ASSERT_EQUALS(subObj1->numAct, 5);

        TS_ASSERT(subObj2->recvProperOrder());
        TS_ASSERT_EQUALS(subObj2->recvMsgs.size(), 0);
        TS_ASSERT_EQUALS(subObj2->numObs, 6); // one extra for obs at time 0
        TS_ASSERT_EQUALS(subObj2->numPrc, 0);
        TS_ASSERT_EQUALS(subObj2->numAct, 0);
    }
};
