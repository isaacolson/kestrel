#pragma once

#include <vector>

#include "SimZcmSubObject.hpp"

class SimZcmObjectTest;

namespace Kestrel {
namespace Sim {
namespace Test {

class SimZcmSubTgrObject: public SimZcmSubObject
{
  public:
    typedef SimZcmSubObject ParentType;
    friend SimZcmObjectTest;

  protected:
    std::vector<std::string> tgrChans;
    std::vector<SubData*>     tgrSubs;

    void tgrAll(std::vector<SubData*>& subVec)
    { for (auto& subData : subVec) setTriggerSubs(subData->sub); }

  public:
    /// Ctor
    SimZcmSubTgrObject(const std::vector<std::string>& tgrChans = {"SIM_TIME"},
                       const std::vector<std::string>& chans = {},
                       f64 obsPeriod = -1, f64 actDelay = 0.005, sz printLvl = 0) :
        SimZcmSubObject(chans, obsPeriod, actDelay, printLvl), tgrChans(tgrChans)
    { setName("SimZcmSubTgr"); }
    /// Dtor
    virtual ~SimZcmSubTgrObject()
    { unsubAll(tgrSubs); }

    void initObserve(const std::vector<const Object*>* world) override
    {
        subAll(tgrSubs, tgrChans);
        tgrAll(tgrSubs);
        SimZcmSubObject::initObserve(world);
    }

    NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world) override
    {
        auto ret = SimZcmSubObject::observe(t, tExpect, world);
        return NextTimeObsAct(obsPeriod < 0 ? NO_OBSERVE : ret.first, ret.second);
    }

    Object* clone() const override { return new SimZcmSubTgrObject(*this); }

    /// Swap
    inline void swap(SimZcmSubTgrObject& o)
    {
        ParentType::swap(o);
        tgrChans.swap(o.tgrChans);
        Assert(tgrSubs.empty() && o.tgrSubs.empty(), "Cannot swap once subscribed");
    }
    /// Copy Ctor
    SimZcmSubTgrObject(const SimZcmSubTgrObject& o) :
        ParentType(o), tgrChans(o.tgrChans)
    {
        Assert(tgrSubs.empty() && o.tgrSubs.empty(), "Cannot copy once subscribed");
    }
    /// Move Ctor
    SimZcmSubTgrObject(SimZcmSubTgrObject&& o) { swap(o); }
    /// Unifying Assignment
    SimZcmSubTgrObject& operator=(SimZcmSubTgrObject o) { swap(o); return *this; }
};

} /* Test */
} /* Sim */
} /* Kestrel */
