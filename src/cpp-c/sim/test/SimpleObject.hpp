#pragma once

#include <iostream>
#include <cmath>

#include "kestrel/sim/Object.hpp"

namespace Kestrel {
namespace Sim {
namespace Test {

class SimpleObject: public Object {
  public:
    typedef Object ParentType;

  public:
    f64  obsPeriod;
    f64  actDelay;
    bool print;
    sz   nChildren;

    bool properInitObs = false;
    sz   numObs        = 0;
    sz   numPrc        = 0;
    sz   numAct        = 0;

  public:
    /// Ctor
    SimpleObject(f64 obsPeriod = 0.01, f64 actDelay = 0.005, bool print = false,
                 sz nChildren = 0) :
        obsPeriod(obsPeriod), actDelay(actDelay), print(print), nChildren(nChildren)
    { setName("Simple"); }
    /// Dtor
    virtual ~SimpleObject() {}

    void initObserve(const std::vector<const Object*>* world) override
    {
        // initObserve must be called once and before the object is running
        properInitObs = !isRunning()   &&
                        !properInitObs &&
                        numObs == 0    &&
                        numPrc == 0    &&
                        numAct == 0;
    }

    NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world) override
    {
        ++numObs;
        if (print) std::cout << "t = " << t << ", tExpect = " << tExpect
                             << " Observing: " << numObs << std::endl;

        return NextTimeObsAct((tExpect + obsPeriod > t) ? tExpect + obsPeriod
                                                        : NEXT_OBSERVE,
                              std::max(tExpect + actDelay, t));
    }

    bool process(f64 tObs, f64 tObsExpect, f64 tAct) override
    {
        // TODO: idea of how to test interactive objects
        // - basically copy this object, but add a random amount of processor usage in the
        //   process function (not just a sleep, actually use the processor)
        // - in observe, each object looks at every other object and ensures that they have
        //   the same number of actions
        // - in act, increment your action counter
        ++numPrc;
        if (print) std::cout << "tObs = " << tObs << ", tObsExpect = " << tObsExpect
                             << ", tAct = " << tAct << " Processing: " << numPrc << std::endl;

        return true;
    }

    bool act(f64 t, f64 tExpect) override
    {
        ++numAct;
        if (print) std::cout << "t = " << t << ", tExpect = " << tExpect
                             << " Acting: " << numAct << "\n" << std::endl;

        return true;
    }

    Object* clone() const override { return new SimpleObject(*this); }

  protected:
    std::vector<Object*> getChildObjects() const override
    {
        std::vector<Object*> v;
        for (sz i = 0; i < nChildren; ++i) {
            v.push_back(new SimpleObject(obsPeriod, actDelay, print, 0));
        }
        return v;
    }

  public:
    /// Swap
    inline void swap(SimpleObject& o)
    {
        ParentType::swap(o);
        std::swap(obsPeriod, o.obsPeriod);
        std::swap(actDelay, o.actDelay);
        std::swap(print, o.print);
        std::swap(nChildren, o.nChildren);
        std::swap(properInitObs, o.properInitObs);
        std::swap(numObs, o.numObs);
        std::swap(numPrc, o.numPrc);
        std::swap(numAct, o.numAct);
    }
    /// Copy Ctor
    SimpleObject(const SimpleObject& o) : ParentType(o),
                                          obsPeriod(o.obsPeriod),
                                          actDelay(o.actDelay),
                                          print(o.print),
                                          nChildren(o.nChildren),
                                          properInitObs(o.properInitObs),
                                          numObs(o.numObs),
                                          numPrc(o.numPrc),
                                          numAct(o.numAct) {}

    /// Move Ctor
    SimpleObject(SimpleObject&& o) { swap(o); }
    /// Unifying Assignment
    SimpleObject& operator=(SimpleObject o) { swap(o); return *this; }
};

} /* Test */
} /* Sim */
} /* Kestrel */
