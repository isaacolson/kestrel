#pragma once

#include <iostream>

#include <deque>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>

#include <algorithm>
#include <memory>
#include <mutex>
#include <utility>

#include <zcm/zcm-cpp.hpp>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"

namespace Kestrel {
namespace Sim {

class Comms
{
  /*** Interface ***/
  public:
    typedef void (*TriggerCb)(f64 t, void* usr);

    /// Ctor, external zcm transport will not be used
    Comms();
    /// Ctor with external zcm url
    Comms(const std::string& externalZcmUrl);
    /// Dtor
    virtual ~Comms();

    bool            good()        const;
    int              err()        const;
    const char* strerror()        const;
    const char* strerrno(int err) const;

    int publish(void* uid, const std::string& channel, const uint8_t* data, uint32_t len);

    /// Send triggerCb == nullptr to signal that the sub shouldn't trigger an observe
    void* subscribe(void* uid, const std::string& channel, zcm::MsgHandler cb, void* usr,
                    TriggerCb triggerCb = nullptr, void* triggerUsr = nullptr);
    void setTrigger(void* uid, void* subscription,
                    TriggerCb triggerCb = nullptr, void* triggerUsr = nullptr);
    bool hasTriggerSubscription(void* uid) const;

    void unsubscribe(void* uid, void* subscription);

    void flushIncoming(f64 t);

    void handleMe(void *uid);

  /*** Internal Datatypes ***/
  private:
    template <typename T> using uptr = std::unique_ptr<T>;
    template <typename T> using sptr = std::shared_ptr<T>;
    struct IncomingMsg;
    class SubData;
    typedef std::pair<sptr<IncomingMsg>,             SubData* > MsgWithSub;
    typedef std::pair<sptr<IncomingMsg>, std::vector<SubData*>> MsgWithSubs;

    static bool compMsgWithSubs(const MsgWithSubs& a, const MsgWithSubs& b)
    { return *a.first < *b.first; }

    // Note: we override the rbuf utime field using the input time * 1e6. This will only
    //       have an impact on sim objects that directly access the receive buf utime
    //       as opposed to using an embedded utime field in the message (which is always
    //       recommended).
    struct IncomingMsg {
        f64                t;
        zcm::ReceiveBuffer rbuf;
        std::string        channel;

        IncomingMsg(f64 t, const zcm::ReceiveBuffer* rbuf, const std::string& channel) :
            t(t), rbuf(*rbuf), channel(channel)
        {
            // Copy constructor of rbuf only copies the POD in it, notably not
            // a deep copy of the data buffer, so we do that here
            this->rbuf.data = new uint8_t[rbuf->data_size];
            std::copy_n(rbuf->data, rbuf->data_size, this->rbuf.data);

            this->rbuf.recv_utime = t * 1e6;
        }

        bool operator<(const IncomingMsg& o) const
        {
            if (t == o.t) {
                // Note: We are breaking ties using channel name for more deterministic
                //       behavior even though it will enforce a kinda arbitrary ordering.
                return channel < o.channel;
            }
            return t < o.t;
        }

        ~IncomingMsg() { delete [] rbuf.data; }
    };

    class SubData {
      private:
        sz                            subNumber;
        void*                         uid;
        std::string                   channel;

        // Data used for subscription handling (referencing back to the parent Comms class)
        const f64&                    currTime;
        std::deque<MsgWithSubs>&      incomingMsgCache;
        const sz&                     cacheIdx;

        // Note : using c style message handler for the main callback to allow us
        //        to interface with an overriden ZCM::subscribe_raw call
        zcm::Subscription*            underlyingSub = nullptr;
        zcm::MsgHandler               cb;
        void*                         cbUsr;

        TriggerCb                     triggerCb;
        void*                         triggerUsr;

        // TODO: Consider using shared ptrs to avoid copy costs.
        //       Currently, we incur 1 extra copy per message per subscription. If we
        //       switch to a shared memory system, we might be able to get down to 1
        //       copy per message. Should be able to make a unique ID for each message
        //       by incrementing an integer each time you call handleNonblock()
        std::deque<sptr<IncomingMsg>> msgs;

      public:
        SubData(sz subNumber, void* uid, const std::string& channel,
                const f64& currTime, std::deque<MsgWithSubs>& incomingMsgCache, const sz& cacheIdx,
                zcm::MsgHandler cb, void* cbUsr, TriggerCb triggerCb, void* triggerUsr) :
            subNumber(subNumber), uid(uid), channel(channel),
            currTime(currTime), incomingMsgCache(incomingMsgCache), cacheIdx(cacheIdx),
            cb(cb), cbUsr(cbUsr), triggerCb(triggerCb), triggerUsr(triggerUsr), msgs()
        { }
        SubData(const SubData&  o) = delete;
        SubData(      SubData&& o) = delete;

        ~SubData()
        {
            msgs.clear();
            if (underlyingSub) {
                std::cerr << "SubData subscription left over : \n"
                          << "  Uid : " << uid << "\n"
                          << "  channel : " << channel << std::endl;
                Assert(false, "Leftover bookkeeping subscription");
            }
        }

        void subscribe(zcm::ZCM* transport)
        {
            underlyingSub = transport->subscribe(channel, &SubData::underlyingSubCb, this);
            Assert(underlyingSub != nullptr,
                   "ZCM subscribe failed, too many subscriptions? (see zcm/nonblocking.c)");
        }

        void unsubscribe(zcm::ZCM* transport)
        { transport->unsubscribe(underlyingSub); underlyingSub = nullptr; }

        void underlyingSubCb(const zcm::ReceiveBuffer* rbuf, const std::string& channel)
        {
            if (isTrigger()) notifyTrigger(currTime);

            // This message is already stored in the cache
            if (incomingMsgCache.size() > cacheIdx) {
                incomingMsgCache[cacheIdx].second.push_back(this);
            } else {
                incomingMsgCache.push_back(
                    MsgWithSubs(std::make_shared<IncomingMsg>(currTime, rbuf, channel), {this}));
            }
        }

        /// Pushes a share memory message to the subs msg deque
        void pushCachedMsg(const sptr<IncomingMsg>& msg) { msgs.push_back(msg); }

        /// Returns the shared ptr front message and removes it from the deque
        sptr<IncomingMsg> popFrontMsg()
        {
            auto ret = msgs.front();
            msgs.pop_front();
            return ret;
        }

        /// Dispatches the callback on a given message
        void dispatchMsg(const IncomingMsg* msg) { cb(&(msg->rbuf), msg->channel.c_str(), cbUsr); }

        /// Sends notification that a message is ready on a triggered subscription
        void notifyTrigger(f64 t) { triggerCb(t, triggerUsr); }

        void setTrigger(TriggerCb triggerCb, void* triggerUsr)
        {
            this->triggerCb  = triggerCb;
            this->triggerUsr = triggerUsr;
        }

        sz    size() { return msgs.size();  }
        bool empty() { return msgs.empty(); }

        sz                 getSubNumber() { return subNumber; }
        const std::string& getChannel()   { return channel; }
        bool               isTrigger()    { return triggerCb != nullptr; }

        /// Returns the order of the 2 SubDatas based on the first message they contain.
        /// Meaningless if either is empty.
        bool operator<(const SubData& o) const
        {
            if (msgs.front()->t == o.msgs.front()->t) {
                if (subNumber == o.subNumber) {
                    // Note: We are breaking ties using channel name for more deterministic
                    //       behavior even though it will enforce a kinda arbitrary ordering.
                    return msgs.front()->channel < o.msgs.front()->channel;
                }
                return subNumber < o.subNumber;
            }
            return msgs.front()->t < o.msgs.front()->t;
        }

        // Note: std::less creates a MAX heap, std::greater creates a MinHeap. Using a
        //       comp struct so we can pass it in as a template arg to priority queue.
        struct SubDataPtrMinHeap {
            bool operator()(const SubData* a, const SubData* b) { return *b < *a; }
        };

        friend std::ostream& operator<<(std::ostream& os, const SubData& subData)
        {
            os << "uid : " << subData.uid << ", channel : " << subData.channel;
            return os;
        }
    };

  /*** Internal Data and Methods ***/
    /// Locks data access in functions that are potentially concurrent (see Comms.cpp for details)
    mutable std::mutex dataLock;

    /// Underlying zcm transport that handles message routing emulation
    zcm::ZCM transport;
    /// Optional zcm transport to make messages visible outside sim
    uptr<zcm::ZCM> extTrans;
    /// Url for external zcm transport
    std::string externalZcmUrl;

    /// Only 1 unique user is allowed to publish on a given channel
    std::unordered_map<std::string, void*> pubChanToUid;

    /// Lookup of subscription data based on uid. This owns the memory. Unfortunately, can't use
    /// unique pointers in here because we actually need to look up items in the set with the ptr.
    std::unordered_map<void*, std::unordered_set<SubData*>> uidToSubData;
    /// Globally incrementing counter of how many subscriptions we've had
    sz subNumber = 0;
    /// Last sim time that was called with flushIncoming
    f64 currTime;
    /// Intermediate storage of incoming messages during flushIncoming
    std::deque<MsgWithSubs> incomingMsgCache;
    /// Current message index within `incomingMsgs` (only used in flushIncoming)
    sz  cacheIdx = 0;

    void init();
    void deinit();

  public:
    // Note: Copy and swap are not implemented in a way that allows them to be called after
    //       Sim::Comms has been used for any publish / subscribe actions. If necessary,
    //       this could be improved in the future, but for now, we don't use it in a way
    //       that would require this. If you use them improperly, they will assert and
    //       inform you of the issue.

    /// Swap
    void swap(Comms& o);
    /// Copy Ctor
    Comms(const Comms& o);
    /// Move Ctor
    Comms(Comms&& o) { swap(o); }
    /// Unifying Assignment
    Comms& operator=(Comms o) { swap(o); return *this; }
};

} /* Sim */
} /* Kestrel */
