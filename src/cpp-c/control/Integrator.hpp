#pragma once

#include <utility>
#include <type_traits>
#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"

#include "kestrel/control/Differentiator.hpp"

namespace Kestrel {
namespace Control {

/// A class that integrates a state vector through time using high order integration given
/// a description of the state's derivative
// Note: have to use "int" here because Eigen uses "int" (else template resolution fails)
template <typename Scalar, int ROWS, int COLS=1>
class Integrator
{
    // TODO: look through all controls code and optimize matrix math for aliasing:
    //       http://eigen.tuxfamily.org/dox/group__TopicAliasing.html
  public:
    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS * COLS;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Integrand;

  protected:
    sz order;

  public:
    /// Ctor
    Integrator(sz order = 1) : order(order)
    {
        static_assert(ROWS > 0, "_ROWS must be > 0");
        static_assert(COLS > 0, "_COLS must be > 0");

        Assert(validOrder(), "Integrator not implemented for arbitrary orders");
    }
    /// Dtor
    virtual ~Integrator() {}

    // TODO: this should probably modify x in place; however, need to be careful for
    //       dealing with rvalues and pushing data into pairs (at least for the Gaussian
    //       extension). Should probably make some copy tracking tests.
    /// Returns a state vector with domains corrected. Defaults to no-op, but in cases of
    /// angles or rotational states, this should be used for wraparound, normalization, etc.
    virtual _Integrand correctDomain(const _Integrand& x) const { return x; }

    /// Integrate using a Differentiator class
    template<typename Differ, typename... Args>
    _Integrand operator()(const _Integrand& x, f64 t, f64 dt, const Differ& diff,
                          Args&&... args) const
    {
        { // Perform static type checking for valididity
            typedef Differentiator<_Scalar, _ROWS, _COLS, Differ::_NUM_INPUTS, Differ::_NUM_NOISES>
                    BaseDiffer;
            static_assert(std::is_base_of<BaseDiffer, Differ>::value,
                          "Integrator requires a valid Differentiator "
                          "class of the proper dimension");
        }

        _Integrand k1 = diff(x, t, std::forward<Args>(args)...);
        if (order == 1) return correctDomain(x + dt * k1);

        // XXX: it might be better or even necessary to correct the domain of x at each
        //      intermediate step of the higher order integration. It might be worth doing some
        //      tests on the accuracty of this, because it could become VERY expensive to be
        //      doing so many domain corrections.
        _Integrand k2 = diff(correctDomain(x + dt/2 * k1), t + dt/2, std::forward<Args>(args)...);
        if (order == 2) return correctDomain(x + dt * k2);

        _Integrand k3 = diff(correctDomain(x + dt/2 * k2), t + dt/2, std::forward<Args>(args)...);
        _Integrand k4 = diff(correctDomain(x + dt   * k3), t + dt,   std::forward<Args>(args)...);
        if (order == 4) return correctDomain(x + dt/6 * (k1 + 2*k2 + 2*k3 + k4));

        return x;
    }

    /// Returns if set to a valid Runge-Kutta order
    inline bool validOrder() const { return order == 1 || order == 2 || order == 4; }

    /// Swap
    inline void swap(Integrator& o) { std::swap(order, o.order); }
    /// Copy Ctor
    Integrator(const Integrator& o) : order(o.order) {}
    /// Move Ctor
    Integrator(Integrator&& o) { swap(o); }
    /// Unifying Assignment
    Integrator& operator=(Integrator o) { swap(o); return *this; }

};

} /* Control */
} /* Kestrel */
