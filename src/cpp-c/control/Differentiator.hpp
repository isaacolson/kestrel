#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

namespace Kestrel {
namespace Control {

/// Represents a dynamic model that can generate the derivative of a state
// Note: have to use "int" here because Eigen uses "int" (else template resolution fails)
template <typename Scalar, int ROWS, int COLS=1, int NUM_INPUTS=0, int NUM_NOISES=0>
class Differentiator
{
  public:
    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS*COLS;
    static constexpr int _NUM_INPUTS = NUM_INPUTS;
    static constexpr int _NUM_NOISES = NUM_NOISES;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    // Note: I know this isn't a real word, needed a word like Integrand but for derivatives
    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Derivand;

    // Note: _StateVec formed by stacking columns of _Derivand (ie column major)
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, 1>           _StateVec;
    typedef Eigen::Matrix<_Scalar, _NUM_INPUTS, 1>           _InputVec;
    typedef Eigen::Matrix<_Scalar, _NUM_NOISES, 1>           _NoiseVec;

  public:
    /// Ctor
    Differentiator()
    {
        static_assert(ROWS > 0, "ROWS must be > 0");
        static_assert(COLS > 0, "COLS must be > 0");
        static_assert(NUM_INPUTS >= 0, "NUM_INPUTS must be >= 0");
        static_assert(NUM_NOISES >= 0, "NUM_NOISES must be >= 0");
    }
    /// Dtor
    virtual ~Differentiator() {}

    /////////////////////////////////////////////////////////////////////////////////////
    // User interface : override these functions to inject desired behavior
    /////////////////////////////////////////////////////////////////////////////////////

    /// Returns the derivative of the state given the input and noise vectors
    virtual _Derivand operator()(const _Derivand& x, f64 t,
                                 const _InputVec& u = _InputVec::Zero(),
                                 const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        // Note: though this function is not pure virtual, all children should override it

        // XXX: this doesn't work on 1x1's

        // Treats input as if rows are derivatives of those above them, then adds input and
        // noise vectors transposed to the bottom row (left aligned)
        _Derivand ret = _Derivand::Zero();
        ret.template block<_ROWS-1, _COLS>(0, 0) = x.template block<_ROWS-1, _COLS>(1, 0);

        // Note: this seems like overkill, but the next line MUST compile for all possible
        //       values of NUM_INPUTS/NOISES (even 0) because it will be statically evaluated
        //       therefore, we need to take the min in a way that evaluates statically
        //       (hence the ternary statement). The surrounding if guards prevent runtime asserts
        //       within Eigen.
        if (_NUM_INPUTS > 0) {
            ret.template block<1, MathUtil::Min<_COLS,_NUM_INPUTS>::val>(_ROWS-1, 0) +=
                u.template block<MathUtil::Min<_COLS,_NUM_INPUTS>::val, 1>(0,0).transpose();
        }
        if (_NUM_NOISES > 0) {
            ret.template block<1, MathUtil::Min<_COLS,_NUM_NOISES>::val>(_ROWS-1, 0) +=
                w.template block<MathUtil::Min<_COLS,_NUM_NOISES>::val, 1>(0,0).transpose();
        }

        return ret;
    }

    /////////////////////////////////////////////////////////////////////////////////////

    /// Swap
    inline void swap(Differentiator& o) {}
    /// Copy Ctor
    Differentiator(const Differentiator& o) {}
    /// Move Ctor
    Differentiator(Differentiator&& o) { swap(o); }
    /// Unifying Assignment
    Differentiator& operator=(Differentiator o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
