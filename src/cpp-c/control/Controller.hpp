#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

namespace Kestrel {
namespace Control {

/// Represents a control law that selects a control input based on the state and a command
template <typename Scalar, int ROWS, int COLS=1, int NUM_INPUTS=1, int NUM_SETPTS=0>
class Controller {
  public:
    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS*COLS;
    static constexpr int _NUM_INPUTS = NUM_INPUTS;
    static constexpr int _NUM_SETPTS = NUM_SETPTS;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    // Note: I know this isn't a real word, needed a word like Integrand but for controllers
    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Controland;

    // Note: _StateVec formed by stacking columns of the _Controland (ie column major)
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, 1>           _StateVec;
    typedef Eigen::Matrix<_Scalar, _NUM_INPUTS, 1>           _InputVec;
    typedef Eigen::Matrix<_Scalar, _NUM_SETPTS, 1>           _SetptVec;

  public:
    /// Ctor
    Controller()
    {
        static_assert(ROWS > 0, "ROWS must be > 0");
        static_assert(COLS > 0, "COLS must be > 0");
        static_assert(NUM_INPUTS > 0, "NUM_INPUTS must be > 0");
        static_assert(NUM_SETPTS >= 0, "NUM_SETPTS must be >= 0");
    }
    /// Dtor
    virtual ~Controller() {}

    /////////////////////////////////////////////////////////////////////////////////////
    // User interface : override these functions to inject desired behavior
    /////////////////////////////////////////////////////////////////////////////////////

    /// Returns the control input given a state and a setpoint. The dt allows the controller
    /// to have time varying state if desired (which also means this operator isn't const)
    virtual _InputVec operator()(const _Controland& x, f64 t, f64 dt,
                                 const _SetptVec& r = _SetptVec::Zero())
    {
        // Note: though this function is not pure virtual, all children should override it

        // Do a VERY simple linear controller as a test
        _InputVec u = _InputVec::Zero();
        u.template block<MathUtil::Min<_COLS, _NUM_INPUTS>::val, 1>(0,0) -=
            x.template block<1, MathUtil::Min<_COLS, _NUM_INPUTS>::val>(0,0).transpose();

        // Note: this seems like overkill, but the next line MUST compile for all possible
        //       values of NUM_SETPTS (even 0) because it will be statically evaluated
        //       therefore, we need to take the min in a way that evaluates statically
        //       The surrounding if guards prevent runtime asserts within Eigen.
        if (_NUM_SETPTS > 0) {
            _SetptVec rCorr = correctDomain(x, r);
            u.template block<MathUtil::Min<_NUM_SETPTS, _NUM_INPUTS>::val, 1>(0,0) +=
                rCorr.template block<MathUtil::Min<_NUM_SETPTS, _NUM_INPUTS>::val, 1>(0,0);
        }

        return u;
    }

    /// Converts a setpoint vector to have the proper domain when compared to a given state.
    /// Default operation is a no-op, but controllers that act on angles, etc. may want
    /// to perform some sort wraparound correction
    virtual _SetptVec correctDomain(const _Controland& x, const _SetptVec& r) const { return r; }

    /////////////////////////////////////////////////////////////////////////////////////

    /// Swap
    inline void swap(Controller& o) {}
    /// Copy Ctor
    Controller(const Controller& o) {}
    /// Move Ctor
    Controller(Controller&& o) { swap(o); }
    /// Unifying Assignment
    Controller& operator=(Controller o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
