#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

namespace Kestrel {
namespace Control {

/// Represents a discrete update to a state vector given an observation as well as generated
/// an estimated observation from the state
template <typename Scalar, int ROWS, int COLS=1, int NUM_OBS=1>
class Observer {
  public:
    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS*COLS;
    static constexpr int _NUM_OBS    = NUM_OBS;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    // Note: I know this isn't a real word, needed a word like Integrand but for observed quantities
    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Observand;
    typedef Eigen::Matrix<_Scalar, _NUM_OBS, 1>              _ObsVec;

    // Note: _StateVec formed by stacking columns of _Observand (ie column major)
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, 1>           _StateVec;

  public:
    /// Ctor
    Observer()
    {
        static_assert(ROWS > 0, "ROWS must be > 0");
        static_assert(COLS > 0, "COLS must be > 0");
        static_assert(NUM_OBS > 0, "NUM_OBS must be > 0");
    }
    /// Dtor
    virtual ~Observer() {}

    /////////////////////////////////////////////////////////////////////////////////////
    // User interface : override these functions to inject desired behavior
    /////////////////////////////////////////////////////////////////////////////////////

    // TODO: add a domain correction both on the residual and the corrected state

    /// Returns the updated state given the discrete observation
    virtual _Observand operator()(const _Observand& x, f64 t, const _ObsVec& y) const
    {
        // Note: though this function is not pure virtual, all children should override it

        // Treats observation as if it is the observed value of the first row of the _Observand.
        // Updates rows of the _Observand each with a progressive power of 1/2 of the residual.
        _Observand ret = x;
        _ObsVec residual = y - estObs(x, t);
        f64 factor = 1;
        for (int i = 0; i < _ROWS; ++i) {
            ret.template block<1, MathUtil::Min<_COLS,_NUM_OBS>::val>(i, 0) += factor *
                residual.template block<MathUtil::Min<_COLS,_NUM_OBS>::val, 1>(0,0).transpose();
            factor *= 0.5;
        }

        return ret;
    }

    /// Returns the expected observation given the observand
    virtual _ObsVec estObs(const _Observand& x, f64 t) const
    {
        // Note: though this function is not pure virtual, all children should override it

        // Treats observation as the first row of the observand
        _ObsVec ret = _ObsVec::Zero();
        ret.template block<MathUtil::Min<_COLS,_NUM_OBS>::val, 1>(0,0) =
            x.template block<1, MathUtil::Min<_COLS,_NUM_OBS>::val>(0,0).transpose();

        return ret;
    }

    /////////////////////////////////////////////////////////////////////////////////////

    /// Swap
    inline void swap(Observer& o) {}
    /// Copy Ctor
    Observer(const Observer& o) {}
    /// Move Ctor
    Observer(Observer&& o) { swap(o); }
    /// Unifying Assignment
    Observer& operator=(Observer o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
