#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"

#include "kestrel/control/LinearizingObserver.hpp"

class GaussianObserverTest;

namespace Kestrel {
namespace Control {

/// Specialization of the discrete observation update that updates a Gaussian distribution
/// according to a locally linearized Kalman Filter update
template <typename Scalar, int ROWS, int COLS=1, int NUM_OBS=1>
class GaussianObserver:
    public  LinearizingObserver<Scalar, ROWS, COLS, NUM_OBS> {
  public:
    typedef LinearizingObserver<Scalar, ROWS, COLS, NUM_OBS> ParentType;

    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS*COLS;
    static constexpr int _NUM_OBS    = NUM_OBS;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    // Note: I know this isn't a real word, needed a word like Integrand but for observed quantities
    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Observand;
    typedef Eigen::Matrix<_Scalar, _NUM_OBS, 1>              _ObsVec;

    // Note: _StateVec formed by stacking columns of _Observand (ie column major)
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, 1>           _StateVec;
    // Note: Covariances and Jacobians for matrix observations will use same order from _Observand
    typedef Eigen::Matrix<_Scalar, _NUM_OBS,    _NUM_STATES> _StateJac;
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_STATES> _StateCov;
    typedef Eigen::Matrix<_Scalar, _NUM_OBS,    _NUM_OBS>    _ObsCov;
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_OBS>    _KalmanGain;

    typedef std::pair<_Observand, _StateCov>                 _StateDist;

  public:
    /// Ctor
    GaussianObserver() {}
    /// Dtor
    virtual ~GaussianObserver() {}

    /////////////////////////////////////////////////////////////////////////////////////
    // User interface : override these functions to inject desired behavior
    /////////////////////////////////////////////////////////////////////////////////////

    // Note: User should implement all virtual functions from the ParentType.
    /// Returns the expected observation given the observand
    using ParentType::estObs;
    /// Returns the Jacobian of the estimated observation with respect to the state
    using ParentType::stateJac;

    /// Because observation without covariances doesn't make sense in the Gaussian context,
    /// this function is replaced by the null-op. If there is a better way to handle a
    /// no-covariance observation for a specific child of this, go ahead and override it.
    virtual _Observand operator()(const _Observand& x, f64 t, const _ObsVec& y) const
    { /* XXX: maybe just assert here? */ return x; }

    /////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////
    // Linearized Gaussian Behavior
    /////////////////////////////////////////////////////////////////////////////////////

  private: mutable int prints = 5;
  public:
    /// Returns the updated state given the discrete observation and covariances in a
    /// linearized update sense (ie extended Kalman filter).
    virtual _StateDist operator()(const _Observand& x, const _StateCov& P, f64 t,
                                  const _ObsVec&    y, const _ObsCov&   R) const
    {
        // Note: though this function is virtual, it is unlikely children will need to override
        //       it. Simply overriding the other member functions will cause this to perform
        //       the optimal linear update.

        _ObsVec   h = estObs(x, t);
        _StateJac H = stateJac(x, t);
        _ObsCov   S = H * P * H.transpose() + R;

        Eigen::FullPivLU<_ObsCov> Slu(S);
        Slu.setThreshold(MathUtil::TINY);

        if (Slu.isInvertible()) {
            // TODO: double check that this is the most efficient way to solve for K using the
            //       LU decomp
            _KalmanGain K = P * H.transpose() * Slu.inverse();
            _StateCov   U = _StateCov::Identity() - K * H;

            // Note: using the recommended update equation from Bar-Shalom Chapter 5 which ensures
            //       symmetry and positive definiteness (provided P and R are symm pos def).
            //       Should be identitcal to the common result: Pnew = U*P = U*P*U' + K*R*K';
            return std::make_pair(_Observand((_StateVec(x.data()) + K * (y - h)).eval().data()),
                                  U * P * U.transpose() + K * R * K.transpose());
        } else {
            Eigen::JacobiSVD<_StateCov> Psvd(P, Eigen::ComputeFullU | Eigen::ComputeFullV);
            _StateVec Psv = Psvd.singularValues();
            // Note: Singular values always returned largest to smallest
            _Scalar minSv = std::max(Psv(0), MathUtil::TINY);
            for (int i = 1; i < _NUM_STATES; ++i) {
                if (Psv(i) > MathUtil::TINY) { // Still in large SV section
                    minSv = Psv(i);
                } else {                       // Started small SV section
                    Psv(i) = minSv;
                }
            }
            _StateCov Pnew = Psvd.matrixU() * Psv.asDiagonal() * Psvd.matrixV().adjoint();

            Eigen::JacobiSVD<_ObsCov> Rsvd(R, Eigen::ComputeFullU | Eigen::ComputeFullV);
            _ObsVec Rsv = Rsvd.singularValues();
            // Note: Singular values always returned largest to smallest
            minSv = std::max(Rsv(0), MathUtil::TINY);
            for (int i = 1; i < _NUM_OBS; ++i) {
                if (Rsv(i) > MathUtil::TINY) { // Still in large SV section
                    minSv = Rsv(i);
                } else {                       // Started small SV section
                    Rsv(i) = minSv;
                }
            }
            _ObsCov Rnew = Rsvd.matrixU() * Rsv.asDiagonal() * Rsvd.matrixV().adjoint();

            if (prints-- > 0) {
                std::cerr << "WARNING: Innovation covariance not invertible" << std::endl;
                std::cerr << "P" << std::endl << P << std::endl;
                std::cerr << "H" << std::endl << H << std::endl;
                std::cerr << "R" << std::endl << R << std::endl;
                std::cerr << "S = H * P * H' + R" << std::endl << S << std::endl;
                std::cerr << "SV of P" << std::endl << Psvd.singularValues() << std::endl;
                std::cerr << "adjusted SV of P" << std::endl << Psv << std::endl;
                std::cerr << "Pnew" << std::endl << Pnew << std::endl;
                std::cerr << "SV of R" << std::endl << Rsvd.singularValues() << std::endl;
                std::cerr << "adjusted SV of R" << std::endl << Rsv << std::endl;
                std::cerr << "Rnew" << std::endl << Rnew << std::endl;
            }
            return (*this)(x, Pnew, t, y, Rnew);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////

    /// Swap
    inline void swap(GaussianObserver& o) { ParentType::swap(o); }
    /// Copy Ctor
    GaussianObserver(const GaussianObserver& o) : ParentType(o) {}
    /// Move Ctor
    GaussianObserver(GaussianObserver&& o) { swap(o); }
    /// Unifying Assignment
    GaussianObserver& operator=(GaussianObserver o) { swap(o); return *this; }

    friend GaussianObserverTest;
};

} /* Control */
} /* Kestrel */
