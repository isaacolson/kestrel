#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"

#include "kestrel/control/LinearizingDifferentiator.hpp"

namespace Kestrel {
namespace Control {

/// Specialization of the dynamic model that generates the derivative of a Gaussian distribution
/// according to the local lineariztaion of the model
template<typename Scalar, int ROWS, int COLS=1, int NUM_INPUTS=0, int NUM_NOISES=1>
class GaussianDifferentiator:
    public  LinearizingDifferentiator<Scalar, ROWS, COLS, NUM_INPUTS, NUM_NOISES> {
  public:
    typedef LinearizingDifferentiator<Scalar, ROWS, COLS, NUM_INPUTS, NUM_NOISES> ParentType;

    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS*COLS;
    static constexpr int _NUM_INPUTS = NUM_INPUTS;
    static constexpr int _NUM_NOISES = NUM_NOISES;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    // Note: I know this isn't a real word, needed a word like Integrand but for derivatives
    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Derivand;

    // Note: _StateVec formed by stacking columns of _Derivand (ie column major)
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, 1>           _StateVec;
    typedef Eigen::Matrix<_Scalar, _NUM_INPUTS, 1>           _InputVec;
    typedef Eigen::Matrix<_Scalar, _NUM_NOISES, 1>           _NoiseVec;
    // Note: Covariances and Jacobians for matrix derivatives will use same order from _Derivand
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_STATES> _StateJac;
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_INPUTS> _InputJac;
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_NOISES> _NoiseJac;
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_STATES> _StateCov;
    typedef Eigen::Matrix<_Scalar, _NUM_INPUTS, _NUM_INPUTS> _InputCov;
    typedef Eigen::Matrix<_Scalar, _NUM_NOISES, _NUM_NOISES> _NoiseCov;

    typedef std::pair<_Derivand, _StateCov>                  _StateDist;

    // TODO: consider inheriting from this class (or just expanding it) so it can represent
    //       a continuous observer as well. Requires being able to represent covariance
    //       on both the system model and the measurement
    //       xhatdot = A*xhat + B*u + L*(y - C*xhat)
    //
    //       Probably just need to make it use two noise vectors : model noise and measurement
    //       noise. Covariance on the model noise is basically added in as we're doing now,
    //       but the measurement decreases the covariance inversely proportional to it's own
    //       covariance. See Freudenberg's book for equations.

  public:
    /// Ctor
    GaussianDifferentiator()
    {
        static_assert(NUM_NOISES > 0, "NUM_NOISES must be > 0");
    }
    /// Dtor
    virtual ~GaussianDifferentiator() {}

    /////////////////////////////////////////////////////////////////////////////////////
    // User interface : override these functions to inject desired behavior
    /////////////////////////////////////////////////////////////////////////////////////

    // Note: User should implement all virtual functions from the ParentType.
    /// Returns the derivative of the mean given the input and noise vectors
    using ParentType::operator();
    /// Returns the Jacobian of the derivative of the state with respect to the state
    using ParentType::stateJac;
    /// Returns the Jacobian of the derivative of the state with respect to the input
    using ParentType::inputJac;
    /// Returns the Jacobian of the derivative of the state with respect to the noise
    using ParentType::noiseJac;

    /// Computes the noise covariance (noise is assumed to be zero mean Gaussian)
    virtual _NoiseCov noiseCov(const _Derivand& x, f64 t,
                               const _InputVec& u = _InputVec::Zero(),
                               const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        // Note: though this function is not pure virtual, all children should override it
        return _NoiseCov::Identity();
    }

    /////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////
    // Linearized Gaussian Behavior (unlikely to be overriden)
    /////////////////////////////////////////////////////////////////////////////////////

    /// Returns the derivative of the state mean and covariance given the input and noise vectors
    virtual _StateDist operator()(const _Derivand& x, const _StateCov& P, f64 t,
                                  const _InputVec& u = _InputVec::Zero(),
                                  const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        // Note: though this function is virtual, it is unlikely any children will
        //       need to override it as it is simply the linearization of a Gaussian Dist
        //       One potential extension would be to use a sample point approach instead
        //       of linearizing the dynamics (ie UKF-like instead of EKF-like)

        return std::make_pair(operator()(x, t, u, w), covDeriv(x, P, t, u, w));
    }

    /// Alternate derivative call using a _StateDist
    _StateDist operator()(const _StateDist& xP, f64 t, const _InputVec& u = _InputVec::Zero(),
                                                       const _NoiseVec& w = _NoiseVec::Zero()) const
    { return operator()(xP.first, xP.second, t, u, w); }

    /// Returns the derivative of the state covariance given the input and noise vectors
    virtual _StateCov covDeriv(const _Derivand& x, const _StateCov& P, f64 t,
                               const _InputVec& u = _InputVec::Zero(),
                               const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        // Note: though this function is virtual, it is unlikely to be overrided (see above)

        _StateJac F = stateJac(x, t, u, w);
        _NoiseJac L = noiseJac(x, t, u, w);
        _NoiseCov Q = noiseCov(x, t, u, w);
        return F * P + P * F.transpose() + L * Q * L.transpose();
    }

    /////////////////////////////////////////////////////////////////////////////////////

    /// Swap
    inline void swap(GaussianDifferentiator& o) { ParentType::swap(o); }
    /// Copy Ctor
    GaussianDifferentiator(const GaussianDifferentiator& o) : ParentType(o) {}
    /// Move Ctor
    GaussianDifferentiator(GaussianDifferentiator&& o) { swap(o); }
    /// Unifying Assignment
    GaussianDifferentiator& operator=(GaussianDifferentiator o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
