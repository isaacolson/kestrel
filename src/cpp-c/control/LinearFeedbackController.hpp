#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "kestrel/control/Controller.hpp"

namespace Kestrel {
namespace Control {

/// Specialized control law where the control input is a linear operation on the state and command
template <typename Scalar, int ROWS, int COLS=1, int NUM_INPUTS=1, int NUM_SETPTS=0>
class LinearFeedbackController:
    public  Controller<Scalar, ROWS, COLS, NUM_INPUTS, NUM_SETPTS> {
  public:
    typedef Controller<Scalar, ROWS, COLS, NUM_INPUTS, NUM_SETPTS> ParentType;

    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS*COLS;
    static constexpr int _NUM_INPUTS = NUM_INPUTS;
    static constexpr int _NUM_SETPTS = NUM_SETPTS;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    // Note: I know this isn't a real word, needed a word like Integrand but for controllers
    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Controland;

    // Note: _StateVec and Gain formed by stacking columns of the _Controland (ie column major)
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, 1>           _StateVec;
    typedef Eigen::Matrix<_Scalar, _NUM_INPUTS, 1>           _InputVec;
    typedef Eigen::Matrix<_Scalar, _NUM_SETPTS, 1>           _SetptVec;

    typedef Eigen::Matrix<_Scalar, _NUM_INPUTS, _NUM_STATES> _StateGain;
    typedef Eigen::Matrix<_Scalar, _NUM_INPUTS, _NUM_SETPTS> _SetptGain;

  public:
    /// Ctor
    LinearFeedbackController() {}
    /// Dtor
    virtual ~LinearFeedbackController() {}

    /////////////////////////////////////////////////////////////////////////////////////
    // User interface : override these functions to inject desired behavior
    /////////////////////////////////////////////////////////////////////////////////////

    /// Returns the state gain given a state and a setpoint. The dt allows the controller
    /// to have time varying state if desired (which also means this operator isn't const)
    /// All arguments defaulted because many controllers return constant values for this
    virtual _StateGain stateGain(const _Controland& x = _Controland::Zero(), f64 t = 0, f64 dt = 0,
                                 const _SetptVec& r = _SetptVec::Zero())
    {
        // Note: though this function is not pure virtual, all children should override it

        // Do a VERY simple linear controller as a test
        _StateGain K = _StateGain::Zero();
        for (int i = 0; i < MathUtil::Min<_COLS, _NUM_INPUTS>::val; ++i) {
            K(i, i*_ROWS) = 1;
        }
        return K;
    }

    /// Returns the setpt gain given a state and a setpoint. The dt allows the controller
    /// to have time varying state if desired (which also means this operator isn't const)
    /// All arguments defaulted because many controllers return constant values for this
    virtual _SetptGain setptGain(const _Controland& x = _Controland::Zero(), f64 t = 0, f64 dt = 0,
                                 const _SetptVec& r = _SetptVec::Zero())
    {
        // Note: though this function is not pure virtual, all children should override it

        // Do a VERY simple linear controller as a test
        _SetptGain G = _SetptGain::Zero();
        for (int i = 0; i < MathUtil::Min<_NUM_SETPTS, _NUM_INPUTS>::val; ++i) {
            G(i, i) = 1;
        }
        return G;
    }

    /// Children controlling angles should definitely override the domain correction
    using ParentType::correctDomain;

    /////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////
    // Linear Feedback Control Behavior
    /////////////////////////////////////////////////////////////////////////////////////

    /// Returns the control input given a state and a setpoint. Uses a linear feedback
    /// rule to compute the feedback.
    virtual _InputVec operator()(const _Controland& x, f64 t, f64 dt,
                                 const _SetptVec& r = _SetptVec::Zero())
    {
        // Note: though this function is virtual, it is unlikely any children will
        //       need to override it as it is simply the pure linear feedback rule.

        _SetptVec rCorr = correctDomain(x, r);
        _InputVec u = -stateGain(x, t, dt, rCorr) * _StateVec(x.data());
        if (_NUM_SETPTS > 0) u += setptGain(x, t, dt, rCorr) * rCorr;
        return u;
    }

    /////////////////////////////////////////////////////////////////////////////////////

    /// Swap
    inline void swap(LinearFeedbackController& o) { ParentType::swap(o); }
    /// Copy Ctor
    LinearFeedbackController(const LinearFeedbackController& o) : ParentType(o) {}
    /// Move Ctor
    LinearFeedbackController(LinearFeedbackController&& o) { swap(o); }
    /// Unifying Assignment
    LinearFeedbackController& operator=(LinearFeedbackController o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
