#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"

#include "kestrel/control/Observer.hpp"

namespace Kestrel {
namespace Control {

/// Specialization of the discrete observation update that can generate the Jacobians
/// of its estimated observation function
template <typename Scalar, int ROWS, int COLS=1, int NUM_OBS=1>
class LinearizingObserver:
    public  Observer<Scalar, ROWS, COLS, NUM_OBS> {
  public:
    typedef Observer<Scalar, ROWS, COLS, NUM_OBS> ParentType;

    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS*COLS;
    static constexpr int _NUM_OBS    = NUM_OBS;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    // Note: I know this isn't a real word, needed a word like Integrand but for observed quantities
    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Observand;
    typedef Eigen::Matrix<_Scalar, _NUM_OBS, 1>              _ObsVec;

    // Note: _StateVec formed by stacking columns of _Observand (ie column major)
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, 1>           _StateVec;
    // Note: Jacobians for matrix observations will use same order from _Observand
    typedef Eigen::Matrix<_Scalar, _NUM_OBS, _NUM_STATES>    _StateJac;

  public:
    /// Ctor
    LinearizingObserver() {}
    /// Dtor
    virtual ~LinearizingObserver() {}

    /////////////////////////////////////////////////////////////////////////////////////
    // User interface : override these functions to inject desired behavior
    /////////////////////////////////////////////////////////////////////////////////////

    // Note: User should implement all virtual functions from the ParentType.
    /// Returns the updated state given the discrete observation
    using ParentType::operator();
    /// Returns the expected observation given the observand
    using ParentType::estObs;

    /// Returns the Jacobian of the estimated observation with respect to the state
    virtual _StateJac stateJac(const _Observand& x, f64 t) const
    {
        // Note: though this function is not pure virtual, all children should override it

        // Treats observation as the first row of the observand
        _StateJac ret = _StateJac::Zero();
        for (int i = 0; i < _COLS; ++i) {
            ret(i, i*_ROWS) = 1;
        }
        return ret;
    }

    /////////////////////////////////////////////////////////////////////////////////////

    /// Swap
    inline void swap(LinearizingObserver& o) { ParentType::swap(o); }
    /// Copy Ctor
    LinearizingObserver(const LinearizingObserver& o) : ParentType(o) {}
    /// Move Ctor
    LinearizingObserver(LinearizingObserver&& o) { swap(o); }
    /// Unifying Assignment
    LinearizingObserver& operator=(LinearizingObserver o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
