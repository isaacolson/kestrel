#pragma once

#include <utility>
#include <complex>
#include <vector>
#include <Eigen/Dense>

#include "kestrel/util/MathUtil.hpp"

#include "kestrel/control/Differentiator.hpp"
#include "kestrel/control/Integrator.hpp"

namespace Kestrel {
namespace Control {

/** Models a proper transfer function of the form:
  *          [ b(0), b(1), ..., b(n-1), b(n) ] [ 1; s; ...; s^(n-1); s^n ]
  *     Y = --------------------------------------------------------------- U
  *          [ a(0), a(1), ..., a(n-1), a(n) ] [ 1; s; ...; s^(n-1); s^n ]
  *
  * by using a modified form of the controllable canonical form:
  *     xdot = A*x + B*u
  *        y = C*x + D*u
  *
  *     A = [ 0  1  0  0  ...  0                         B = [ 0
  *           0  0  1  0  ...  0                               0
  *           .  .  .  .  ...  .                               0
  *           0  0  0  0  ...  1                               0
  *           [ -a(0:n-1)/a(n) ] ]                             1 ]
  *     C = [ (b(0:n-1) - a(0:n-1)/a(n)*b(n)) / a(n) ]   D = b(n)/a(n)
  *
  * Allows vector input and output, but applies the same transfer function to each element
  * independently. Cannot currently handle transfer matrices.
  * TODO: I think this requires a LOT more math, but handling transfer matrices would be cool
  *
  * ORDER   : the order of the numerator and denominator polynomials (note that the coefficients
  *           vector is necessarily ORDER+1 long.
  * NUM_ELTS: number of independent inputs that will be filtered
  *
  * Note: if the user requires domain correction, it should be fairly trivial to inherit from
  *       this class and define a new Model (derived from _TFModel) and/or Integrator.
  * TODO: might be able to make this even easier by providing virtual functions within
  *       TransferFunction and calling them from the Model and Integ. See this reference on
  *       calling a wrapper class's functions within the nested class:
  *       http://stackoverflow.com/a/3058343
  *       Idea: for a domain correction that goes from y -> k*y, we can actually simply adjust
  *             x -> k*x to achieve the desired outcome. However, it might actually be better
  *             to only adjust those elements of x that have a nonzero effect on y due to C.
  *             This will help preserve some of the velocity components of the state if the
  *             domain correction is only effecting the top of the state vector (e.g. for a
  *             quaternion domain correction).
  *
  * TODO: there is another canonical form that uses the states as y, dy, ddy, ... until
  *       the last minus the number of zeros. This form is MUCH more complex to construct
  *       but might be advantageous to examine at some point. The primary complexity comes
  *       from the definition of the B matrix.
  *
  *       A = same as above
  *       C = [ 1, 0, 0, ..., 0 ]
  *       D = b(n)/a(n)
  *
  *       res  = [ b(0:n-1) - a(0:n-1)*b(n)/a(n) ]'     (that is, as a column vector)
  *       base = [ a(1), a(2), a(3), a(4),  ..., a(n)
  *                a(2), a(3), a(4),  ..., a(n),    0
  *                a(3), a(4),  ..., a(n),    0,    0
  *                a(4),  ..., a(n),    0,    0,  ...
  *                 ..., a(n),    0,    0,  ...,    0
  *                a(n),    0,    0,  ...,    0,    0 ]
  *
  *       B = base \ res
  *
  *       Note that the "base" matrix is effectively an upper left triangular matrix
  *       whose up-right diagonals are a(1:n). Because solving for B involves an
  *       inverse, it would be advisable to use one of the built in Eigen solvers
  *       and ensure that the result is well conditioned. In general (I think), this will
  *       result in the first few elements of B being zero while the last k of them
  *       will be nonzero, where k is the number of zeros of the transfer function's
  *       numerator. Therefore, this form might be convenient for representing the first
  *       few states of the system as the actual output (and its first few derivatives)
  *       which the user might care about. However, it is definitely not as well
  *       conditioned as the controllable canonical form because the last elements of
  *       B will tend to get large which may cause integration to need a very small
  *       timestep.
  */
template <typename Scalar, int ORDER, int NUM_ELTS=1>
class TransferFunction {
  public:
    typedef Scalar _Scalar;
    static constexpr int _ORDER    = ORDER;
    static constexpr int _NUM_ELTS = NUM_ELTS;

    typedef Eigen::Matrix<              _Scalar,  _ORDER+1,      1> _CoeffVec;
    typedef Eigen::Matrix<std::complex<_Scalar>,  _ORDER+1,      1> _FreqVec;
    typedef Eigen::Matrix<              _Scalar, _NUM_ELTS,      1> _InputVec;
    typedef Eigen::Matrix<              _Scalar, _NUM_ELTS,      1> _OutputVec;

    typedef Eigen::Matrix<              _Scalar,    _ORDER, _ORDER> _StateJac;
    typedef Eigen::Matrix<              _Scalar,    _ORDER,      1> _InputJac;
    typedef Eigen::Matrix<              _Scalar,         1, _ORDER> _OutputJac;

    typedef Integrator<_Scalar, _ORDER, _NUM_ELTS>  _TFInteg;

    class _TFModel:
        public  Differentiator<_Scalar, _ORDER, _NUM_ELTS, _NUM_ELTS> {
      public:
        typedef Differentiator<_Scalar, _ORDER, _NUM_ELTS, _NUM_ELTS> ParentType;

        using typename ParentType::_Derivand;
        using typename ParentType::_NoiseVec;

        _StateJac A;
        _InputJac B;

        bool isInvertible;
        _StateJac AInv;

      public:
        /// Ctor
        _TFModel(const _CoeffVec& denom = _CoeffVec::Identity()) :
            A(_StateJac::Zero()), B(_InputJac::Zero()), isInvertible(false), AInv(_StateJac::Zero())
        {
            static_assert(_ORDER > 0, "Cannot construct transfer function with order < 1");
            static_assert(_NUM_ELTS > 0, "Transfer function needs at least 1 element");

            Assert(fabs(denom(_ORDER)) > MathUtil::TINY, "highest order denom coeff may not be 0");

            A.template diagonal<1>().setOnes();
            A.template block<1, _ORDER>(_ORDER-1, 0) =
                -denom.template block<_ORDER, 1>(0, 0).transpose() / denom(_ORDER);

            B(_ORDER-1) = 1;

            Eigen::FullPivLU<_StateJac> lu(A);
            lu.setThreshold(MathUtil::TINY);
            isInvertible = lu.isInvertible();
            if (isInvertible) AInv = lu.inverse();
        }
        /// Dtor
        virtual ~_TFModel() {}

        _Derivand operator()(const _Derivand& x, const _InputVec& u = _InputVec::Zero()) const
        { return operator()(x, 0, u); }

        _Derivand operator()(const _Derivand& x, f64 t,
                             const _InputVec& u = _InputVec::Zero(),
                             const _NoiseVec& w = _NoiseVec::Zero()) const override
        { return A*x + B*u.transpose(); }

        /// Returns Jacobian of the state with respect to the state
        _StateJac stateJac() const { return A; }
        /// Returns Jacobian of the state with respect to the input
        _InputJac inputJac() const { return B; }

        /// Returns if the Jacobian of the state with respect to the state is invertible
        bool stateJacIsInvertible() const { return isInvertible; }
        /// Returns inverse of the Jacobian of the state with respect to the state
        _StateJac stateJacInv() const { return AInv; }

        /// Swap
        inline void swap(_TFModel& o)
        { ParentType::swap(o); A.swap(o.A); B.swap(o.B);
          std::swap(isInvertible, o.isInvertible); AInv.swap(o.AInv); }
        /// Copy Ctor
        _TFModel(const _TFModel& o) : ParentType(o), A(o.A), B(o.B),
                                      isInvertible(o.isInvertible), AInv(o.AInv) {}
        /// Move Ctor
        _TFModel(_TFModel&& o) { swap(o); }
        /// Unifying Assignment
        _TFModel& operator=(_TFModel o) { swap(o); return *this; }
    };

  private:
    _CoeffVec denom;
    _CoeffVec numer;

    _TFModel model;
    _TFInteg integ;

    _OutputJac C;
    _Scalar    D;

    std::vector<_OutputJac> derivC;
    std::vector<   _Scalar> derivD;

    // synatx note: typename is required because Derivand is a dependent type
    typename _TFInteg::_Integrand x;

    static _CoeffVec getDefaultCoeffs()
    { _CoeffVec ret = _CoeffVec::Zero(); ret[0] = 1; return ret; }

  public:
    /// Ctor
    TransferFunction(const _CoeffVec& denom = getDefaultCoeffs(),
                     const _CoeffVec& numer = getDefaultCoeffs(),
                     size_t integOrder=1) :
        denom(denom), numer(numer), model(denom), integ(integOrder),
        D(numer(_ORDER)/denom(_ORDER)), derivC(), derivD(), x(_TFInteg::_Integrand::Zero())
    {
        static_assert(_ORDER > 0, "Cannot construct transfer function with order < 1");
        static_assert(_NUM_ELTS > 0, "Transfer function needs at least 1 element");

        Assert(fabs(denom(_ORDER)) > MathUtil::TINY, "highest order denom coeff may not be 0");
        Assert(!numer.isZero(MathUtil::TINY), "numer must not be identically zero");

        C = computeOutputJac(denom, numer);

        _CoeffVec tmpNumer = numer;
        while (fabs(tmpNumer(_ORDER)) < MathUtil::TINY) {
            // Effectively multiply numerator by s
            // Note: ".eval()" solves aliasing issues:
            //       http://eigen.tuxfamily.org/dox/group__TopicAliasing.html
            tmpNumer.template block<_ORDER,1>(1,0) = tmpNumer.template block<_ORDER,1>(0,0).eval();
            tmpNumer(0) = 0;
            derivC.push_back(computeOutputJac(denom, tmpNumer));
            derivD.push_back(tmpNumer(_ORDER)/denom(_ORDER));
        }
    }
    /// Dtor
    virtual ~TransferFunction() {}

    /// Returns the TF output given an input and a period over which the input was applied
    virtual _OutputVec operator()(const _InputVec& u, f64 dt)
    {
        x = integ(x, 0, dt, model, u);
        return (C*x).transpose() + D*u;
    }

    // Note: if you override operator(), you'll need to "using ParentType::operator();" to
    //       inherit this scalar version
    /// Scalar handle for NUM_ELTS = 1 only
    _Scalar operator()(_Scalar u, f64 dt)
    {
        static_assert(_NUM_ELTS == 1, "Scalar function only available for scalar TF");
        _OutputVec y; y(0) = u;
        y = operator()(y, dt);
        return y(0);
    }

    // Note: if you override deriv, you'll need to "using ParentType::deriv;" to
    //       inherit this scalar version
    /// Returns the specified derivative of the TF output given the input
    virtual _OutputVec deriv(sz derivOrder, const _InputVec& u) const
    {
        Assert(0 < derivOrder && derivOrder <= derivC.size(), "Invalid deriv order");
        return (derivC[derivOrder-1]*x).transpose() + derivD[derivOrder-1]*u;
    }

    /// Scalar handle for NUM_ELTS = 1 only
    _Scalar deriv(sz derivOrder, _Scalar u) const
    {
        static_assert(_NUM_ELTS == 1, "Scalar function only available for scalar TF");
        _OutputVec y; y(0) = u;
        y = deriv(derivOrder, y);
        return y(0);
    }

    /// Initialize filter by computing the steady state of the transfer function (note: will
    /// not work for transfer functions with non-invertible steady state gain matrices)
    virtual _OutputVec initialize(const _InputVec& u)
    {
        Assert(stateJacIsInvertible(), "Cannot initialize non-invertible transfer function");
        // X = (sI - A)^-1 * B * U       s = 0 (steady state)
        x = -stateJacInv() * inputJac() * u.transpose();
        return (C*x).transpose() + D*u;
    }

    // Note: if you override initialize, you'll need to "using ParentType::initialize;" to
    //       inherit this scalar version
    /// Scalar handle for NUM_ELTS = 1 only
    _Scalar initialize(_Scalar u)
    {
        static_assert(_NUM_ELTS == 1, "Scalar function only available for scalar TF");
        _OutputVec y; y(0) = u;
        y = initialize(y);
        return y(0);
    }

    /// Resets the internal state of the TF
    void reset() { x.setZero(); }

    /// Returns Jacobian of the state with respect to the state
    _StateJac     stateJac() const { return model.stateJac(); }
    /// Returns Jacobian of the state with respect to the input
    _InputJac     inputJac() const { return model.inputJac(); }
    /// Returns Jacobian of the output with respect to the state
    _OutputJac   outputJac() const { return C; }
    /// Returns the feed forward gain
    _Scalar    feedForward() const { return D; }

    /// Returns if the Jacobian of the state with respect to the state is invertible
    bool stateJacIsInvertible() const { return model.stateJacIsInvertible(); }
    /// Returns inverse of the Jacobian of the state with respect to the state
    _StateJac stateJacInv() const { return model.stateJacInv(); }

    /// Returns the gain of the transfer function at the given frequency (in rad/s)
    _Scalar gain(_Scalar freq)
    {
        return std::abs(evalfr(freq));
    }
    /// Returns the phase of the transfer function at the given frequency (in rad/s)
    _Scalar phase(_Scalar freq)
    {
        return std::arg(evalfr(freq));
    }
    /// Returns the complex value of the transfer function at the given frequency (in rad/s)
    std::complex<_Scalar> evalfr(_Scalar freq)
    {
        std::complex<_Scalar> jw(0, freq);
        _FreqVec freqVec = _FreqVec::Ones();
        for (sz i = 1; i <= _ORDER; ++i) {
            for (sz j = 0; j < i; ++j) freqVec(i) *= jw;
        }

        // Note: the (0) is to convert 1x1 matrices back to the std::complex<_Scalar>
        return (numer.transpose() * freqVec)(0) / (denom.transpose() * freqVec)(0);
    }

    /// Returns the OutputJac for the given numer and denom
    static _OutputJac computeOutputJac(const _CoeffVec& denom, const _CoeffVec& numer)
    {
        return (  numer.template block<_ORDER, 1>(0,0).transpose()
                - denom.template block<_ORDER, 1>(0,0).transpose() / denom(_ORDER) * numer(_ORDER))
               / denom(_ORDER);
    }

    /// Swap
    inline void swap(TransferFunction& o)
    { denom.swap(o.denom); numer.swap(o.numer); model.swap(o.model); integ.swap(o.integ);
      C.swap(o.C); std::swap(D, o.D); derivC.swap(o.derivC); derivD.swap(o.derivD); x.swap(o.x); }
    /// Copy Ctor
    TransferFunction(const TransferFunction& o) :
        denom(o.denom), numer(o.numer), model(o.model), integ(o.integ), C(o.C), D(o.D),
        derivC(o.derivC), derivD(o.derivD), x(o.x) {}
    /// Move Ctor
    TransferFunction(TransferFunction&& o) { swap(o); }
    /// Unifying Assignment
    TransferFunction& operator=(TransferFunction o) { swap(o); return *this; }
};

template<typename Scalar, int NUM_ELTS>
static TransferFunction<Scalar, 2, NUM_ELTS> createSecondOrderLowpass(Scalar natFreq,
                                                                      Scalar damping = 0.7,
                                                                      sz integOrder = 1)
{
    return TransferFunction<Scalar, 2, NUM_ELTS>({natFreq*natFreq, 2*damping*natFreq, 1},
                                                 {natFreq*natFreq, 0, 0}, integOrder);
}

template<typename Scalar, int NUM_POLES, int NUM_ELTS>
static TransferFunction<Scalar, NUM_POLES, NUM_ELTS> createButterworthLowpass(Scalar natFreq,
                                                                              sz integOrder = 1)
{
    static_assert(NUM_POLES > 0, "Cannot construct filter with < 1 pole");
    static_assert(NUM_ELTS  > 0, "Cannot construct filter with < 1 element");

    Assert(natFreq > MathUtil::TINY, "natural frequency must be positive");

    std::vector<std::complex<Scalar>> poles;
    for (int i = 0; i < NUM_POLES / 2; ++i) {
        Scalar angle = MathUtil::PI / (2 * NUM_POLES) * (2 * (i + 1) + NUM_POLES - 1);

        Scalar real = natFreq * cos(angle);
        Scalar imag = natFreq * sin(angle);
        std::complex<Scalar> posPole {real,  imag};
        std::complex<Scalar> negPole {real, -imag};

        poles.push_back(posPole);
        poles.push_back(negPole);
    }

    if (NUM_POLES % 2 == 1) {
        std::complex<Scalar> pole {-natFreq, 0};
        poles.push_back(pole);
    }

    std::vector<std::complex<Scalar>> coeffs (NUM_POLES + 1, 0);
    coeffs[0] = -poles[0];
    coeffs[1] = 1;
    // Multiply existing coefficents by (s - pole)
    for (size_t i = 1; i < poles.size(); ++i) {
        // Shift to 1 higher order of s (ie multiply by s)
        for (size_t j = coeffs.size() - 1; j > 0; --j) {
            coeffs[j] = coeffs[j - 1];
        }
        coeffs[0] = 0;

        // Add result of the multiplication by -pole
        for (size_t j = 0; j < coeffs.size() - 1; ++j) {
            coeffs[j] += coeffs[j + 1] * - poles[i];
        }
    }

    typename TransferFunction<Scalar, NUM_POLES, NUM_ELTS>::_CoeffVec denom;
    for (size_t i = 0; i < coeffs.size(); ++i) {
        Assert(coeffs[i].imag() < MathUtil::TINY, "coefficients should be real");
        denom[i] = coeffs[i].real();
    }

    typename TransferFunction<Scalar, NUM_POLES, NUM_ELTS>::_CoeffVec numer;
    numer.setZero();
    numer[0] = denom[0];

    return TransferFunction<Scalar, NUM_POLES, NUM_ELTS>(denom, numer, integOrder);
}

} /* Control */
} /* Kestrel */
