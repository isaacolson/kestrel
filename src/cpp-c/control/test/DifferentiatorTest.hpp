#pragma once

#include <cxxtest/TestSuite.h>

#include "kestrel/util/TestUtil.hpp"

#include "Differentiator.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace TestUtil;
using namespace Control;

class DifferentiatorTest: public CxxTest::TestSuite {
  private:
    typedef Differentiator<f64, 3, 3, 4, 2> Diff;
    Diff diff;

    f64 t;

    Matrix3d x;
    Vector4d u;
    Vector2d w;

  public:
    void setUp() override
    {
        t = 0.0;
        x << 1, 2, 3, 4, 5, 6, 7, 8, 9;
        u << 1, 2, 3, 4;
        w << 1, 2;
    }
    void tearDown() override {}

    void testDiff()
    {
        Matrix3d expect = Matrix3d::Zero();
        expect.block<1, 3>(0,0)  = x.block<1, 3>(1,0);
        expect.block<1, 3>(1,0)  = x.block<1, 3>(2,0);
        expect.block<1, 3>(2,0)  = u.block<3, 1>(0,0).transpose();
        expect.block<1, 2>(2,0) += w.block<2, 1>(0,0).transpose();

        TS_ASSERT(matEquals(diff(x, t, u, w), expect));
    }

    void testDefaultInput()
    {
        u.setZero();
        w.setZero();

        Matrix3d expect = Matrix3d::Zero();
        expect.block<1, 3>(0,0)  = x.block<1, 3>(1,0);
        expect.block<1, 3>(1,0)  = x.block<1, 3>(2,0);
        expect.block<1, 3>(2,0)  = u.block<3, 1>(0,0).transpose();
        expect.block<1, 2>(2,0) += w.block<2, 1>(0,0).transpose();

        TS_ASSERT(matEquals(diff(x, t), expect));
    }

    void testZeroDims()
    {
        Differentiator<f64, 3, 3, 0, 0> zdimDiff;

        Matrix3d expect = Matrix3d::Zero();
        expect.block<1, 3>(0,0)  = x.block<1, 3>(1,0);
        expect.block<1, 3>(1,0)  = x.block<1, 3>(2,0);

        TS_ASSERT(matEquals(zdimDiff(x, t), expect));
        TS_ASSERT(matEquals(zdimDiff(x, t), diff(x, t)));
    }
};
