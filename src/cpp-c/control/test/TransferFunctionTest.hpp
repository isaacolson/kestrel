#pragma once

#include <cxxtest/TestSuite.h>

#include <iostream>

#include "kestrel/util/TestUtil.hpp"

#include "TransferFunction.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace TestUtil;
using namespace Control;

class TransferFunctionTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testMatrices()
    {
        {
            constexpr int ORDER = 1;
            Matrix<f64, ORDER+1, 1> denom; denom << 2, 1;
            Matrix<f64, ORDER+1, 1> numer; numer << 1, 0;
            TransferFunction<f64, ORDER, 1> tf(denom, numer);
            Matrix<f64, ORDER, ORDER> A = tf.stateJac();
            Matrix<f64, ORDER,     1> B = tf.inputJac();
            Matrix<f64, ORDER,     1> C = tf.outputJac();
            f64                       D = tf.feedForward();

            Matrix<f64, ORDER, ORDER> Aexp; Aexp << -2;
            Matrix<f64, ORDER,     1> Bexp; Bexp << 1;
            Matrix<f64, ORDER,     1> Cexp; Cexp << 1;
            f64                       Dexp        = 0;

            TS_ASSERT(matEquals(A, Aexp));
            TS_ASSERT(matEquals(B, Bexp));
            TS_ASSERT(matEquals(C, Cexp));
            TS_ASSERT_EQUALS(D, Dexp);
        }

        {
            constexpr int ORDER = 2;
            Matrix<f64, ORDER+1, 1> denom; denom << 1, 2, 3;
            Matrix<f64, ORDER+1, 1> numer; numer << 3, 1, 2;
            TransferFunction<f64, ORDER, 1> tf(denom, numer);
            Matrix<f64, ORDER, ORDER> A = tf.stateJac();
            Matrix<f64, ORDER,     1> B = tf.inputJac();
            Matrix<f64, ORDER,     1> C = tf.outputJac();
            f64                       D = tf.feedForward();

            Matrix<f64, ORDER, ORDER> Aexp; Aexp << 0, 1, -1/3.0, -2/3.0;
            Matrix<f64, ORDER,     1> Bexp; Bexp << 0, 1;
            Matrix<f64, ORDER,     1> Cexp; Cexp << (3 - 1/3.0*2)/3.0, (1 - 2/3.0*2)/3.0;
            f64                       Dexp        = 2/3.0;

            TS_ASSERT(matEquals(A, Aexp));
            TS_ASSERT(matEquals(B, Bexp));
            TS_ASSERT(matEquals(C, Cexp));
            TS_ASSERT_EQUALS(D, Dexp);
        }

        {
            constexpr int ORDER = 3;
            Matrix<f64, ORDER+1, 1> denom; denom << 4, 3, 2, 1;
            Matrix<f64, ORDER+1, 1> numer; numer << 1, 2, 3, 4;
            TransferFunction<f64, ORDER, 1> tf(denom, numer);
            Matrix<f64, ORDER, ORDER> A = tf.stateJac();
            Matrix<f64, ORDER,     1> B = tf.inputJac();
            Matrix<f64, ORDER,     1> C = tf.outputJac();
            f64                       D = tf.feedForward();

            Matrix<f64, ORDER, ORDER> Aexp; Aexp << 0, 1, 0,
                                                    0, 0, 1,
                                                  -4, -3, -2;
            Matrix<f64, ORDER,     1> Bexp; Bexp << 0, 0, 1;
            Matrix<f64, ORDER,     1> Cexp; Cexp << 1 - 4*4, 2 - 3*4, 3-2*4;
            f64                       Dexp        = 4;

            TS_ASSERT(matEquals(A, Aexp));
            TS_ASSERT(matEquals(B, Bexp));
            TS_ASSERT(matEquals(C, Cexp));
            TS_ASSERT_EQUALS(D, Dexp);
        }
    }

    void testFirstOrderLowPass()
    {
        f64 gain = 0.5;
        TransferFunction<f64, 1, 1> tf({1/gain, 1/gain}, {1, 0}, 4);

        f64 dt = 0.01;
        for (f64 t = 0; t <= 4; t += dt) {
            f64 y = tf(1, dt);

            // approx values (if gain was 1)
            // t = 1     y ~ 0.63
            // t = 2     y ~ 0.86
            // t = 3     y ~ 0.95

            if        (t < 1) {
                TS_ASSERT_LESS_THAN_EQUALS(0.00*gain, y);
                TS_ASSERT_LESS_THAN_EQUALS(y, 0.65*gain);
            } else if (t < 2) {
                TS_ASSERT_LESS_THAN_EQUALS(0.60*gain, y);
                TS_ASSERT_LESS_THAN_EQUALS(y, 0.89*gain);
            } else if (t < 3) {
                TS_ASSERT_LESS_THAN_EQUALS(0.84*gain, y);
                TS_ASSERT_LESS_THAN_EQUALS(y, 0.98*gain);
            } else {// t < 4
                TS_ASSERT_LESS_THAN_EQUALS(0.93*gain, y);
                TS_ASSERT_LESS_THAN_EQUALS(y, 1.00*gain);
            }
        }
    }

    void testSecondOrderLowPass()
    {
        auto tf = createSecondOrderLowpass<f64, 2>(1, 0.5, 4);

        f64 dt = 0.01;
        for (f64 t = 0; t <= 10; t += dt) {
            auto y = tf({1, 2}, dt);

            // approx values:
            // t =  1    y ~ 0.3403
            // t =  2    y ~ 0.8494
            // t =  3    y ~ 1.1244
            // t =  4    y ~ 1.1531
            // t =  5    y ~ 1.0746
            // t =  6    y ~ 1.0023
            // t =  7    y ~ 0.9744
            // t =  8    y ~ 0.9790
            // t =  9    y ~ 0.9929
            // t = 10    y ~ 1.0022

            if        (t <  1) {
                TS_ASSERT_LESS_THAN_EQUALS(0.00, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 0.37);
            } else if (t <  2) {
                TS_ASSERT_LESS_THAN_EQUALS(0.31, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 0.88);
            } else if (t <  3) {
                TS_ASSERT_LESS_THAN_EQUALS(0.82, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.15);
            } else if (t <  4) {
                TS_ASSERT_LESS_THAN_EQUALS(1.09, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.18);
            } else if (t <  5) {
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.18);
                TS_ASSERT_LESS_THAN_EQUALS(1.04, y(0));
            } else if (t <  6) {
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.10);
                TS_ASSERT_LESS_THAN_EQUALS(0.97, y(0));
            } else if (t <  7) {
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.03);
                TS_ASSERT_LESS_THAN_EQUALS(0.94, y(0));
            } else if (t <  8) {
                TS_ASSERT_LESS_THAN_EQUALS(0.94, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.01);
            } else if (t <  9) {
                TS_ASSERT_LESS_THAN_EQUALS(0.95, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.03);
            } else {// t < 10
                TS_ASSERT_LESS_THAN_EQUALS(0.97, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.03);
            }

            TS_ASSERT_DELTA(y(0), y(1)/2, 0.02);
        }
    }

    void testSecondOrderHighPass()
    {
        f64 omega = 1;
        f64  zeta = std::sqrt(2);

        TransferFunction<f64, 2, 1> tf({2*omega*omega, 2*2*zeta*omega, 2}, {0, 0, 2}, 4);

        f64 dt = 0.001;
        for (f64 t = 0; t <= 10; t += dt) {
            f64 yHf = sin(50*t) + cos(40*t);
            f64 yLf = sin(0.1*t) + 3;
            f64 y = tf(yHf + yLf, dt);

            if (t > 5) {
                TS_ASSERT_DELTA(y, yHf, 0.2);
            }
        }
    }

    void testInitReset()
    {
        {
            TransferFunction<f64, 2, 1> tf({1, 1, 1}, {1, 0, 0}, 4);

            f64 y = tf.initialize(2);
            TS_ASSERT_DELTA(y, 2, 0.01);
            y = tf(2, 0.1);
            TS_ASSERT_DELTA(y, 2, 0.01);

            tf.reset();
            TS_ASSERT_DELTA(tf(0, 0.01), 0, MathUtil::TINY);
        }

        {
            TransferFunction<f64, 2, 2> tf({1, 1, 1}, {0, 0, 1}, 4);

            Matrix<f64, 2, 1> y = tf.initialize({3, 1});
            TS_ASSERT_DELTA(y(0), 0, 0.01);
            TS_ASSERT_DELTA(y(1), 0, 0.01);
            y = tf({3, 1}, 0.1);
            TS_ASSERT_DELTA(y(0), 0, 0.01);
            TS_ASSERT_DELTA(y(1), 0, 0.01);
        }
    }

    void testBode()
    {
        {
            TransferFunction<f64, 1, 1> tf({1, 1}, {1, 0}, 4);
            complex<f64> v;

            v = tf.evalfr(0);
            TS_ASSERT_DELTA(v.real(), 1, 0.01);
            TS_ASSERT_DELTA(v.imag(), 0, 0.01);
            TS_ASSERT_DELTA(  abs(v), 1, 0.01);
            TS_ASSERT_DELTA(  arg(v), 0, 0.01);

            v = tf.evalfr(2);
            TS_ASSERT_DELTA(v.real(),  0.20, 0.01);
            TS_ASSERT_DELTA(v.imag(), -0.40, 0.01);
            TS_ASSERT_DELTA(  abs(v),  0.45, 0.01);
            TS_ASSERT_DELTA(  arg(v), -1.11, 0.01);
        }

        {
            TransferFunction<f64, 2, 2> tf({1, 2, 1}, {2, 0, 0}, 4);
            complex<f64> v;

            v = tf.evalfr(0);
            TS_ASSERT_DELTA(v.real(), 2, 0.01);
            TS_ASSERT_DELTA(v.imag(), 0, 0.01);
            TS_ASSERT_DELTA(  abs(v), 2, 0.01);
            TS_ASSERT_DELTA(  arg(v), 0, 0.01);

            v = tf.evalfr(2);
            TS_ASSERT_DELTA(v.real(), -0.24, 0.01);
            TS_ASSERT_DELTA(v.imag(), -0.32, 0.01);
            TS_ASSERT_DELTA(  abs(v),  0.40, 0.01);
            TS_ASSERT_DELTA(  arg(v), -2.21, 0.01);
        }

        {
            TransferFunction<f64, 2, 1> tf({1, 2, 1}, {0, 0, 1}, 4);
            complex<f64> v;

            v = tf.evalfr(0);
            TS_ASSERT_DELTA(v.real(), 0, 0.01);
            TS_ASSERT_DELTA(v.imag(), 0, 0.01);
            TS_ASSERT_DELTA(  abs(v), 0, 0.01);
            TS_ASSERT_DELTA(  arg(v), 0, 0.01);

            v = tf.evalfr(2);
            TS_ASSERT_DELTA(v.real(), 0.48, 0.01);
            TS_ASSERT_DELTA(v.imag(), 0.64, 0.01);
            TS_ASSERT_DELTA(  abs(v), 0.80, 0.01);
            TS_ASSERT_DELTA(  arg(v), 0.93, 0.01);
        }

        {
            TransferFunction<f64, 2, 1> tf({1, 2, 1}, {0, 2, 0}, 4);
            complex<f64> v;

            v = tf.evalfr(0);
            TS_ASSERT_DELTA(v.real(), 0, 0.01);
            TS_ASSERT_DELTA(v.imag(), 0, 0.01);
            TS_ASSERT_DELTA(  abs(v), 0, 0.01);
            TS_ASSERT_DELTA(  arg(v), 0, 0.01);

            v = tf.evalfr(2);
            TS_ASSERT_DELTA(v.real(),  0.64, 0.01);
            TS_ASSERT_DELTA(v.imag(), -0.48, 0.01);
            TS_ASSERT_DELTA(  abs(v),  0.80, 0.01);
            TS_ASSERT_DELTA(  arg(v), -0.64, 0.01);
        }
    }

    void testDeriv()
    {
        {
            f64 w = 1, z = 0.5;
            TransferFunction<f64, 2, 2>   tf({w*w, 2*z*w, 1}, {w*w, 0, 0}, 4);
            TransferFunction<f64, 2, 2>  dtf({w*w, 2*z*w, 1}, {0, w*w, 0}, 4);
            TransferFunction<f64, 2, 2> ddtf({w*w, 2*z*w, 1}, {0, 0, w*w}, 4);

            f64 dt = 0.01;
            for (f64 t = 0; t <= 10; t += dt) {
                Matrix<f64, 2, 1> u; u << 1, 2;
                tf(u, dt);
                auto     dy =   tf.deriv(1, u);
                auto    ddy =   tf.deriv(2, u);
                auto  dyExp =  dtf(u, dt);
                auto ddyExp = ddtf(u, dt);

                TS_ASSERT(matEquals( dy,  dyExp));
                TS_ASSERT(matEquals(ddy, ddyExp));
            }
        }

        {
            f64 w = 1, z = 0.5;
            TransferFunction<f64, 2, 1>   tf({w*w, 2*z*w, 1}, {w*w, 2*z*w, 0}, 4);
            TransferFunction<f64, 2, 1>  dtf({w*w, 2*z*w, 1}, {0, w*w, 2*z*w}, 4);

            f64 dt = 0.01;
            for (f64 t = 0; t <= 10; t += dt) {
                f64 u = 1;
                tf(u, dt);
                f64    dy =  tf.deriv(1, u);
                f64 dyExp = dtf(u, dt);

                TS_ASSERT_DELTA(dy,  dyExp, 0.001);
            }
        }
    }

    void testButterworth()
    {
        auto tf = createButterworthLowpass<f64, 3, 2>(1, 4);

        f64 dt = 0.01;
        for (f64 t = 0; t <= 10; t += dt) {
            auto y = tf({1, 2}, dt);

            // approx values:
            // t =  1    y ~ 0.09
            // t =  2    y ~ 0.44
            // t =  3    y ~ 0.81
            // t =  4    y ~ 1.02
            // t =  5    y ~ 1.08 (note this is basically the maximum)
            // t =  6    y ~ 1.05
            // t =  7    y ~ 1.00
            // t =  8    y ~ 0.98
            // t =  9    y ~ 0.98
            // t = 10    y ~ 0.99

            if        (t <  1) {
                TS_ASSERT_LESS_THAN_EQUALS(0.00, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 0.11);
            } else if (t <  2) {
                TS_ASSERT_LESS_THAN_EQUALS(0.07, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 0.46);
            } else if (t <  3) {
                TS_ASSERT_LESS_THAN_EQUALS(0.42, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 0.83);
            } else if (t <  4) {
                TS_ASSERT_LESS_THAN_EQUALS(0.79, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.04);
            } else if (t <  5) {
                TS_ASSERT_LESS_THAN_EQUALS(1.00, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.10);
            } else if (t <  6) {
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.10);
                TS_ASSERT_LESS_THAN_EQUALS(1.03, y(0));
            } else if (t <  7) {
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.07);
                TS_ASSERT_LESS_THAN_EQUALS(0.98, y(0));
            } else if (t <  8) {
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.02);
                TS_ASSERT_LESS_THAN_EQUALS(0.96, y(0));
            } else if (t <  9) {
                TS_ASSERT_LESS_THAN_EQUALS(0.96, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.00);
            } else {// t < 10
                TS_ASSERT_LESS_THAN_EQUALS(0.97, y(0));
                TS_ASSERT_LESS_THAN_EQUALS(y(0), 1.01);
            }

            TS_ASSERT_DELTA(y(0), y(1)/2, 0.02);
        }
    }
};
