#pragma once

#include <type_traits>
#include <cxxtest/TestSuite.h>

#include <Eigen/Dense>

#include "kestrel/util/TestUtil.hpp"

#include "LinearizingObserver.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace TestUtil;
using namespace Control;

// This class is only to test if it properly inherits const statics and typenames
class Child: public LinearizingObserver<f64, 3, 1, 2> {
  public:
    typedef LinearizingObserver<f64, 3, 1, 2> ParentType;

  private:
    /// Returns the updated state given the discrete observation
    using ParentType::operator();
    /// Returns the expected observation given the observand
    using ParentType::estObs;
    /// Returns the Jacobian of the estimated observation with respect to the state
    using ParentType::stateJac;

  public:
    /// Ctor
    Child(/* arguments */) {}
    /// Dtor
    virtual ~Child() {}

    /// Swap
    inline void swap(Child& o) { ParentType::swap(o); }
    /// Copy Ctor
    Child(const Child& o) : ParentType(o) {}
    /// Move Ctor
    Child(Child&& o) { swap(o); }
    /// Unifying Assignment
    Child& operator=(Child o) { swap(o); return *this; }
};

class LinearizingObserverTest: public CxxTest::TestSuite {
  private:
    typedef LinearizingObserver<f64, 3, 2, 3> Obs;
    Obs obs;

    f64 t;
    Matrix<f64, 3, 2> x;
    Matrix<f64, 3, 1> y;

  public:
    void setUp() override
    {
        t = 0.0;
        x << 1, 2, 3, 4, 5, 6;
        y << 2, 4, 6;
    }
    void tearDown() override {}

    void testEstObs()
    {
        Obs::_ObsVec yEst = obs.estObs(x, t);
        Obs::_ObsVec yExp; yExp << x(0,0), x(0,1), 0;
        TS_ASSERT(matEquals(yEst, yExp));
    }

    void testObs()
    {
        Obs::_Observand x2 = obs(x, t, y);

        Matrix<f64, 3, 3> L1; L1 << 1,    0, 0,
                                    0.5,  0, 0,
                                    0.25, 0, 0;
        Matrix<f64, 1, 2> S1; S1 << 1, 0;
        Matrix<f64, 3, 3> L2; L2 << 0, 1,    0,
                                    0, 0.5,  0,
                                    0, 0.25, 0;
        Matrix<f64, 1, 2> S2; S2 << 0, 1;
        Matrix<f64, 3, 2> x2exp = x + L1 * (y - obs.estObs(x, t)) * S1
                                    + L2 * (y - obs.estObs(x, t)) * S2;

        TS_ASSERT(matEquals(x2, x2exp));
    }

    void testJac()
    {
        Obs::_StateJac H = obs.stateJac(x, t);
        Obs::_StateJac Hexp; Hexp << 1, 0, 0, 0, 0, 0,
                                    0, 0, 0, 1, 0, 0,
                                    0, 0, 0, 0, 0, 0;

        TS_ASSERT(matEquals(H, Hexp));
    }

    void testInheritance()
    {
        bool val = is_same<Child::_Scalar, f64>::value;
        TS_ASSERT(val);
        val = is_same<Child::_Observand, Matrix<f64, 3, 1>>::value;
        TS_ASSERT(val);
        val = is_same<Child::_ObsVec, Matrix<f64, 2, 1>>::value;
        TS_ASSERT(val);
        val = is_same<Child::_StateVec, Matrix<f64, 3, 1>>::value;
        TS_ASSERT(val);
        val = is_same<Child::_StateJac, Matrix<f64, 2, 3>>::value;
        TS_ASSERT(val);

        TS_ASSERT_EQUALS(Child::_ROWS, 3);
        TS_ASSERT_EQUALS(Child::_COLS, 1);
        TS_ASSERT_EQUALS(Child::_NUM_OBS, 2);

    }
};
