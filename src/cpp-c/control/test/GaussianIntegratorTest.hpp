#pragma once

#include <iostream>
#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/EigenUtil.hpp"
#include "kestrel/util/TestUtil.hpp"

#include "GaussianDifferentiator.hpp"
#include "GaussianIntegrator.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace EigenUtil;
using namespace TestUtil;
using namespace Control;

class GaussianIntegratorTest: public CxxTest::TestSuite {
  private:
    const static int n = 3;
    const static int m = 2;
    const static int p = 3;
    const static int q = 1;
    Matrix<f64, n, m>     x;
    Matrix<f64, n*m, n*m> P;
    Matrix<f64, p, 1>     u;
    Matrix<f64, q, 1>     w;

    f64 t = 0;
    f64 dt = 0.1;

    GaussianDifferentiator<f64, n, m, p, q> diff;
    GaussianIntegrator<f64, n, m> integ;

  public:
    void setUp() override
    {
        x.setRandom();
        P.setIdentity();
        u.setRandom();
        w.setRandom();

        integ = GaussianIntegrator<f64, n, m>(1);
    }
    void tearDown() override {}

    void testIntegrate()
    {
        auto expect = integ(x, t, dt, diff, u, w);
        auto dist   = integ(x, P, t, dt, diff, u, w);

        TS_ASSERT(matEquals(expect, dist.first));
        TS_ASSERT(matEquals(dist.second, forceSym(dist.second)));
        TS_ASSERT_LESS_THAN(P.determinant(), dist.second.determinant());
    }
};
