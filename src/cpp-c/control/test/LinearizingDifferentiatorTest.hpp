#pragma once

#include <cxxtest/TestSuite.h>

#include "kestrel/util/TestUtil.hpp"

#include "LinearizingDifferentiator.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace Control;
using namespace TestUtil;

class LinearizingDifferentiatorTest: public CxxTest::TestSuite {
  private:
    typedef LinearizingDifferentiator<f64, 3, 3, 4, 2> Diff;
    Diff diff;

    f64 t;

    Matrix3d x;
    Vector4d u;
    Vector2d w;

  public:
    void setUp() override
    {
        t = 0.0;
        x << 1, 2, 3, 4, 5, 6, 7, 8, 9;
        u << 1, 2, 3, 4;
        w << 1, 2;
    }
    void tearDown() override {}

    void testDiff()
    {
        Matrix3d expect;
        expect.block<1, 3>(0,0)  = x.block<1, 3>(1,0);
        expect.block<1, 3>(1,0)  = x.block<1, 3>(2,0);
        expect.block<1, 3>(2,0)  = u.block<3, 1>(0,0).transpose();
        expect.block<1, 2>(2,0) += w.block<2, 1>(0,0).transpose();

        TS_ASSERT(matEquals(diff(x, t, u, w), expect));
    }

    void testJac()
    {
        Diff::_StateJac dx = diff.stateJac(x, t, u, w);
        Diff::_InputJac du = diff.inputJac(x, t, u, w);
        Diff::_NoiseJac dw = diff.noiseJac(x, t, u, w);

        // The default differentiator is linear with all inputs
        Matrix<f64, 3*3, 1> xVec(x.data());
        Matrix<f64, 3*3, 1> xDotVec = dx * xVec;
        xDotVec += du * u;
        xDotVec += dw * w;

        Matrix<f64, 3, 3> xDot(xDotVec.data());
        TS_ASSERT(matEquals(diff(x, t, u, w), xDot));
    }
};
