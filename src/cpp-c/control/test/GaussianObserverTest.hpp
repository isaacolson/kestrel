#pragma once

#include <iostream>
#include <sstream>
#include <cxxtest/TestSuite.h>

#include <Eigen/Dense>

#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/EigenUtil.hpp"
#include "kestrel/util/TestUtil.hpp"

#include "GaussianDifferentiator.hpp"
#include "GaussianIntegrator.hpp"
#include "GaussianObserver.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace MathUtil;
using namespace EigenUtil;
using namespace TestUtil;
using namespace Control;

class GaussianObserverTest: public CxxTest::TestSuite {
  private:
    typedef GaussianObserver<f64, 3, 2, 3> Obs;
    Obs obs;

    f64 t;
    f64 dt;
    Obs::_Observand x;
    Obs::_StateVec  xVec;
    Obs::_StateCov  P;
    Obs::_ObsVec    y;
    Obs::_ObsCov    R;

  public:
    void setUp() override
    {
        t = 0.0;
        dt = 0.1;
        x    << 1, 2, 3, 4, 5, 6;
        xVec << 1, 3, 5, 2, 4, 6; // col major ordering of x
        P.setIdentity();
        y    << 2, 4, 6;
        R.setIdentity();
    }
    void tearDown() override {}

    void testEstObs()
    {
        Obs::_ObsVec yEst = obs.estObs(x, t);
        Obs::_ObsVec yExp; yExp << x(0,0), x(0,1), 0;
        TS_ASSERT(matEquals(yEst, yExp));
    }

    void testJac()
    {
        Obs::_StateJac H = obs.stateJac(x, t);
        Obs::_StateJac Hexp; Hexp << 1, 0, 0, 0, 0, 0,
                                    0, 0, 0, 1, 0, 0,
                                    0, 0, 0, 0, 0, 0;

        TS_ASSERT(matEquals(H, Hexp));
    }

    void testScalar()
    {
        GaussianObserver<f64, 1, 1, 1> o;

        typedef Eigen::Matrix<f64, 1, 1> OneByOne;

        OneByOne x = OneByOne::Constant(1);
        OneByOne P = OneByOne::Constant(1);

        OneByOne y = OneByOne::Constant(2);
        OneByOne R = OneByOne::Constant(1);
        OneByOne K = P * (P + R).inverse();

        OneByOne xExp = x + K*(y-x);
        OneByOne PExp = (OneByOne::Identity() - K) * P;
        decltype(o)::_StateDist res = o(x, P, t, y, R);

        TS_ASSERT(matEquals(res.first,  xExp));
        TS_ASSERT(matEquals(res.second, PExp));

        y(0) = 1.5;
        R(0) = 0.7;
        K    = P * (P + R).inverse();
        xExp = x + K*(y-x);
        PExp = (OneByOne::Identity() - K) * P;
        res  = o(x, P, t, y, R);

        TS_ASSERT(matEquals(res.first,  xExp));
        TS_ASSERT(matEquals(res.second, PExp));

        y    = o.estObs(x, t);
        R(0) = 1;
        K    = P * (P + R).inverse();
        xExp = x;
        PExp = (OneByOne::Identity() - K) * P;
        res  = o(x, P, t, y, R);

        TS_ASSERT(matEquals(res.first,  xExp));
        TS_ASSERT(matEquals(res.second, PExp));
    }

    void testObsNoCross()
    {
        // Should only update first row because no cross covariance
        Obs::_Observand xExp = x;
        xExp.block<1, 2>(0, 0) += 0.5*(y.block<2, 1>(0, 0).transpose() - x.block<1, 2>(0, 0));
        Obs::_StateCov  PExp = P;
        PExp(0,0) /= 2;
        PExp(3,3) /= 2;
        auto res = obs(x, P, t, y, R);

        TS_ASSERT(matEquals(res.first,  xExp));
        TS_ASSERT(matEquals(res.second, PExp));
    }

    void testObsCross()
    {
        // Use default derivative model that integrates up rows
        typedef GaussianDifferentiator<Obs::_Scalar, Obs::_ROWS, Obs::_COLS> Diff;
        typedef GaussianIntegrator    <Obs::_Scalar, Obs::_ROWS, Obs::_COLS> Int;

        Diff d;
        Int  i;

        for (int j = 0; j < 3; ++j) {
            auto tmp = i(x, P, t, dt, d);
            P = tmp.second;
        }

        // We should have positive cross terms due to the integration, so correcting positively
        // will increase lower order terms. Covariance should be symmetric
        auto res = obs(x, P, t, y, R);

        for (int j = 0; j < 6; ++j) {
            stringstream ss; ss << "j=" << j;
            TSM_ASSERT_LESS_THAN(ss.str().c_str(), x(j), res.first(j));
        }
        TS_ASSERT_LESS_THAN(res.second.determinant(), P.determinant());
        TS_ASSERT(matEquals(res.second - forceSym(res.second), Obs::_StateCov::Zero()));

        // actually work through the math
        Obs::_ObsVec     h = obs.estObs(x, t);
        Obs::_StateJac   H = obs.stateJac(x, t);
        Obs::_ObsCov     S = H * P * H.transpose() + R;
        Obs::_KalmanGain K = P * H.transpose() * S.inverse();

        Obs::_Observand  xExp;
        Obs::_StateVec   xExpVec = xVec + K*(y - h);
        for (int j = 0; j < Obs::_COLS; ++j) {
            for (int k = 0; k < Obs::_ROWS; ++k) {
                xExp(k, j) = xExpVec(k + Obs::_ROWS*j);
            }
        }
        TS_ASSERT(matEquals(res.first, xExp));

        Obs::_StateCov PExp = (Obs::_StateCov::Identity() - K*H) * P;
        TS_ASSERT(matEquals(res.second, PExp));
    }

    void testCovFix()
    {
        P(3,3) = MathUtil::TINY;
        R(1,1) = MathUtil::TINY;
        R(2,2) = MathUtil::TINY;

        obs.prints = 0;
        auto res = obs(x, P, t, y, R);

        Eigen::JacobiSVD<Obs::_StateCov> Psvd(res.second);
        Obs::_StateVec Psv = Psvd.singularValues();
        TS_ASSERT_LESS_THAN_EQUALS(MathUtil::TINY, Psv(Obs::_NUM_STATES-1));
    }
};
