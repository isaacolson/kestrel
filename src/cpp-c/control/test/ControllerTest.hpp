#pragma once

#include <iostream>
#include <Eigen/Dense>
#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TestUtil.hpp"

#include "Controller.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace MathUtil;
using namespace TestUtil;
using namespace Control;

class ControllerTest: public CxxTest::TestSuite {
  private:
    typedef Controller<f64, 3, 2, 2, 3> Ctrl;
    Ctrl ctrl;

    f64 t;
    f64 dt;
    Matrix<f64, 3, 2> x;
    Matrix<f64, 3, 1> r;

  public:
    void setUp() override
    {
        t  = 0.0;
        dt = 0.1;
        x << 1, 2, 3, 4, 5, 6;
        r << 2, 4, 6;
    }
    void tearDown() override {}

    void testZeroSized()
    {
        Controller<f64, 3, 2, 2, 0> ctrl;
        Matrix<f64, 2, 1> uExp; uExp << -x(0,0), -x(0,1);
        TS_ASSERT(matEquals(ctrl(x, t, dt), uExp));
    }

    void testControl()
    {
        Matrix<f64, 2, 1> uExp; uExp << r(0) - x(0,0), r(1) - x(0,1);
        TS_ASSERT(matEquals(ctrl(x, t, dt, r), uExp));
    }
};
