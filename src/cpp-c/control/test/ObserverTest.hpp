#pragma once

#include <cxxtest/TestSuite.h>

#include "kestrel/util/TestUtil.hpp"

#include "Observer.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace TestUtil;
using namespace Control;

class ObserverTest: public CxxTest::TestSuite {
  private:
    typedef Observer<f64, 3, 2, 3> Obs;
    Obs obs;

    f64 t;
    Matrix<f64, 3, 2> x;
    Matrix<f64, 3, 1> y;

  public:
    void setUp() override
    {
        t = 0.0;
        x << 1, 2, 3, 4, 5, 6;
        y << 2, 4, 6;
    }
    void tearDown() override {}

    void testEstObs()
    {
        Obs::_ObsVec yEst = obs.estObs(x, t);
        Obs::_ObsVec yExp; yExp << x(0,0), x(0,1), 0;
        TS_ASSERT(matEquals(yEst, yExp));
    }

    void testObs()
    {
        Obs::_Observand x2 = obs(x, t, y);

        Matrix<f64, 3, 3> L1; L1 << 1,    0, 0,
                                    0.5,  0, 0,
                                    0.25, 0, 0;
        Matrix<f64, 1, 2> S1; S1 << 1, 0;
        Matrix<f64, 3, 3> L2; L2 << 0, 1,    0,
                                    0, 0.5,  0,
                                    0, 0.25, 0;
        Matrix<f64, 1, 2> S2; S2 << 0, 1;
        Matrix<f64, 3, 2> x2exp = x + L1 * (y - obs.estObs(x, t)) * S1
                                    + L2 * (y - obs.estObs(x, t)) * S2;

        TS_ASSERT(matEquals(x2, x2exp));
    }
};
