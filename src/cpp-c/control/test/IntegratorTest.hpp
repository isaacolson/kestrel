#pragma once

#include <iostream>
#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/TestUtil.hpp"

#include "Integrator.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace TestUtil;
using namespace Control;

class CopyTrackerMatrix
{
  public:
    Matrix<f64, 3, 1> m;
    bool copied;

  public:
    /// Ctor
    CopyTrackerMatrix(const Matrix<f64, 3, 1> &m = Matrix<f64, 3, 1>::Zero()) :
        m(m), copied(false) {}
    /// Dtor
    virtual ~CopyTrackerMatrix() {}

    /// Swap
    void swap(CopyTrackerMatrix& o) { m.swap(o.m); std::swap(copied, o.copied); }
    /// Copy Ctor
    CopyTrackerMatrix(const CopyTrackerMatrix& o) : m(o.m), copied(true) {}
    /// Move Ctor
    CopyTrackerMatrix(CopyTrackerMatrix&& o) { swap(o); }
    /// Unifying Assignment
    CopyTrackerMatrix& operator=(CopyTrackerMatrix o) { swap(o); return *this; }
};

class ScalarDeriv: public Differentiator<f64, 3, 1, 1>
{
  public:
    typedef Differentiator<f64, 3, 1, 1> ParentType;

  public:
    /// Ctor
    ScalarDeriv() {}
    /// Dtor
    virtual ~ScalarDeriv() {}

    /// Returns the derivative of the state given the input and noise vectors
    virtual _Derivand operator()(const _Derivand& x, f64 t,
                                 const _InputVec& u = _InputVec::Zero(),
                                 const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        return operator()(x, t, u(0));
    }

    /// Returns the derivative of the state given the input and noise vectors
    virtual _Derivand operator()(const _Derivand& x, f64 t, f64 u = 0) const
    {
        _Derivand ret = x;
        ret.block(0, 0, 3-1, 1) = x.block(1, 0, 3-1, 1);
        ret(3-1) = u;
        return ret;
    }

    /// Swap
    inline void swap(ScalarDeriv& o) { ParentType::swap(o); }
    /// Copy Ctor
    ScalarDeriv(const ScalarDeriv& o) : ParentType(o) {}
    /// Move Ctor
    ScalarDeriv(ScalarDeriv&& o) { swap(o); }
    /// Unifying Assignment
    ScalarDeriv& operator=(ScalarDeriv o) { swap(o); return *this; }
};

class MatrixDeriv: public Differentiator<f64, 3, 1, 3>
{
  public:
    typedef Differentiator<f64, 3, 1, 3> ParentType;

  public:
    /// Ctor
    MatrixDeriv() {}
    /// Dtor
    virtual ~MatrixDeriv() {}

    /// Returns the derivative of the state given the input and noise vectors
    virtual _Derivand operator()(const _Derivand& x, f64 t,
                                 const _InputVec& u = _InputVec::Zero(),
                                 const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        _Derivand ret = x;
        ret.block(0, 0, 3-1, 1) = x.block(1, 0, 3-1, 1);
        return ret + u;
    }

    /// Swap
    inline void swap(MatrixDeriv& o) { ParentType::swap(o); }
    /// Copy Ctor
    MatrixDeriv(const MatrixDeriv& o) : ParentType(o) {}
    /// Move Ctor
    MatrixDeriv(MatrixDeriv&& o) { swap(o); }
    /// Unifying Assignment
    MatrixDeriv& operator=(MatrixDeriv o) { swap(o); return *this; }
};

class CopylessDeriv: public MatrixDeriv
{
  public:
    typedef MatrixDeriv ParentType;

  public:
    /// Ctor
    CopylessDeriv() {}
    /// Dtor
    virtual ~CopylessDeriv() {}

    /// Returns the derivative of the state given the input and noise vectors
    virtual _Derivand operator()(const _Derivand& x, f64 t,
                                 const _InputVec& u = _InputVec::Zero(),
                                 const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        CopyTrackerMatrix uNoCopy(u);
        return operator()(x, t, uNoCopy);
    }

    virtual _Derivand operator()(const _Derivand& x, f64 t,
                                 const CopyTrackerMatrix& u) const
    {
        TS_ASSERT(!u.copied);
        return ParentType::operator()(x, t, u.m);
    }

    /// Swap
    inline void swap(CopylessDeriv& o) { ParentType::swap(o); }
    /// Copy Ctor
    CopylessDeriv(const CopylessDeriv& o) : ParentType(o) {}
    /// Move Ctor
    CopylessDeriv(CopylessDeriv&& o) { swap(o); }
    /// Unifying Assignment
    CopylessDeriv& operator=(CopylessDeriv o) { swap(o); return *this; }
};

class Deriv3by3: public Differentiator<f64, 3, 3>
{
  public:
    typedef Differentiator<f64, 3, 3> ParentType;

  public:
    /// Ctor
    Deriv3by3() {}
    /// Dtor
    virtual ~Deriv3by3() {}

    /// Returns the derivative of the state given the input and noise vectors
    virtual _Derivand operator()(const _Derivand& x, f64 t,
                                 const _InputVec& u = _InputVec::Zero(),
                                 const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        return -0.5 * x;
    }

    /// Swap
    inline void swap(Deriv3by3& o) { ParentType::swap(o); }
    /// Copy Ctor
    Deriv3by3(const Deriv3by3& o) : ParentType(o) {}
    /// Move Ctor
    Deriv3by3(Deriv3by3&& o) { swap(o); }
    /// Unifying Assignment
    Deriv3by3& operator=(Deriv3by3 o) { swap(o); return *this; }
};

template <typename Scalar, int ROWS, int COLS=1, int NUM_INPUTS=0, int NUM_NOISES=0>
class FakeDiffer
{
  public:
    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS*COLS;
    static constexpr int _NUM_INPUTS = NUM_INPUTS;
    static constexpr int _NUM_NOISES = NUM_NOISES;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    // Note: I know this isn't a real word, needed a word like Integrand but for derivatives
    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Derivand;

    // Note: _StateVec formed by stacking columns of _Derivand (ie column major)
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, 1>           _StateVec;
    typedef Eigen::Matrix<_Scalar, _NUM_INPUTS, 1>           _InputVec;
    typedef Eigen::Matrix<_Scalar, _NUM_NOISES, 1>           _NoiseVec;

    /// Ctor
    FakeDiffer()
    {
        static_assert(ROWS > 0, "ROWS must be > 0");
        static_assert(COLS > 0, "COLS must be > 0");
        static_assert(NUM_INPUTS >= 0, "NUM_INPUTS must be >= 0");
        static_assert(NUM_NOISES >= 0, "NUM_NOISES must be >= 0");
    }
    /// Dtor
    virtual ~FakeDiffer() {}

    /// Returns the derivative of the state given the input and noise vectors
    virtual _Derivand operator()(const _Derivand& x, f64 t,
                                 const _InputVec& u = _InputVec::Zero(),
                                 const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        return -x;
    }

    /// Swap
    inline void swap(FakeDiffer& o) {}
    /// Copy Ctor
    FakeDiffer(const FakeDiffer& o) {}
    /// Move Ctor
    FakeDiffer(FakeDiffer&& o) { swap(o); }
    /// Unifying Assignment
    FakeDiffer& operator=(FakeDiffer o) { swap(o); return *this; }

};

class IntegratorsTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testSimpleIntegrate()
    {
        Matrix<f64, 3, 1> x{1, 2, 3};
        f64 t  = 0.0;
        f64 dt = 1;
        f64 u  = 0.5;

        Integrator<f64, 3> integrate(1);

        ScalarDeriv   scalarDeriv;
        MatrixDeriv   matrixDeriv;
        CopylessDeriv copylessDeriv;

        auto y = integrate(x, t, dt, scalarDeriv, u);
        Matrix<f64, 3, 1> yExpect = x + dt * scalarDeriv(x, t, u);
        TS_ASSERT(matEquals(y, yExpect));

        y = integrate(x, t, dt, matrixDeriv, Matrix<f64, 3, 1>{0, 0, u});
        yExpect = x + dt * matrixDeriv(x, t, {0, 0, u});
        TS_ASSERT(matEquals(y, yExpect));

        y = integrate(x, t, dt, copylessDeriv, CopyTrackerMatrix({0, 0, u}));
        yExpect = x + dt * copylessDeriv(x, t, {{0, 0, u}});
        TS_ASSERT(matEquals(y, yExpect));

        CopyTrackerMatrix uNoCopy({0, 0, u});
        y = integrate(x, t, dt, copylessDeriv, uNoCopy);
        yExpect = x + dt * copylessDeriv(x, t, uNoCopy);
        TS_ASSERT(matEquals(y, yExpect));
    }

    void testMatrixIntegrate()
    {
        Matrix<f64, 3, 3> x = Matrix<f64, 3, 3>::Constant(2);
        f64  t = 0.0;
        f64 dt = 1.0;

        Integrator<f64, 3, 3> integrate(1);

        Deriv3by3 deriv3by3;

        auto y = integrate(x, t, dt, deriv3by3);
        Matrix<f64, 3, 3> yExpect = x + dt * deriv3by3(x, t);
        TS_ASSERT(matEquals(y, yExpect));
    }

    void testRK4Accuracy()
    {
        // TODO: test the fourth order RK integration's accuracy
    }

    void testDiffClass()
    {
        Differentiator<f64, 3, 2, 2, 3> diff;
        Integrator<f64, 3, 2> integrate(1);

        Matrix<f64, 3, 2> x; x << 1, 2, 3, 4, 5, 6;
        Vector2d u; u << 1, 2;
        Vector3d w; w << 1, 2, 3;

        f64  t = 0.0;
        f64 dt = 1.0;

        Matrix<f64, 3, 2> xdot = diff(x, t, u, w);
        Matrix<f64, 3, 2> yExpect = x + dt * xdot;

        auto y = integrate(x, t, dt, diff, u, w);
        TS_ASSERT(matEquals(y, yExpect));

        // Testing default zeros on u and w
        xdot = diff(x, t);
        yExpect = x + dt * xdot;
        y = integrate(x, t, dt, diff);
        TS_ASSERT(matEquals(y, yExpect));
    }

    // Obviously, this test can't be left in here, but if uncommented,
    // it should result in compile time errors
    void testCompileTimeFailure()
    {
        /*
        Matrix<f64, 3, 1> x{1, 2, 3};
        f64 t  = 0.0;
        f64 dt = 1;
        Matrix<f64, 3, 1> u{0.5, 0.4, 0.3};

        Integrator<f64, 3> integrate(1);
        ScalarDeriv              scalarDeriv;
        FakeDiffer<f64, 3, 1, 3> faker;

        // Faker is not a Differentiator even though it looks nearly like one
        auto y = integrate(x, t, dt, faker, u);
        Matrix<f64, 3, 1> yExpect = x + dt * faker(x, t, u);
        TS_ASSERT(matEquals(y, yExpect));

        // ScalarDeriv has no way to handle these input args
        y = integrate(x, t, dt, scalarDeriv, 1, 2, 3, 4);
        yExpect = x + dt * scalarDeriv(x, t, 1, 2, 3, 4);
        TS_ASSERT(matEquals(y, yExpect));
        // */
    }
};
