#pragma once

#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/EigenUtil.hpp"
#include "kestrel/util/TestUtil.hpp"

#include "GaussianDifferentiator.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace EigenUtil;
using namespace TestUtil;
using namespace Control;

class GaussianDifferentiatorTest: public CxxTest::TestSuite {
  private:
    typedef GaussianDifferentiator<f64, 3, 2, 3, 1> Diff;
    Diff diff;

    f64 t;

    Matrix<f64, 3,   2>   x;
    Matrix<f64, 3*2, 3*2> P;
    Matrix<f64, 3,   1>   u;
    Matrix<f64, 1,   1>   w;

  public:
    void setUp() override
    {
        t = 0.0;
        x << 1, 2, 3, 4, 5, 6;
        P.setIdentity();
        u << 1, 2, 3;
        w << 1;
    }
    void tearDown() override {}

    void testDiff()
    {
        Matrix<f64, 3, 2> expect;
        expect.block<1, 2>(0,0)  = x.block<1, 2>(1,0);
        expect.block<1, 2>(1,0)  = x.block<1, 2>(2,0);
        expect.block<1, 2>(2,0)  = u.block<2, 1>(0,0).transpose();
        expect.block<1, 1>(2,0) += w.block<1, 1>(0,0).transpose();

        TS_ASSERT(matEquals(diff(x, t, u, w), expect));
    }

    void testJac()
    {
        Diff::_StateJac dx = diff.stateJac(x, t, u, w);
        Diff::_InputJac du = diff.inputJac(x, t, u, w);
        Diff::_NoiseJac dw = diff.noiseJac(x, t, u, w);

        // The default differentiator is linear with all inputs
        Matrix<f64, 3*2, 1> xVec(x.data());
        Matrix<f64, 3*2, 1> xDotVec = dx * xVec;
        xDotVec += du * u;
        xDotVec += dw * w;

        Matrix<f64, 3, 2> xDot(xDotVec.data());
        TS_ASSERT(matEquals(diff(x, t, u, w), xDot));
    }

    void testCovDiff()
    {
        f64 dt = 0.01;
        Diff::_StateDist dist = diff(x, P, t, u, w);

        TS_ASSERT(matEquals(dist.first, diff(x, t, u, w)));
        TS_ASSERT(matEquals(dist.second, diff.covDeriv(x, P, t, u, w)));
        TS_ASSERT(matEquals(dist.second, forceSym(dist.second)));
        TS_ASSERT_LESS_THAN(P.determinant(), (P+dt*dist.second).determinant());

        TS_ASSERT(matEquals(dist.first, diff(x, t, u, w)));
        TS_ASSERT(matEquals(dist.second, diff.covDeriv(x, P, t, u, w)));
        TS_ASSERT(matEquals(dist.second, forceSym(dist.second)));
        TS_ASSERT_LESS_THAN(P.determinant(), (P+dt*dist.second).determinant());

        dist = diff(x, P, t);
        TS_ASSERT(matEquals(dist.first, diff(x, t)));
        TS_ASSERT(matEquals(dist.second, forceSym(dist.second)));
        TS_ASSERT_LESS_THAN(P.determinant(), (P+dt*dist.second).determinant());
    }

    // will static assert:
    void testZeroDim()
    {
        /*
        GaussianDifferentiator<f64, 3, 2, 0, 0> zdimDiff;
        f64 dt = 0.01;
        auto dist = zdimDiff(x, P, t);

        TS_ASSERT(matEquals(dist.first, zdimDiff(x, t)));
        TS_ASSERT(matEquals(dist.second, forceSym(dist.second)));
        TS_ASSERT_LESS_THAN(P.determinant(), (P+dt*dist.second).determinant());
        // */
    }
};
