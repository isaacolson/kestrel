#pragma once

#include <iostream>
#include <Eigen/Dense>
#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TestUtil.hpp"

#include "LinearFeedbackController.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace MathUtil;
using namespace TestUtil;
using namespace Control;

class LinearFeedbackControllerTest: public CxxTest::TestSuite {
  private:
    typedef LinearFeedbackController<f64, 3, 2, 2, 3> Ctrl;
    Ctrl ctrl;

    f64 t;
    f64 dt;
    Matrix<f64, 3, 2> x;
    Matrix<f64, 3, 1> r;

  public:
    void setUp() override
    {
        t  = 0.0;
        dt = 0.1;
        x << 1, 2, 3, 4, 5, 6;
        r << 2, 4, 6;
    }
    void tearDown() override {}

    void testZeroSized()
    {
        LinearFeedbackController<f64, 3, 2, 2, 0> ctrl;
        Matrix<f64, 2, 1> uExp; uExp << -x(0,0), -x(0,1);
        TS_ASSERT(matEquals(ctrl(x, t, dt), uExp));
    }

    void testGains()
    {
        Ctrl::_StateGain KExp; KExp.setZero(); KExp(0,0) = 1; KExp(1,3) = 1;
        Ctrl::_SetptGain GExp; GExp.setZero(); GExp(0,0) = 1; GExp(1,1) = 1;
        TS_ASSERT(matEquals(ctrl.stateGain(x, t, dt, r), KExp));
        TS_ASSERT(matEquals(ctrl.setptGain(x, t, dt, r), GExp));
    }

    void testControl()
    {
        Matrix<f64, 2, 1> uExp; uExp << r(0) - x(0,0), r(1) - x(0,1);
        TS_ASSERT(matEquals(ctrl(x, t, dt, r), uExp));
    }
};
