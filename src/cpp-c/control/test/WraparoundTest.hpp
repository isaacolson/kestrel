#pragma once

#include <iostream>
#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/EigenUtil.hpp"
#include "kestrel/util/TestUtil.hpp"

#include "GaussianDifferentiator.hpp"
#include "GaussianIntegrator.hpp"
#include "LinearFeedbackController.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace MathUtil;
using namespace EigenUtil;
using namespace TestUtil;
using namespace Control;

class WraparoundIntegrator: public GaussianIntegrator<f64, 1, 1> {
  public:
    typedef GaussianIntegrator<f64, 1, 1> ParentType;

    using ParentType::correctDomain;
    _Integrand correctDomain(const _Integrand& x) const override
    {
        _Integrand ret = x;
        ret(0) = radToPiPi(ret(0));
        return ret;
    }

  public:
    /// Ctor
    WraparoundIntegrator(sz order = 1): ParentType(order) {}
    /// Dtor
    virtual ~WraparoundIntegrator() {}

    /// Swap
    inline void swap(WraparoundIntegrator& o) { ParentType::swap(o); }
    /// Copy Ctor
    WraparoundIntegrator(const WraparoundIntegrator& o) : ParentType(o) {}
    /// Move Ctor
    WraparoundIntegrator(WraparoundIntegrator&& o) { swap(o); }
    /// Unifying Assignment
    WraparoundIntegrator& operator=(WraparoundIntegrator o) { swap(o); return *this; }
};

class WraparoundDifferentiator: public GaussianDifferentiator<f64, 1, 1, 1, 1> {
  public:
    typedef GaussianDifferentiator<f64, 1, 1, 1, 1> ParentType;

    using ParentType::operator();
    _Derivand operator()(const _Derivand& x, f64 t,
                         const _InputVec& u = _InputVec::Zero(),
                         const _NoiseVec& w = _NoiseVec::Zero()) const override
    { return u + w; }

    _StateJac stateJac(const _Derivand& x, f64 t,
                       const _InputVec& u = _InputVec::Zero(),
                       const _NoiseVec& w = _NoiseVec::Zero()) const override
    { return _StateJac::Zero(); }

    _InputJac inputJac(const _Derivand& x, f64 t,
                       const _InputVec& u = _InputVec::Zero(),
                       const _NoiseVec& w = _NoiseVec::Zero()) const override
    { return _InputJac::Identity(); }

    _NoiseJac noiseJac(const _Derivand& x, f64 t,
                       const _InputVec& u = _InputVec::Zero(),
                       const _NoiseVec& w = _NoiseVec::Zero()) const override
    { return _NoiseJac::Identity(); }

    _NoiseCov noiseCov(const _Derivand& x, f64 t,
                       const _InputVec& u = _InputVec::Zero(),
                       const _NoiseVec& w = _NoiseVec::Zero()) const override
    { return _NoiseCov::Identity(); }

  public:
    /// Ctor
    WraparoundDifferentiator() {}
    /// Dtor
    virtual ~WraparoundDifferentiator() {}

    /// Swap
    inline void swap(WraparoundDifferentiator& o) { ParentType::swap(o); }
    /// Copy Ctor
    WraparoundDifferentiator(const WraparoundDifferentiator& o) : ParentType(o) {}
    /// Move Ctor
    WraparoundDifferentiator(WraparoundDifferentiator&& o) { swap(o); }
    /// Unifying Assignment
    WraparoundDifferentiator& operator=(WraparoundDifferentiator o) { swap(o); return *this; }
};

class WraparoundController: public LinearFeedbackController<f64, 1, 1, 1, 1> {
  public:
    typedef LinearFeedbackController<f64, 1, 1, 1, 1> ParentType;

    _StateGain stateGain(const _Controland& x = _Controland::Zero(), f64 t = 0, f64 dt = 0,
                         const _SetptVec& r = _SetptVec::Zero()) override
    { return _StateGain::Identity(); }

    _SetptGain setptGain(const _Controland& x = _Controland::Zero(), f64 t = 0, f64 dt = 0,
                         const _SetptVec& r = _SetptVec::Zero()) override
    { return _SetptGain::Identity(); }

    _SetptVec correctDomain(const _Controland& x, const _SetptVec& r) const override
    {
        _StateVec rCorr;
        rCorr(0) = radDiffToPiPi(x(0), radToPiPi(r(0)));
        return rCorr;
    }

  public:
    /// Ctor
    WraparoundController(/* arguments */) {}
    /// Dtor
    virtual ~WraparoundController() {}

    /// Swap
    inline void swap(WraparoundController& o) { ParentType::swap(o); }
    /// Copy Ctor
    WraparoundController(const WraparoundController& o) : ParentType(o) {}
    /// Move Ctor
    WraparoundController(WraparoundController&& o) { swap(o); }
    /// Unifying Assignment
    WraparoundController& operator=(WraparoundController o) { swap(o); return *this; }
};

class WraparoundTest: public CxxTest::TestSuite {
  private:
    Matrix<f64, 1, 1> x;
    Matrix<f64, 1, 1> P;
    Matrix<f64, 1, 1> u;
    Matrix<f64, 1, 1> r;

    f64 dt = 0.1;

    WraparoundDifferentiator diff;
    WraparoundIntegrator     integ;
    WraparoundController     ctrl;

  public:
    void setUp() override
    {
        x.setConstant(0);
        P.setConstant(1);
        u.setConstant(1);
        r.setConstant(1);

        integ = WraparoundIntegrator(4);
    }
    void tearDown() override {}

    void testIntegrate()
    {
        WraparoundIntegrator::_StateDist xP(x, P);
        WraparoundIntegrator::_StateDist lastXP(xP);

        for (f64 t = 0; t < 5; t+=dt) {
            xP = integ(xP, t, dt, diff, u);

            TSM_ASSERT_LESS_THAN_EQUALS("State Too Large",  xP.first(0), PI);
            TSM_ASSERT_LESS_THAN(       "State Too Small", -PI, xP.first(0));
            TS_ASSERT_LESS_THAN(lastXP.second.determinant(), xP.second.determinant());
            TS_ASSERT(matEquals(xP.second, forceSym(xP.second)));

            lastXP = xP;
        }
    }

    void testControl()
    {
        x(0) = 2;
        r(0) = -2;
        WraparoundIntegrator::_StateDist xP(x, P);
        WraparoundIntegrator::_StateDist lastXP(xP);

        for (f64 t = 0; t < 5; t+=dt) {
            u = ctrl(xP.first, t, dt, r);
            xP = integ(xP, t, dt, diff, u);

            TSM_ASSERT_LESS_THAN_EQUALS("State Too Large",  xP.first(0), PI);
            TSM_ASSERT_LESS_THAN(       "State Too Small", -PI, xP.first(0));
            TSM_ASSERT("Controller took the wrong direction",
                          xP.first(0) < -2+TINY
                       || xP.first(0) >  2-TINY);

            lastXP = xP;
        }
    }
};
