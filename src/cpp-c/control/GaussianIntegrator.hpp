#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/EigenUtil.hpp"

#include "kestrel/control/GaussianDifferentiator.hpp"
#include "kestrel/control/Integrator.hpp"

namespace Kestrel {
namespace Control {

/// Specialization of the state integrator that propagates a Gaussian distribution
template <typename Scalar, int ROWS, int COLS=1>
class GaussianIntegrator:
    public  Integrator<Scalar, ROWS, COLS>  {
  public:
    typedef Integrator<Scalar, ROWS, COLS> ParentType;

    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS * COLS;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Integrand;

    // Note: _StateVec formed by stacking columns of _Integrand (ie column major)
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, 1>           _StateVec;
    // Note: Coviances and Jacobians for matrix derivatives will use same ordering from _Integrand
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_STATES> _StateCov;
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_STATES> _StateJac;

    typedef std::pair<_Integrand, _StateCov>                 _StateDist;

  protected:
    using ParentType::order;

  public:
    /// Ctor
    GaussianIntegrator(sz order = 1): ParentType(order) {}
    /// Dtor
    virtual ~GaussianIntegrator() {}

    /// Note: due to how templates work, children will likely need to have this using statement
    ///       if they override correctDomain in order to get all the other overloadings
    using ParentType::correctDomain;

    /// Returns a state vector and covariance with domains corrected. Defaults to no-op on
    /// the state vector and a forceSym on the covariance. Inherits the state correction
    /// behavior from the state only version of the function. If speed is more important
    /// to the application than accuracy, it may be wise to override this function and
    /// remove the forceSym call (especially if P is large)
    virtual _StateDist correctDomain(const _Integrand& x, const _StateCov& P) const
    { return _StateDist(correctDomain(x), EigenUtil::forceSym(P)); }

    /// Correct domain with alternate args
    _StateDist correctDomain(const _StateDist& xP) const
    { return correctDomain(xP.first, xP.second); }

    using ParentType::operator();

    // TODO: This integrator currently uses an EKF-like approach of linearizing the dynamics
    //       for the integration. It could be useful to also build a UKF-like version that
    //       uses sample points to linearize the statistics instead of the dynamics.

    // TODO: averaging the derivatives *should* work for even nonlinear things like quaternions,
    //       but may want to look into providing a virtual method that does the final RK
    //       extrapolation (ie x + dt/6 * (dx1 + 2*dx2 + 2*dx3 + dx4))
    /// Integrate using a Differentiator class
    template<typename Differ, typename... Args>
    _StateDist operator()(const _Integrand& x, const _StateCov& P, f64 t, f64 dt,
                          const Differ& diff, Args&&... args) const
    {
        { // Perform static type checking for valididity
            typedef GaussianDifferentiator<_Scalar, _ROWS, _COLS, Differ::_NUM_INPUTS,
                                           Differ::_NUM_NOISES> BaseDiffer;
            static_assert(std::is_base_of<BaseDiffer, Differ>::value,
                          "GaussianIntegrator requires a valid GaussianDifferentiator "
                          "class of the proper dimension");
        }

        _StateDist kxPtmp;

        kxPtmp         = diff(x, P, t, std::forward<Args>(args)...);
        _Integrand k1x = kxPtmp.first;
        _StateCov  k1P = kxPtmp.second;
        if (order == 1) return correctDomain(x + dt * k1x, P + dt * k1P);

        kxPtmp         = diff(correctDomain(x + dt/2 * k1x, P + dt/2 * k1P), t + dt/2,
                              std::forward<Args>(args)...);
        _Integrand k2x = kxPtmp.first;
        _StateCov  k2P = kxPtmp.second;
        if (order == 2) return correctDomain(x + dt * k2x, P + dt * k2P);

        kxPtmp         = diff(correctDomain(x + dt/2 * k2x, P + dt/2 * k2P), t + dt/2,
                              std::forward<Args>(args)...);
        _Integrand k3x = kxPtmp.first;
        _StateCov  k3P = kxPtmp.second;

        kxPtmp         = diff(correctDomain(x + dt * k3x, P + dt * k3P), t + dt,
                              std::forward<Args>(args)...);
        _Integrand k4x = kxPtmp.first;
        _StateCov  k4P = kxPtmp.second;

        if (order == 4) return correctDomain(x + dt/6 * (k1x + 2*k2x + 2*k3x + k4x),
                                             P + dt/6 * (k1P + 2*k2P + 2*k3P + k4P));

        return std::make_pair(x, P);
    }

    /// Integrate using a Differentiator class with alternate args
    template<typename Differ, typename... Args>
    _StateDist operator()(const _StateDist& xP, f64 t, f64 dt,
                          const Differ& diff, Args&&... args) const
    { return operator()(xP.first, xP.second, t, dt, diff, std::forward<Args>(args)...); }

    using ParentType::validOrder;

    /// Swap
    inline void swap(GaussianIntegrator& o) { ParentType::swap(o); }
    /// Copy Ctor
    GaussianIntegrator(const GaussianIntegrator& o): ParentType(o) {}
    /// Move Ctor
    GaussianIntegrator(GaussianIntegrator&& o) { swap(o); }
    /// Unifying Assignment
    GaussianIntegrator& operator=(GaussianIntegrator o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
