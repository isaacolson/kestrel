#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"

#include "kestrel/control/Differentiator.hpp"

namespace Kestrel {
namespace Control {

/// Specialization of the dynamic model that can generate the Jacobians of its differentiate func
template<typename Scalar, int ROWS, int COLS=1, int NUM_INPUTS=0, int NUM_NOISES=0>
class LinearizingDifferentiator:
    public  Differentiator<Scalar, ROWS, COLS, NUM_INPUTS, NUM_NOISES> {
  public:
    typedef Differentiator<Scalar, ROWS, COLS, NUM_INPUTS, NUM_NOISES> ParentType;

    typedef Scalar _Scalar;
    static constexpr int _ROWS       = ROWS;
    static constexpr int _COLS       = COLS;
    static constexpr int _NUM_STATES = ROWS*COLS;
    static constexpr int _NUM_INPUTS = NUM_INPUTS;
    static constexpr int _NUM_NOISES = NUM_NOISES;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int _storeType = (_ROWS == 1 && _COLS > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    // Note: I know this isn't a real word, needed a word like Integrand but for derivatives
    typedef Eigen::Matrix<_Scalar, _ROWS, _COLS, _storeType> _Derivand;

    // Note: _StateVec formed by stacking columns of _Derivand (ie column major)
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, 1>           _StateVec;
    typedef Eigen::Matrix<_Scalar, _NUM_INPUTS, 1>           _InputVec;
    typedef Eigen::Matrix<_Scalar, _NUM_NOISES, 1>           _NoiseVec;
    // Note: Jacobians for matrix derivatives will use same order from _Derivand
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_STATES> _StateJac;
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_INPUTS> _InputJac;
    typedef Eigen::Matrix<_Scalar, _NUM_STATES, _NUM_NOISES> _NoiseJac;

  public:
    /// Ctor
    LinearizingDifferentiator() {}
    /// Dtor
    virtual ~LinearizingDifferentiator() {}

    /////////////////////////////////////////////////////////////////////////////////////
    // User interface : override these functions to inject desired behavior
    /////////////////////////////////////////////////////////////////////////////////////

    // Note: User should implement all virtual functions from the ParentType.
    /// Returns the derivative of the state given the input and noise vectors
    using ParentType::operator();

    /// Returns the Jacobian of the derivative of the state with respect to the state
    virtual _StateJac stateJac(const _Derivand& x, f64 t,
                               const _InputVec& u = _InputVec::Zero(),
                               const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        // Note: though this function is not pure virtual, all children should override it
        _StateJac ret = _StateJac::Zero();
        for (int c = 0; c < _COLS; ++c) {
            for (int r = 1; r < _ROWS; ++r) {
                ret(c*_ROWS + (r-1), c*_ROWS + r) = 1;
            }
        }
        return ret;
    }

    /// Returns the Jacobian of the derivative of the state with respect to the input
    virtual _InputJac inputJac(const _Derivand& x, f64 t,
                               const _InputVec& u = _InputVec::Zero(),
                               const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        // Note: though this function is not pure virtual, all children should override it
        _InputJac ret = _InputJac::Zero();
        for (int c = 0; c < _NUM_INPUTS && c < _COLS; ++c) {
            ret(c*_ROWS+(_ROWS-1), c) = 1;
        }
        return ret;
    }

    /// Returns the Jacobian of the derivative of the state with respect to the noise
    virtual _NoiseJac noiseJac(const _Derivand& x, f64 t,
                               const _InputVec& u = _InputVec::Zero(),
                               const _NoiseVec& w = _NoiseVec::Zero()) const
    {
        // Note: though this function is not pure virtual, all children should override it
        _NoiseJac ret = _NoiseJac::Zero();
        for (int c = 0; c < _NUM_NOISES && c < _COLS; ++c) {
            ret(c*_ROWS+(_ROWS-1), c) = 1;
        }
        return ret;
    }

    /////////////////////////////////////////////////////////////////////////////////////

    /// Swap
    inline void swap(LinearizingDifferentiator& o) { ParentType::swap(o); }
    /// Copy Ctor
    LinearizingDifferentiator(const LinearizingDifferentiator& o) : ParentType(o) {}
    /// Move Ctor
    LinearizingDifferentiator(LinearizingDifferentiator&& o) { swap(o); }
    /// Unifying Assignment
    LinearizingDifferentiator& operator=(LinearizingDifferentiator o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
