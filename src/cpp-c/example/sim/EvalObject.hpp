#pragma once

#include <iostream>
#include <utility>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "kestrel/sim/Object.hpp"

#include "ProcEltObject.hpp"

namespace Kestrel {
namespace Sim {
namespace Example {

class EvalObject: public Object {
  public:
    typedef Object ParentType;

  private:
    f64 dt;
    f64 tLast;

  public:
    /// Ctor
    EvalObject(f64 dt, f64 t0 = 0) : dt(dt), tLast(t0) { setName("Evaluator"); }
    /// Dtor
    virtual ~EvalObject() {}

    NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world) override
    {
        if (t == tLast) return NextTimeObsAct(t, t);

        for (auto o : *world) {
            if (const ProcEltObject* pe = dynamic_cast<const ProcEltObject*>(o)) {
                if (pe->obsPeriod > 0) {
                    // Note: observations happen before actions, so use tLast as the indicator
                    //       for how many actions should have been taken (first observation
                    //       happens at t=0).
                    sz  completedCycles = floor(tLast / pe->obsPeriod);
                    f64 cycleTime       = tLast - completedCycles * pe->obsPeriod;
                    sz  expectAct       = (cycleTime >= pe->actDelay) ? completedCycles + 1
                                                                      : completedCycles;

                    // Note: because floating point accuracy issues, it's impossible to exactly
                    //       determine if cycleTime is just over or just under the projected
                    //       action time. Therefore, we don't enforce the action number
                    //       immediately around the exact time the action should take place.
                    // TODO: for this reason, it would probably be a good idea to consider
                    //       switching to a integer based simulation time.

                    if (std::fabs(cycleTime - pe->actDelay) < MathUtil::TINY) continue;

                    if (expectAct != pe->numAct) {
                        std::cout << pe->getName() << " has wrong number of actions, "
                                  << "Expected: " << expectAct
                                  << ", Actual: " << pe->numAct
                                  << ", t: " << tLast << std::endl;
                    }
                }
            }
        }

        tLast = t;
        return NextTimeObsAct(t, t);
    }

    bool process(f64 tObs, f64 tObsExpect, f64 tAct) override { return true; }

    bool act(f64 t, f64 tExpect) override { return true; }

    Object* clone() const override { return new EvalObject(*this); }

    /// Swap
    inline void swap(EvalObject& o)
    { ParentType::swap(o); std::swap(dt, o.dt); std::swap(tLast, o.tLast); }
    /// Copy Ctor
    EvalObject(const EvalObject& o) : ParentType(o), dt(o.dt), tLast(o.tLast) {}
    /// Move Ctor
    EvalObject(EvalObject&& o) { swap(o); }
    /// Unifying Assignment
    EvalObject& operator=(EvalObject o) { swap(o); return *this; }
};

} /* Example */
} /* Sim */
} /* Kestrel */
