#pragma once

#include <iostream>
#include <utility>
#include <unordered_map>
#include <mutex>
#include <vector>
#include <string>

#include "kestrel/sim/Object.hpp"

namespace Kestrel {

enum ObjectStates { WAITING, OBSERVING, PROCESSING, ACTING, NUM_OBJECT_STATES };
static const std::vector<std::string> ObjectStatesNames { "WAITING", "OBSERVING",
                                                          "PROCESSING", "ACTING" };

class StateTracker {
  private:
    mutable std::mutex                                   lock;
    std::unordered_map<const Sim::Object*, ObjectStates> states;
    sz                                                   numObs = 0;
    sz                                                   numAct = 0;
    sz                                                   errors = 0;

  public:
    static constexpr bool PRINT_ON_ERROR = false;
    static constexpr bool PRINT_ALWAYS   = true;

    /// Ctor
    StateTracker() {}
    /// Dtor
    virtual ~StateTracker() {}

    void setState(const Sim::Object* obj, ObjectStates state)
    {
        std::unique_lock<std::mutex> lk(lock);

        ObjectStates oldState = states[obj]; // if not initialized yet, this should be WAITING
        states[obj] = state;

        if ((oldState != OBSERVING) && (state == OBSERVING)) ++numObs;
        if ((oldState == OBSERVING) && (state != OBSERVING)) --numObs;
        if ((oldState !=    ACTING) && (state ==    ACTING)) ++numAct;
        if ((oldState ==    ACTING) && (state !=    ACTING)) --numAct;

        if (checkError()) {
            std::cout << obj->getName() << " transitioned to " << ObjectStatesNames[state]
                      << " incorrectly" << std::endl;
        }
    }

    // Note: Lock should already be owned (or in thread safe code segment)
    bool checkError(bool printAlways = PRINT_ON_ERROR)
    {
        bool errorNow = (numObs > 0) && (numAct > 0);

        if (errorNow) ++errors;

        if (errorNow || (printAlways == PRINT_ALWAYS)) {
            std::cout << "Encountered " << errors << " total state errors";

            if (errorNow) std::cout << ": current error: NumObs = "  << numObs
                                    << ", NumAct = " << numAct;

            std::cout << std::endl;
        }

        return errorNow;
    }

    /// Swap
    inline void swap(StateTracker& o)
    {
        std::unique_lock<std::mutex> lk1(  lock, std::defer_lock);
        std::unique_lock<std::mutex> lk2(o.lock, std::defer_lock);
        std::lock(lk1, lk2);
        states.swap(o.states);
        std::swap(numObs, o.numObs);
        std::swap(numAct, o.numAct);
        std::swap(errors, o.errors);
    }
    /// Copy Ctor
    StateTracker(const StateTracker& o)
    {
        std::unique_lock<std::mutex> lk(o.lock);
        states = o.states;
        numObs = o.numObs;
        numAct = o.numAct;
        errors = o.errors;
    }
    /// Move Ctor
    StateTracker(StateTracker&& o) { swap(o); }
    /// Unifying Assignment
    StateTracker& operator=(StateTracker o) { swap(o); return *this; }
};

} /* Kestrel */
