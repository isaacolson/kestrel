#pragma once

#include <iostream>
#include <utility>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TimeUtil.hpp"
#include "kestrel/sim/Object.hpp"

#include "StateTracker.hpp"

namespace Kestrel {
namespace Sim {
namespace Example {

class EvalObject;

class ProcEltObject: public Object {
  public:
    typedef Object ParentType;
    friend EvalObject;

  private:
    f64 obsPeriod;
    f64 actDelay;
    f64 speed;
    StateTracker* st;

    f64 procMean;
    f64 procVar;

    bool print;

    sz numAct = 0;

  public:
    /// Ctor
    ProcEltObject(f64 obsPeriod = 0.01, f64 actDelay = 0, f64 speed = 1.0,
                  StateTracker* st = nullptr, bool print = false) :
        obsPeriod(obsPeriod), actDelay(actDelay), speed(speed), st(st), print(print)
    {
        Assert(speed > 0, "Speed must be a positive number to generate timings");

        setName(std::string("ProcElt: (") + std::to_string(obsPeriod) + std::string(", ")
                                          + std::to_string(actDelay)  + std::string(")"));

        // Processing time is centered at 3/4 the act delay with noise of 2 sigma = 1/4
        // the delta. This should result in processing times sometimes go over the intended
        // duration, but not frequently
        f64 base = actDelay / speed;
        procMean = 4.0/4.0 * base;
        procVar  = base * base / 64.0;
    }
    /// Dtor
    virtual ~ProcEltObject() {}

    virtual void useCpu(u64 wakeUtime)
    {
        // Children should do something interesting with the alloted time, but return when
        // utime() is approximately wakeUtime

        u64 now = TimeUtil::utime();
        if (now < wakeUtime) TimeUtil::usleep(wakeUtime - now);
    }

    NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world) override
    {
        // Note: there is a decent amount of lock contention on these locks, so expect
        //       the observe function to take a non-zero amount of time
        if (st) st->setState(this, OBSERVING);

        if (st) st->setState(this, PROCESSING);

        return NextTimeObsAct(std::max(tExpect + obsPeriod, t),
                              std::max(tExpect +  actDelay, t));
    }

    bool process(f64 tObs, f64 tObsExpect, f64 tAct) override
    {
        // XXX: change this to new math util RNG functions
        std::random_device rd;
        std::minstd_rand gen(rd());
        std::normal_distribution<f64> dist(0.0, 1.0);

        f64 dt       = std::max(std::sqrt(procVar) * dist(gen) + procMean, 0.0);
        u64 now      = TimeUtil::utime();
        u64 wakeTime = now + (u64)(dt * 1e6);

        useCpu(wakeTime);
        return true;
    }

    bool act(f64 t, f64 tExpect) override
    {
        // Note: there is a decent amount of lock contention on these locks, so expect
        //       the observe function to take a non-zero amount of time
        if (st) st->setState(this, ACTING);
        ++numAct;
        if (st) st->setState(this, WAITING);
        return true;
    }

    Object* clone() const override { return new ProcEltObject(*this); }

    /// Swap
    inline void swap(ProcEltObject& o)
    {
        ParentType::swap(o);
        std::swap(obsPeriod, o.obsPeriod);
        std::swap(actDelay,  o.actDelay);
        std::swap(speed,     o.speed);
        std::swap(st,        o.st);
        std::swap(procMean,  o.procMean);
        std::swap(procVar,   o.procVar);
        std::swap(print,     o.print);
        std::swap(numAct,    o.numAct);
    }
    /// Copy Ctor
    ProcEltObject(const ProcEltObject& o) : ParentType(o),
                                            obsPeriod(o.obsPeriod),
                                            actDelay(o.actDelay),
                                            speed(o.speed),
                                            st(o.st),
                                            procMean(o.procMean),
                                            procVar(o.procVar),
                                            print(o.print),
                                            numAct(o.numAct) {}
    /// Move Ctor
    ProcEltObject(ProcEltObject&& o) { swap(o); }
    /// Unifying Assignment
    ProcEltObject& operator=(ProcEltObject o) { swap(o); return *this; }
};

} /* Example */
} /* Sim */
} /* Kestrel */
