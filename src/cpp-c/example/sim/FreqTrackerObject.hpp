#pragma once

#include <cmath>
#include <utility>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TimeUtil.hpp"
#include "kestrel/sim/Object.hpp"

namespace Kestrel {
namespace Sim {
namespace Example {

class FreqTrackerObject: public Object {
  public:
    typedef Object ParentType;

  private:
    f64 obsPeriod;
    TimeUtil::StatTimer timer;

  public:
    /// Ctor (tMax used to reserve vector of loop times)
    FreqTrackerObject(f64 obsPeriod = 0.01, f64 tMax = 10) :
        obsPeriod(obsPeriod), timer(false) { setName("FreqTracker"); }
    /// Dtor
    virtual ~FreqTrackerObject() {}

    const TimeUtil::StatTimer& getTimer() const { return timer; }

    NextTimeObsAct observe(f64 t, f64 tExpect, const std::vector<const Object*>* world) override
    {
        // Timer methods are perfectly safe to use when not started yet
        timer.toc();
        timer.start();
        return NextTimeObsAct(std::max(tExpect + obsPeriod, t), t);
    }

    bool process(f64 tObs, f64 tObsExpect, f64 tAct) override { return true; }

    bool act(f64 t, f64 tExpect) override { return true; }

    Object* clone() const override { return new FreqTrackerObject(*this); }

  protected:
    void stopFunc() override
    {
        timer.stopNoRecord();
        ParentType::stopFunc();
    }

  public:

    /// Swap
    inline void swap(FreqTrackerObject& o)
    {
        ParentType::swap(o);
        std::swap(obsPeriod, o.obsPeriod);
        timer.swap(o.timer);
    }
    /// Copy Ctor
    FreqTrackerObject(const FreqTrackerObject& o) : ParentType(o),
                                                    obsPeriod(o.obsPeriod),
                                                    timer(o.timer) {}
    /// Move Ctor
    FreqTrackerObject(FreqTrackerObject&& o) { swap(o); }
    /// Unifying Assignment
    FreqTrackerObject& operator=(FreqTrackerObject o) { swap(o); return *this; }
};

} /* Example */
} /* Sim */
} /* Kestrel */
