#include <iostream>

#include "kestrel/util/types.hpp"
#include "kestrel/util/TimeUtil.hpp"
#include "kestrel/sim/Manager.hpp"
#include "kestrel/sim/SimStopperObject.hpp"

#include "StateTracker.hpp"
#include "FreqTrackerObject.hpp"
#include "ProcEltObject.hpp"
#include "EvalObject.hpp"

using namespace std;
using namespace Kestrel;
using namespace Sim;
using namespace Sim::Example;

int main(int argc, const char *argv[])
{
    f64 tMax          = 100;
    f64 dt            = 0.01;
    f64 speed         = 100.0;
    f64 freqTrackerDt = 0.1;

    StateTracker st;

    FreqTrackerObject* freqTracker = new FreqTrackerObject(freqTrackerDt, tMax);

    Manager man(dt, speed);
    man.addObject(new SimStopperObject(tMax, &man));
    man.addObject(freqTracker);

    man.addObject(new ProcEltObject(0,    0,     speed, &st)); // Every loop elt (e.g. dynamics sim)
    man.addObject(new ProcEltObject(0.02, 0.001, speed, &st)); // high freq sensor elt (e.g. imu)
    man.addObject(new ProcEltObject(0.1,  0.08,  speed, &st)); // low  freq sensor elt (e.g. laser)
    man.addObject(new ProcEltObject(1.0,  0.90,  speed, &st)); // Heavy processing elt (e.g. planner)

    man.addObject(new EvalObject(dt));

    u64 startTime = TimeUtil::utime();

    man.start();
    man.join();

    u64 stopTime = TimeUtil::utime();

    cout << endl;
    if (speed > 0) cout << "Expected period: " << freqTrackerDt / speed << endl;
    cout << "Actual period: "   << freqTracker->getTimer() << endl
         << "Expected execuation time: " << tMax
         << ", Actual: " << (stopTime - startTime) / 1.0e6 << endl;
    st.checkError(StateTracker::PRINT_ALWAYS);

    auto perfStats = man.getPerf();
    for (auto& stat : perfStats) cout << stat << endl;

    return 0;
}
