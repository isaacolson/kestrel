#pragma once

#include "kestrel/util/Rotations.hpp"
#include "kestrel/control/ObserverSystem.hpp"

using namespace Kestrel;
using namespace Rotations;
using Control::ObserverSystem;

// Note: this system intentionally does NOT normalize the quaternion after each operation
class QuatSys: public ObserverSystem<f64, 7, 3, 3> {
  public:
    typedef ObserverSystem<f64, 7, 3, 3> ParentType;

    enum StateIdx {
        QUATW,
        QUATX,
        QUATY,
        QUATZ,
        ANGVX,
        ANGVY,
        ANGVZ,
        NUM_STATES,
        NUM_QUAT=4,
        NUM_ANGV=3,
    };
    enum InputIdx {
        ANGAX,
        ANGAY,
        ANGAZ,
        NUM_INPUTS,
        NUM_ANGA=3,
    };
    enum NoiseIdx {
        ANGAX_NOISE,
        ANGAY_NOISE,
        ANGAZ_NOISE,
        NUM_NOISES,
        NUM_ANGA_NOISES=3,
    };

  public:
    /// Ctor
    QuatSys(const StateVec& x = stateZero(), const StateCov& P = StateCov::Identity(),
            sz order = 1) : ParentType(x, P, order) {}
    /// Dtor
    virtual ~QuatSys() {}

    static StateVec stateZero()
    {
        StateVec ret; ret << 1, 0, 0, 0, 0, 0, 0;
        return ret;
    }

    /// The derivative of the state vector given the state, the time, input, and noise
    using ParentType::deriv;
    virtual StateVec deriv(const StateVec& x, f64 t = 0.0,
                           const InputVec& u = InputVec::Zero(),
                           const NoiseVec& w = NoiseVec::Zero()) const
    {
        StateVec xdot = StateVec::Zero();
        xdot.block<NUM_QUAT, 1>(QUATW, 0) = quatRotDeriv(x.block<NUM_QUAT, 1>(QUATW, 0),
                                                         x.block<NUM_ANGV, 1>(ANGVX, 0));
        xdot.block<NUM_ANGV, 1>(ANGVX, 0) = u + w;
        return xdot;
    }

    /// The jacobian of the derivative function with respect to the state vector
    using ParentType::derivJac;
    virtual StateJac derivJac(const StateVec& x, f64 t = 0.0,
                              const InputVec& u = InputVec::Zero(),
                              const NoiseVec& w = NoiseVec::Zero()) const
    {
        // Treats the system as a simple integrator
        StateJac F = StateJac::Zero();
        F.block<NUM_QUAT, NUM_QUAT>(QUATW, QUATW) =
            quatRotDerivJacRot(x.block<NUM_QUAT, 1>(QUATW, 0),
                               x.block<NUM_ANGV, 1>(ANGVX, 0));
        F.block<NUM_QUAT, NUM_ANGV>(QUATW, ANGVX) =
            quatRotDerivJacAngVel(x.block<NUM_QUAT, 1>(QUATW, 0));
        return F;
    }

    /// The noise covariance matrix
    using ParentType::noiseCov;
    virtual NoiseCov noiseCov(const StateVec& x, f64 t = 0.0,
                              const InputVec& u = InputVec::Zero(),
                              const NoiseVec& w = NoiseVec::Zero()) const
    {
        // Pick 2 sigma bounds: var = (2*sigma)^2 / 4
        return 0.25 * (0.1 * 0.1) * NoiseCov::Identity();
    }

    /// The jacobian of the derivative function with respect to the noise vector
    using ParentType::noiseJac;
    virtual NoiseJac noiseJac(const StateVec& x, f64 t = 0.0,
                              const InputVec& u = InputVec::Zero(),
                              const NoiseVec& w = NoiseVec::Zero()) const
    {
        NoiseJac L = NoiseJac::Zero();
        L.block<NUM_ANGV, NUM_ANGA_NOISES>(ANGVX, ANGAX_NOISE).setIdentity();
        return L;
    }

    /// Correct the state vector for domain restricted values (such as angles that wrap around
    /// or required normalization). This function will be called after every prediction and
    /// correction. Note: this is intended to modify the input vector, not copy it.
    using ParentType::correctDomain;

    /// Swap
    inline void swap(QuatSys& o) { ParentType::swap(o); }
    /// Copy Ctor
    QuatSys(const QuatSys& o) : ParentType(o) {}
    /// Move Ctor
    QuatSys(QuatSys&& o) { swap(o); }
    /// Unifying Assignment
    QuatSys& operator=(QuatSys o) { swap(o); return *this; }
};
