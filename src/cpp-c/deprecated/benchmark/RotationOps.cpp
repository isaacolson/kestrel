#include <cstdlib>
#include <iostream>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TimeUtil.hpp"
#include "kestrel/util/Rotations.hpp"

#include "kestrel/benchmark/QuatSys.hpp"
#include "kestrel/benchmark/RmatSys.hpp"

/*
 * This is part of a unit-test-like benchmark suite of quaternions vs rotation matrices. Will
 * be testing a series of common operations using both rotation types to determine which would
 * be best to use in an actual program.
 *
 * Expectations:
 *   Quaternions should be much faster to integrate because there are fewer elements and the
 *     normalization is simpler. Kalman corrections using quaternions should also be  faster
 *     because the matrix inverse is smaller.
 *   Rotation matrices should be much faster to rotate a vector.
 *
 * Set of operations:
 *   - Convert angular velocity -> derivative
 *   - Integrate
 *   - Predict (ie integrate with covariances)
 *   - Normalize
 *   - Correct (will need to ensure the covariance is reasonable for what a sim would be)
 */

class QuatOps: public QuatSys {
  public:
    typedef QuatSys ParentType;

    enum QuatObsIdx {
        QUATWOBS,
        QUATXOBS,
        QUATYOBS,
        QUATZOBS,
        NUM_QUATOBS
    };
    typedef Eigen::Matrix<f64, NUM_QUATOBS, 1>           QuatObsVec;
    typedef Eigen::Matrix<f64, NUM_QUATOBS, NUM_QUATOBS> QuatObsCov;
    typedef Eigen::Matrix<f64, NUM_QUATOBS, NUM_STATES>  QuatObsJac;

  protected:
    using ParentType::ParentType::x;
    using ParentType::ParentType::P;

  public:
    /// Ctor
    QuatOps(const StateVec& x = stateZero(), const StateCov& P = StateCov::Identity(),
            sz order = 1) : ParentType(x, P, order) {}
    /// Dtor
    virtual ~QuatOps() {}

    void setState(const StateVec& x) { this->x = x; }
    void setCovar(const StateCov& P) { this->P = P; }

    void setAngVel(const Eigen::Matrix<f64, NUM_ANGV, 1>& omega)
    { x.block<NUM_ANGV, 1>(ANGVX, 0) = omega; }

    StateVec& normalize()
    {
        x.block<NUM_QUAT, 1>(QUATW, 0).normalize();
        return x;
    }

    QuatObsVec quatObsVec(const StateVec& x) const
    {
        return x.block<NUM_QUATOBS, 1>(QUATW, 0);
    }
    QuatObsVec quatObsVec() const { return quatObsVec(x); }

    QuatObsCov quatObsCov(const StateVec& x) const
    {
        // would like to have the covariance represent an rotation of some angle plus some small
        // inflation covariance on each term for filter stability. One way to do this is to
        // integrate the covariance of an angular velocity for some small timestep.

        // Derivation partially done in kestrel/src/matlab/loadQuatObsCovDerivation.m
        // basically it revolves around thinking about the integral of a constant angular vel
        // yielding a rotation. Essentially we are equating the following:
        // quatDeriv * dt = rotDeriv(quat, angVel) * dt = rotDeriv(quat, angVel * dt)
        static f64        angCov   = 0.25 * (0.05 * 0.05);
        static f64        smallCov = 0.25 * (0.001 * 0.001); // may need to tune this
        static NoiseCov   Qang     = angCov * NoiseCov::Identity();
        static QuatObsCov Qconst   = smallCov * QuatObsCov::Identity();
        return Rotations::quatObsCov(x.block<NUM_QUAT, 1>(QUATW, 0), Qang) + Qconst;
    }
    QuatObsCov quatObsCov() const { return quatObsCov(x); }

    QuatObsJac quatObsJac() const
    {
        QuatObsJac H = QuatObsJac::Zero();
        H(QUATWOBS, QUATW) = 1.0;
        H(QUATXOBS, QUATX) = 1.0;
        H(QUATYOBS, QUATY) = 1.0;
        H(QUATZOBS, QUATZ) = 1.0;
        return H;
    }

    /// Swap
    inline void swap(QuatOps& o) { ParentType::swap(o); }
    /// Copy Ctor
    QuatOps(const QuatOps& o) : ParentType(o) {}
    /// Move Ctor
    QuatOps(QuatOps&& o) { swap(o); }
    /// Unifying Assignment
    QuatOps& operator=(QuatOps o) { swap(o); return *this; }
};


class RmatOps: public RmatSys {
  public:
    typedef RmatSys ParentType;

    enum RmatObsIdx {
        RMAT00OBS,
        RMAT10OBS,
        RMAT20OBS,
        RMAT01OBS,
        RMAT11OBS,
        RMAT21OBS,
        RMAT02OBS,
        RMAT12OBS,
        RMAT22OBS,
        NUM_RMATOBS
    };
    typedef Eigen::Matrix<f64, NUM_RMATOBS, 1>           RmatObsVec;
    typedef Eigen::Matrix<f64, NUM_RMATOBS, NUM_RMATOBS> RmatObsCov;
    typedef Eigen::Matrix<f64, NUM_RMATOBS, NUM_STATES>  RmatObsJac;

  protected:
    using ParentType::ParentType::x;
    using ParentType::ParentType::P;

  public:
    /// Ctor
    RmatOps(const StateVec& x = stateZero(), const StateCov& P = StateCov::Identity(),
            sz order = 1) : ParentType(x, P, order) {}
    /// Dtor
    virtual ~RmatOps() {}

    void setState(const StateVec& x) { this->x = x; }
    void setCovar(const StateCov& P) { this->P = P; }

    void setAngVel(const Eigen::Matrix<f64, NUM_ANGV, 1>& omega)
    { x.block<NUM_ANGV, 1>(ANGVX, 0) = omega; }

    StateVec& normalize()
    {
        x.block<NUM_RMAT, 1>(RMAT00, 0) = vecSvdNormalized(x.block<NUM_RMAT, 1>(RMAT00, 0));
        return x;
    }

    RmatObsVec rmatObsVec(const StateVec& x) const
    {
        return x.block<NUM_RMATOBS, 1>(RMAT00, 0);
    }
    RmatObsVec rmatObsVec() const { return rmatObsVec(x); }

    RmatObsCov rmatObsCov(const StateVec& x) const
    {
        static f64        angCov   = 0.25 * (0.05 * 0.05);
        static f64        smallCov = 0.25 * (0.001 * 0.001); // may need to tune this
        static NoiseCov   Qang     = angCov * NoiseCov::Identity();
        static RmatObsCov Qconst   = smallCov * RmatObsCov::Identity();
        return Rotations::rmatObsCov(x.block<NUM_RMAT, 1>(RMAT00, 0), Qang) + Qconst;
    }
    RmatObsCov rmatObsCov() const { return rmatObsCov(x); }

    RmatObsJac rmatObsJac() const
    {
        RmatObsJac H = RmatObsJac::Zero();
        H(RMAT00OBS, RMAT00) = 1.0;
        H(RMAT10OBS, RMAT10) = 1.0;
        H(RMAT20OBS, RMAT20) = 1.0;
        H(RMAT01OBS, RMAT01) = 1.0;
        H(RMAT11OBS, RMAT11) = 1.0;
        H(RMAT21OBS, RMAT21) = 1.0;
        H(RMAT02OBS, RMAT02) = 1.0;
        H(RMAT12OBS, RMAT12) = 1.0;
        H(RMAT22OBS, RMAT22) = 1.0;
        return H;
    }

    /// Swap
    inline void swap(RmatOps& o) { ParentType::swap(o); }
    /// Copy Ctor
    RmatOps(const RmatOps& o) : ParentType(o) {}
    /// Move Ctor
    RmatOps(RmatOps&& o) { swap(o); }
    /// Unifying Assignment
    RmatOps& operator=(RmatOps o) { swap(o); return *this; }
};

using namespace std;
using namespace Eigen;
using namespace MathUtil;
using namespace TimeUtil;

int main(int argc, const char *argv[])
{
    if (argc < 3) {
        cout << "usage: " << argv[0] << " <loops> <ops per loop>" << endl;
        exit(1);
    }
    u32 loops = atoi(argv[1]);
    u32 iterPerToc = atoi(argv[2]);
    f64 dt = 0.01;

    Vector3d omegaMin(-3.0, -3.0, -3.0);
    Vector3d omegaMax( 3.0,  3.0,  3.0);
    Matrix3d dAngleCov;
    dAngleCov << 0.25*(0.05*0.05), 0, 0,
                 0, 0.25*(0.05*0.05), 0,
                 0, 0, 0.25*(0.05*0.05);

    // Quaternion Section
    {
        QuatOps::StateVec xInit = QuatOps::stateZero();
        QuatOps::StateCov PInit = 0.25 * (0.1 * 0.1) * QuatOps::StateCov::Identity();
        QuatOps::InputVec uZero = QuatOps::InputVec::Zero();
        QuatOps::NoiseVec wZero = QuatOps::NoiseVec::Zero();

        QuatOps ops(xInit, PInit, 4);

        StatTimer timerDeriv(false, loops);
        StatTimer timerInteg(false, loops);
        StatTimer timerPred( false, loops);
        StatTimer timerNorm( false, loops);
        StatTimer timerCorr( false, loops);

        for (u32 i = 0; i < loops; ++i) {
            ops.setState(xInit);
            ops.setCovar(PInit);
            ops.setAngVel(unifNoise(omegaMin, omegaMax));

            timerDeriv.start();
            for (u32 j = 0; j < iterPerToc; ++j) ops.deriv();
            timerDeriv.stop();

            timerInteg.start();
            for (u32 j = 0; j < iterPerToc; ++j) ops.integ(0, dt, uZero, wZero);
            timerInteg.stop();

            ops.setState(xInit);
            ops.setCovar(PInit);
            ops.setAngVel(unifNoise(omegaMin, omegaMax));

            // This will build up some good covariance between orientation and velocity
            timerPred.start();
            for (u32 j = 0; j < iterPerToc; ++j) ops.predict(0, dt, uZero, wZero);
            timerPred.stop();

            timerNorm.start();
            for (u32 j = 0; j < iterPerToc; ++j) ops.normalize();
            timerNorm.stop();

            Vector3d angleNoise = gausNoise(dAngleCov, true);
            AngleAxisd dAngle(angleNoise.norm(), angleNoise.normalized());
            Quaterniond quatObs = toQuat(ops.quatObsVec()) * dAngle;
            // Note: that in this case, the noise is applied to the measurement while the
            //       expected measurement is more "correct" according to the sim. Because we
            //       are just benchmarking, that's ok.
            timerCorr.start();
            ops.correct(toVec(quatObs), ops.quatObsCov(), ops.quatObsVec(), ops.quatObsJac());
            timerCorr.stop();
        }

        cout << "Quaternion derivative timer per " << iterPerToc << " ops: " << timerDeriv << endl;
        cout << "Quaternion integrate timer  per " << iterPerToc << " ops: " << timerInteg << endl;
        cout << "Quaternion predict timer    per " << iterPerToc << " ops: " << timerPred  << endl;
        cout << "Quaternion normalize timer  per " << iterPerToc << " ops: " << timerNorm  << endl;
        cout << "Quaternion correction timer per op: " << timerCorr << endl;
    }

    cout << endl;

    // Rotation Matrix Section
    {
        RmatOps::StateVec xInit = RmatOps::stateZero();
        RmatOps::StateCov PInit = 0.25 * (0.1 * 0.1) * RmatOps::StateCov::Identity();
        RmatOps::InputVec uZero = RmatOps::InputVec::Zero();
        RmatOps::NoiseVec wZero = RmatOps::NoiseVec::Zero();

        RmatOps ops(xInit, PInit, 4);

        StatTimer timerDeriv(false, loops);
        StatTimer timerInteg(false, loops);
        StatTimer timerPred( false, loops);
        StatTimer timerNorm( false, loops);
        StatTimer timerCorr( false, loops);

        for (u32 i = 0; i < loops; ++i) {
            ops.setState(xInit);
            ops.setCovar(PInit);
            ops.setAngVel(unifNoise(omegaMin, omegaMax));

            timerDeriv.start();
            for (u32 j = 0; j < iterPerToc; ++j) ops.deriv();
            timerDeriv.stop();

            timerInteg.start();
            for (u32 j = 0; j < iterPerToc; ++j) ops.integ(0, dt, uZero, wZero);
            timerInteg.stop();

            ops.setState(xInit);
            ops.setCovar(PInit);
            ops.setAngVel(unifNoise(omegaMin, omegaMax));

            timerPred.start();
            for (u32 j = 0; j < iterPerToc; ++j) ops.predict(0, dt, uZero, wZero);
            timerPred.stop();

            timerNorm.start();
            for (u32 j = 0; j < iterPerToc; ++j) ops.normalize();
            timerNorm.stop();

            Vector3d angleNoise = gausNoise(dAngleCov, true);
            AngleAxisd dAngle(angleNoise.norm(), angleNoise.normalized());
            Matrix3d rmatObs = toMat(ops.rmatObsVec()) * dAngle;
            timerCorr.start();
            ops.correct(toVec(rmatObs), ops.rmatObsCov(), ops.rmatObsVec(), ops.rmatObsJac());
            timerCorr.stop();
        }

        cout << "RotMatrix  derivative timer per " << iterPerToc << " ops: " << timerDeriv << endl;
        cout << "RotMatrix  integrate timer  per " << iterPerToc << " ops: " << timerInteg << endl;
        cout << "RotMatrix  predict timer    per " << iterPerToc << " ops: " << timerPred  << endl;
        cout << "RotMatrix  normalize timer  per " << iterPerToc << " ops: " << timerNorm  << endl;
        cout << "RotMatrix  correction timer per op: " << timerCorr << endl;
    }
    return 0;
}
