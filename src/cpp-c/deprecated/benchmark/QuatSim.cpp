#include <iostream>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TimeUtil.hpp"
#include "kestrel/util/Rotations.hpp"

#include "kestrel/benchmark/QuatSys.hpp"

// TODO: need to add corrections to this sim, also need to make sure that correcting a quaternion
//       through the kalman filter actually causes the right change in value or if we need to do
//       something like slerp.
//       Also keep in mind, normalizing the quaternion doesn't correct error accumulated in the
//       covariance.

using namespace Kestrel;
using namespace Rotations;

class QuatSim: public QuatSys {
  public:
    typedef QuatSys ParentType;

  public:
    /// Ctor
    QuatSim(const StateVec& x = stateZero(), const StateCov& P = StateCov::Identity(),
            sz order = 1) : ParentType(x, P, order) {}
    /// Dtor
    virtual ~QuatSim() {}

    /// Correct the state vector for domain restricted values (such as angles that wrap around
    /// or required normalization). This function will be called after every prediction and
    /// correction. Note: this is intended to modify the input vector, not copy it.
    using ParentType::correctDomain;
    virtual StateVec& correctDomain(StateVec& x) const
    {
        x.block<NUM_QUAT, 1>(QUATW, 0).normalize();
        return x;
    }

    /// Swap
    inline void swap(QuatSim& o) { ParentType::swap(o); }
    /// Copy Ctor
    QuatSim(const QuatSim& o) : ParentType(o) {}
    /// Move Ctor
    QuatSim(QuatSim&& o) { swap(o); }
    /// Unifying Assignment
    QuatSim& operator=(QuatSim o) { swap(o); return *this; }
};

using namespace std;

int main(int argc, const char *argv[])
{
    QuatSim::StateVec xInit = QuatSim::stateZero();
    QuatSim::StateCov PInit = QuatSim::StateCov::Identity();
    QuatSim sim(xInit, PInit, 4);

    QuatSim::InputVec uZero = QuatSim::InputVec::Zero();
    QuatSim::NoiseVec wZero = QuatSim::NoiseVec::Zero();

    f64 tmax = 1000;
    f64 dt   = 0.1;
    TimeUtil::StatTimer timerFull(true, tmax/dt);
    for (f64 t = 0; t < tmax - MathUtil::TINY; t += dt) {
        QuatSim::InputVec u = QuatSim::InputVec::Zero();
        QuatSim::NoiseVec w = MathUtil::gausNoise(sim.noiseCov(sim.getState(), t, u));

        sim.printCsv(cout, t, u, w, "SIM");

        sim.predict(t, dt, u, w);

        timerFull.toc();
    }
    sim.printCsv(cout, tmax, uZero, wZero, "SIM");

    timerFull.stopNoRecord();
    cerr << "Full loop timer (including prints): " << timerFull << endl;

    return 0;
}
