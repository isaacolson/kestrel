#include <iostream>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TimeUtil.hpp"
#include "kestrel/util/Rotations.hpp"

#include "kestrel/benchmark/RmatSys.hpp"

// TODO: need to add corrections to this sim, also need to make sure that correcting a rot mat
//       through the kalman filter actually causes the right change in value or if we need to do
//       something like slerp.
//       Also keep in mind, normalizing the rotation matrix doesn't correct error accumulated in
//       the covariance.

using namespace Kestrel;
using namespace Rotations;

class RmatSim: public RmatSys {
  public:
    typedef RmatSys ParentType;

  public:
    /// Ctor
    RmatSim(const StateVec& x = stateZero(), const StateCov& P = StateCov::Identity(),
            sz order = 1) : ParentType(x, P, order) {}
    /// Dtor
    virtual ~RmatSim() {}

    /// Correct the state vector for domain restricted values (such as angles that wrap around
    /// or required normalization). This function will be called after every prediction and
    /// correction. Note: this is intended to modify the input vector, not copy it.
    using ParentType::correctDomain;
    virtual StateVec& correctDomain(StateVec& x) const
    {
        x.block<NUM_RMAT, 1>(RMAT00, 0) = vecSvdNormalized(x.block<NUM_RMAT, 1>(RMAT00, 0));
        return x;
    }

    /// Swap
    inline void swap(RmatSim& o) { ParentType::swap(o); }
    /// Copy Ctor
    RmatSim(const RmatSim& o) : ParentType(o) {}
    /// Move Ctor
    RmatSim(RmatSim&& o) { swap(o); }
    /// Unifying Assignment
    RmatSim& operator=(RmatSim o) { swap(o); return *this; }
};

using namespace std;

int main(int argc, const char *argv[])
{
    RmatSim::StateVec xInit = RmatSim::stateZero();
    RmatSim::StateCov PInit = RmatSim::StateCov::Identity();
    RmatSim sim(xInit, PInit, 4);

    RmatSim::InputVec uZero = RmatSim::InputVec::Zero();
    RmatSim::NoiseVec wZero = RmatSim::NoiseVec::Zero();

    f64 tmax = 1000;
    f64 dt   = 0.1;
    TimeUtil::StatTimer timerFull(true, tmax/dt);
    for (f64 t = 0; t < tmax - MathUtil::TINY; t += dt) {
        RmatSim::InputVec u = RmatSim::InputVec::Zero();
        RmatSim::NoiseVec w = MathUtil::gausNoise(sim.noiseCov(sim.getState(), t, u));

        sim.printCsv(cout, t, u, w, "SIM");

        sim.predict(t, dt, u, w);

        timerFull.toc();
    }
    sim.printCsv(cout, tmax, uZero, wZero, "SIM");

    timerFull.stopNoRecord();
    cerr << "Full loop timer (including prints): " << timerFull << endl;

    return 0;
}
