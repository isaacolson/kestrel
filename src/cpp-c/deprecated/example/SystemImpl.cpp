#include <iostream>
#include <vector>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TimeUtil.hpp"
#include "kestrel/control/System.hpp"

using namespace Kestrel;
using Control::System;

class BasicSystem: public System<f64, 3, 1, 0> {
    typedef System<f64, 3, 1, 0> ParentType;
  public:
    /// Ctor
    BasicSystem(const StateVec& x = StateVec::Zero(), sz order = 1) : ParentType(x, order) {}
    /// Dtor
    virtual ~BasicSystem() {}

    /// The derivative of the state vector given the state, the time, input, and noise
    using ParentType::deriv;
    StateVec deriv(const StateVec& x, f64 t = 0.0,
                   const InputVec& u = InputVec::Zero(),
                   const NoiseVec& w = NoiseVec::Zero()) const override
    {
        // X = [sin(t), cos(t), sin(t) - cos(t)]
        return StateVec{ cos(t), -sin(t), u(0) };
    }

    /// Swap
    inline void swap(BasicSystem& o) { ParentType::swap(o); }
    /// Copy Ctor
    BasicSystem(const BasicSystem& o) {}
    /// Move Ctor
    BasicSystem(BasicSystem&& o) { swap(o); }
    /// Unifying Assignment
    BasicSystem& operator=(BasicSystem o) { swap(o); return *this; }
};

using namespace std;

int main(int argc, const char *argv[])
{
    BasicSystem sys({0, 1, -1}, 4);

    f64 tmax = 1000.0;
    f64 dt = 0.1;
    TimeUtil::StatTimer timer(true, tmax/dt);
    for (f64 t = 0.0; t < tmax - MathUtil::TINY; t += dt) {
        BasicSystem::InputVec u;
        u(0) = cos(t) + sin(t);
        sys.printCsv(cout, t, u);
        sys.integ(t, dt, u);

        timer.toc();
    }
    sys.printCsv(cout, tmax);

    timer.stopNoRecord();
    timer.printStats(cerr);

    return 0;
}
