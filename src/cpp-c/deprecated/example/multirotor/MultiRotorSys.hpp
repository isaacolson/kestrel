#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/assert.hpp"
#include "kestrel/util/PhysicsUtil.hpp"
#include "kestrel/util/Rotations.hpp"

#include "kestrel/control/System.hpp"

namespace Kestrel {
namespace MultiRotor {

enum StateIdx {
    POSX,
    POSY,
    POSZ,
    VELX,
    VELY,
    VELZ,
    QUATW,
    QUATX,
    QUATY,
    QUATZ,
    ANGVX,
    ANGVY,
    ANGVZ,
    NUM_POS=3,
    NUM_VEL=3,
    NUM_QUAT=4,
    NUM_ANGV=3,
    NUM_STATE=13,
};
enum InputIdx {
    LIFT,
    MOMENTX,
    MOMENTY,
    MOMENTZ,
    NUM_LIFT=1,
    NUM_MOMENT=3,
    NUM_INPUT=4,
};
enum NoiseIdx {
    NOISEFX,
    NOISEFY,
    NOISEFZ,
    NOISEMX,
    NOISEMY,
    NOISEMZ,
    NUM_NOISEF=3,
    NUM_NOISEM=3,
    NUM_NOISE=6,
};
enum InertiaIdx {
    IXX,
    IYY,
    IZZ,
    NUM_INERTIA=3
};

typedef Eigen::Matrix<f64, NUM_INERTIA, 1>         InertiaVec; ///< [ Ixx; Iyy; Izz ]
typedef Eigen::Matrix<f64, NUM_MOMENT, NUM_MOMENT> MomentMult;
typedef Eigen::Matrix<f64, NUM_VEL, 1>             VelDotVec;
typedef Eigen::Matrix<f64, NUM_ANGV, 1>            AngVelDotVec;

class MultiRotorSys: public Control::System<f64, NUM_STATE, NUM_INPUT, NUM_NOISE> {
  public:
    typedef Control::System<f64, NUM_STATE, NUM_INPUT, NUM_NOISE> ParentType;

  protected:
    // TODO: add actuator limits
    f64        m;
    InertiaVec I;

  public:
    /// Ctor
    MultiRotorSys(const StateVec& x = stateZero(), f64 m = 1,
                       const InertiaVec& I = InertiaVec::Ones(), sz order = 1) :
        ParentType(x, order), m(m), I(I)
    {
        Assert(m > 0, "Mass cannot be negative");
        for (int i = 0; i < NUM_INERTIA; ++i) { Assert(I(i) > 0, "Inertia cannot be negative"); }
    }
    /// Dtor
    virtual ~MultiRotorSys() {}

    static inline StateVec stateZero()
    {
        StateVec ret = StateVec::Zero();
        ret.block<NUM_QUAT, 1>(QUATW, 0) = Rotations::quatVecIdentity<Scalar>();
        return ret;
    }

    using ParentType::deriv;
    virtual StateVec deriv(const StateVec& x, f64 t = 0.0,
                           const InputVec& u = InputVec::Zero(),
                           const NoiseVec& w = NoiseVec::Zero()) const
    {
        StateVec ret = StateVec::Zero();

        ret.block<NUM_POS, 1>(POSX, 0) = x.block<NUM_VEL, 1>(VELX, 0);

        ret.block<NUM_VEL, 1>(VELX, 0) = - VelDotVec(0, 0, PhysicsUtil::GRAVITY)
                                         + Rotations::quatToMat(x.block<NUM_QUAT,1>(QUATW,0))
                                               .block<NUM_VEL, NUM_LIFT>(0, 2) * u(LIFT) / m
                                         + w.block<NUM_NOISEF, 1>(NOISEFX, 0) / m;

        ret.block<NUM_QUAT, 1>(QUATW, 0) = Rotations::quatRotDeriv(x.block<NUM_QUAT, 1>(QUATW, 0),
                                                                   x.block<NUM_ANGV, 1>(ANGVX, 0));

        ret.block<NUM_ANGV, 1>(ANGVX, 0) = derivAngVelF(x, I) + derivAngVelG(x, I) *
                                           (  u.block<NUM_MOMENT, 1>(MOMENTX, 0)
                                            + w.block<NUM_NOISEM, 1>(NOISEMX, 0) );
        return ret;
    }

    /// Correct the state vector for domain restricted values (such as angles that wrap around
    /// or required normalization). This function will be called after every prediction and
    /// correction. Note: this is intended to modify the input vector, not copy it.
    using ParentType::correctDomain;
    virtual StateVec& correctDomain(StateVec& x) const
    {
        Rotations::QuatVectorized<Scalar> q = x.block<NUM_QUAT, 1>(QUATW,0).normalized();
        Rotations::quatToPositive(q); // it seems that a block cannot resolve to a MatrixBase&
        x.block<NUM_QUAT, 1>(QUATW,0) = q;
        return x;
    }

    /// The offset part of the angvel derivative function
    static inline AngVelDotVec derivAngVelF(const StateVec& x, const InertiaVec& I)
    {
        return AngVelDotVec( (I(IYY)-I(IZZ)) / I(IXX) * x(ANGVY) * x(ANGVZ),
                             (I(IZZ)-I(IXX)) / I(IYY) * x(ANGVX) * x(ANGVZ),
                             (I(IXX)-I(IYY)) / I(IZZ) * x(ANGVX) * x(ANGVY) );
    }

    /// The coefficient part of the angvel derivative function
    static inline MomentMult derivAngVelG(const StateVec& x, const InertiaVec& I)
    {
        MomentMult ret = MomentMult::Zero();
        ret(IXX,IXX) = 1/I(IXX);
        ret(IYY,IYY) = 1/I(IYY);
        ret(IZZ,IZZ) = 1/I(IZZ);
        return ret;
    }

    /// Swap
    inline void swap(MultiRotorSys& o) { ParentType::swap(o); std::swap(m, o.m); I.swap(o.I); }
    /// Copy Ctor
    MultiRotorSys(const MultiRotorSys& o) : ParentType(o), m(o.m), I(o.I) {}
    /// Move Ctor
    MultiRotorSys(MultiRotorSys&& o) { swap(o); }
    /// Unifying Assignment
    MultiRotorSys& operator=(MultiRotorSys o) { swap(o); return *this; }
};

} /* MultiRotor */
} /* Kestrel */
