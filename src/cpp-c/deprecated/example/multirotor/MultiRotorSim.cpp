#include <iostream>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TimeUtil.hpp"

#include "MultiRotorSys.hpp"
#include "MultiRotorSmc.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace MathUtil;
using namespace TimeUtil;
using namespace MultiRotor;

MultiRotorSmc::SetptVec generateZqSetpt(f64 t);
MultiRotorSmc::SetptVec generatePtSetpt(f64 t);

int main(int argc, const char *argv[])
{
    MultiRotorSys::StateVec x0 = MultiRotorSys::stateZero();
    f64 m = 4.5;
    InertiaVec I(0.152, 0.172, 0.315); // napkin estimates for the dji s900
    MultiRotorSys sys(x0, m, I, 4);

    MultiRotorSmc::PGainMatrix   pGainDynamics;
    pGainDynamics.setIdentity(); pGainDynamics *= 1.00;
    MultiRotorSmc::PGainMatrix   pBoundary;
    pBoundary.setIdentity();     pBoundary     *= 0.30;
    MultiRotorSmc::PGainMatrix   pGainConverge;
    pGainConverge.setIdentity(); pGainConverge *= 0.20;
    MultiRotorSmc::ZGainList     zGains(1.0, 0.2, 0.3); // dynamics, boundary, converge)
    MultiRotorSmc::QGainMatrix   qGain;
    qGain.setIdentity();         qGain         *= 4.00;
    MultiRotorSmc::WGainMatrix   wBoundary;
    wBoundary.setIdentity();     wBoundary     *= 0.20;
    MultiRotorSmc::WGainMatrix   wGainConverge;
    wGainConverge.setIdentity(); wGainConverge *= 2.00;
    f64                          pdNatFreq = 0.2 * TWOPI;
    f64                          zdNatFreq = 0.5 * TWOPI;
    f64                          qdNatFreq = 2.0 * TWOPI;
    f64                          wdNatFreq = 5.0 * TWOPI;
    f64                          pdDotMax = 3.0;
    f64                          zdDotMax = 3.0;
    f64                          qdDotMax = 0.0;
    f64                          wdDotMax = 0.0;
    f64                          mMin = 3;
    f64                          mMax = 5;
    InertiaVec                   IMin(0.1, 0.1, 0.2);
    InertiaVec                   IMax(0.2, 0.2, 0.4);
    MultiRotorSmc::NoiseVec      maxDist; maxDist << 1.0, 1.0, 15.0, 0.6, 0.6, 0.6;

    MultiRotorSmc smc(&sys, pGainDynamics, pBoundary, pGainConverge,
                            zGains, qGain, wBoundary, wGainConverge,
                            pdNatFreq, zdNatFreq, qdNatFreq, wdNatFreq,
                            pdDotMax, zdDotMax, qdDotMax, wdDotMax,
                            mMin, mMax, IMin, IMax, maxDist);

    Matrix<f64, NUM_NOISE, NUM_NOISE> noiseCov; noiseCov.setZero();
    noiseCov(NOISEFX, NOISEFX) = 0.25 * (0.01*0.01);
    noiseCov(NOISEFY, NOISEFY) = 0.25 * (2.50*2.50);
    noiseCov(NOISEFZ, NOISEFZ) = 0.25 * (14.0*14.0);
    noiseCov(NOISEMX, NOISEMX) = 0.25 * (0.50*0.50);
    noiseCov(NOISEMY, NOISEMY) = 0.25 * (0.50*0.50);
    noiseCov(NOISEMZ, NOISEMZ) = 0.25 * (0.50*0.50);
    MultiRotorSys::NoiseVec w = gausNoise(noiseCov, true);

    f64 tmax = 40;
    f64 dt = 0.01;
    StatTimer timer(true, tmax/dt);
    StatTimer timerNoPrint(false, tmax/dt);
    for (f64 t = 0; t < tmax - TINY; t += dt) {
        // generate some noise
        if (t - (i32)(t) - 0.01 < 0) {
            w = gausNoise(noiseCov, true);
        }

        // change the setpoint
        smc.setSetptModeXyzt(true);
        MultiRotorSmc::SetptVec yd = generatePtSetpt(t);
        //smc.setSetptModeXyzt(false);
        //MultiRotorSmc::SetptVec yd = generateZqSetpt(t);

        // apply the control
        MultiRotorSys::InputVec u = smc.control(t, dt, yd);

        timerNoPrint.stop();

        // output and simulate
        sys.printCsv(cout, t, u, w, "SYS");
        smc.printCsv(cout, t, yd, "SMC");

        timerNoPrint.start();

        sys.integ(t, dt, u, w);

        timer.toc();
    }
    sys.printCsv(cout, tmax, MultiRotorSys::InputVec::Zero(),
                             MultiRotorSys::NoiseVec::Zero(), "SYS");

    timer.stopNoRecord();
    timerNoPrint.stopNoRecord();

    cerr << "Loop timer (with    prints): " << timer << endl;
    cerr << "Loop timer (without prints): " << timerNoPrint << endl;

    return 0;
}

MultiRotorSmc::SetptVec generateZqSetpt(f64 t)
{
    MultiRotorSmc::SetptVec yd = MultiRotorSmc::setptZero(false);

    // Z
    if (t < 2) {
        yd(MultiRotorSmc::ZQSETPTPZ) = 0;
    } else if (t < 5) {
        yd(MultiRotorSmc::ZQSETPTPZ) = 1;
    } else if (t < 15) {
        yd(MultiRotorSmc::ZQSETPTPZ) = 10;
    } else if (t < 25) {
        yd(MultiRotorSmc::ZQSETPTPZ) = 10 + std::sin(TWOPI/5 * t);
    } else {
        yd(MultiRotorSmc::ZQSETPTPZ) = 0;
    }
    // Q
    if (t < 2) {
        yd(MultiRotorSmc::ZQSETPTQW) = 1.0;
        yd(MultiRotorSmc::ZQSETPTQX) = 0.0;
        yd(MultiRotorSmc::ZQSETPTQY) = 0.0;
        yd(MultiRotorSmc::ZQSETPTQZ) = 0.0;
    } else if (t < 8) {
        yd(MultiRotorSmc::ZQSETPTQW) = std::cos(0.7/2);
        yd(MultiRotorSmc::ZQSETPTQX) = std::sqrt(3)/3*std::sin(0.7/2);
        yd(MultiRotorSmc::ZQSETPTQY) = std::sqrt(3)/3*std::sin(0.7/2);
        yd(MultiRotorSmc::ZQSETPTQZ) = std::sqrt(3)/3*std::sin(0.7/2);
    } else if (t < 10) {
        yd(MultiRotorSmc::ZQSETPTQW) = 1.0;
        yd(MultiRotorSmc::ZQSETPTQX) = 0.0;
        yd(MultiRotorSmc::ZQSETPTQY) = 0.0;
        yd(MultiRotorSmc::ZQSETPTQZ) = 0.0;
    } else if (t < 20) {
        f64 theta = toRad(60) * std::sin(TWOPI/5 * t);
        f64 phi   = toRad(30) * std::sin(TWOPI/4 * t);
        f64 psi   = toRad(45) * std::sin(TWOPI/3 * t);
        yd(MultiRotorSmc::ZQSETPTQW) = std::cos(theta/2);
        yd(MultiRotorSmc::ZQSETPTQX) = std::cos(psi)*std::cos(phi)*std::sin(theta/2);
        yd(MultiRotorSmc::ZQSETPTQY) = std::cos(psi)*std::sin(phi)*std::sin(theta/2);
        yd(MultiRotorSmc::ZQSETPTQZ) = std::sin(psi)*std::sin(theta/2);
    } else {
        yd(MultiRotorSmc::ZQSETPTQW) = 1.0;
        yd(MultiRotorSmc::ZQSETPTQX) = 0.0;
        yd(MultiRotorSmc::ZQSETPTQY) = 0.0;
        yd(MultiRotorSmc::ZQSETPTQZ) = 0.0;
    }

    return yd;
}

MultiRotorSmc::SetptVec generatePtSetpt(f64 t)
{
    MultiRotorSmc::SetptVec yd = MultiRotorSmc::setptZero(true);

    // XY
    if (t < 2) {
        yd(MultiRotorSmc::PTSETPTPX) = 0;
        yd(MultiRotorSmc::PTSETPTPY) = 0;
    } else if (t < 25) {
        yd(MultiRotorSmc::PTSETPTPX) = 1;
        yd(MultiRotorSmc::PTSETPTPY) = -1;
    } else {
        yd(MultiRotorSmc::PTSETPTPX) = 0;
        yd(MultiRotorSmc::PTSETPTPY) = 0;
    }
    // Z
    if (t < 2) {
        yd(MultiRotorSmc::PTSETPTPZ) = 0;
    } else if (t < 5) {
        yd(MultiRotorSmc::PTSETPTPZ) = 1;
    } else if (t < 15) {
        yd(MultiRotorSmc::PTSETPTPZ) = 10;
    } else if (t < 25) {
        yd(MultiRotorSmc::PTSETPTPZ) = 10 + std::sin(TWOPI/5 * t);
    } else {
        yd(MultiRotorSmc::PTSETPTPZ) = 0;
    }
    // Yaw
    if (t < 2) {
        yd(MultiRotorSmc::PTSETPTTZ) = 0;
    } else if (t < 25) {
        yd(MultiRotorSmc::PTSETPTTZ) = 1;
    } else {
        yd(MultiRotorSmc::PTSETPTTZ) = 0;
    }

    return yd;
}
