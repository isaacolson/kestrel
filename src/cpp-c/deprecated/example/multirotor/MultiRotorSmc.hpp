#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/PhysicsUtil.hpp"
#include "kestrel/util/Rotations.hpp"

#include "kestrel/control/Controller.hpp"
#include "kestrel/control/VelLimSecOrderLowpass.hpp"
#include "MultiRotorSys.hpp"

namespace Kestrel {
namespace MultiRotor {

class MultiRotorSmc: public Control::Controller<f64, 5, MultiRotorSys> {
  public:
    typedef Control::Controller<f64, 5, MultiRotorSys> ParentType;
    typedef SystemType::NoiseVec                       NoiseVec;

    enum ZQSetptIdx { // height - quaternion setpoints
        ZQSETPTPZ,
        ZQSETPTQW,
        ZQSETPTQX,
        ZQSETPTQY,
        ZQSETPTQZ,
        NUM_ZQSETPTZ=1,
        NUM_ZQSETPTQ=4,
    };
    enum PZTSetptIdx { // position - yaw setpoints
        PTSETPTPX,
        PTSETPTPY,
        PTSETPTPZ,
        PTSETPTTZ,
        PTSETPTUNUSED,
        NUM_PTSETPTP=2,
        NUM_PTSETPTZ=1,
        NUM_PTSETPTT=1,
    };
    // Note: this seems a bit like doubling up enums, but the above is meant to index the
    //       5 element setpt vector of the controller while the below index the internal
    //       setpt vectors used by the independent sections of the control.
    enum PSetptIdx {
        PSETPTPX,
        PSETPTPY,
        NUM_PSETPT=2,
    };
    enum ZSetptIdx {
        ZSETPTPZ,
        NUM_ZSETPT=1,
    };
    enum TSetptIdx {
        TSETPTTZ,
        NUM_TSETPT=1,
    };
    enum QSetptIdx {
        QSETPTQW,
        QSETPTQX,
        QSETPTQY,
        QSETPTQZ,
        NUM_QSETPT=4,
    };
    enum QErrorIdx {
        QERRORQX,
        QERRORQY,
        QERRORQZ,
        NUM_QERROR=3,
    };
    enum WSetptIdx {
        WSETPTWX,
        WSETPTWY,
        WSETPTWZ,
        NUM_WSETPT=3,
    };
    typedef Eigen::Matrix<Scalar, NUM_PSETPT, 1>               PSetptVec;
    typedef Eigen::Matrix<Scalar, NUM_ZSETPT, 1>               ZSetptVec;
    typedef Eigen::Matrix<Scalar, NUM_TSETPT, 1>               TSetptVec;
    typedef Eigen::Matrix<Scalar, NUM_QSETPT, 1>               QSetptVec;
    typedef Eigen::Matrix<Scalar, NUM_QERROR, 1>               QErrorVec;
    typedef Eigen::Matrix<Scalar, NUM_WSETPT, 1>               WSetptVec;

    enum ZGainsIdx {
        LAMBDAZ,
        PHIZ,
        ETAZ,
        NUM_ZGAINS=3,
    };
    typedef Eigen::Matrix<Scalar, NUM_PSETPT, NUM_PSETPT>      PGainMatrix; // Used for xy
    typedef Eigen::Matrix<Scalar, NUM_ZGAINS, 1>               ZGainList;   // Used for z
    typedef Eigen::Matrix<Scalar, NUM_QERROR, NUM_QERROR>      QGainMatrix; // Used for quat
    typedef Eigen::Matrix<Scalar, NUM_WSETPT, NUM_WSETPT>      WGainMatrix; // Used for angvel

    typedef Control::VelLimSecOrderLowpass<Scalar, NUM_PSETPT> PFilterType;
    typedef Control::VelLimSecOrderLowpass<Scalar, NUM_ZSETPT> ZFilterType;
    typedef Control::VelLimSecOrderLowpass<Scalar, NUM_QSETPT> QFilterType;
    typedef Control::VelLimSecOrderLowpass<Scalar, NUM_WSETPT> WFilterType;

  protected:
    PGainMatrix pGainDynamics;
    PGainMatrix pGainBoundary;
    PGainMatrix pGainConverge;
    ZGainList   zGains;
    QGainMatrix qGain;
    WGainMatrix wGainBoundary;
    WGainMatrix wGainConverge;

    PFilterType pdFilt;
    ZFilterType zdFilt;
    QFilterType qdFilt;
    WFilterType wdFilt;

    f64 mEst;  ///< Geometric mean of min/max mass
    f64 dmMin; ///< mMin = dmMin * mEst
    f64 dmMax; ///< mMax = dmMax * mEst

    InertiaVec IEst;  ///< Geometric mean of min/max Inertia
    InertiaVec dIMin; ///< IMin = dIMin .* IEst
    InertiaVec dIMax; ///< IMax = dIMax .* IEst

    NoiseVec maxDist; ///< Maximum disturbance magnitude (elts >= 0)

    bool setptModeXyzt;

  public:
    /// Ctor (note the boundary argument for the angvel control will be inverted)
    MultiRotorSmc(const MultiRotorSys* sys           = nullptr,
                  const PGainMatrix&   pGainDynamics = PGainMatrix::Identity(),
                  const PGainMatrix&   pBoundary     = PGainMatrix::Identity(),
                  const PGainMatrix&   pGainConverge = PGainMatrix::Identity(),
                  const ZGainList&     zGains        = ZGainList::Ones(),
                  const QGainMatrix&   qGain         = QGainMatrix::Identity(),
                  const WGainMatrix&   wBoundary     = WGainMatrix::Identity(),
                  const WGainMatrix&   wGainConverge = WGainMatrix::Identity(),
                  f64                  pdNatFreq     = 1,
                  f64                  zdNatFreq     = 1,
                  f64                  qdNatFreq     = 1,
                  f64                  wdNatFreq     = 1,
                  f64                  pdDotMax      = 0,
                  f64                  zdDotMax      = 0,
                  f64                  qdDotMax      = 0,
                  f64                  wdDotMax      = 0,
                  f64                  mMin          = 1,
                  f64                  mMax          = 1,
                  InertiaVec           IMin          = InertiaVec::Ones(),
                  InertiaVec           IMax          = InertiaVec::Ones(),
                  NoiseVec             maxDist       = NoiseVec::Zero() ) :
        ParentType(sys),
        pGainDynamics(pGainDynamics), pGainConverge(pGainConverge),
        zGains(zGains), qGain(qGain), wGainConverge(wGainConverge),
        pdFilt(pdNatFreq, 1.0, pdDotMax, 4, true),
        zdFilt(zdNatFreq, 1.0, zdDotMax, 4, true),
        qdFilt(qdNatFreq, 1.0, qdDotMax, 4, true),
        wdFilt(wdNatFreq, 1.0, wdDotMax, 4, true),
        maxDist(maxDist), setptModeXyzt(false)
    {
        Assert(fabs(pBoundary.determinant()) > MathUtil::TINY*MathUtil::TINY,
               "Boundary matrix is not invertible");
        pGainBoundary = pBoundary.inverse();

        Assert(zGains(PHIZ) > MathUtil::TINY, "Boundary must be > 0");

        Assert(fabs(wBoundary.determinant()) > MathUtil::TINY*MathUtil::TINY*MathUtil::TINY,
               "Boundary matrix is not invertible");
        wGainBoundary = wBoundary.inverse();

        Assert(mMin > 0 && mMax > 0, "Mass cannot be negative");
        Assert(mMin <= mMax, "Improper mass ordering");

        mEst  = std::sqrt(mMin * mMax);
        dmMin = std::sqrt(mMin / mMax);
        dmMax = std::sqrt(mMax / mMin);

        for (int i = 0; i < NUM_INERTIA; ++i) {
            Assert(IMin(i) > 0 && IMax(i) > 0, "Inertia cannot be negative");
            Assert(IMin(i) <= IMax(i), "Improper Inertia ordering");
        }
        IEst  = (IMin.cwiseProduct( IMax)).cwiseSqrt();
        dIMin = (IMin.cwiseQuotient(IMax)).cwiseSqrt();
        dIMax = (IMax.cwiseQuotient(IMin)).cwiseSqrt();
    }
    /// Dtor
    virtual ~MultiRotorSmc() {}

    bool getSetptModeXyzt() const { return setptModeXyzt; }
    void setSetptModeXyzt(bool setptModeXyzt) { this->setptModeXyzt = setptModeXyzt; }

    static SetptVec setptZero(bool setptModeXyzt = false)
    {
        SetptVec ret = SetptVec::Zero();
        if (!setptModeXyzt) {
            ret.block<NUM_ZQSETPTQ, 1>(ZQSETPTQW, 0) = Rotations::quatVecIdentity<Scalar>();
        }
        return ret;
    }

    /// Returns the control input given a state and a setpoint
    using ParentType::control;
    virtual InputVec control(const StateVec& x, f64 t, f64 dt, const SetptVec& yd)
    {
        InputVec u = InputVec::Zero();

        PSetptVec pdbar = PSetptVec::Zero();
        TSetptVec tdbar = TSetptVec::Zero();
        ZSetptVec zdbar = ZSetptVec::Zero();
        QSetptVec qdbar = QSetptVec::Zero();

        if (setptModeXyzt) {
            pdbar = yd.block<NUM_PTSETPTP, 1>(PTSETPTPX, 0);
            zdbar = yd.block<NUM_PTSETPTZ, 1>(PTSETPTPZ, 0);
            tdbar = yd.block<NUM_PTSETPTT, 1>(PTSETPTTZ, 0);
        } else {
            zdbar = yd.block<NUM_ZQSETPTZ, 1>(ZQSETPTPZ, 0);
            qdbar = yd.block<NUM_ZQSETPTQ, 1>(ZQSETPTQW, 0);
        }

        ZSetptVec Fz = zControl(x, t, dt, zdbar);

        PSetptVec Fxy = PSetptVec::Zero();
        if (setptModeXyzt) {
            Fxy   = ptControl(x, t, dt, pdbar);
            qdbar = quatFromForces(Fxy, Fz, tdbar);
        }

        WSetptVec wdbar = qControl(x, t, dt, qdbar);

        u.block<NUM_LIFT, 1>(LIFT, 0)      = liftFromFz(x, Fz);
        u.block<NUM_MOMENT, 1>(MOMENTX, 0) = wControl(x, t, dt, wdbar);

        return u;
    }

    // Returns the Z force necessary to control z
    ZSetptVec zControl(const StateVec& x, f64 t, f64 dt, const ZSetptVec& zdbar)
    {
        ZSetptVec z    = x.block<NUM_ZSETPT, 1>(POSZ, 0);
        ZSetptVec zdot = x.block<NUM_ZSETPT, 1>(VELZ, 0);

        // Do setpoint filtering for zd
        ZSetptVec zd = zdbar;
        static bool zFirst = true;
        if (zFirst) {
            ZFilterType::OutputVec zInit = ZFilterType::OutputVec::Zero();
            zInit.block<NUM_ZSETPT, 1>(0, 0) = z;
            zInit.block<NUM_ZSETPT, 1>(0, 1) = zdot;
            zdFilt.setOutput(zInit);

            zFirst = false;
        }
        ZSetptVec zddot  = ZSetptVec::Zero();
        ZSetptVec zddot2 = ZSetptVec::Zero();
        if (zdFilt.getDenom()(0, 0) > 0) {
            zdFilt.filter(t, dt, zdbar);
            zd     = zdFilt.getOutput().block<NUM_ZSETPT, 1>(0, 0);
            zddot  = zdFilt.getOutput().block<NUM_ZSETPT, 1>(0, 1);
            zddot2 = zdFilt.getLastDeriv().block<NUM_ZSETPT, 1>(0, 1);
        }

        // Calculate Error
        ZSetptVec ze    = zd    - z;
        ZSetptVec zedot = zddot - zdot;
        ZSetptVec Sz    = zedot + zGains(LAMBDAZ)*ze;

        // Account for uncertainty
        ZSetptVec g; g(0) = PhysicsUtil::GRAVITY;
        ZSetptVec zUncertainty = (1 - 1/dmMax)*(g + zddot2 + zGains(LAMBDAZ)*zedot).cwiseAbs()
                                 + maxDist.block<NUM_ZSETPT, 1>(NOISEFZ, 0) / (mEst*dmMin);
        ZSetptVec kz = dmMax * (zUncertainty + ZSetptVec::Constant(zGains(ETAZ)));

        ZSetptVec Fz = mEst * (g + zddot2 + zGains(LAMBDAZ)*zedot + kz*Sz/zGains(PHIZ));

        // Note: Have to at least limit the z controller from being below a minimum to prevent the
        //       vehicle from wanting to flip over
        if (Fz(0) < 0.1*g(0)*mEst) Fz(0) = 0.1*g(0)*mEst;

        return Fz;
    }

    // Compute the lift force from the desired Z force
    ZSetptVec liftFromFz(const StateVec& x, const ZSetptVec& Fz)
    {
        QSetptVec q    = x.block<NUM_QSETPT, 1>(QUATW, 0);

        // TODO: add special logic if rotation is too great
        Rotations::RotationMatrix<Scalar> Rba = Rotations::quatToMat(q);

        ZSetptVec Fl = Fz / Rba(2,2);

        // Some actuator limits for sanity
        ZSetptVec g; g(0) = PhysicsUtil::GRAVITY;
        if (Fl(0) > 3.0*g(0)*mEst) Fl(0) = 3.0*g(0)*mEst;
        if (Fl(0) < 0.1*g(0)*mEst) Fl(0) = 0.1*g(0)*mEst;

        return Fl;
    }

    PSetptVec ptControl(const StateVec& x, f64 t, f64 dt, const PSetptVec& pdbar)
    {
        PSetptVec p    = x.block<NUM_PSETPT, 1>(POSX, 0);
        PSetptVec pdot = x.block<NUM_PSETPT, 1>(VELX, 0);

        // Do setpoint filtering for pd
        PSetptVec pd = pdbar;
        static bool pFirst = true;
        if (pFirst) {
            PFilterType::OutputVec pInit = PFilterType::OutputVec::Zero();
            pInit.block<NUM_PSETPT, 1>(0, 0) = p;
            pInit.block<NUM_PSETPT, 1>(0, 1) = pdot;
            pdFilt.setOutput(pInit);

            pFirst = false;
        }
        PSetptVec pddot  = PSetptVec::Zero();
        PSetptVec pddot2 = PSetptVec::Zero();
        if (pdFilt.getDenom()(0, 0) > 0) {
            pdFilt.filter(t, dt, pdbar);
            pd     = pdFilt.getOutput().block<NUM_PSETPT, 1>(0, 0);
            pddot  = pdFilt.getOutput().block<NUM_PSETPT, 1>(0, 1);
            pddot2 = pdFilt.getLastDeriv().block<NUM_PSETPT, 1>(0, 1);
        }
        pddot2.setZero();

        // Compute error
        PSetptVec pe    = pd - p;
        PSetptVec pedot = pddot - pdot;
        PSetptVec Sp    = pedot + pGainDynamics * pe;

        // Account for uncertainty
        PGainMatrix pUncertainty = PGainMatrix::Zero();
        pUncertainty.diagonal()  = (1.0 - 1.0/dmMax) * (pddot2 + pGainDynamics * pedot).cwiseAbs()
                                   + maxDist.block<NUM_PSETPT, 1>(NOISEFX, 0);
        PGainMatrix kp           = dmMax * (pUncertainty + pGainConverge);
        PSetptVec   Fxy          = mEst * (pddot2 + pGainDynamics * pedot
                                           + kp * (pGainBoundary * Sp));

        return Fxy;
    }

    // Computes the desired quaternion from the input forces and yaw
    QSetptVec quatFromForces(const PSetptVec& Fxy, const ZSetptVec& Fz, const TSetptVec& yaw)
    {
        QSetptVec qdbar = Rotations::quatVecIdentity<Scalar>();

        // TODO: if this is too big, limit the xy force here.
        // TODO: though this should never be 0 in normal flight, should have a guard here
        Scalar Fl = sqrt(Fxy.squaredNorm() + Fz.squaredNorm());

        VelDotVec FlZonly{0, 0, Fl};
        VelDotVec FlXyz{Fxy(0), Fxy(1), Fz(0)};

        Scalar FlMax = 3.0*PhysicsUtil::GRAVITY*mEst;
        if (Fl == 0) {
            return qdbar;
        } else if (Fl > FlMax) {
            FlZonly(2) = FlMax;
            FlXyz *= FlMax / Fl;
        }

        Rotations::Quaternion<Scalar> qYaw(
            Rotations::AngleAxis<Scalar>(yaw(0), Eigen::Vector3d::UnitZ()));

        Rotations::Quaternion<Scalar> qPhi;
        qPhi.setFromTwoVectors(FlZonly, FlXyz);

        qdbar = Rotations::toVec(qPhi * qYaw);

        return qdbar;
    }

    // Returns the angular velocity necessary to control q
    WSetptVec qControl(const StateVec& x, f64 t, f64 dt, const QSetptVec& qdbar)
    {
        QSetptVec q = x.block<NUM_QSETPT, 1>(QUATW, 0);
        WSetptVec w = x.block<NUM_WSETPT, 1>(ANGVX, 0);

        // Do setpoint filtering for qd
        QSetptVec qd = qdbar;
        static bool qFirst = true;
        if (qFirst) {
            QFilterType::OutputVec qInit = QFilterType::OutputVec::Zero();
            qInit.block<NUM_QSETPT, 1>(0, 0) = q;
            qInit.block<NUM_QSETPT, 1>(0, 1) = Rotations::quatRotDeriv(q, w);
            qdFilt.setOutput(qInit);

            qFirst = false;
        }
        QSetptVec qddot  = QSetptVec::Zero();
        QSetptVec qddot2 = QSetptVec::Zero();
        if (qdFilt.getDenom()(0, 0) > 0) {
            qdFilt.filter(t, dt, qdbar);
            qd     = qdFilt.getOutput().block<NUM_QSETPT, 1>(0, 0);
            qddot  = qdFilt.getOutput().block<NUM_QSETPT, 1>(0, 1);
            qddot2 = qdFilt.getLastDeriv().block<NUM_QSETPT, 1>(0, 1);
        }

        // Calculate error
        // x * qe = yd -> qe = xinv * yd
        QSetptVec qinv = Rotations::quatInv(q);
        Rotations::QuatCompMat<Scalar> QLqinv = Rotations::quatCompLmat(qinv);
        Rotations::QuatCompMat<Scalar> QRqd   = Rotations::quatCompRmat(qd);
        QSetptVec qe = QRqd * qinv; // = QLqinv * qd
        // TODO: special logic if qe(0) = 0 (ie the rotation angle is PI)
        QErrorVec Sq = qe.block<NUM_QERROR, 1>(QSETPTQX,0);

        // Calculate mapping of input to error
        Rotations::QuatDotJacAngVel<Scalar> omegaToQdot = Rotations::quatRotDerivJacAngVel(q);
        Eigen::Matrix<Scalar, NUM_QUAT, NUM_QUAT> qdotToQinvdot;
        qdotToQinvdot.setIdentity();
        qdotToQinvdot(1,1) = qdotToQinvdot(2,2) = qdotToQinvdot(3,3) = -1;
        Rotations::QuatDotJacAngVel<Scalar> omegaToQinvdot = qdotToQinvdot * omegaToQdot;
        Eigen::Matrix<Scalar, NUM_WSETPT, NUM_QERROR> inputToError =
            QRqd.template block<NUM_WSETPT, NUM_QUAT>(QSETPTQX, QSETPTQW) * omegaToQinvdot;
        // This matrix will always be invertible if qe(0) != 0

        WSetptVec wdbar = inputToError.inverse()
                          * (- QLqinv.template block<NUM_WSETPT, NUM_QSETPT>(QSETPTQX, QSETPTQW)
                               * qddot
                             - qGain * Sq);

        return wdbar;
    }

    // Returns the moments necessary to control w
    WSetptVec wControl(const StateVec& x, f64 t, f64 dt, const WSetptVec& wdbar)
    {
        WSetptVec w = x.block<NUM_WSETPT, 1>(ANGVX, 0);

        // Do setpoint filtering for wd
        WSetptVec wd = wdbar;
        static bool wFirst = true;
        if (wFirst) {
            WFilterType::OutputVec wInit = WFilterType::OutputVec::Zero();
            wInit.block<NUM_WSETPT, 1>(0,0) = w;
            wdFilt.setOutput(wInit);

            wFirst = false;
        }
        WSetptVec wddot  = WSetptVec::Zero();
        WSetptVec wddot2 = WSetptVec::Zero();
        if (wdFilt.getDenom()(0, 0) > 0) {
            wdFilt.filter(t, dt, wd);
            wd     = wdFilt.getOutput().block<NUM_WSETPT, 1>(0, 0);
            wddot  = wdFilt.getOutput().block<NUM_WSETPT, 1>(0, 1);
            wddot2 = wdFilt.getLastDeriv().block<NUM_WSETPT, 1>(0, 1);
        }

        // TODO: can probably precalculate many of these matrices and inverses as long as we
        //       aren't adapting the inertia estimates
        // Calculate error and best-estimate terms
        WSetptVec  Sw    = wd - w;
        MomentMult GwEst = SystemType::derivAngVelG(x, IEst);
        WSetptVec  FwEst = SystemType::derivAngVelF(x, IEst);

        // Account for uncertainty
        WSetptVec  dmaxFw = calcDmaxFw(x, IEst, dIMin, dIMax);
        MomentMult dminGw = SystemType::derivAngVelG(x, dIMax); // using dIMax here is correct
        MomentMult dmaxGw = SystemType::derivAngVelG(x, dIMin); // using dIMin here is correct

        WGainMatrix wUncertainty = WGainMatrix::Zero();
        wUncertainty.diagonal() = (MomentMult::Identity() - dminGw) * (wddot - FwEst).cwiseAbs()
                                  + dmaxFw
                                  + dmaxGw * GwEst * maxDist.block<NUM_NOISEM, 1>(NOISEMX, 0);
        WGainMatrix kw = dminGw.inverse() * (wUncertainty + wGainConverge);

        WSetptVec M = GwEst.inverse() * (wddot - FwEst + kw*(wGainBoundary*Sw));
        return M;
    }

    inline WSetptVec calcDmaxFw(const StateVec& x, const InertiaVec& est,
                                const InertiaVec& minD, const InertiaVec& maxD)
    {
        WSetptVec ret;
        Eigen::Matrix<f64, 4, 1> opts;

        opts(0) = est(IZZ) - est(IYY) + (est(IYY)*maxD(IYY) - est(IZZ)*minD(IZZ))/minD(IXX);
        opts(1) = est(IZZ) - est(IYY) + (est(IYY)*minD(IYY) - est(IZZ)*maxD(IZZ))/minD(IXX);
        opts(2) = est(IZZ) - est(IYY) + (est(IYY)*maxD(IYY) - est(IZZ)*minD(IZZ))/maxD(IXX);
        opts(3) = est(IZZ) - est(IYY) + (est(IYY)*minD(IYY) - est(IZZ)*maxD(IZZ))/maxD(IXX);
        ret(0) = opts.cwiseAbs().maxCoeff() / est(IXX) * fabs(x(ANGVY)*x(ANGVZ));

        opts(0) = est(IXX) - est(IZZ) + (est(IZZ)*maxD(IZZ) - est(IXX)*minD(IXX))/minD(IYY);
        opts(1) = est(IXX) - est(IZZ) + (est(IZZ)*minD(IZZ) - est(IXX)*maxD(IXX))/minD(IYY);
        opts(2) = est(IXX) - est(IZZ) + (est(IZZ)*maxD(IZZ) - est(IXX)*minD(IXX))/maxD(IYY);
        opts(3) = est(IXX) - est(IZZ) + (est(IZZ)*minD(IZZ) - est(IXX)*maxD(IXX))/maxD(IYY);
        ret(1) = opts.cwiseAbs().maxCoeff() / est(IYY) * fabs(x(ANGVX)*x(ANGVZ));

        opts(0) = est(IYY) - est(IXX) + (est(IXX)*maxD(IXX) - est(IYY)*minD(IYY))/minD(IZZ);
        opts(1) = est(IYY) - est(IXX) + (est(IXX)*minD(IXX) - est(IYY)*maxD(IYY))/minD(IZZ);
        opts(2) = est(IYY) - est(IXX) + (est(IXX)*maxD(IXX) - est(IYY)*minD(IYY))/maxD(IZZ);
        opts(3) = est(IYY) - est(IXX) + (est(IXX)*minD(IXX) - est(IYY)*maxD(IYY))/maxD(IZZ);
        ret(2) = opts.cwiseAbs().maxCoeff() / est(IZZ) * fabs(x(ANGVX)*x(ANGVY));

        return ret;
    }

    /// Prints data in CSV form to the given ostream with given leader string,
    ///   deliminator, and line terminator.
    //    Format: leader, time, state, setpt,
    //            zd, zdFilt, zddotFilt, zddot2Filt,
    //            qd, qdFilt, qddotFilt, zddot2Filt,
    //            wd, wdFilt, wddotFilt, wddot2Filt,
    /// Note that there WILL be a trailing deliminator on each line.
    /// Passing in term = '\0' will skip terminating character output
    /// Passing in Leader = empty string will skip leader output
    using ParentType::printCsv;
    virtual void printCsv(std::ostream& os, const StateVec& x, f64 t,
                                            const SetptVec& yd = SetptVec::Zero(),
                                            const std::string& leader = "",
                                            const std::string& delim = ",",
                                            const std::string& term = "\n") const
    {
        ParentType::printCsv(os, x, t, yd, leader, delim, "");

        PFilterType::OutputVec pd = pdFilt.getOutput();
        PFilterType::InputVec  pdLastInput = pdFilt.getLastInput();
        PFilterType::OutputVec pdLastDeriv = pdFilt.getLastDeriv();
        os << "pd" << delim;
        for (int i = 0; i < NUM_PSETPT; ++i) { os << pdLastInput(i,0) << delim; }
        os << "pdFilt" << delim;
        for (int i = 0; i < NUM_PSETPT; ++i) { os << pd(i,0) << delim; }
        os << "pddotFilt" << delim;
        for (int i = 0; i < NUM_PSETPT; ++i) { os << pd(i,1) << delim; }
        os << "pddot2Filt" << delim;
        for (int i = 0; i < NUM_PSETPT; ++i) { os << pdLastDeriv(i,1) << delim; }

        ZFilterType::OutputVec zd = zdFilt.getOutput();
        ZFilterType::InputVec  zdLastInput = zdFilt.getLastInput();
        ZFilterType::OutputVec zdLastDeriv = zdFilt.getLastDeriv();
        os << "zd" << delim;
        for (int i = 0; i < NUM_ZSETPT; ++i) { os << zdLastInput(i,0) << delim; }
        os << "zdFilt" << delim;
        for (int i = 0; i < NUM_ZSETPT; ++i) { os << zd(i,0) << delim; }
        os << "zddotFilt" << delim;
        for (int i = 0; i < NUM_ZSETPT; ++i) { os << zd(i,1) << delim; }
        os << "zddot2Filt" << delim;
        for (int i = 0; i < NUM_ZSETPT; ++i) { os << zdLastDeriv(i,1) << delim; }

        QFilterType::OutputVec qd = qdFilt.getOutput();
        QFilterType::InputVec  qdLastInput = qdFilt.getLastInput();
        QFilterType::OutputVec qdLastDeriv = qdFilt.getLastDeriv();
        os << "qd" << delim;
        for (int i = 0; i < NUM_QSETPT; ++i) { os << qdLastInput(i,0) << delim; }
        os << "qdFilt" << delim;
        for (int i = 0; i < NUM_QSETPT; ++i) { os << qd(i,0) << delim; }
        os << "qddotFilt" << delim;
        for (int i = 0; i < NUM_QSETPT; ++i) { os << qd(i,1) << delim; }
        os << "qddot2Filt" << delim;
        for (int i = 0; i < NUM_QSETPT; ++i) { os << qdLastDeriv(i,1) << delim; }

        WFilterType::OutputVec wd = wdFilt.getOutput();
        WFilterType::InputVec  wdLastInput = wdFilt.getLastInput();
        WFilterType::OutputVec wdLastDeriv = wdFilt.getLastDeriv();
        os << "wd" << delim;
        for (int i = 0; i < NUM_WSETPT; ++i) { os << wdLastInput(i,0) << delim; }
        os << "wdFilt" << delim;
        for (int i = 0; i < NUM_WSETPT; ++i) { os << wd(i,0) << delim; }
        os << "wddotFilt" << delim;
        for (int i = 0; i < NUM_WSETPT; ++i) { os << wd(i,1) << delim; }
        os << "wddot2Filt" << delim;
        for (int i = 0; i < NUM_WSETPT; ++i) { os << wdLastDeriv(i,1) << delim; }

        if (!term.empty()) os << term;
    }

    /// Swap
    inline void swap(MultiRotorSmc& o)
    {
        ParentType::swap(o);
        pGainDynamics.swap(o.pGainDynamics);
        pGainBoundary.swap(o.pGainBoundary);
        pGainConverge.swap(o.pGainConverge);
        zGains.swap(o.zGains);
        qGain.swap(o.qGain);
        wGainBoundary.swap(o.wGainBoundary);
        wGainConverge.swap(o.wGainConverge);
        pdFilt.swap(o.pdFilt);
        zdFilt.swap(o.zdFilt);
        qdFilt.swap(o.qdFilt);
        wdFilt.swap(o.wdFilt);
        std::swap(mEst, o.mEst);
        std::swap(dmMin, o.dmMin);
        std::swap(dmMax, o.dmMax);
        IEst.swap(o.IEst);
        dIMin.swap(o.dIMin);
        dIMax.swap(o.dIMax);
        maxDist.swap(o.maxDist);
        std::swap(setptModeXyzt, o.setptModeXyzt);
    }
    /// Copy Ctor
    MultiRotorSmc(const MultiRotorSmc& o) :
        ParentType(o),
        pGainDynamics(o.pGainDynamics),
        pGainBoundary(o.pGainBoundary),
        pGainConverge(o.pGainConverge),
        zGains(o.zGains),
        qGain(o.qGain),
        wGainBoundary(o.wGainBoundary),
        wGainConverge(o.wGainConverge),
        pdFilt(o.pdFilt),
        zdFilt(o.zdFilt),
        qdFilt(o.qdFilt),
        wdFilt(o.wdFilt),
        mEst(o.mEst),
        dmMin(o.dmMin),
        dmMax(o.dmMax),
        IEst(o.IEst),
        dIMin(o.dIMin),
        dIMax(o.dIMax),
        maxDist(o.maxDist),
        setptModeXyzt(o.setptModeXyzt)
    {}
    /// Move Ctor
    MultiRotorSmc(MultiRotorSmc&& o) { swap(o); }
    /// Unifying Assignment
    MultiRotorSmc& operator=(MultiRotorSmc o) { swap(o); return *this; }
};

} /* MultiRotor */
} /* Kestrel */
