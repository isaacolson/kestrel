#include <iostream>

#include "kestrel/util/types.hpp"
#include "kestrel/util/assert.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TimeUtil.hpp"
#include "kestrel/control/ObserverSystem.hpp"

using namespace Kestrel;
using Control::ObserverSystem;

class SimExample: public ObserverSystem<f64, 4, 2, 5> {
  public:
    // Modeling a simple wheeled robot system
    enum StateIdx {
        XPOSE,
        YPOSE,
        STEER,
        YAW,
        NUM_STATES
    };
    enum InputIdx {
        DRIVE_VEL,
        STEER_VEL,
        NUM_INPUTS
    };
    enum NoiseIdx {
        XPOSE_NOISE,
        YPOSE_NOISE,
        YAW_NOISE,
        DRIVE_VEL_NOISE,
        STEER_VEL_NOISE,
        NUM_NOISES
    };

    typedef ObserverSystem<f64, NUM_STATES, NUM_INPUTS, NUM_NOISES> ParentType;

    enum XyObsIdx {
        XOBS,
        YOBS,
        NUM_XYOBS
    };

    typedef Eigen::Matrix<f64, NUM_XYOBS, 1>          XyObsVec;
    typedef Eigen::Matrix<f64, NUM_XYOBS, NUM_XYOBS>  XyObsCov;
    typedef Eigen::Matrix<f64, NUM_XYOBS, NUM_STATES> XyObsJac;

    enum YawObsIdx {
        YAWOBS,
        NUM_YAWOBS
    };

    typedef Eigen::Matrix<f64, NUM_YAWOBS, 1>          YawObsVec;
    typedef Eigen::Matrix<f64, NUM_YAWOBS, NUM_YAWOBS> YawObsCov;
    typedef Eigen::Matrix<f64, NUM_YAWOBS, NUM_STATES> YawObsJac;

  public:
    /// Ctor
    SimExample(const StateVec& x = StateVec::Zero(), const StateCov& P = StateCov::Identity(),
               sz order = 1) : ParentType(x, P, order) {}
    /// Dtor
    virtual ~SimExample() {}

    /***************************************************************************/
    /* Core System Impl ********************************************************/
    /***************************************************************************/

    /// The derivative of the state vector given the state, the time, input, and noise
    using ParentType::deriv;
    virtual StateVec deriv(const StateVec& x, f64 t = 0.0,
                           const InputVec& u = InputVec::Zero(),
                           const NoiseVec& w = NoiseVec::Zero()) const
    {
        f64 drive_vel = u(DRIVE_VEL) + w(DRIVE_VEL_NOISE);
        f64 steer_vel = u(STEER_VEL) + w(STEER_VEL_NOISE);
        return StateVec(cos(x(YAW))*cos(x(STEER))*drive_vel + w(XPOSE_NOISE),
                        sin(x(YAW))*cos(x(STEER))*drive_vel + w(YPOSE_NOISE),
                        steer_vel,
                        sin(x(STEER))*drive_vel + w(YAW_NOISE));
    }

    /// The jacobian of the derivative function with respect to the state vector
    using ParentType::derivJac;
    virtual StateJac derivJac(const StateVec& x, f64 t = 0.0,
                              const InputVec& u = InputVec::Zero(),
                              const NoiseVec& w = NoiseVec::Zero()) const
    {
        StateJac F = StateJac::Zero();
        // Partials of cos(x(YAW))*cos(x(STEER))*u(DRIVE_VEL)
        F(XPOSE, YAW)   = -sin(x(YAW))*cos(x(STEER))*(u(DRIVE_VEL) + w(DRIVE_VEL_NOISE));
        F(XPOSE, STEER) = -cos(x(YAW))*sin(x(STEER))*(u(DRIVE_VEL) + w(DRIVE_VEL_NOISE));
        // Partials of sin(x(YAW))*cos(x(STEER))*u(DRIVE_VEL)
        F(YPOSE, YAW)   =  cos(x(YAW))*cos(x(STEER))*(u(DRIVE_VEL) + w(DRIVE_VEL_NOISE));
        F(YPOSE, STEER) = -sin(x(YAW))*sin(x(STEER))*(u(DRIVE_VEL) + w(DRIVE_VEL_NOISE));
        // Partials of u(STEER_VEL)
        // Partials of sin(x(STEER))*u(DRIVE_VEL)
        F(YAW, STEER) = cos(x(STEER))*(u(DRIVE_VEL) + w(DRIVE_VEL_NOISE));

        return F;
    }

    /// The noise covariance matrix
    using ParentType::noiseCov;
    virtual NoiseCov noiseCov(const StateVec& x, f64 t = 0.0,
                              const InputVec& u = InputVec::Zero(),
                              const NoiseVec& w = NoiseVec::Zero()) const
    {
        NoiseCov Q = NoiseCov::Zero();
        // Pick 2 sigma bounds: var = (2*sigma)^2 / 4
        Q(XPOSE_NOISE,     XPOSE_NOISE)     = 0.25 * (0.005 * 0.005); // m / s
        Q(YPOSE_NOISE,     YPOSE_NOISE)     = 0.25 * (0.005 * 0.005); // m / s
        Q(YAW_NOISE,       YAW_NOISE)       = 0.25 * (0.005 * 0.005); // rad / s
        Q(DRIVE_VEL_NOISE, DRIVE_VEL_NOISE) = 0.25 * (0.010 * 0.010); // m / s^2
        Q(STEER_VEL_NOISE, STEER_VEL_NOISE) = 0.25 * (0.200 * 0.200); // rad / s
        return Q;
    }

    /// The jacobian of the derivative function with respect to the noise vector
    using ParentType::noiseJac;
    virtual NoiseJac noiseJac(const StateVec& x, f64 t = 0.0,
                              const InputVec& u = InputVec::Zero(),
                              const NoiseVec& w = NoiseVec::Zero()) const
    {
        NoiseJac L = NoiseJac::Zero();
        L(XPOSE, XPOSE_NOISE)     = 1;
        L(XPOSE, DRIVE_VEL_NOISE) = cos(x(YAW))*cos(x(STEER));
        L(YPOSE, YPOSE_NOISE)     = 1;
        L(YPOSE, DRIVE_VEL_NOISE) = sin(x(YAW))*cos(x(STEER));
        L(STEER, STEER_VEL_NOISE) = 1;
        L(YAW,   YAW_NOISE)       = 1;
        return L;
    }

    /// Correct the state vector for domain restricted values (such as angles that wrap around)
    using ParentType::correctDomain;
    virtual StateVec& correctDomain(StateVec& x) const
    {
        x(STEER) = MathUtil::radToPiPi(x(STEER));
        x(YAW)   = MathUtil::radToPiPi(x(YAW));
        return x;
    }

    XyObsVec xyObsVec(const StateVec& x) const
    {
        XyObsVec h;
        h(XOBS) = x(XPOSE);
        h(YOBS) = x(YPOSE);
        return h;
    }

    XyObsCov xyObsCov() const
    {
        XyObsCov R = XyObsCov::Zero();
        // Pick 2 sigma bounds: var = (2*sigma)^2 / 4
        R(XOBS, XOBS) = 0.25 * (0.25 * 0.25);
        R(YOBS, YOBS) = 0.25 * (0.25 * 0.25);
        return R;
    }

    XyObsJac xyObsJac() const
    {
        XyObsJac H = XyObsJac::Zero();
        H(XOBS, XPOSE) = 1.0;
        H(YOBS, YPOSE) = 1.0;
        return H;
    }

    YawObsVec yawObsVec(const StateVec& x) const
    {
        // This function is used to generate the sim's yaw observations, so no wraparound handling
        YawObsVec h;
        h(YAWOBS) = x(YAW);
        return h;
    }

    YawObsVec yawObsVec(const StateVec& x, const YawObsVec& z) const
    {
        // requires knowledge of the measurement so we can account for wrap around
        YawObsVec h;
        h(YAWOBS) = x(YAW);
        while(z(YAWOBS) - h(YAWOBS) <= -MathUtil::PI) h(YAWOBS) -= 2*MathUtil::PI;
        while(z(YAWOBS) - h(YAWOBS) >   MathUtil::PI) h(YAWOBS) += 2*MathUtil::PI;
        return h;
    }

    YawObsCov yawObsCov() const
    {
        YawObsCov R = YawObsCov::Zero();
        // Pick 2 sigma bounds: var = (2*sigma)^2 / 4
        R(YAWOBS, YAWOBS) = 0.25 * (0.2 * 0.2);
        return R;
    }

    YawObsJac yawObsJac() const
    {
        YawObsJac H = YawObsJac::Zero();
        H(YAWOBS, YAW) = 1.0;
        return H;
    }

    /***************************************************************************/
    /* End Core System Impl ****************************************************/
    /***************************************************************************/

    /// Swap
    inline void swap(SimExample& o) { ParentType::swap(o); }
    /// Copy Ctor
    SimExample(const SimExample& o) : ParentType(o) {}
    /// Move Ctor
    SimExample(SimExample&& o) { swap(o); }
    /// Unifying Assignment
    SimExample& operator=(SimExample o) { swap(o); return *this; }
};

using namespace std;

int main(int argc, const char *argv[])
{
    SimExample::StateVec xInit = SimExample::StateVec::Zero();
    SimExample::StateCov PInit = SimExample::StateCov::Identity();
    SimExample sim(xInit, PInit, 4);

    SimExample::StateVec xSim = SimExample::StateVec::Zero();
    SimExample::InputVec uZero = SimExample::InputVec::Zero();
    SimExample::NoiseVec wZero = SimExample::NoiseVec::Zero();

    { // Testing that the measurement wraparound is handled properly
        SimExample::StateVec x = SimExample::StateVec::Zero();
        x(SimExample::YAW) = -3.10;
        SimExample::YawObsVec z = SimExample::YawObsVec::Constant(3.10);
        SimExample::YawObsVec h = sim.yawObsVec(x, z);
        SimExample::YawObsVec delta = z - h;
        Assert(delta(SimExample::YAWOBS) > -0.10, "Measurement estimate must handle wraparound");
        Assert(delta(SimExample::YAWOBS) < -0.08, "Measurement estimate must handle wraparound");
    }

    f64 xyObsPeriod = 1.0;
    f64 lastXyObs   = -1.1 * xyObsPeriod;

    f64 yawObsPeriod = 0.5;
    f64 lastYawObs   = -0.5 * yawObsPeriod;

    f64 tmax = 100;
    f64 dt = 0.1;
    TimeUtil::StatTimer timerFull(true, tmax/dt);
    TimeUtil::StatTimer timerPred(false, tmax/dt);
    TimeUtil::StatTimer timerCorrXy(false, tmax/dt);
    TimeUtil::StatTimer timerCorrYaw(false, tmax/dt);
    for (f64 t = 0; t < tmax - MathUtil::TINY; t+=dt) {
        SimExample::InputVec u(1, sin(t));
        SimExample::NoiseVec w = MathUtil::gausNoise(sim.noiseCov(xSim, t, u));

        sim.printCsv(cout, xSim, t, u, w, "SIM");
        sim.printCsv(cout, t, u, wZero, "EST");

        if (t > lastXyObs + xyObsPeriod - MathUtil::TINY) {
            timerCorrXy.start();
            lastXyObs = t;
            SimExample::XyObsCov R = sim.xyObsCov();
            SimExample::XyObsVec z = sim.xyObsVec(xSim) + MathUtil::gausNoise(R, true);
            SimExample::XyObsVec h = sim.xyObsVec(sim.getState());
            SimExample::XyObsJac H = sim.xyObsJac();
            sim.correct(z, R, h, H);
            timerCorrXy.stop();
            sim.printObsCsv(cout, z, t, "OBS_XY");
            sim.printCsv(cout, t, u, wZero, "EST");
        }

        if (t > lastYawObs + yawObsPeriod - MathUtil::TINY) {
            timerCorrYaw.start();
            lastYawObs = t;
            SimExample::YawObsCov R = sim.yawObsCov();
            SimExample::YawObsVec z = sim.yawObsVec(xSim) + MathUtil::gausNoise(R, true);
            SimExample::YawObsVec h = sim.yawObsVec(sim.getState(), z);
            SimExample::YawObsJac H = sim.yawObsJac();
            sim.correct(z, R, h, H);
            timerCorrYaw.stop();
            sim.printObsCsv(cout, z, t, "OBS_Yaw");
            sim.printCsv(cout, t, u, wZero, "EST");
        }

        xSim = sim.integ(xSim, t, dt, u, w);
        timerPred.start();
        sim.predict(t, dt, u);
        timerPred.stop();

        timerFull.toc();
    }
    sim.printCsv(cout, xSim, tmax, uZero, wZero, "SIM");
    sim.printCsv(cout, tmax, uZero, wZero, "EST");

    timerFull.stopNoRecord();
    cerr << "Full loop timer (including prints): " << timerFull << endl;
    cerr << "Prediction timer: " << timerPred << endl;
    cerr << "XY Correction timer: " << timerCorrXy << endl;
    cerr << "Yaw Correction timer: " << timerCorrYaw << endl;

    return 0;
}
