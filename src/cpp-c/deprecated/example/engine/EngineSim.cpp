#include <iostream>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "EngineSystem.hpp"
#include "EngineSmc.hpp"
#include "EngineFlc.hpp"
#include "EnginePid.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;

int main(int argc, const char *argv[])
{
    EngineSystem::StateVec x0 = EngineSystem::stateZero();
    x0(EngineSystem::MA) = 0.0021; // kg
    x0(EngineSystem::WE) = 138.7;  // rad/s
    x0(EngineSystem::C4) = EngineSystem::nominalC4() * 1.0;
    EngineSystem sys(x0, 4);

    EngineSmc::GainsVector smcGains;
    smcGains << 2, 1, 4, 0.1, 250000;
    EngineSystem smcSys(sys);
    EngineSmc    smcCtrl(&smcSys, smcGains);

    EngineSystem flcSys(sys);
    EngineFlc    flcCtrl(&flcSys);

    EnginePid::GainsMatrix pidGains;
    pidGains << 15, 3, 1;
    EngineSystem pidSys(sys);
    EnginePid    pidCtrl(&pidSys, pidGains, 1, 4);

    f64 tmax = 40;
    f64 dt = 0.01;
    f64 noisePeriod = 1;
    f64 noiseFreq = 2*MathUtil::PI / noisePeriod;
    int noiseWindow = 7; // should be an odd number >= 3
    f64 vLow  = 17.8816;
    f64 vHigh = 26.8224;
    f64 setptPeriod = 20;
    f64 setptFreq = 2*MathUtil::PI / setptPeriod;

    for (f64 t = 0; t < tmax - MathUtil::TINY; t += dt) {
        EngineSmc::SetptVec yd;
        if (t < 2)       yd(0) = sys.weFromVel(vLow);
        else if (t < 10) yd(0) = sys.weFromVel(vHigh);
        else if (t < 30) yd(0) = sys.weFromVel(vHigh - 0.25*(vHigh-vLow)*sin(setptFreq*(t-10)));
        else if (t < 40) yd(0) = sys.weFromVel(vHigh);

        EngineSystem::NoiseVec w;
        w(0) = 0.5 * smcSys.nominalC4() * noiseFreq * cos(noiseFreq*t);
        f64 noisePhase = t / noisePeriod;
        noisePhase -= noiseWindow * (int)(noisePhase/noiseWindow);
        if (0.25 <= noisePhase && noisePhase <= noiseWindow/2 + 0.25) w(0) = 0;
        if (noiseWindow/2 + 0.75 <= noisePhase && noisePhase <= noiseWindow - 0.25) w(0) = 0;

        // disable noise
        //w(0) = 0;

        // Sliding Mode Controller
        EngineSystem::InputVec smcU = smcCtrl.control(t, dt, yd);
        smcSys.printCsv(cout, t, smcU, w, "SMCSYS");
        smcCtrl.printCsv(cout, t, yd, "SMCSET");
        smcSys.integ(t, dt, smcU, w);

        // Feedback Linearization Controller
        EngineSystem::InputVec flcU = flcCtrl.control(t, dt, yd);
        flcSys.printCsv(cout, t, flcU, w, "FLCSYS");
        flcCtrl.printCsv(cout, t, yd, "FLCSET");
        flcSys.integ(t, dt, flcU, w);

        // PID controller
        EngineSystem::InputVec pidU = pidCtrl.control(t, dt, yd);
        pidSys.printCsv(cout, t, pidU, w, "PIDSYS");
        pidCtrl.printCsv(cout, t, yd, "PIDSET");
        pidSys.integ(t, dt, pidU, w);
    }
    smcSys.printCsv(cout, tmax, EngineSystem::InputVec::Zero(),
                                EngineSystem::NoiseVec::Zero(), "SMCSYS");
    flcSys.printCsv(cout, tmax, EngineSystem::InputVec::Zero(),
                                EngineSystem::NoiseVec::Zero(), "FLCSYS");
    pidSys.printCsv(cout, tmax, EngineSystem::InputVec::Zero(),
                                EngineSystem::NoiseVec::Zero(), "PIDSYS");

    return 0;
}
