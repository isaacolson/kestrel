#pragma once

#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "kestrel/control/Controller.hpp"
#include "EngineSystem.hpp"

namespace Kestrel {

class EngineFlc: public Control::Controller<f64, 1, EngineSystem> {
  public:
    typedef Control::Controller<f64, 1, EngineSystem> ParentType;

  protected:

  public:
    /// Ctor
    EngineFlc(const SystemType* sys) : ParentType(sys)
    { Assert(sys != nullptr, "Requires an EngineSystem"); }
    /// Dtor
    virtual ~EngineFlc() {}

    /// Returns the control input given a state and a setpoint
    virtual InputVec control(const StateVec& x, f64 t, f64 dt, const SetptVec& yd)
    {
        SystemType::ConstantsVec constants = sys->getNomConstants();
        SystemType::DerivedVec   derived   = sys->calcNomDerived();
        f64 Je = constants(SystemType::JE);
        f64 c1 = constants(SystemType::C1);
        f64 c2 = constants(SystemType::C2);
        f64 c3 = constants(SystemType::C3);
        f64 c4 = sys->nominalC4();
        f64 c5 = constants(SystemType::C5);
        f64 Ti = derived(SystemType::TI);
        f64 Tf = constants(SystemType::TF);
        f64 Td = derived(SystemType::TD);
        f64 Tr = constants(SystemType::TR);

        f64 we = x(SystemType::WE);
        f64 ma = x(SystemType::MA);

        f64 weDot    = 1/Je * (Ti - Tf - Td - Tr);
        f64 weDotDot = yd(0) - we - 2*weDot;

        InputVec u;
        u(0) = SystemType::invThrottleCurve(
                   Je/(c3*c1) * (weDotDot + 1/Je * (c3*c2*we*ma + (c5+2*c4*we)*weDot)));
        return u;
    }
    using ParentType::control;

    /// Swap
    inline void swap(EngineFlc& o)
    { ParentType::swap(o); }
    /// Copy Ctor
    EngineFlc(const EngineFlc& o) : ParentType(o) {}
    /// Move Ctor
    EngineFlc(EngineFlc&& o) { swap(o); }
    /// Unifying Assignment
    EngineFlc& operator=(EngineFlc o) { swap(o); return *this; }
};

} /* Kestrel */
