#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "kestrel/control/Controller.hpp"
#include "kestrel/control/LinearFilter.hpp"
#include "EngineSystem.hpp"

namespace Kestrel {

class EngineSmc: public Control::Controller<f64, 1, EngineSystem> {
  public:
    typedef Control::Controller<f64, 1, EngineSystem> ParentType;
    typedef Eigen::Matrix<f64, 5, 1>                  GainsVector;

    enum GainsIdx {
        ETA1,
        PHI1,
        LAMBDA2,
        TAU2,
        RHO1,
    };

  protected:
    GainsVector gains;
    Control::LinearFilter<f64, 3, 1> setptFilt;
    Control::LinearFilter<f64, 2, 1> synthInputFilt;
    f64 estC4;

  public:
    /// Ctor
    EngineSmc(const SystemType* sys, const GainsVector& gains) :
        ParentType(sys), gains(gains),
        setptFilt({1, 2, 1}, 1, {sys->getState()(SystemType::WE), 0}, 4),
        synthInputFilt({1, gains(TAU2)}, 1, sys->getState()(SystemType::MA), 4),
        estC4(SystemType::nominalC4())
    { Assert(sys != nullptr, "Requires an EngineSystem"); }
    /// Dtor
    virtual ~EngineSmc() {}

    /// Returns the control input given a state and a setpoint
    virtual InputVec control(const StateVec& x, f64 t, f64 dt, const SetptVec& yd)
    {
        setptFilt.filter(t, dt, yd);
        decltype(setptFilt)::OutputVec wed = setptFilt.getOutput();

        f64 we = x(SystemType::WE);
        f64 ma = x(SystemType::MA);

        SystemType::ConstantsVec constants = sys->getNomConstants();
        f64 Je = constants(SystemType::JE);
        f64 c1 = constants(SystemType::C1);
        f64 c2 = constants(SystemType::C2);
        f64 c3 = constants(SystemType::C3);
        f64 Tf = constants(SystemType::TF);
        f64 Tr = constants(SystemType::TR);

        f64 S1      = we - wed(0);

        // TODO: adaptation doesn't actually work that well with the other noises baked in yet
        //       need to rederived the adaptation formula
        // Use adapted value of c4 to estimate Td
        //StateVec xEstC4(x);
        //xEstC4(SystemType::C4) = estC4;
        //SystemType::DerivedVec derived = sys->calcDerived(xEstC4);
        //f64 Td = derived(SystemType::TD);
        SystemType::DerivedVec derived = sys->calcNomDerived();
        f64 Td = derived(SystemType::TD);

        // Simple adaptive - if no other error than c4
        //f64 b       = c3 / Je;
        //f64 ghat    = - (Tf + Td + Tr) / Je;
        //f64 k1      = gains(ETA1);
        //decltype(synthInputFilt)::InputVec madBar;
        //madBar.setConstant(1/b * (-ghat + wed(1) - k1*S1 / gains(PHI1)));

        f64 bhat    = 2*c3 / (sqrt(3)*Je);
        f64 ghat    = - (Tf + Td + Tr) / Je;
        f64 betaMin = 1 / sqrt(3);
        f64 gamma   = (Tf + 2*Td + 2*Tr) / Je;
        f64 k1      = 1/betaMin * ((1 - betaMin) * fabs(ghat - wed(1)) + gamma + gains(ETA1));
        decltype(synthInputFilt)::InputVec madBar;
        madBar.setConstant(1/bhat * (-ghat + wed(1) - k1*S1 / gains(PHI1)));

        static bool first = true;
        if (first) {
            synthInputFilt.setOutput(madBar);
            first = false;
        }
        decltype(synthInputFilt)::OutputVec madDot = synthInputFilt.deriv(t, madBar);
        decltype(synthInputFilt)::OutputVec mad    = synthInputFilt.filter(t, dt, madBar);

        f64 S2 = ma - mad(0);

        InputVec u;
        u(0) = SystemType::invThrottleCurve((c2*we*ma + madDot(0) - gains(LAMBDA2)*S2) / c1);

        // Adapt to learn c4, but only if we are properly controlling the throttle
        //if (MathUtil::TINY < u(0) && u(0) < 79.46 - MathUtil::TINY) {
            //f64 c4Dot   = - we*we / (Je*gains(RHO1)) * S1;
            //estC4      += dt*c4Dot;
        //}

        return u;
    }
    using ParentType::control;

    /// Prints data in CSV form to the given ostream with given leader string,
    ///   deliminator, and line terminator. Format: leader, time, state, setpt,
    //                                              smoothSetpt, synthInput, learned c4
    /// Note that there WILL be a trailing deliminator on each line.
    virtual void printCsv(std::ostream& os, const StateVec& x, f64 t,
                                            const SetptVec& yd = SetptVec::Zero(),
                                            const std::string& leader = "",
                                            const std::string& delim = ",",
                                            const std::string& term = "\n") const
    {
        ParentType::printCsv(os, x, t, yd, leader, delim, "");
        os << setptFilt.getOutput()(0) << delim;
        os << synthInputFilt.getOutput()(0) << delim;
        os << "c4" << delim << estC4 << delim;
        if (!term.empty()) os << term;
    }
    using ParentType::printCsv;

    /// Swap
    inline void swap(EngineSmc& o)
    { ParentType::swap(o); gains.swap(o.gains); setptFilt.swap(o.setptFilt);
      synthInputFilt.swap(o.synthInputFilt); std::swap(estC4, o.estC4); }
    /// Copy Ctor
    EngineSmc(const EngineSmc& o) :
        ParentType(o), gains(o.gains), setptFilt(o.setptFilt), synthInputFilt(o.synthInputFilt),
        estC4(o.estC4) {}
    /// Move Ctor
    EngineSmc(EngineSmc&& o) { swap(o); }
    /// Unifying Assignment
    EngineSmc& operator=(EngineSmc o) { swap(o); return *this; }
};

} /* Kestrel */
