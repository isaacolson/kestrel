#pragma once

#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "kestrel/control/System.hpp"

namespace Kestrel {

class EngineSystem: public Control::System<f64, 3, 1, 1> {
  public:
    typedef Control::System<f64, 3, 1, 1> ParentType;

    enum StateIdx {
        MA, // Air mass
        WE, // Engine angular velocity
        C4, // coefficient of drag wrt WE
    };

    typedef Eigen::Matrix<f64, 8, 1> ConstantsVec;
    enum ConstantsIdx {
        JE, // Effective vehicle inertia
        TR, // Rolling Torque
        C1, // Throttle coeff
        C2, // exhaust coeff
        C3, // input torque coeff
        C5, // friction torque coeff
        C6, // friction torque offset
        C7, // wheel circumference
    };

    typedef Eigen::Matrix<f64, 3, 1> DerivedVec;
    enum DerivedIdx {
        TI, // Input Torque
        TF, // Friction Torque
        TD, // Drag Torque
    };

  protected:
    using ParentType::x;
    ConstantsVec constants;

  public:
    /// Ctor
    EngineSystem(const StateVec& x = stateZero(), sz order = 1) : ParentType(x, order)
    { setConstants(); }
    /// Dtor
    virtual ~EngineSystem() {}

    inline f64 getVel(const StateVec& x) const { return constants(C7) * x(WE); }
    inline f64 getVel() const                  { return getVel(x); }
    inline f64 weFromVel(f64 vel) const        { return vel / constants(C7); }

    static inline f64 nominalC4() { return 0.0026; /* Nms^2  */ }
    static inline f64 nominalJE() { return 36.42;  /* kg*m^2 */ }
    static inline f64 nominalTR() { return 21.5;   /* Nm     */ }
    static inline StateVec stateZero()
    {
        StateVec ret = StateVec::Zero();
        ret(C4) = nominalC4();
        return ret;
    }
    static inline f64 throttleCurve(f64 angleDegrees)
    {
        if (angleDegrees < 0) angleDegrees = 0;
        if (angleDegrees > 79.46) return 1;
        return  1 - cos(MathUtil::toRad(1.14*angleDegrees - 1.06));
    }
    static inline f64 invThrottleCurve(f64 throttle)
    {
        if (throttle >= 0.9917)    return 79.46;
        if (throttle < 0.00017113) return 0;
        return (MathUtil::toDeg(acos(1 - throttle)) + 1.06) / 1.14;
    }

    inline ConstantsVec getConstants() const { return constants; }
    inline void setConstants()
    {
        constants(JE) = nominalJE() * 1.5;
        constants(TR) = nominalTR() * 1.5;
        constants(C1) = 0.6;    // kg/s
        constants(C2) = 0.0952; // unitless
        constants(C3) = 47469;  // Nm/kg
        constants(C5) = 0.1056; // Nms
        constants(C6) = 15.1;   // Nm
        constants(C7) = 0.1289; // m
    }

    inline DerivedVec calcDerived(const StateVec& x) const
    {
        DerivedVec ret;
        ret(TI) = constants(C3) * x(MA);
        ret(TF) = constants(C5) * x(WE) + constants(C6);
        ret(TD) = x(C4) * x(WE) * x(WE);
        return ret;
    }
    inline DerivedVec calcDerived() const { return calcDerived(x); }

    inline ConstantsVec getNomConstants() const
    {
        ConstantsVec cst = constants;
        cst(JE) = nominalJE();
        cst(TR) = nominalTR();
        return cst;
    }

    inline DerivedVec calcNomDerived(const StateVec& x) const
    {
        DerivedVec ret;
        ret(TI) = constants(C3) * x(MA);
        ret(TF) = constants(C5) * x(WE) + constants(C6);
        ret(TD) = nominalC4() * x(WE) * x(WE);
        return ret;
    }
    inline DerivedVec calcNomDerived() const { return calcNomDerived(x); }

    /// The derivative of the state vector given the state, the time, input, and noise
    virtual StateVec deriv(const StateVec& x, f64 t = 0.0,
                           const InputVec& u = InputVec::Zero(),
                           const NoiseVec& w = NoiseVec::Zero()) const
    {
        DerivedVec derived = calcDerived(x);

        StateVec ret;
        ret(MA) = constants(C1)*throttleCurve(u(0)) - constants(C2)*x(WE)*x(MA);
        ret(WE) = 1/constants(JE) * (derived(TI) - derived(TF) - derived(TD) - constants(TR));
        ret(C4) = w(0);

        return ret;
    }

    /// Correct the state vector for domain restricted values (such as angles that wrap around
    /// or required normalization). This function will be called after every integration.
    /// Note: this is intended to modify the input vector, not copy it.
    virtual StateVec& correctDomain(StateVec& x) const
    {
        if (x(MA) < 0) x(MA) = 0;
        if (x(WE) < 0) x(WE) = 0;
        if (x(C4) < 0) x(C4) = 0;

        return x;
    }

    /// Swap
    inline void swap(EngineSystem& o) { ParentType::swap(o); constants.swap(o.constants); }
    /// Copy Ctor
    EngineSystem(const EngineSystem& o) : ParentType(o), constants(o.constants) {}
    /// Move Ctor
    EngineSystem(EngineSystem&& o) { swap(o); }
    /// Unifying Assignment
    EngineSystem& operator=(EngineSystem o) { swap(o); return *this; }
};

} /* Kestrel */
