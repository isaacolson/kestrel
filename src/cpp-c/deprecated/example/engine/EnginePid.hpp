#pragma once

#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "kestrel/control/PidController.hpp"
#include "kestrel/control/LinearFilter.hpp"
#include "EngineSystem.hpp"

namespace Kestrel {

class EnginePid: public Control::PidController<f64, 1, EngineSystem> {
  public:
    typedef Control::PidController<f64, 1, EngineSystem> ParentType;

  protected:
    Control::LinearFilter<f64, 3, 1> setptFilt;

  public:
    /// Ctor
    EnginePid(const SystemType* sys, const GainsMatrix& gains, Scalar tf,
              sz derivFiltIntOrder = 1) : ParentType(sys, gains, tf, derivFiltIntOrder),
        setptFilt({1, 2, 1}, 1, {sys->getState()(SystemType::WE), 0}, 4)
    { Assert(sys != nullptr, "Requires an EngineSystem"); }
    /// Dtor
    virtual ~EnginePid() {}

    /// Returns the control input given a state and a setpoint
    virtual InputVec control(const StateVec& x, f64 t, f64 dt, const SetptVec& yd)
    {
        setptFilt.filter(t, dt, yd);
        decltype(setptFilt)::OutputVec wed = setptFilt.getOutput();
        SetptVec ydOpt = SetptVec::Constant(wed(0));
        return ParentType::control(x, t, dt, ydOpt);
    }
    using ParentType::control;

    /// Maps the state vector into the setpt space
    virtual SetptVec stateToSetpt(const StateVec& x) const
    { return x.template block<NUM_SETPTS, 1>(SystemType::WE, 0); }
    using ParentType::stateToSetpt;

    /// Returns if the derivative mapping is valid for the given setpt index
    virtual inline bool derivMappable(int idx) const { return false; }

    /// Maps the state vector into the derivative of the setpt space if such a mapping exists
    virtual SetptVec stateToSetptDeriv(const StateVec& x) const { return SetptVec::Zero(); }
    using ParentType::stateToSetptDeriv;

    /// Prints data in CSV form to the given ostream with given leading string,
    ///   deliminator, and line terminator. Format: leader, time, state, setpt, map(state->setpt),
    //    map(state->setptDeriv), error, filteredSetpt
    /// Note that there WILL be a trailing deliminator on each line.
    virtual void printCsv(std::ostream& os, const StateVec& x, f64 t,
                                            const SetptVec& yd = SetptVec::Zero(),
                                            const std::string& leader = "",
                                            const std::string& delim = ",",
                                            const std::string& term = "\n") const
    {
        ParentType::printCsv(os, x, t, yd, leader, delim, "");

        os << "ydFilt" << delim << setptFilt.getOutput()(0) << delim;
        if (!term.empty()) os << term;
    }
    using ParentType::printCsv;

    /// Swap
    inline void swap(EnginePid& o) { ParentType::swap(o); setptFilt.swap(o.setptFilt); }
    /// Copy Ctor
    EnginePid(const EnginePid& o) : ParentType(o), setptFilt(o.setptFilt) {}
    /// Move Ctor
    EnginePid(EnginePid&& o) { swap(o); }
    /// Unifying Assignment
    EnginePid& operator=(EnginePid o) { swap(o); return *this; }
};

} /* Kestrel */
