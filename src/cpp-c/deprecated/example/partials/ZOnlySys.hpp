#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/assert.hpp"
#include "kestrel/util/PhysicsUtil.hpp"
#include "kestrel/util/Rotations.hpp"

#include "kestrel/control/System.hpp"

namespace Kestrel {

class ZOnlySys: public Control::System<f64, 2, 1, 1> {
  public:
    typedef Control::System<f64, 2, 1, 1>  ParentType;
    typedef Rotations::QuatVectorized<f64> QuatVec;

    enum StateIdx {
        ZPOS,
        ZVEL,
        NUM_Z=2,
    };
    enum InputIdx {
        LIFT,
        NUM_LIFT=1,
    };
    enum NoiseIdx {
        LIFT_NOISE,
        NUM_LIFT_NOISE=1,
    };

  protected:
    f64     m;
    QuatVec q;

  public:
    /// Ctor
    ZOnlySys(const StateVec& x = StateVec::Zero(), f64 m = 1,
             QuatVec q = Rotations::quatVecIdentity<f64>(),
             sz order = 1) : ParentType(x, order), m(m), q(q)
    { Assert(m > 0, "Mass cannot be negative"); }
    /// Dtor
    virtual ~ZOnlySys() {}

    inline QuatVec getQuat() const { return q; }
    inline void    setQuat(const QuatVec& q) { this->q = Rotations::quatPositive(q.normalized()); }

    /// The derivative of the state vector given the state, the time, input, and noise
    using ParentType::deriv;
    virtual StateVec deriv(const StateVec& x, f64 t = 0.0,
                           const InputVec& u = InputVec::Zero(),
                           const NoiseVec& w = NoiseVec::Zero()) const
    {
        StateVec ret;
        ret(0) = x(1);
        ret(1) = (Rotations::quatToMat(q)(2,2) * u(0) + w(0))/m - PhysicsUtil::GRAVITY;
        return ret;
    }

    /// Swap
    inline void swap(ZOnlySys& o) { ParentType::swap(o); std::swap(m, o.m); q.swap(o.q); }
    /// Copy Ctor
    ZOnlySys(const ZOnlySys& o) : ParentType(o), m(o.m), q(o.q) {}
    /// Move Ctor
    ZOnlySys(ZOnlySys&& o) { swap(o); }
    /// Unifying Assignment
    ZOnlySys& operator=(ZOnlySys o) { swap(o); return *this; }
};

} /* Kestrel */
