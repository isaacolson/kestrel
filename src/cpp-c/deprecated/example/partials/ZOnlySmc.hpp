#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/PhysicsUtil.hpp"
#include "kestrel/util/Rotations.hpp"

#include "kestrel/control/Controller.hpp"
#include "kestrel/control/LinearFilter.hpp"
#include "ZOnlySys.hpp"

namespace Kestrel {

class ZOnlySmc: public Control::Controller<f64, 1, ZOnlySys> {
  public:
    typedef Control::Controller<f64, 1, ZOnlySys>        ParentType;
    typedef Eigen::Matrix<f64, 3, 1>                     GainList;
    typedef Control::LinearFilter<f64, 4, 1, NUM_SETPTS> FilterType;

    enum StateIdx {
        ZPOS,
        ZVEL,
        NUM_Z=2,
    };
    enum InputIdx {
        LIFT,
        NUM_LIFT=1,
    };
    enum GainsIdx {
        LAMBDAZ,
        PHIZ,
        ETAZ,
        NUM_GAINS=3,
    };

  protected:
    GainList   gains; ///< [ lambdaz, phiz, etaz ]

    f64        setptFiltNatFreq;
    FilterType setptFilt;

    f64 mEst;
    f64 dmMin;
    f64 dmMax;
    f64 maxDisturbance;

  public:
    /// Ctor
    ZOnlySmc(const ZOnlySys* sys, const GainList& gains = GainList::Ones(),
             f64 setptFiltNatFreq = 0, f64 mMin = 1, f64 mMax = 1, f64 maxDisturbance = 0,
             sz order = 1) :
        ParentType(sys), gains(gains), setptFiltNatFreq(setptFiltNatFreq),
        setptFilt({setptFiltNatFreq*setptFiltNatFreq*setptFiltNatFreq,
                   3*setptFiltNatFreq*setptFiltNatFreq, 3*setptFiltNatFreq, 1},
                  setptFiltNatFreq*setptFiltNatFreq*setptFiltNatFreq,
                  FilterType::OutputVec::Zero(), order),
        maxDisturbance(maxDisturbance)
    {
        Assert(sys != nullptr, "ZOnlySmc requires a system");
        Assert(gains(PHIZ) > MathUtil::TINY, "Boundary must be > 0");
        Assert(mMin > 0 && mMax > 0, "Mass cannot be negative");
        Assert(mMin <= mMax, "Improper mass ordering");

        mEst  = std::sqrt(mMin * mMax);
        dmMin = std::sqrt(mMin / mMax);
        dmMax = std::sqrt(mMax / mMin);
    }
    /// Dtor
    virtual ~ZOnlySmc() {}

    /// Returns the control input given a state and a setpoint
    using ParentType::control;
    virtual InputVec control(const StateVec& x, f64 t, f64 dt, const SetptVec& yd)
    {
        // Do setpoint filtering for wd and wddot
        static bool first = true;
        if (first) {
            FilterType::OutputVec init = FilterType::OutputVec::Zero();
            init.block<NUM_SETPTS, 1>(0,0) = yd;
            setptFilt.setOutput(init);
            first = false;
        }
        SetptVec zd     = yd;
        SetptVec zddot  = SetptVec::Zero();
        SetptVec zddot2 = SetptVec::Zero();
        if (setptFiltNatFreq > 0) {
            setptFilt.filter(t, dt, yd);
            zd     = setptFilt.getOutput().block<NUM_SETPTS, 1>(0,0);
            zddot  = setptFilt.getOutput().block<NUM_SETPTS, 1>(0,1);
            zddot2 = setptFilt.getOutput().block<NUM_SETPTS, 1>(0,2);
            // Note: zddot2 estimate is very spikey, and tends to cause bad performance
            //       on step functions, seems better to just keep it under control
            if (zddot2(0) >  10) zddot2(0) =  10;
            if (zddot2(0) < -10) zddot2(0) = -10;
        }

        // Calculate error and best-estimate terms
        SetptVec ze    = zd    - x.block<NUM_SETPTS, 1>(ZPOS, 0);
        SetptVec zedot = zddot - x.block<NUM_SETPTS, 1>(ZVEL, 0);
        SetptVec Sz = zedot + gains(LAMBDAZ)*ze;
        // TODO: add special logic if rotation is too great
        f64      rotation = Rotations::quatToMat(sys->getQuat())(2,2);
        SetptVec g; g(0) = PhysicsUtil::GRAVITY;

        // Account for uncertainty
        SetptVec uncertainty = (1 - 1/dmMax)*(g + zddot2 + gains(LAMBDAZ)*zedot).cwiseAbs()
                               + SetptVec::Constant(maxDisturbance / (mEst*dmMin));
        SetptVec kz = dmMax * (uncertainty + SetptVec::Constant(gains(ETAZ)));

        InputVec u = mEst / rotation * (g + zddot2 + gains(LAMBDAZ)*zedot
                                        + kz*Sz/gains(PHIZ));

        // Some actuator limits for sanity
        if (u(0) > 3.0*g(0)*mEst) u(0) = 3.0*g(0)*mEst;
        if (u(0) < 0.1*g(0)*mEst) u(0) = 0.1*g(0)*mEst;
        return u;
    }

    /// Prints data in CSV form to the given ostream with given leader string,
    ///   deliminator, and line terminator.
    //    Format: leader, time, state, setpt, setptFilt, setptDotFilt
    /// Note that there WILL be a trailing deliminator on each line.
    using ParentType::printCsv;
    virtual void printCsv(std::ostream& os, const StateVec& x, f64 t,
                                            const SetptVec& yd = SetptVec::Zero(),
                                            const std::string& leader = "",
                                            const std::string& delim = ",",
                                            const std::string& term = "\n") const
    {
        ParentType::printCsv(os, x, t, yd, leader, delim, "");

        FilterType::OutputVec zdFilt = setptFilt.getOutput();
        os << "ydFilt" << delim;
        for (int i = 0; i < NUM_SETPTS; ++i) { os << zdFilt(i,0) << delim; }
        os << "yddotFilt" << delim;
        for (int i = 0; i < NUM_SETPTS; ++i) { os << zdFilt(i,1) << delim; }
        os << "yddotdotFilt" << delim;
        for (int i = 0; i < NUM_SETPTS; ++i) { os << zdFilt(i,2) << delim; }

        if (!term.empty()) os << term;
    }

    /// Swap
    inline void swap(ZOnlySmc& o) { ParentType::swap(o); gains.swap(o.gains);
        std::swap(setptFiltNatFreq, o.setptFiltNatFreq); setptFilt.swap(o.setptFilt);
        std::swap(mEst, o.mEst); std::swap(dmMin, o.dmMin); std::swap(dmMax, o.dmMax);
        std::swap(maxDisturbance, o.maxDisturbance); }
    /// Copy Ctor
    ZOnlySmc(const ZOnlySmc& o) : ParentType(o), gains(o.gains),
        setptFiltNatFreq(o.setptFiltNatFreq), setptFilt(o.setptFilt), mEst(o.mEst), dmMin(o.dmMin),
        dmMax(o.dmMax), maxDisturbance(o.maxDisturbance) {}
    /// Move Ctor
    ZOnlySmc(ZOnlySmc&& o) { swap(o); }
    /// Unifying Assignment
    ZOnlySmc& operator=(ZOnlySmc o) { swap(o); return *this; }
};

} /* Kestrel */
