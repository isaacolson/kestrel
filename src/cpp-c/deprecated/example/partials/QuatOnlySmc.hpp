#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/Rotations.hpp"

#include "kestrel/control/Controller.hpp"
#include "kestrel/control/LinearFilter.hpp"
#include "QuatOnlySys.hpp"

namespace Kestrel {

class QuatOnlySmc: public Control::Controller<f64, 4, QuatOnlySys> {
  public:
    typedef Control::Controller<f64, 4, QuatOnlySys>     ParentType;
    typedef Eigen::Matrix<f64, 3, 3>                     GainMatrix;
    typedef Control::LinearFilter<f64, 3, 1, NUM_SETPTS> FilterType;

  protected:
    GainMatrix gain;
    f64        setptFiltNatFreq;
    FilterType setptFilt;

  public:
    /// Ctor
    QuatOnlySmc(const QuatOnlySys* sys = nullptr, const GainMatrix& gain = GainMatrix::Identity(),
                f64 setptFiltNatFreq = 0, sz order = 1) :
        ParentType(sys), gain(gain), setptFiltNatFreq(setptFiltNatFreq),
        setptFilt({setptFiltNatFreq*setptFiltNatFreq, 2*setptFiltNatFreq, 1},
                  setptFiltNatFreq*setptFiltNatFreq, FilterType::OutputVec::Zero(), order) { }
        // Note: it is ok to start the setpt filter at 0 even though it is not a valid quat because
        //       on the first run of control it will be reset to the actual setpt

    /// Dtor
    virtual ~QuatOnlySmc() {}

    /// Returns the control input given a state and a setpoint
    using ParentType::control;
    virtual InputVec control(const StateVec& x, f64 t, f64 dt, const SetptVec& yd)
    {
        // Do filtering on qd and qddot
        static bool first = true;
        if (first) {
            FilterType::OutputVec init = FilterType::OutputVec::Zero();
            init.block<NUM_SETPTS, 1>(0,0) = yd;
            setptFilt.setOutput(init);
            first = false;
        }
        SetptVec qd    = yd;
        SetptVec qddot = SetptVec::Zero();
        if (setptFiltNatFreq > 0) {
            setptFilt.filter(t, dt, yd);
            qd    = setptFilt.getOutput().block<NUM_SETPTS, 1>(0,0);
            qddot = setptFilt.getOutput().block<NUM_SETPTS, 1>(0,1);
        }

        // Calculate error
        // x * qe = yd -> qe = xinv * yd
        SetptVec qinv = Rotations::quatInv(x);
        Rotations::QuatCompMat<f64> QLqinv = Rotations::quatCompLmat(qinv);
        Rotations::QuatCompMat<f64> QRqd   = Rotations::quatCompRmat(qd);
        SetptVec qe = QRqd * qinv; // = QLqinv * qd
        // TODO: special logic if qe(0) = 0 (ie the rotation angle is PI)
        InputVec Sq = qe.block<NUM_INPUTS, 1>(1,0);

        // Calculate mapping of input to error
        Rotations::QuatDotJacAngVel<f64> omegaToQdot = Rotations::quatRotDerivJacAngVel(x);
        Eigen::Matrix<f64, 4, 4> qdotToQinvdot = Eigen::Matrix<f64, 4, 4>::Identity();
        qdotToQinvdot(1,1) = qdotToQinvdot(2,2) = qdotToQinvdot(3,3) = -1;
        Rotations::QuatDotJacAngVel<f64> omegaToQinvdot = qdotToQinvdot * omegaToQdot;
        Eigen::Matrix<f64, 3, 3> inputToError = QRqd.template block<3, 4>(1, 0) * omegaToQinvdot;
        // This matrix will always be invertible if qe(0) != 0

        InputVec u = inputToError.inverse() * (- QLqinv.template block<3, 4>(1, 0) * qddot
                                               - gain * Sq);

        return u;
    }

    /// Prints data in CSV form to the given ostream with given leader string,
    ///   deliminator, and line terminator.
    //    Format: leader, time, state, setpt, setptFilt, setptDotFilt
    /// Note that there WILL be a trailing deliminator on each line.
    using ParentType::printCsv;
    virtual void printCsv(std::ostream& os, const StateVec& x, f64 t,
                                            const SetptVec& yd = SetptVec::Zero(),
                                            const std::string& leader = "",
                                            const std::string& delim = ",",
                                            const std::string& term = "\n") const
    {
        ParentType::printCsv(os, x, t, yd, leader, delim, "");

        FilterType::OutputVec qdFilt = setptFilt.getOutput();
        os << "ydFilt" << delim;
        for (int i = 0; i < NUM_SETPTS; ++i) { os << qdFilt(i,0) << delim; }
        os << "yddotFilt" << delim;
        for (int i = 0; i < NUM_SETPTS; ++i) { os << qdFilt(i,1) << delim; }

        if (!term.empty()) os << term;
    }

    /// Swap
    inline void swap(QuatOnlySmc& o) { ParentType::swap(o); gain.swap(o.gain);
        std::swap(setptFiltNatFreq, o.setptFiltNatFreq); setptFilt.swap(o.setptFilt); }
    /// Copy Ctor
    QuatOnlySmc(const QuatOnlySmc& o) : ParentType(o), gain(o.gain),
        setptFiltNatFreq(o.setptFiltNatFreq), setptFilt(o.setptFilt) { }
    /// Move Ctor
    QuatOnlySmc(QuatOnlySmc&& o) { swap(o); }
    /// Unifying Assignment
    QuatOnlySmc& operator=(QuatOnlySmc o) { swap(o); return *this; }
};

} /* Kestrel */
