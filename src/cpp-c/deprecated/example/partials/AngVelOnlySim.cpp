#include <iostream>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "AngVelOnlySys.hpp"
#include "AngVelOnlySmc.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;

int main(int argc, const char *argv[])
{
    AngVelOnlySys::InertiaVec inertia(0.9,0.8,1.4);
    AngVelOnlySys sys(AngVelOnlySys::StateVec::Zero(), inertia, 4);

    AngVelOnlySmc::GainMatrix boundary = 0.10*AngVelOnlySmc::GainMatrix::Identity();
    AngVelOnlySmc::GainMatrix convergeGain = 1.0*AngVelOnlySmc::GainMatrix::Identity();
    f64                       setptFiltNatFreq = 25;
    AngVelOnlySmc::InertiaVec minInertia(0.5, 0.5, 0.5);
    AngVelOnlySmc::InertiaVec maxInertia(1.5, 1.5, 1.5);
    AngVelOnlySmc::InputVec   maxDisturbance(1.00, 1.00, 1.00);
    AngVelOnlySmc smc(&sys, boundary, convergeGain, setptFiltNatFreq, minInertia, maxInertia,
                      maxDisturbance, 4);

    f64 tmax = 20;
    f64 dt = 0.01;
    AngVelOnlySmc::SetptVec yd = AngVelOnlySmc::SetptVec::Zero();
    Matrix3d noiseCov = Matrix3d::Identity() * 0.25 * (1.00*1.00); // 95% bounds on noise
    AngVelOnlySys::NoiseVec w = MathUtil::gausNoise(noiseCov, true);

    for (f64 t = 0; t < tmax - MathUtil::TINY; t += dt) {
        if (t - (i32)(t) - 0.01 < 0) {
            w = MathUtil::gausNoise(noiseCov, true);
        }

        if (t < 10) {
            yd(0) = 1.0*sin(2*MathUtil::PI*t / 5);
            yd(1) = 0.7*sin(2*MathUtil::PI*t / 3);
            yd(2) = 0.5*cos(2*MathUtil::PI*t / 2);
        } else {
            yd.setOnes();
        }

        AngVelOnlySys::InputVec u = smc.control(t, dt, yd);

        sys.printCsv(cout, t, u, w, "SYS");
        smc.printCsv(cout, t, yd, "SMC");
        sys.integ(t, dt, u, w);
    }
    sys.printCsv(cout, tmax, AngVelOnlySys::InputVec::Zero(),
                             AngVelOnlySys::NoiseVec::Zero(), "SYS");

    return 0;
}
