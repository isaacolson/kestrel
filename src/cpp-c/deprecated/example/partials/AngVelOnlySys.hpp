#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/assert.hpp"

#include "kestrel/control/System.hpp"

namespace Kestrel {

class AngVelOnlySys: public Control::System<f64, 3, 3, 3> {
  public:
    typedef Control::System<f64, 3, 3, 3> ParentType;

    typedef Eigen::Matrix<f64, 3, 1>      InertiaVec; ///< [ Ixx; Iyy; Izz ]
    typedef Eigen::Matrix<f64, 3, 3>      InputMult;

    enum StateIdx {
        ANGVX,
        ANGVY,
        ANGVZ,
        NUM_ANGV=3
    };
    enum InputIdx {
        MOMENTX,
        MOMENTY,
        MOMENTZ,
        NUM_MOMENT=3
    };
    enum NoiseIdx {
        MOMENTX_NOISE,
        MOMENTY_NOISE,
        MOMENTZ_NOISE,
        NUM_MOMENT_NOISE=3
    };
    enum InertiaIdx {
        IXX,
        IYY,
        IZZ,
        NUM_INERTIA=3
    };

  protected:
    InertiaVec inertia;

  public:
    /// Ctor
    AngVelOnlySys(const StateVec& x = StateVec::Zero(),
                  const InertiaVec& inertia = InertiaVec::Ones(), sz order = 1) :
        ParentType(x, order), inertia(inertia) {}
    /// Dtor
    virtual ~AngVelOnlySys() {}

    /// The derivative of the state vector given the state, the time, input, and noise
    using ParentType::deriv;
    virtual StateVec deriv(const StateVec& x, f64 t = 0.0,
                           const InputVec& u = InputVec::Zero(),
                           const NoiseVec& w = NoiseVec::Zero()) const
    {
        return derivF(x, inertia) + derivG(x, inertia) * (u + w);
    }

    /// The offset part of the derivative function
    static inline StateVec derivF(const StateVec& x, const InertiaVec& inertia)
    {
        return StateVec( (inertia(IYY)-inertia(IZZ)) / inertia(IXX) * x(ANGVY) * x(ANGVZ),
                         (inertia(IZZ)-inertia(IXX)) / inertia(IYY) * x(ANGVX) * x(ANGVZ),
                         (inertia(IXX)-inertia(IYY)) / inertia(IZZ) * x(ANGVX) * x(ANGVY) );
    }

    /// The coefficient part of the derivative function
    static inline InputMult derivG(const StateVec& x, const InertiaVec& inertia)
    {
        InputMult ret = InputMult::Identity();
        ret(IXX,IXX) = 1/inertia(IXX);
        ret(IYY,IYY) = 1/inertia(IYY);
        ret(IZZ,IZZ) = 1/inertia(IZZ);
        return ret;
    }

    /// Swap
    inline void swap(AngVelOnlySys& o) { ParentType::swap(o); inertia.swap(o.inertia); }
    /// Copy Ctor
    AngVelOnlySys(const AngVelOnlySys& o) : ParentType(o), inertia(o.inertia) {}
    /// Move Ctor
    AngVelOnlySys(AngVelOnlySys&& o) { swap(o); }
    /// Unifying Assignment
    AngVelOnlySys& operator=(AngVelOnlySys o) { swap(o); return *this; }
};

} /* Kestrel */
