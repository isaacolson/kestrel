#include <iostream>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "QuatOnlySys.hpp"
#include "QuatOnlySmc.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;

int main(int argc, const char *argv[])
{
    QuatOnlySys::StateVec x0 = QuatOnlySys::stateZero();
    QuatOnlySys sys(x0, 4);

    QuatOnlySmc::GainMatrix gain = 4*QuatOnlySmc::GainMatrix::Identity();
    QuatOnlySmc smc(&sys, gain, 10, 4);

    f64 tmax = 20;
    f64 dt = 0.01;
    QuatOnlySmc::SetptVec yd = QuatOnlySys::stateZero();
    Matrix3d omegadCov = Matrix3d::Identity() * 0.25 * (0.2*0.2); // 95% bounds on noise angvel
    QuatOnlySys::NoiseVec w = MathUtil::gausNoise(omegadCov, true);

    for (f64 t = 0; t < tmax - MathUtil::TINY; t += dt) {
        if (t - (i32)(t) - 0.01 < 0) {
            w = MathUtil::gausNoise(omegadCov, true);
        }

        if (t < 10) {
            QuatOnlySys::InputVec omegad( 1.0*sin(2*MathUtil::PI*t / 5),
                                          0.7*sin(2*MathUtil::PI*t / 3),
                                          0.5*cos(2*MathUtil::PI*t / 2));
            f64 deltaTheta     = dt * omegad.norm();
            Vector3d deltaAxis = omegad.normalized();

            QuatOnlySmc::SetptVec deltaYd(cos(deltaTheta/2),
                                          deltaAxis(0)*sin(deltaTheta/2),
                                          deltaAxis(1)*sin(deltaTheta/2),
                                          deltaAxis(2)*sin(deltaTheta/2));
            Rotations::QuatCompMat<f64> deltaYdMat = Rotations::quatCompRmat(deltaYd);
            yd = deltaYdMat*yd;
        } else {
            yd = QuatOnlySys::stateZero();
        }

        QuatOnlySys::InputVec u = smc.control(t, dt, yd);

        sys.printCsv(cout, t, u, w, "SYS");
        smc.printCsv(cout, t, yd, "SMC");
        sys.integ(t, dt, u, w);
    }
    sys.printCsv(cout, tmax, QuatOnlySys::InputVec::Zero(), QuatOnlySys::NoiseVec::Zero(), "SYS");

    return 0;
}
