#include <iostream>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/Rotations.hpp"

#include "ZOnlySys.hpp"
#include "ZOnlySmc.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;

int main(int argc, const char *argv[])
{
    ZOnlySys::StateVec x0 = ZOnlySys::StateVec::Zero();
    f64 m = 2.5;
    ZOnlySys::QuatVec q = Rotations::quatVecIdentity<f64>();
    ZOnlySys sys(x0, m, q, 4);

    ZOnlySmc::GainList gains(0.5, 0.3, 0.2);
    f64 setptFiltNatFreq = 2*MathUtil::PI;
    f64 mMin = 3;
    f64 mMax = 5;
    f64 maxDisturbance = 4;

    ZOnlySmc smc(&sys, gains, setptFiltNatFreq, mMin, mMax, maxDisturbance, 4);

    ZOnlySmc::SetptVec yd = ZOnlySmc::SetptVec::Zero();
    Matrix<f64, 1, 1> noiseCov = Matrix<f64, 1, 1>::Identity() * 0.25 * (3.5*3.5);
    //Matrix<f64, 1, 1> noiseCov = Matrix<f64, 1, 1>::Identity() * 0.25 * (0.1*0.1);
    ZOnlySys::NoiseVec w = MathUtil::gausNoise(noiseCov, true);

    f64 tmax = 20;
    f64 dt = 0.01;
    for (f64 t = 0; t < tmax - MathUtil::TINY; t += dt) {
        if (t - (i32)(t) - 0.01 < 0) {
            w = MathUtil::gausNoise(noiseCov, true);
        }

        if (t < 10) {
            yd(0) = 1.0*sin(2*MathUtil::PI*t / 5);
        } else {
            yd(0) = 2;
        }

        Vector3d omega(0.2*sin(2*MathUtil::PI*t / 5),
                       0.3*sin(2*MathUtil::PI*t / 3),
                       0.4*cos(2*MathUtil::PI*t / 2));
        f64      deltaTheta = dt * omega.norm();
        Vector3d deltaAxis  = omega.normalized();
        Vector4d deltaQuat(cos(deltaTheta/2),
                           deltaAxis(0)*sin(deltaTheta/2),
                           deltaAxis(1)*sin(deltaTheta/2),
                           deltaAxis(2)*sin(deltaTheta/2));
        Rotations::QuatCompMat<f64> deltaQuatMat = Rotations::quatCompRmat(deltaQuat);
        sys.setQuat(deltaQuatMat * sys.getQuat());

        ZOnlySys::InputVec u = smc.control(t, dt, yd);

        sys.printCsv(cout, t, u, w, "SYS");
        smc.printCsv(cout, t, yd, "SMC");
        sys.integ(t, dt, u, w);
    }
    sys.printCsv(cout, tmax, ZOnlySys::InputVec::Zero(), ZOnlySys::NoiseVec::Zero(), "SYS");

    return 0;
}
