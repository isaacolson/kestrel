#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/assert.hpp"
#include "kestrel/util/Rotations.hpp"

#include "kestrel/control/System.hpp"

namespace Kestrel {

class QuatOnlySys: public Control::System<f64, 4, 3, 3> {
  public:
    typedef Control::System<f64, 4, 3, 3> ParentType;

    enum StateIdx {
        QUATW,
        QUATX,
        QUATY,
        QUATZ,
        NUM_QUAT=4,
    };
    enum InputIdx {
        ANGVX,
        ANGVY,
        ANGVZ,
        NUM_ANGV=3,
    };
    enum NoiseIdx {
        ANGVX_NOISE,
        ANGVY_NOISE,
        ANGVZ_NOISE,
        NUM_ANGV_NOISES=3,
    };

  public:
    /// Ctor
    QuatOnlySys(const StateVec& x = stateZero(), sz order = 1) : ParentType(x, order) {}
    /// Dtor
    virtual ~QuatOnlySys() {}

    static StateVec stateZero() { StateVec ret; ret << 1, 0, 0, 0; return ret; }

    /// The derivative of the state vector given the state, the time, input, and noise
    using ParentType::deriv;
    virtual StateVec deriv(const StateVec& x, f64 t = 0.0,
                           const InputVec& u = InputVec::Zero(),
                           const NoiseVec& w = NoiseVec::Zero()) const
    { return Rotations::quatRotDeriv(x, u + w); }

    /// Correct the state vector for domain restricted values (such as angles that wrap around
    /// or required normalization). This function will be called after every prediction and
    /// correction. Note: this is intended to modify the input vector, not copy it.
    using ParentType::correctDomain;
    virtual StateVec& correctDomain(StateVec& x) const
    { x.normalize(); Rotations::quatToPositive(x); return x; }

    /// Swap
    inline void swap(QuatOnlySys& o) { ParentType::swap(o); }
    /// Copy Ctor
    QuatOnlySys(const QuatOnlySys& o) : ParentType(o) {}
    /// Move Ctor
    QuatOnlySys(QuatOnlySys&& o) { swap(o); }
    /// Unifying Assignment
    QuatOnlySys& operator=(QuatOnlySys o) { swap(o); return *this; }
};

} /* Kestrel */
