#pragma once

#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"

#include "kestrel/control/Controller.hpp"
#include "kestrel/control/LinearFilter.hpp"
#include "AngVelOnlySys.hpp"

namespace Kestrel {

class AngVelOnlySmc: public Control::Controller<f64, 3, AngVelOnlySys> {
  public:
    typedef Control::Controller<f64, 3, AngVelOnlySys>   ParentType;
    typedef Eigen::Matrix<f64, 3, 3>                     GainMatrix;
    typedef Control::LinearFilter<f64, 3, 1, NUM_SETPTS> FilterType;
    using InputMult  = SystemType::InputMult;
    using InertiaVec = SystemType::InertiaVec;

    enum SetptIdx {
        ANGVX,
        ANGVY,
        ANGVZ,
        NUM_ANGV=3
    };
    enum InputIdx {
        MOMENTX,
        MOMENTY,
        MOMENTZ,
        NUM_MOMENT=3
    };
    enum InertiaIdx {
        IXX,
        IYY,
        IZZ,
        NUM_INERTIA=3
    };

  protected:
    GainMatrix boundaryGain;   ///< inverse(phi) in the SMC formulation
    GainMatrix convergeGain;   ///< eta in the SMC formulation

    f64        setptFiltNatFreq;
    FilterType setptFilt;

    InertiaVec estInertia;     ///< Geometric mean of min/max Inertia
    InertiaVec minDInertia;    ///< minInertia = minDInertia .* estInertia
    InertiaVec maxDInertia;    ///< maxInertia = maxDInertia .* estInertia
    InputVec   maxDisturbance; ///< Maximum disturbance magnitude (elts >= 0)

  public:
    /// Ctor
    AngVelOnlySmc(const AngVelOnlySys* sys = nullptr,
                  const GainMatrix& boundary = GainMatrix::Identity(),
                  const GainMatrix& convergeGain = GainMatrix::Identity(),
                  f64 setptFiltNatFreq = 0,
                  const InertiaVec& minInertia = InertiaVec::Ones(),
                  const InertiaVec& maxInertia = InertiaVec::Ones(),
                  const InputVec&   maxDisturbance = InputVec::Zero(),
                  sz order = 1) :
        ParentType(sys), convergeGain(convergeGain), setptFiltNatFreq(setptFiltNatFreq),
        setptFilt({setptFiltNatFreq*setptFiltNatFreq, 2*setptFiltNatFreq, 1},
                  setptFiltNatFreq*setptFiltNatFreq, FilterType::OutputVec::Zero(), order),
        maxDisturbance(maxDisturbance)
    {
        Assert(fabs(boundary.determinant()) > MathUtil::TINY*MathUtil::TINY*MathUtil::TINY,
               "Boundary matrix is not invertible");
        boundaryGain = boundary.inverse();

        for (int i = 0; i < NUM_INERTIA; ++i) {
            Assert(minInertia(i) > 0 && maxInertia(i) > 0, "Inertia cannot be negative");
            Assert(minInertia(i) <= maxInertia(i), "Improper Inertia ordering");
        }
        estInertia  = (minInertia.cwiseProduct(maxInertia)).cwiseSqrt();
        minDInertia = (minInertia.cwiseQuotient(maxInertia)).cwiseSqrt();
        maxDInertia = (maxInertia.cwiseQuotient(minInertia)).cwiseSqrt();
    }
    /// Dtor
    virtual ~AngVelOnlySmc() {}

    /// Returns the control input given a state and a setpoint
    using ParentType::control;
    virtual InputVec control(const StateVec& x, f64 t, f64 dt, const SetptVec& yd)
    {
        // Do setpoint filtering for wd and wddot
        static bool first = true;
        if (first) {
            FilterType::OutputVec init = FilterType::OutputVec::Zero();
            init.block<NUM_SETPTS, 1>(0,0) = yd;
            setptFilt.setOutput(init);
            first = false;
        }
        SetptVec wd    = yd;
        SetptVec wddot = SetptVec::Zero();
        if (setptFiltNatFreq > 0) {
            setptFilt.filter(t, dt, yd);
            wd    = setptFilt.getOutput().block<NUM_SETPTS, 1>(0,0);
            wddot = setptFilt.getOutput().block<NUM_SETPTS, 1>(0,1);
        }

        // TODO: can probably precalculate many of these matrices and inverses as long as we
        //       aren't adapting the inertia estimates
        // Calculate error and best-estimate terms
        SetptVec  Sw    = wd - x;
        InputMult GwEst = SystemType::derivG(x, estInertia);
        InputVec  FwEst = SystemType::derivF(x, estInertia);

        // Account for uncertainty
        InputVec  dmaxFw = calcDmaxFw(x, estInertia, minDInertia, maxDInertia);
        InputMult dminGw = SystemType::derivG(x, maxDInertia); // using maxDInertia here is correct
        InputMult dmaxGw = SystemType::derivG(x, minDInertia); // using minDInertia here is correct

        GainMatrix uncertainty = GainMatrix::Zero();
        uncertainty.diagonal() = (InputMult::Identity() - dminGw) * (wddot - FwEst).cwiseAbs()
                                 + dmaxFw + dmaxGw * GwEst * maxDisturbance;
        GainMatrix kw = dminGw.inverse() * (uncertainty + convergeGain);

        return GwEst.inverse() * (wddot - FwEst + kw*(boundaryGain*Sw));
    }

    inline InputVec calcDmaxFw(const StateVec& x, const InertiaVec& est,
                               const InertiaVec& minD, const InertiaVec& maxD)
    {
        InputVec ret;
        Eigen::Matrix<f64, 4, 1> opts;

        opts(0) = est(IZZ) - est(IYY) + (est(IYY)*maxD(IYY) - est(IZZ)*minD(IZZ))/minD(IXX);
        opts(1) = est(IZZ) - est(IYY) + (est(IYY)*minD(IYY) - est(IZZ)*maxD(IZZ))/minD(IXX);
        opts(2) = est(IZZ) - est(IYY) + (est(IYY)*maxD(IYY) - est(IZZ)*minD(IZZ))/maxD(IXX);
        opts(3) = est(IZZ) - est(IYY) + (est(IYY)*minD(IYY) - est(IZZ)*maxD(IZZ))/maxD(IXX);
        ret(0) = opts.cwiseAbs().maxCoeff() / est(IXX) * fabs(x(ANGVY)*x(ANGVZ));

        opts(0) = est(IXX) - est(IZZ) + (est(IZZ)*maxD(IZZ) - est(IXX)*minD(IXX))/minD(IYY);
        opts(1) = est(IXX) - est(IZZ) + (est(IZZ)*minD(IZZ) - est(IXX)*maxD(IXX))/minD(IYY);
        opts(2) = est(IXX) - est(IZZ) + (est(IZZ)*maxD(IZZ) - est(IXX)*minD(IXX))/maxD(IYY);
        opts(3) = est(IXX) - est(IZZ) + (est(IZZ)*minD(IZZ) - est(IXX)*maxD(IXX))/maxD(IYY);
        ret(1) = opts.cwiseAbs().maxCoeff() / est(IYY) * fabs(x(ANGVX)*x(ANGVZ));

        opts(0) = est(IYY) - est(IXX) + (est(IXX)*maxD(IXX) - est(IYY)*minD(IYY))/minD(IZZ);
        opts(1) = est(IYY) - est(IXX) + (est(IXX)*minD(IXX) - est(IYY)*maxD(IYY))/minD(IZZ);
        opts(2) = est(IYY) - est(IXX) + (est(IXX)*maxD(IXX) - est(IYY)*minD(IYY))/maxD(IZZ);
        opts(3) = est(IYY) - est(IXX) + (est(IXX)*minD(IXX) - est(IYY)*maxD(IYY))/maxD(IZZ);
        ret(2) = opts.cwiseAbs().maxCoeff() / est(IZZ) * fabs(x(ANGVX)*x(ANGVY));

        return ret;
    }

    /// Prints data in CSV form to the given ostream with given leader string,
    ///   deliminator, and line terminator.
    //    Format: leader, time, state, setpt, setptFilt, setptDotFilt
    /// Note that there WILL be a trailing deliminator on each line.
    using ParentType::printCsv;
    virtual void printCsv(std::ostream& os, const StateVec& x, f64 t,
                                            const SetptVec& yd = SetptVec::Zero(),
                                            const std::string& leader = "",
                                            const std::string& delim = ",",
                                            const std::string& term = "\n") const
    {
        ParentType::printCsv(os, x, t, yd, leader, delim, "");

        FilterType::OutputVec wdFilt = setptFilt.getOutput();
        os << "ydFilt" << delim;
        for (int i = 0; i < NUM_SETPTS; ++i) { os << wdFilt(i,0) << delim; }
        os << "yddotFilt" << delim;
        for (int i = 0; i < NUM_SETPTS; ++i) { os << wdFilt(i,1) << delim; }

        if (!term.empty()) os << term;
    }

    /// Swap
    inline void swap(AngVelOnlySmc& o) { ParentType::swap(o); boundaryGain.swap(o.boundaryGain);
        convergeGain.swap(o.convergeGain); std::swap(setptFiltNatFreq, o.setptFiltNatFreq);
        setptFilt.swap(o.setptFilt); estInertia.swap(o.estInertia);
        minDInertia.swap(o.minDInertia); maxDInertia.swap(o.maxDInertia);
        maxDisturbance.swap(o.maxDisturbance); }
    /// Copy Ctor
    AngVelOnlySmc(const AngVelOnlySmc& o) : ParentType(o), boundaryGain(o.boundaryGain),
        convergeGain(o.convergeGain), setptFiltNatFreq(o.setptFiltNatFreq), setptFilt(o.setptFilt),
        estInertia(o.estInertia), minDInertia(o.minDInertia), maxDInertia(o.maxDInertia),
        maxDisturbance(o.maxDisturbance) {}
    /// Move Ctor
    AngVelOnlySmc(AngVelOnlySmc&& o) { swap(o); }
    /// Unifying Assignment
    AngVelOnlySmc& operator=(AngVelOnlySmc o) { swap(o); return *this; }
};

} /* Kestrel */
