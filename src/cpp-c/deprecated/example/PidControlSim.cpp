#include <iostream>

#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "kestrel/control/System.hpp"
#include "kestrel/control/PidController.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace Control;

int main(int argc, const char *argv[])
{
    System<f64, 2, 1, 1> sys; // d/dt { x, xdot } = { xdot, u } by default

    Matrix<f64, 1, 3> gains; gains << 4, 0.5, 3.0;
    f64 tf = 0.2;
    PidController<f64, 1, decltype(sys)> ctrl(&sys, gains, tf, 4);

    f64 tmax = 60;
    f64 dt = 0.1;
    for (f64 t = 0.0; t < tmax - MathUtil::TINY; t += dt) {
        Matrix<f64, 1, 1> yd;
        if (t < 2) {
            yd(0) = 0;
        } else if (t < 10) {
            yd(0) = 2; // step up
        } else if (t < 20) {
            yd(0) = 0; // step back down
        } else if (t < 40) {
            yd(0) = 1*sin(2*MathUtil::PI/4.0 * (t - 20));
        } else {
            yd(0) = 1*cos(2*MathUtil::PI/4.0 * (t - 40));
        }

        decltype(sys)::InputVec u = ctrl.control(t, dt, yd);
        decltype(sys)::NoiseVec w;
        w.setZero();

        sys.printCsv(cout, t, u, w, "SYS");
        ctrl.printCsv(cout, t, yd, "SET");
        sys.integ(t, dt, u, w);
    }
    sys.printCsv(cout, 100.0);

    return 0;
}
