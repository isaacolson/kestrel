#pragma once

#include <iostream>

#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/EigenUtil.hpp"

#include "kestrel/control/LinearFilter.hpp"

namespace Kestrel {
namespace Control {

/** Models a filter of the Laplacian form:
 *      X(s) = wn^2 / (wn^2 + 2*zeta*wn*s + s^2)  * Y(s)
 *  Where x is the output, y is the input, wn is the natural frequency, and zeta is the damping
 *  ratio.
 *
 *  Different from a normal linear filter, this filter allows a configurable limit on the norm
 *  of the output derivative.
 */
template <typename _Scalar, int _NUM_ELTS=1>
class VelLimSecOrderLowpass: public LinearFilter<_Scalar, 3, 1, _NUM_ELTS> {
  public:
    typedef LinearFilter<_Scalar, 3, 1, _NUM_ELTS> ParentType;
    typedef _Scalar Scalar;
    static const int NUM_DENOM  = 3;
    static const int NUM_NUMER  = 1;
    static const int NUM_ELTS   = _NUM_ELTS;
    static const int NUM_OUTPUT = 2;

    typedef Eigen::Matrix<Scalar, NUM_DENOM, 1>         DenomVec;
    typedef Eigen::Matrix<Scalar, NUM_NUMER, 1>         NumerVec;
    /// ordered with each input having a row of increasing order derivatives
    typedef Eigen::Matrix<Scalar, NUM_ELTS, NUM_NUMER>  InputVec;
    /// ordered with each output having a row of increasing order derivatives
    typedef Eigen::Matrix<Scalar, NUM_ELTS, NUM_OUTPUT> OutputVec;
    /// velocity component for each element
    typedef Eigen::Matrix<Scalar, NUM_ELTS, 1>          VelVec;

  protected:
    using ParentType::denomCoeffs;
    using ParentType::numerCoeffs;

    // TODO: allow velocity to be limited by vector magnitude of the elements instead of cwise
    //       (important for xy control)
    VelVec velMax;
    bool   normMode; // If true, restrict the velocity vectors magnitude instead of cwise

  public:
    /// Ctor
    VelLimSecOrderLowpass(Scalar natFreq = 1, Scalar dampRatio = 1, VelVec velMax = VelVec::Zero(),
                          sz order = 1, bool storeLast = false) :
        ParentType(createSecondOrderLP<Scalar, NUM_ELTS>(natFreq, dampRatio, order, storeLast)),
        velMax(velMax), normMode(false) {}
    /// Ctor for normMode
    VelLimSecOrderLowpass(Scalar natFreq, Scalar dampRatio, Scalar velMaxNorm,
                          sz order = 1, bool storeLast = false) :
        ParentType(createSecondOrderLP<Scalar, NUM_ELTS>(natFreq, dampRatio, order, storeLast)),
        velMax(VelVec::Constant(velMaxNorm)), normMode(true) {}
    /// Dtor
    virtual ~VelLimSecOrderLowpass() {}

    inline VelVec getVelMax() const { return velMax; }
    inline void setVelMax(const VelVec& velMax) { this->velMax = velMax; }
    inline bool getNormMode() const { return normMode; }
    inline void setNormMode(bool normMode) { this->normMode = normMode; }

    using ParentType::deriv;
    /// The derivative of the output vector given the output vector and the input
    virtual OutputVec deriv(const OutputVec& x, f64 t = 0.0,
                            const InputVec& y = InputVec::Zero()) const
    {
        OutputVec ret = OutputVec::Zero();

        // Note: here we know that NUM_OUTPUT=2, but might as well leave it to be
        //       potentially generalizable in the future (similarly for NUM_DENOM)

        // linear integration
        ret.template block<NUM_ELTS, NUM_OUTPUT-1>(0, 0) =
            x.template block<NUM_ELTS, NUM_OUTPUT-1>(0, 1);

        // Solution to the ODE in a velocity limiting manner
        VelVec optVel  = (y * numerCoeffs - x.template block<NUM_ELTS, NUM_OUTPUT-1>(0, 0)
                                            * denomCoeffs.template block<NUM_DENOM-2, 1>(0, 0))
                         / denomCoeffs(NUM_DENOM-2);

        VelVec constrainedVel;
        if (!normMode) {
            constrainedVel = optVel.cwiseMin(velMax);
            constrainedVel = constrainedVel.cwiseMax(-velMax);

            for (int i = 0; i < NUM_ELTS; ++i) {
                if (velMax(i) <= 0) {
                    constrainedVel(i) = optVel(i);
                }
            }
        } else {
            constrainedVel = optVel;
            Scalar norm = optVel.norm();
            if (0 < velMax(0) && velMax(0) < norm) {
                constrainedVel *= (velMax(0) / norm);
            }
        }

        ret.template block<NUM_ELTS, 1>(0, NUM_OUTPUT-1) =
            (constrainedVel - x.template block<NUM_ELTS, 1>(0, NUM_OUTPUT-1))
            * denomCoeffs(NUM_DENOM-2) / denomCoeffs(NUM_DENOM-1);

        return ret;
    }

    using ParentType::filter;
    using ParentType::correctDomain;
    using ParentType::printCsv;

    /// Swap
    inline void swap(VelLimSecOrderLowpass& o) { ParentType::swap(o); velMax.swap(o.velMax);
        std::swap(normMode, o.normMode); }
    /// Copy Ctor
    VelLimSecOrderLowpass(const VelLimSecOrderLowpass& o) : ParentType(o), velMax(o.velMax),
        normMode(o.normMode) {}
    /// Move Ctor
    VelLimSecOrderLowpass(VelLimSecOrderLowpass&& o) { swap(o); }
    /// Unifying Assignment
    VelLimSecOrderLowpass& operator=(VelLimSecOrderLowpass o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
