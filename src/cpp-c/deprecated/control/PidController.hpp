#pragma once

#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/EigenUtil.hpp"

#include "kestrel/control/System.hpp"
#include "kestrel/control/LinearFilter.hpp"
#include "kestrel/control/Controller.hpp"

namespace Kestrel {
namespace Control {

template <typename _Scalar, int _NUM_SETPTS, typename _SystemType>
class PidController: public Controller<_Scalar, _NUM_SETPTS, _SystemType> {
  public:
    typedef _Scalar Scalar;
    typedef _SystemType SystemType;
    static const int NUM_SETPTS = _NUM_SETPTS;
    static const int NUM_STATES = SystemType::NUM_STATES;
    static const int NUM_INPUTS = SystemType::NUM_INPUTS;

    typedef Controller<Scalar, NUM_SETPTS, SystemType> ParentType;

    typedef Eigen::Matrix<Scalar, NUM_SETPTS, 1> SetptVec;
    typedef Eigen::Matrix<Scalar, NUM_STATES, 1> StateVec;
    typedef Eigen::Matrix<Scalar, NUM_INPUTS, 1> InputVec;

    // Note: setpt vector and input vector to system assumed to be of same size and in same order

    /// Matrix containing rows of [Kp, Ki, Kd] for each setpoint/ctrl-input
    typedef Eigen::Matrix<Scalar, NUM_SETPTS, 3> GainsMatrix;
    /// Matrix containing rows of [error, int(error), deriv(error) for each setpoint
    typedef Eigen::Matrix<Scalar, NUM_SETPTS, 3> ErrorMatrix;
    typedef Eigen::Matrix<Scalar, NUM_SETPTS, 1> DerivFilterGainVec;
    typedef LinearFilter<Scalar, 2>              DerivFilter;

    // The transfer function for the controller is: ctrl = (Kp + Ki / s + Kd * s / (tf*s + 1))*error
    // If tf = 0, there is no filter action on the derivative. Note for good performance, this
    // should NOT be smaller than your sample timestep.

  protected:
    using ParentType::sys;
    SetptVec           lastSetpt;
    ErrorMatrix        error;
    GainsMatrix        gains;
    DerivFilterGainVec tf;
    // TODO: as soon as filters let you do more than 1 variable at once (with different filter
    //       consts), stop using an array here
    DerivFilter        filt[NUM_SETPTS];

  public:
    /// Ctor
    PidController(const SystemType* sys = nullptr, const GainsMatrix& gains = GainsMatrix::Zero(),
                  const DerivFilterGainVec& tf = DerivFilterGainVec::Zero(),
                  sz derivFiltIntOrder = 1) :
        ParentType(sys), lastSetpt(SetptVec::Zero()), error(ErrorMatrix::Zero()),
                         gains(gains), tf(tf)
    {
        for (int i = 0; i < NUM_SETPTS; ++i) {
            if (tf(i) > 0) {
                filt[i] = DerivFilter({1, tf(i)}, 1, 0, derivFiltIntOrder);
            }
        }
    }
    /// Ctor with scalar tf
    PidController(const SystemType* sys, const GainsMatrix& gains, Scalar tf,
                  sz derivFiltIntOrder = 1) :
        ParentType(sys), lastSetpt(SetptVec::Zero()), error(ErrorMatrix::Zero()),
                         gains(gains), tf(DerivFilterGainVec::Constant(tf))
    {
        static_assert(NUM_SETPTS == 1, "scalar ctor only for single setpoint");
        if (tf > 0) filt[0] = DerivFilter({1, tf}, 1, 0, derivFiltIntOrder);
    }
    /// Dtor
    virtual ~PidController() {}

    /// Returns the internal gains matrix
    inline GainsMatrix getGains() const { return gains; }
    /// Sets the internal gains matrix
    inline void setGains(const GainsMatrix& gains) { this->gains = gains; }
    /// Returns the internal error matrix: [ prop, int, deriv ]
    inline ErrorMatrix getError() const { return error; }
    // TODO: consider allowing user to change derivative filter coeffs after construction

    /***************************************************************************/
    /* User Implemented ********************************************************/
    /***************************************************************************/

    /// Returns the control input given a state and a setpoint
    virtual InputVec control(const StateVec& x, f64 t, f64 dt, const SetptVec& yd)
    {
        // Note: though this function is not pure virtual, all children should override it

        Assert(NUM_SETPTS == NUM_INPUTS,
               "Base PID class only allows 1 to 1 mapping of setpoints to inputs");

        SetptVec y             = stateToSetpt(x);
        SetptVec yDot          = stateToSetptDeriv(x);
        SetptVec propError     = yd - y;
        SetptVec lastPropError = error.template block<NUM_SETPTS, 1>(0, 0);

        error.template block<NUM_SETPTS, 1>(0, 0) = propError;
        if (dt > 0) {
            error.template block<NUM_SETPTS, 1>(0, 1) += dt*propError;
            error.template block<NUM_SETPTS, 1>(0, 2) = (propError - lastPropError) / dt;
        } else {
            error.template block<NUM_SETPTS, 1>(0, 2).setZero();
        }

        for (int i = 0; i < NUM_SETPTS; ++i) {
            if (dt > 0) {
                // Apply the mapped derivatives
                Scalar derivErrorOffset = 0.0;
                if (derivMappable(i)) {
                    // derivError = ((ydcurr - ycurr) - (ydlast - ylast)) / dt
                    // If we are using a mapped derivative, we instead want:
                    // derivError = (ydcurr - ydlast) / dt - ydotcurr
                    error(i, 2) = (yd(i) - lastSetpt(i)) / dt;
                    derivErrorOffset = yDot(i);
                }

                // run the filter
                if (tf(i) > 0) error(i, 2) = filt[i].filter(t, dt, error(i, 2))(0);

                // Subtract out the mapped derivative, if any
                error(i, 2) -= derivErrorOffset;
            }
        }
        lastSetpt = yd;

        InputVec u;
        for (int i = 0; i < NUM_INPUTS; ++i) {
            u(i) = gains.template block<1, 3>(i, 0).dot(error.template block<1, 3>(i, 0));
        }
        return u;
    }

    /// Maps the state vector into the setpt space, could be a 1 to 1 mapping or a more complex func
    virtual SetptVec stateToSetpt(const StateVec& x) const
    {
        // Note: though this function is not pure virtual, all children should override it

        // Simply a mapping of the first NUM_SETPTS elements of the state vector
        return x.template block<NUM_SETPTS, 1>(0, 0);
    }

    /// Returns if the derivative mapping is valid for the given setpt index
    virtual inline bool derivMappable(int idx) const
    {
        // Note: though this function is not pure virtual, all children should override it

        // For this example, there is only derivative mapping if there are 2x states as setpts
        if (NUM_STATES == 2*NUM_SETPTS) return true;
        return false;
    }

    /// Maps the state vector into the derivative of the setpt space if such a mapping exists
    virtual SetptVec stateToSetptDeriv(const StateVec& x) const
    {
        // Note: though this function is not pure virtual, all children should override it

        // For this example, there is only derivative mapping if there are 2x states as setpts
        if (NUM_STATES == 2*NUM_SETPTS) return x.template block<NUM_SETPTS, 1>(NUM_SETPTS, 0);
        return SetptVec::Zero();
    }

    /***************************************************************************/
    /* End User Implemented ****************************************************/
    /***************************************************************************/

    /***************************************************************************/
    /* Functions operating on the internal state *******************************/
    /***************************************************************************/

    /* Note: Implementors MUST specify "using ParentType::funcName;" for each of the
     * following so that the redefinitions of the above virtual functions don't shadow
     * these implementations */

    using ParentType::control;

    /// Maps the state vector into the setpt space using the internal system's state vector
    inline SetptVec stateToSetpt() const
    { Assert(sys != nullptr, "no internal system"); return stateToSetpt(sys->getState()); }

    /// Maps the state vector into the deriv of the setpt space using the internal system's state
    inline SetptVec stateToSetptDeriv() const
    { Assert(sys != nullptr, "no internal system"); return stateToSetptDeriv(sys->getState()); }

    /***************************************************************************/
    /* End Functions operating on the internal state ***************************/
    /***************************************************************************/

    /// Prints data in CSV form to the given ostream with given leading string,
    ///   deliminator, and line terminator. Format: leader, time, state, setpt, map(state->setpt),
    //    map(state->setptDeriv), error
    /// Note that there WILL be a trailing deliminator on each line.
    virtual void printCsv(std::ostream& os, const StateVec& x, f64 t,
                                            const SetptVec& yd = SetptVec::Zero(),
                                            const std::string& leader = "",
                                            const std::string& delim = ",",
                                            const std::string& term = "\n") const
    {
        ParentType::printCsv(os, x, t, yd, leader, delim, "");

        Eigen::IOFormat fmt = EigenUtil::linearFormat(delim);

        SetptVec y    = stateToSetpt(x);
        SetptVec ydot = stateToSetptDeriv(x);
        os << "y" << delim << y.format(fmt) << delim;
        os << "ydot" << delim << ydot.format(fmt) << delim;
        // Note: the following actually outputs the error matrix by row, i.e. it
        //       will output error(0, 0), error(0, 1), error(0, 2), ... This implies that it
        //       will be outputing groups of {propError, intError, derivError}
        os << "error" << delim << error.format(fmt) << delim;
        if (!term.empty()) os << term;
    }
    using ParentType::printCsv;

    /// Swap
    inline void swap(PidController& o)
    {
        ParentType::swap(o);
        lastSetpt.swap(o.lastSetpt);
        error.swap(o.error);
        gains.swap(o.gains);
        tf.swap(o.tf);
        for (int i = 0; i < NUM_SETPTS; ++i) {
            filt[i].swap(o.filt[i]);
        }
    }
    /// Copy Ctor
    PidController(const PidController& o) : ParentType(o), lastSetpt(o.lastSetpt), error(o.error),
                                                           gains(o.gains), tf(o.tf)
    {
        for (int i = 0; i < NUM_SETPTS; ++i) {
            filt[i] = o.filt[i];
        }
    }
    /// Move Ctor
    PidController(PidController&& o) { swap(o); }
    /// Unifying Assignment
    PidController& operator=(PidController o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
