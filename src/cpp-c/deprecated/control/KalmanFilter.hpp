#pragma once

#include <iostream>
#include <cmath>
#include <utility>
#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "kestrel/control/Integrator.hpp"
#include "kestrel/control/GaussianIntegrator.hpp"

namespace Kestrel {
namespace Control {

template <typename _Scalar, int _NUM_STATES, int _NUM_INPUTS, int _NUM_NOISES>
class KalmanFilter {
  public:
    typedef _Scalar Scalar;
    static const int NUM_STATES = _NUM_STATES;
    static const int NUM_INPUTS = _NUM_INPUTS;
    static const int NUM_NOISES = _NUM_NOISES;

    typedef Eigen::Matrix<Scalar, NUM_STATES, 1>          StateVec;
    typedef Eigen::Matrix<Scalar, NUM_INPUTS, 1>          InputVec;
    typedef Eigen::Matrix<Scalar, NUM_NOISES, 1>          NoiseVec;
    typedef Eigen::Matrix<Scalar, NUM_STATES, NUM_STATES> StateCov;
    typedef Eigen::Matrix<Scalar, NUM_STATES, NUM_STATES> StateJac;
    typedef Eigen::Matrix<Scalar, NUM_NOISES, NUM_NOISES> NoiseCov;
    typedef Eigen::Matrix<Scalar, NUM_STATES, NUM_NOISES> NoiseJac;

    typedef std::pair<StateVec, StateCov>                 StateDist;
    typedef GaussianIntegrator<Scalar, NUM_STATES>        GaussianInt;

    // Note: have to use "int" here because Eigen uses "int" (else template resolution fails)
    // TODO: maybe it would be better to just completely switch over to "int" instead.
    //       Eigen uses int because "Dynamic" is actually -1, so maybe we could switch to int
    //       as well and allow Dynamic sizes
    template<int NUM_OBS> using ObsVec     = Eigen::Matrix<Scalar, NUM_OBS, 1>;
    template<int NUM_OBS> using ObsCov     = Eigen::Matrix<Scalar, NUM_OBS, NUM_OBS>;
    template<int NUM_OBS> using ObsJac     = Eigen::Matrix<Scalar, NUM_OBS, NUM_STATES>;
    template<int NUM_OBS> using KalmanGain = Eigen::Matrix<Scalar, NUM_STATES, NUM_OBS>;

  private:
    GaussianInt i;

  public:
    /// Ctor
    KalmanFilter(sz order=1): i(order) {}
    /// Dtor
    virtual ~KalmanFilter() {}

    // Note: often it the user will pass a const InputVec& u as one of Arg2&&... args
    template <typename... Args1, typename... Args2>
    StateDist predict(const StateVec& x, const StateCov& P, f64 t, f64 dt,
                      StateVec   (*f)(const StateVec& x, f64 t, Args1... args),
                      StateJac   (*F)(const StateVec& x, f64 t, Args1... args),
                      NoiseCov   (*Q)(const StateVec& x, f64 t, Args1... args),
                      NoiseJac   (*L)(const StateVec& x, f64 t, Args1... args),
                      Args2&&... args) const
    {
        // Note: https://hal.archives-ouvertes.fr/hal-00904376/document has a derivation of
        //       the C-D EKF that has the equations as:
        //       xdot = f(x,t,u)
        //       Pdot = F(x,t,u) * P + P * F(x, t, u)' + L(x,t,u) * Q * L(x,t,u)'
        return i(x, P, t, dt, f, F, Q, L, std::forward<Args2>(args)...);
    }

    template <int NUM_OBS>
    StateDist correct(const StateVec&        x, const StateCov&        P,
                      const ObsVec<NUM_OBS>& z, const ObsCov<NUM_OBS>& R,
                      const ObsVec<NUM_OBS>& h, const ObsJac<NUM_OBS>& H) const
    {
        // Note: we don't use function pointers in this because we aren't using integrators, so
        //       there is nothing to gain from using the pointers - you only need to evaluate
        //       the matrices once, so might as well just pass them in

        ObsCov<NUM_OBS> S = H * P * H.transpose() + R;

        // Determinant check : we are actually worried here if the determinant is less than
        //                     TINY2^(NUM_OBS) because each covariance on the diagonal might
        //                     be on the order of TINY2 (it's a square term), and in the diag
        //                     matrix case, the determinant is the product of the diagonals
        f64 minValid = 1;
        for (int i = 0; i < NUM_OBS; ++i) { minValid *= MathUtil::TINY2; }
        if (S.determinant() < minValid) {
            static int prints = 5;
            if (prints-- > 0) {
                std::cerr << "WARNING: Innovation covariance not invertible" << std::endl;
                std::cerr << "P" << std::endl << P << std::endl;
                std::cerr << "H" << std::endl << H << std::endl;
                std::cerr << "R" << std::endl << R << std::endl;
                std::cerr << "S = H * P * H' + R" << std::endl << S << std::endl;
                std::cerr << "det(S) = " << S.determinant() << std::endl;
            }
            // XXX: not sure if this is the right thing to do, but attempting to inflate the
            //      covariance of the diagonal terms of P to try to make it invertible next time
            //      may be better to actually just assert, or may be better to try fix it more
            //      intelligently and scream louder to alert user.
            // Note: this assumes that Scalar has an implemented min function
            Scalar minVal = P(0,0);
            for (int i = 1; i < NUM_STATES; ++i) {
                minVal = std::min(minVal, P(i,i));
            }
            if (minVal < 0) minVal = -1.1 * minVal;
            if (minVal < MathUtil::TINY) minVal = MathUtil::TINY;

            return std::make_pair(x, P + StateCov::Identity()*minVal);
        }

        KalmanGain<NUM_OBS> K = P * H.transpose() * S.inverse();
        return std::make_pair(x + K*(z - h), (StateCov::Identity() - K * H) * P);
    }

    /// Swap
    inline void swap(KalmanFilter& o) { i.swap(o.i); }
    /// Copy Ctor
    KalmanFilter(const KalmanFilter& o): i(o.i) {}
    /// Move Ctor
    KalmanFilter(KalmanFilter&& o) { swap(o); }
    /// Unifying Assignment
    KalmanFilter& operator=(KalmanFilter o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
