#pragma once

#include <iostream>
#include <cxxtest/TestSuite.h>
#include <Eigen/Dense>

#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/TestUtil.hpp"

#include "System.hpp"
#include "PidController.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace MathUtil;
using namespace TestUtil;
using namespace Control;

class PidControllerTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testInit()
    {
        System<f64, 2, 1> sys;
        Matrix<f64, 1, 3> gains; gains << 1, 0, 0;
        PidController<f64, 1, decltype(sys)> ctrl(&sys, gains);
        TS_ASSERT(ctrl.getSys() == &sys);
        TS_ASSERT(matEquals(ctrl.getGains(), gains));
    }

    void testControl()
    {
        System<f64, 3, 1> sys;
        Matrix<f64, 1, 3> gains; gains << 2, 1, 0.5;
        f64 tf = 0.0;
        PidController<f64, 1, decltype(sys)> ctrl(&sys, gains, tf, 4);

        f64 dt = 0.5;
        f64 t  = 0;
        Matrix<f64, 1, 1> yd; yd << 1;
        Matrix<f64, 1, 1> u = ctrl.control(t, dt, yd);

        Matrix<f64, 1, 3> error = ctrl.getError();

        TS_ASSERT_DELTA(error(0), 1, TINY);
        TS_ASSERT_DELTA(error(1), 1*dt, TINY);
        TS_ASSERT_DELTA(error(2), 1/dt, TINY);
        TS_ASSERT_DELTA(u(0), error(0)*2 + error(1)*1 + error(2)*0.5, TINY);

        // Ensure derivatives are not accounting for system derivatives
        ctrl = PidController<f64, 1, decltype(sys)>(&sys, gains, tf, 4);
        ctrl.control({0, 1, 1}, t, dt, yd);
        error = ctrl.getError();
        TS_ASSERT_DELTA(error(2), 1/dt, TINY);
    }

    void testFilteredControl()
    {
        System<f64, 3, 1> sys;
        Matrix<f64, 1, 3> gains; gains << 2, 1, 0.5;
        f64 tf = 1.0;
        PidController<f64, 1, decltype(sys)> ctrl(&sys, gains, tf, 4);

        f64 dt = 0.5;
        f64 t  = 0;
        Matrix<f64, 1, 1> yd; yd << 1;

        f64 prevDeriv = 1/dt;
        for (sz i = 0; i < 10; ++i) {
            ctrl.control(t, dt, yd);
            Matrix<f64, 1, 3> error = ctrl.getError();
            TS_ASSERT_LESS_THAN(error(2), prevDeriv);
            TS_ASSERT_LESS_THAN(0, error(2));
            prevDeriv = error(2);
        }

        ctrl = PidController<f64, 1, decltype(sys)>(&sys, gains, tf, 4);
        prevDeriv = 0;
        for (sz i = 0; i < 10; ++i) {
            yd(0) = i+1;
            ctrl.control(t, dt, yd);
            Matrix<f64, 1, 3> error = ctrl.getError();
            TS_ASSERT_LESS_THAN(error(2), 1/dt);
            TS_ASSERT_LESS_THAN(prevDeriv, error(2));
            prevDeriv = error(2);
        }
    }

    void testDerivMapping()
    {
        System<f64, 2, 1> sys;
        Matrix<f64, 1, 3> gains; gains << 2, 1, 0.5;
        f64 tf = 0.0;
        PidController<f64, 1, decltype(sys)> ctrl(&sys, gains, tf, 4);

        f64 dt = 0.5;
        f64 t  = 0;
        Matrix<f64, 1, 1> yd; yd << 1;
        f64 y    = 0;
        f64 ydot = 2;
        // cancel out part of the change in setpoint with the given derivative
        Matrix<f64, 1, 1> u = ctrl.control({y, ydot}, t, dt, yd);

        Matrix<f64, 1, 3> error = ctrl.getError();

        TS_ASSERT_DELTA(error(0), yd(0) - y, TINY);
        TS_ASSERT_DELTA(error(1), (yd(0) - y)*dt, TINY);
        TS_ASSERT_DELTA(error(2), yd(0)/dt - ydot, TINY);
        TS_ASSERT_DELTA(u(0), error.dot(gains), TINY);

        // Ensure that applying control again still has the right derivative error
        f64 ydlast = yd(0);
        yd(0) = 2;
        y = -1;
        ydot = -1;
        ctrl.control({y, ydot}, t, dt, yd);
        error = ctrl.getError();
        TS_ASSERT_DELTA(error(2), (yd(0)-ydlast)/dt - ydot, TINY);

        // Ensure that applying control again still has the right derivative error
        ydlast = yd(0);
        yd(0) = 10;
        y = -10;
        ydot = 10;
        ctrl.control({y, ydot}, t, dt, yd);
        error = ctrl.getError();
        TS_ASSERT_DELTA(error(2), (yd(0)-ydlast)/dt - ydot, TINY);
    }

    void testMultidimControl()
    {
        System<f64, 4, 2> sys;
        Matrix<f64, 2, 3> gains; gains << 2, 1, 0.5,
                                          3, 2, 1.0;
        Matrix<f64, 2, 1> tf; tf << 1.0, 2.0;
        PidController<f64, 2, decltype(sys)> ctrl(&sys, gains, tf, 4);

        f64 dt = 0.1;
        f64 t  = 0;
        Matrix<f64, 2, 1> yd; yd << 2, 1;
        Matrix<f64, 2, 1> u = ctrl.control(t, dt, yd);
        Matrix<f64, 2, 3> error = ctrl.getError();

        TS_ASSERT_DELTA(error(0,0), 2, TINY);
        TS_ASSERT_DELTA(error(0,1), 2*dt, TINY);
        TS_ASSERT_LESS_THAN(0, error(0,2));
        TS_ASSERT_LESS_THAN(error(0,2), 2/dt);
        TS_ASSERT_DELTA(error(1,0), 1, TINY);
        TS_ASSERT_DELTA(error(1,1), 1*dt, TINY);
        TS_ASSERT_LESS_THAN(0, error(1,2));
        TS_ASSERT_LESS_THAN(error(1,2), 1/dt);

        TS_ASSERT_DELTA(u(0), error(0,0)*gains(0,0)+error(0,1)*gains(0,1) + error(0,2)*gains(0,2),
                        TINY);
        TS_ASSERT_DELTA(u(1), error(1,0)*gains(1,0)+error(1,1)*gains(1,1) + error(1,2)*gains(1,2),
                        TINY);

        tf.setZero();
        ctrl = PidController<f64, 2, decltype(sys)>(&sys, gains, tf, 4);
        ctrl.control({0, 0, 1, 2}, t, dt, yd);
        error = ctrl.getError();

        TS_ASSERT_DELTA(error(0,2), 2/dt-1, TINY);
        TS_ASSERT_DELTA(error(1,2), 1/dt-2, TINY);
    }
};
