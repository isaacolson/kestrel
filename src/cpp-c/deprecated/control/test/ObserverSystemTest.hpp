#pragma once

#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/TestUtil.hpp"

#include "ObserverSystem.hpp"

using namespace Eigen;
using namespace Kestrel;
using namespace TestUtil;
using namespace Control;

class ObserverSystemTest: public CxxTest::TestSuite {
  private:
    typedef Eigen::Matrix<f64, 3, 1> StateVec;
    typedef Eigen::Matrix<f64, 3, 3> StateCov;
    typedef ObserverSystem<f64, 3, 1, 1> SysType;

    SysType sys;

  public:
    void setUp() override { sys = SysType(); }
    void tearDown() override {}

    void testInit()
    {
        TS_ASSERT(matEquals(sys.getState(), StateVec::Zero()));
        TS_ASSERT(matEquals(sys.getCovar(), StateCov::Identity()));
    }

    void testEigenIdentityNonSquare()
    {
        Eigen::Matrix<f64, 3, 2> I32 = Eigen::Matrix<f64, 3, 2>::Identity();
        Eigen::Matrix<f64, 3, 2> expected32; expected32 << 1, 0,
                                                           0, 1,
                                                           0, 0;
        TS_ASSERT(matEquals(I32, expected32));

        Eigen::Matrix<f64, 2, 3> I23 = Eigen::Matrix<f64, 2, 3>::Identity();
        Eigen::Matrix<f64, 2, 3> expected23; expected23 << 1, 0, 0,
                                                           0, 1, 0;
        TS_ASSERT(matEquals(I23, expected23));
    }

    void testEigenMatrixEltSingleSubscriptAccess()
    {
        Eigen::Matrix<f64, 2, 2> m; m << 1, 2,
                                         3, 4;

        // Note: you have to use the parens, cannot use brackets
        TS_ASSERT_EQUALS(m(0), 1);
        TS_ASSERT_EQUALS(m(1), 3); // This might not be what you'd expect
        TS_ASSERT_EQUALS(m(2), 2); // This might not be what you'd expect
        TS_ASSERT_EQUALS(m(3), 4);
        // It appears that by default matrices are stored columns first
        // (which is probably obvious if you read the documentation, but this proves it)
    }
};
