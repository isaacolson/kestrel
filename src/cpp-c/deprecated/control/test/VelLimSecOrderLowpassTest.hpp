#pragma once

#include <iostream>
#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/TestUtil.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "LinearFilter.hpp"
#include "VelLimSecOrderLowpass.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace MathUtil;
using namespace TestUtil;
using namespace Control;

class VelLimSecOrderLowpassTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testEquivOneDim()
    {
        Matrix<f64, 1, 1> maxVel; maxVel << 0;

        auto f1 = createSecondOrderLP<f64, 1>(3, 1, 4, true);
        auto f2 = VelLimSecOrderLowpass<f64, 1>(3, 1, maxVel, 4, true);

        Matrix<f64, 1, 1> y; y << 10;

        Matrix<f64, 1, 2> x; x << 1, 2;

        TS_ASSERT(matEquals(f1.deriv(0, y), f2.deriv(0, y)));
        TS_ASSERT(matEquals(f1.deriv(x, 0, y), f2.deriv(x, 0, y)));
        TS_ASSERT(matEquals(f1.filter(0, 0.1, y), f2.filter(0, 0.1, y)));
    }

    void testEquivMultiDim()
    {
        Matrix<f64, 3, 1> maxVel; maxVel << 0, 0, 0;

        auto f1 = createSecondOrderLP<f64, 3>(3, 1, 4, true);
        auto f2 = VelLimSecOrderLowpass<f64, 3>(3, 1, maxVel, 4, true);

        Matrix<f64, 3, 1> y; y << 10, -20, 30;

        Matrix<f64, 3, 2> x; x << 1, 2, 2, 3, 3, 4;

        TS_ASSERT(matEquals(f1.deriv(0, y), f2.deriv(0, y)));
        TS_ASSERT(matEquals(f1.deriv(x, 0, y), f2.deriv(x, 0, y)));
        TS_ASSERT(matEquals(f1.filter(0, 0.1, y), f2.filter(0, 0.1, y)));
    }

    void testMultiDimLimit()
    {
        Matrix<f64, 3, 1> maxVel; maxVel << 10, 10, 10;

        auto f1 = createSecondOrderLP<f64, 3>(3, 1, 4, true);
        auto f2 = VelLimSecOrderLowpass<f64, 3>(3, 1, maxVel, 4, true);

        Matrix<f64, 3, 1> y; y << 10, -20, 30;

        Matrix<f64, 3, 2> x; x << 1, 2, 2, 3, 3, 4;

        // in all cases, the filter should be restricted, so the
        for (sz i = 0; i < 3; ++i) {
            TS_ASSERT_LESS_THAN(fabs(f2.deriv(0, y)(i, 1)), fabs(f1.deriv(0, y)(i, 1)));
            TS_ASSERT_LESS_THAN(fabs(f2.deriv(x, 0, y)(i, 1)), fabs(f1.deriv(x, 0, y)(i, 1)));
        }

        f64 dt = 0.1;
        f64 tmax = 20;
        for (f64 t = 0; t < tmax - MathUtil::TINY; t += dt) {
            auto x = f2.filter(t, dt, y);
            for (sz i = 0; i < 3; ++i) {
                TS_ASSERT_LESS_THAN_EQUALS(fabs(x(i, 1)), maxVel(i));
            }
        }
    }

    void testMultiDimNormLimit()
    {
        f64 maxVel = 10;
        auto f = VelLimSecOrderLowpass<f64, 3>(3, 1, maxVel, 4, true);

        TS_ASSERT(f.getNormMode());

        Matrix<f64, 3, 1> y; y << 10, -20, 30;

        f64 dt = 0.1;
        f64 tmax = 20;
        for (f64 t = 0; t < tmax - MathUtil::TINY; t += dt) {
            auto x = f.filter(t, dt, y);
            f64 norm = x.block<3, 1>(0, 1).norm();
            TS_ASSERT_LESS_THAN_EQUALS(norm, maxVel);
        }
    }
};
