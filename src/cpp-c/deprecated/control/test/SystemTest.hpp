#pragma once

#include <iostream>

#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/TestUtil.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "System.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace TestUtil;
using namespace Control;

class SystemTest: public CxxTest::TestSuite {
  private:
    typedef System<f64, 3, 0, 0> Sys;
    Sys s;

  public:
    void setUp() override { s = Sys(); }
    void tearDown() override {}

    void testInit()
    {
        TS_ASSERT(matEquals(s.getState(), Vector3d::Zero()));

        Vector3d x{1, 2, 3};
        s = Sys(x);
        TS_ASSERT(matEquals(s.getState(), x));
    }

    void testDeriv()
    {
        Vector3d x{1, 2, 3};
        Vector3d expect{x[1], x[2], 0};

        s = Sys(x);
        TS_ASSERT(matEquals(s.deriv(s.getState()), expect));
        TS_ASSERT(matEquals(s.deriv(), expect));
    }

    void testInteg()
    {
        Vector3d x{1, 2, 3};
        Vector3d expect{x[0]+x[1], x[1]+x[2], x[2]};

        s = Sys(x);
        TS_ASSERT(matEquals(s.integ(s.getState(), 0, 1), expect));
        TS_ASSERT(matEquals(s.integ(0, 1), expect));
        TS_ASSERT(matEquals(s.getState(), expect));
    }

    void testEigenZeroDim()
    {
        Matrix<f64, 0, 1> a = Matrix<f64, 0, 1>::Zero();
        TS_ASSERT_EQUALS(a.rows(), 0);
        TS_ASSERT_EQUALS(a.cols(), 1);
    }

    void testEigenNegativeDim()
    {
        /*
         * this actuall throws a static assert in eigen because -1 is reserved for Dynamic
         *  Matrix<f64, -1, 1> a = Matrix<f64, -1, 1>::Zero();
         *  TS_ASSERT_EQUALS(a.rows(), -1);
         *  TS_ASSERT_EQUALS(a.cols(),  1);
         */
    }
};
