#pragma once

#include <iostream>
#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/TestUtil.hpp"
#include "kestrel/util/MathUtil.hpp"

#include "LinearFilter.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace MathUtil;
using namespace TestUtil;
using namespace Control;

class LinearFilterTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testLowpass()
    {
        LinearFilter<f64, 3, 1> f({1, 2, 1}, 1, {0, 0}, 1, true);

        f64 dt = 1;
        f64 y = 1;
        Matrix<f64, 1, 2> output = f.getOutput();
        Matrix<f64, 1, 2> deriv;
        Matrix<f64, 1, 2> expect;

        for (sz i = 0; i < 10; ++i) {
            deriv(0) = output(1);
            deriv(1) = (y - output(0) - 2*output(1));
            expect = output + dt * deriv;
            output = f.filter(0, dt, y);
            TS_ASSERT(matEquals(output, expect));
            TS_ASSERT_DELTA(y, f.getLastInput()(0), TINY);
            TS_ASSERT(matEquals(deriv, f.getLastDeriv()));
        }
    }

    void testHighpass()
    {
        f64 tau = 20;
        LinearFilter<f64, 2, 2> f({1, tau}, {0, tau}, 0, 4);

        f64 dt = 0.01;
        f64  t = 0;
        Matrix<f64, 1, 2> y;

        for (sz i = 0; i < 300; ++i) {
            y(0) = sin(2*t) + 2;
            y(1) = 2*cos(2*t);

            f.filter(0, dt, y);
            t += dt;
        }

        TS_ASSERT_DELTA(f.getOutput()(0), sin(2*t), 0.01);
    }

    void testMultiLowpass()
    {
        Matrix<f64, 4, 2> x0 = Matrix<f64, 4, 2>::Zero();
        LinearFilter<f64, 3, 1, 4> f({1, 2, 1}, 1, x0, 1, true);

        f64 dt = 1;
        Matrix<f64, 4, 1> y; y << 1, 2, 3, 4;
        Matrix<f64, 4, 2> output = f.getOutput();
        Matrix<f64, 4, 2> deriv;
        Matrix<f64, 4, 2> expect;

        for (sz i = 0; i < 10; ++i) {
            deriv.block<4,1>(0,0) = output.block<4,1>(0,1);
            deriv.block<4,1>(0,1) = y - output.block<4,1>(0,0) - 2*output.block<4,1>(0,1);
            expect = output + deriv*dt;
            output = f.filter(0, dt, y);
            TS_ASSERT(matEquals(output, expect));
            TS_ASSERT(matEquals(y, f.getLastInput()));
            TS_ASSERT(matEquals(deriv, f.getLastDeriv()));
        }
    }

    void testMultiHighpass()
    {
        f64 tau = 20;
        Matrix<f64, 3, 1> x0 = Matrix<f64, 3, 1>::Zero();
        LinearFilter<f64, 2, 2, 3> f({1, tau}, {0, tau}, x0, 4);

        f64 dt = 0.01;
        f64  t = 0;
        Matrix<f64, 3, 2> y;

        for (sz i = 0; i < 300; ++i) {
            y(0,0) = sin(2*t) + 2;   y(0,1) = 2*cos(2*t);
            y(1,0) = sin(3*t) + 2;   y(1,1) = 3*cos(3*t);
            y(2,0) = sin(4*t) + 2;   y(2,1) = 4*cos(4*t);

            f.filter(0, dt, y);
            t += dt;
        }

        Matrix<f64, 3, 1> x = f.getOutput();
        TS_ASSERT_DELTA(x(0), sin(2*t), 0.01);
        TS_ASSERT_DELTA(x(1), sin(3*t), 0.01);
        TS_ASSERT_DELTA(x(2), sin(4*t), 0.01);
    }

    void testNoFilter()
    {
        Matrix<f64, 1, 1> den; den << 1;
        LinearFilter<f64, 1, 1, 1, 1> f(den, 1, 0, 1);

        f64 dt = 0.01;
        f.filter(0, dt, 1);
        TS_ASSERT_DELTA(f.getOutput()(0), 1, TINY);
    }

    void testEigenIdentityOfVectors()
    {
        Vector3d a = Vector3d::Identity();
        Vector3d b(1.0, 0.0, 0.0);
        Matrix<f64, 1, 3> c = Matrix<f64, 1, 3>::Identity();
        Matrix<f64, 1, 3> d(1.0, 0.0, 0.0);
        TS_ASSERT(matEquals(a, b));
        TS_ASSERT(matEquals(c, d));
    }
};
