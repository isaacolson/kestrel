#pragma once

#include <iostream>
#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/TestUtil.hpp"

#include "System.hpp"
#include "KalmanFilter.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace TestUtil;
using namespace Control;

class BasicSystem: public System<f64, 3, 1, 1> {
  public:
    typedef System<f64, 3, 1, 1> Parent;

  public:
    /// Ctor
    BasicSystem(sz order): Parent(StateVec::Zero(), order) {}
    /// Dtor
    virtual ~BasicSystem() {}

    using Parent::deriv;
    StateVec deriv(const StateVec& x, f64 t = 0.0,
                   const InputVec& u = InputVec::Zero(),
                   const NoiseVec& w = NoiseVec::Zero()) const override
    {
        // Treats the system as a simple integrator with input and noise added to last state
        StateVec ret = x;
        ret.block(0, 0, 3-1, 1) = x.block(1, 0, 3-1, 1);
        ret(3-1) = u(0) + w(0);
        return ret;
    }
    // need to have the same variadic args as the handles below
    static StateVec derivHandle(const StateVec& x, f64 t, const BasicSystem& sys,
                                const InputVec& u)
    { return sys.deriv(x, t, u); }

    Matrix3d F(const StateVec& x, f64 t) const
    {
        Matrix3d F; F << 0, 1, 0,
                         0, 0, 1,
                         0, 0, 0;
        return F;
    }
    static Matrix3d Fhandle(const StateVec& x, f64 t, const BasicSystem& sys,
                            const InputVec& u)
    { return sys.F(x, t); }

    Matrix<f64, 1, 1> Q(const StateVec& x, f64 t) const
    {
        Matrix<f64, 1, 1> Q; Q << 1;
        return Q;
    }

    static Matrix<f64, 1, 1> Qhandle(const StateVec& x, f64 t, const BasicSystem& sys,
                                     const InputVec& u)
    { return sys.Q(x, t); }

    Matrix<f64, 3, 1> L(const StateVec& x, f64 t) const
    {
        Matrix<f64, 3, 1> L; L << 0,
                                  0,
                                  1;
        return L;
    }
    static Matrix<f64, 3, 1> Lhandle(const StateVec& x, f64 t, const BasicSystem& sys,
                                     const InputVec& u)
    { return sys.L(x, t); }

    /// Swap
    void swap(BasicSystem& o) { Parent::swap(o); }
    /// Copy Ctor
    BasicSystem(const BasicSystem& o): Parent(o) {}
    /// Move Ctor
    BasicSystem(BasicSystem&& o) { swap(o); }
    /// Unifying Assignment
    BasicSystem& operator=(BasicSystem o) { swap(o); return *this; }
};

class KalmanFilterTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testFilter()
    {
        sz order = 4;
        BasicSystem sys(order);
        KalmanFilter<f64, 3, 1, 1> kf(order);

        Matrix<f64, 1, 1> u = Matrix<f64, 1, 1>::Constant(1);
        Matrix3d P = Matrix3d::Identity();

        KalmanFilter<f64, 3, 1, 1>::StateDist predOutput =
                                        kf.predict(sys.getState(), P, 0, 1,
                                                   BasicSystem::derivHandle,
                                                   BasicSystem::Fhandle,
                                                   BasicSystem::Qhandle,
                                                   BasicSystem::Lhandle,
                                                   sys, u);

        // Predicting the state forward is fairly easy, so we can verify that simply
        auto expectPredState = sys.integ(sys.getState(), 0, 1, u);
        TS_ASSERT(matEquals(predOutput.first, expectPredState));

        Matrix<f64, 1, 1> z = Matrix<f64, 1, 1>::Constant(0.6);
        Matrix<f64, 1, 1> R = Matrix<f64, 1, 1>::Constant(0.3);
        Matrix<f64, 1, 3> H; H << 0, 1, 0;

        KalmanFilter<f64, 3, 1, 1>::StateDist corrOutput =
                                        kf.correct(predOutput.first,
                                                   predOutput.second,
                                                   z, R, H*predOutput.first, H);

        // State should be closer to observation now than before prediction
        TS_ASSERT_LESS_THAN(fabs(z[0] - (H*corrOutput.first)[0]),
                            fabs(z[0] - (H*predOutput.first)[0]));
        // We also know that the change should have propagated to other states
        TS_ASSERT_DIFFERS(corrOutput.first[0], predOutput.first[0]);
        TS_ASSERT_DIFFERS(corrOutput.first[1], predOutput.first[1]);
        TS_ASSERT_DIFFERS(corrOutput.first[2], predOutput.first[2]);
    }

    typedef Eigen::Matrix<f64, 1, 1> OneByOne;
    void testMath()
    {
        KalmanFilter<f64, 1, 1, 1> kf(1);

        f64 t  = 0;
        f64 dt = 1;

        OneByOne x = OneByOne::Constant(2);
        OneByOne P = OneByOne::Constant(0.5);
        OneByOne u = OneByOne::Constant(0.5);

        decltype(kf)::StateDist predOutput = kf.predict(x, P, t, dt, fOneDim,
                                                        FOneDim, QOneDim, LOneDim, u);

        OneByOne expectState = x + dt * fOneDim(x, t, u);
        OneByOne expectCov   = P + dt * (FOneDim(x, t, u) * P + P * FOneDim(x, t, u).transpose()
                                         + LOneDim(x, t, u) * QOneDim(x, t, u)
                                           * LOneDim(x, t, u).transpose());
        TS_ASSERT(matEquals(predOutput.first, expectState));
        TS_ASSERT(matEquals(predOutput.second, expectCov));


        x = predOutput.first;
        P = predOutput.second;
        OneByOne z  = OneByOne::Constant(1.6);
        OneByOne R  = OneByOne::Constant(0.3);
        OneByOne H  = OneByOne::Constant(1);

        decltype(kf)::StateDist corrOutput = kf.correct(x, P, z, R, H*x, H);

        OneByOne S = H * P * H.transpose() + R;
        OneByOne K = P * H.transpose() * S.inverse();
        expectState = x + K*(z-H*x);
        expectCov   = (OneByOne::Identity() - K*H)*P;
        TS_ASSERT(matEquals(corrOutput.first, expectState));
        TS_ASSERT(matEquals(corrOutput.second, expectCov));
    }

    static OneByOne fOneDim(const OneByOne& x, f64 t, const OneByOne& u)
    {
        return -0.5*x + u;
    }

    static OneByOne FOneDim(const OneByOne& x, f64 t, const OneByOne& u)
    {
        return OneByOne::Constant(0.5);
    }

    static OneByOne QOneDim(const OneByOne& x, f64 t, const OneByOne& u)
    {
        return OneByOne::Constant(0.2);
    }

    static OneByOne LOneDim(const OneByOne& x, f64 t, const OneByOne& u)
    {
        return OneByOne::Constant(1);
    }
};
