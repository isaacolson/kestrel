#pragma once

#include <iostream>

#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/EigenUtil.hpp"

#include "kestrel/control/Integrator.hpp"

namespace Kestrel {
namespace Control {

/** Models a filter of the Laplacian form:
 *      X(s) = Num(s) / Den(s) * Y(s)
 *      Num(s) = n0 + n1*s + n2*s^2 + ...
 *      Den(s) = d0 + d1*s + d2*s^2 + ...
 *  Where x is the output and y is the input. An equivalent state space model
 *  of this filter is given by:
 *      d0*x + d1*(dx/dt) + d2*(d2x/dt2) + ... = n0*y + n1*(dy/dt) + n2 * (d2y/dt2) + ...
 *  Where the user will input the value of y and its derivatives (as many as necessary)
 *  and this will output the value of x and its derivatives
 */
template <typename _Scalar, int _NUM_DENOM, int _NUM_NUMER=1, int _NUM_ELTS=1,
                            int _NUM_OUTPUT=_NUM_DENOM-1>
class LinearFilter {
  public:
    typedef _Scalar Scalar;
    static constexpr int NUM_DENOM  = _NUM_DENOM;
    static constexpr int NUM_NUMER  = _NUM_NUMER;
    static constexpr int NUM_ELTS   = _NUM_ELTS;
    static constexpr int NUM_OUTPUT = _NUM_OUTPUT;

    // Note: need this to prevent an Eigen static assert when you try to store a row vector
    //       as column major
    static constexpr int storeTypeIn =
        (NUM_ELTS == 1 && NUM_NUMER > 1) ? Eigen::RowMajor : Eigen::ColMajor;
    static constexpr int storeTypeOut =
        (NUM_ELTS == 1 && NUM_OUTPUT > 1) ? Eigen::RowMajor : Eigen::ColMajor;

    // TODO: Though this class can filter vectors, it only does so with the same numer/denom
    //       for every element. Potentially expand to allow individually tuned filters per elt
    typedef Eigen::Matrix<Scalar, NUM_DENOM, 1>         DenomVec;
    typedef Eigen::Matrix<Scalar, NUM_NUMER, 1>         NumerVec;
    // TODO: could make it so this class always takes the scalar value of the input and does
    //       numerical derivatives internally to find whatever order derivs it needs
    /// ordered with each input having a row of increasing order derivatives
    typedef Eigen::Matrix<Scalar, NUM_ELTS, NUM_NUMER, storeTypeIn>   InputVec;
    /// ordered with each output having a row of increasing order derivatives
    typedef Eigen::Matrix<Scalar, NUM_ELTS, NUM_OUTPUT, storeTypeOut> OutputVec;

    typedef Integrator<Scalar, NUM_ELTS, NUM_OUTPUT>                  OutputVecInt;

  protected:
    OutputVec    x;
    OutputVecInt i;
    DenomVec     denomCoeffs; ///< ordered from lowest exponent to highest [d0; d1; d2; ...]
    NumerVec     numerCoeffs; ///< ordered from lowest exponent to highest [n0; n1; n2; ...]

    bool      storeLast; ///< if the filter should store the last used input and deriv
    InputVec  lastInput;
    OutputVec lastDeriv;
    // TODO: for nice output, it seems like storing the last input and last derivative might be nice

  public:
    /// Ctor
    LinearFilter(const DenomVec& denomCoeffs = DenomVec::Ones(),
                 const NumerVec& numerCoeffs = NumerVec::Identity(),
                 const OutputVec& initVal    = OutputVec::Zero(),
                 sz order = 1, bool storeLast = false) :
        x(initVal), i(order), denomCoeffs(denomCoeffs), numerCoeffs(numerCoeffs),
        storeLast(storeLast), lastInput(InputVec::Zero()), lastDeriv(OutputVec::Zero())
    {
        static_assert(NUM_DENOM >= 1, "LinearFilter cannot have fewer than 1 denominator coeff");
        static_assert(NUM_NUMER >= 1, "LinearFilter cannot have fewer than 1 numerator coeff");
        static_assert(   (NUM_DENOM > 1 && NUM_OUTPUT == NUM_DENOM-1)
                      || (NUM_DENOM == 1 && NUM_OUTPUT == 1),
                      "Outputs must be appropriately sized");
        static_assert(NUM_ELTS != 0, "LinearFilter cannot have 0 elements");
        Assert(i.validOrder(), "need valid integrator order");
    }
    // Ctor with Scalar as numerator (only for NUM_NUMER=1)
    LinearFilter(const DenomVec& denomCoeffs,
                 Scalar numerCoeff,
                 const OutputVec& initVal = OutputVec::Zero(),
                 sz order = 1, bool storeLast = false) :
        x(initVal), i(order), denomCoeffs(denomCoeffs), numerCoeffs(NumerVec::Constant(numerCoeff)),
        storeLast(storeLast), lastInput(InputVec::Zero()), lastDeriv(OutputVec::Zero())
    {
        static_assert(NUM_DENOM >= 1, "LinearFilter cannot have fewer than 1 denominator coeff");
        static_assert(NUM_NUMER == 1, "LinearFilter requires 1 numerator coeff for scalar ctor");
        static_assert(   (NUM_DENOM > 1 && NUM_OUTPUT == NUM_DENOM-1)
                      || (NUM_DENOM == 1 && NUM_OUTPUT == 1),
                      "Outputs must be appropriately sized");
        static_assert(NUM_ELTS != 0, "LinearFilter cannot have 0 elements");
        Assert(i.validOrder(), "need valid integrator order");
    }
    // Ctor with Scalar as initial condition (only for NUM_DENOM<=2)
    LinearFilter(const DenomVec& denomCoeffs,
                 const NumerVec& numerCoeffs,
                 Scalar initVal,
                 sz order = 1, bool storeLast = false) :
        x(OutputVec::Constant(initVal)), i(order), denomCoeffs(denomCoeffs),
        numerCoeffs(numerCoeffs), storeLast(storeLast), lastInput(InputVec::Zero()),
        lastDeriv(OutputVec::Zero())
    {
        static_assert(NUM_DENOM >= 1, "LinearFilter cannot have fewer than 1 denominator coeff");
        static_assert(NUM_DENOM <= 2, "LinearFilter requires 1 or 2 denom coeff for scalar ctor");
        static_assert(NUM_NUMER >= 1, "LinearFilter cannot have fewer than 1 numerator coeff");
        static_assert(   (NUM_DENOM > 1 && NUM_OUTPUT == NUM_DENOM-1)
                      || (NUM_DENOM == 1 && NUM_OUTPUT == 1),
                      "Outputs must be appropriately sized");
        static_assert(NUM_ELTS == 1, "LinearFilter requires 1 element for scalar ctor");
        Assert(i.validOrder(), "need valid integrator order");
    }
    // Ctor with Scalar as numerator and initial condition (only for NUM_DENOM<=2, NUM_NUMER=1)
    LinearFilter(const DenomVec& denomCoeffs,
                 Scalar numerCoeff,
                 Scalar initVal,
                 sz order = 1, bool storeLast = false) :
        x(OutputVec::Constant(initVal)), i(order), denomCoeffs(denomCoeffs),
        numerCoeffs(NumerVec::Constant(numerCoeff)), storeLast(storeLast),
        lastInput(InputVec::Zero()), lastDeriv(OutputVec::Zero())
    {
        static_assert(NUM_DENOM >= 1, "LinearFilter cannot have fewer than 1 denominator coeff");
        static_assert(NUM_DENOM <= 2, "LinearFilter requires 1 or 2 denom coeff for scalar ctor");
        static_assert(NUM_NUMER == 1, "LinearFilter requires 1 numerator coeff for scalar ctor");
        static_assert(   (NUM_DENOM > 1 && NUM_OUTPUT == NUM_DENOM-1)
                      || (NUM_DENOM == 1 && NUM_OUTPUT == 1),
                      "Outputs must be appropriately sized");
        static_assert(NUM_ELTS == 1, "LinearFilter requires 1 element for scalar ctor");
        Assert(i.validOrder(), "need valid integrator order");
    }
    /// Dtor
    virtual ~LinearFilter() {}

    inline OutputVec getOutput() const { return x; }
    inline void setOutput(const OutputVec& x) { this->x = x; }
    inline DenomVec getDenom() const { return denomCoeffs; }
    inline void setDenom(const DenomVec& d) { denomCoeffs = d; }
    inline NumerVec getNumer() const { return numerCoeffs; }
    inline void setNumer(const NumerVec& n) { numerCoeffs = n; }
    inline InputVec getLastInput() const { return lastInput; }
    inline OutputVec getLastDeriv() const { return lastDeriv; }

    /***************************************************************************/
    /* User Implemented ********************************************************/
    /***************************************************************************/

    /// Update a given filtered output using a timestep and the observation
    OutputVec filter(const OutputVec& x, f64 t, f64 dt,
                     const InputVec& y = InputVec::Zero()) const
    {
        OutputVec res;
        if (NUM_DENOM > 1) {
            res = i(x, t, dt, derivHandle, *this, y);
        } else {
            // Note: actually have to use the non-templated version here so this part compiles
            //       even if NUM_DENOM != 1, so we lose static size comparisons on this line
            res.block(0, 0, NUM_ELTS, NUM_OUTPUT) = y*numerCoeffs / denomCoeffs(0);
        }
        return correctDomain(res);
    }

    /// The derivative of the output vector given the output vector and the input
    virtual OutputVec deriv(const OutputVec& x, f64 t = 0.0,
                            const InputVec& y = InputVec::Zero()) const
    {
        OutputVec ret = OutputVec::Zero();

        if (NUM_DENOM > 2) { // fill out the first n-1 derivatives with simple integration
            // Note: actually have to use the non-templated version here so this part compiles
            //       even if NUM_DENOM == 1, so we lose static size comparisons on this line
            ret.block(0, 0, NUM_ELTS, NUM_OUTPUT-1) = x.block(0, 1, NUM_ELTS, NUM_OUTPUT-1);
        }

        if (NUM_DENOM > 1) { // highest order derivative set to ODE solution
            // Note: actually have to use the non-templated version here so this part compiles
            //       even if NUM_DENOM == 1, so we lose static size comparisons on this line
            ret.block(0, NUM_OUTPUT-1, NUM_ELTS, 1)
                = (y * numerCoeffs - x * denomCoeffs.block(0, 0, NUM_DENOM-1, 1))
                  / denomCoeffs(NUM_DENOM-1);
        }

        return ret;
    }

    /// Correct the state vector for domain restricted values (such as angles that wrap around
    /// or required normalization). This function will be called after every integration.
    /// Note: this is intended to modify the input vector, not copy it.
    // TODO: Could improve in future for applying this to angles or quaternions, etc.
    // TODO: this gets more complicated because we'll need to correct the observation based on
    //       what the output or vice versa
    virtual OutputVec& correctDomain(OutputVec& x) const
    {
        // Note: though this function is not pure virtual, all children should override it

        return x;
    }

    /***************************************************************************/
    /* End User Implemented ****************************************************/
    /***************************************************************************/

    /***************************************************************************/
    /* Functions operating on the internal state and/or with scalar inputs *****/
    /***************************************************************************/

    /* Note: Implementors MUST specify "using ParentType::funcName;" for each of the
     * following so that the redefinitions of the above virtual functions don't shadow
     * these implementations */

    /// Update the internal filtered output using a timestep and the observation
    inline OutputVec filter(f64 t, f64 dt, const InputVec& y = InputVec::Zero())
    {
        if (storeLast) {
            lastInput = y;
            lastDeriv = deriv(x, t, y);
        }
        return x = filter(x, t, dt, y);
    }

    /// Update a given filtered output using a timestep and the scalar observation
    inline OutputVec filter(const OutputVec& x, f64 t, f64 dt, Scalar y) const
    {
        static_assert(NUM_NUMER == 1, "only available for single input filters");
        return filter(x, t, dt, InputVec::Constant(y));
    }

    /// Update the internal filtered output using a timestep and the scalar observation
    inline OutputVec filter(f64 t, f64 dt, Scalar y)
    {
        static_assert(NUM_NUMER == 1, "only available for single input filters");
        static_assert(NUM_ELTS  == 1, "only available for single input filters");
        if (storeLast) {
            lastInput.setConstant(y);
            lastDeriv = deriv(x, t, lastInput);
        }
        return filter(t, dt, InputVec::Constant(y));
    }

    /// derivative function using internal state
    inline OutputVec deriv(f64 t = 0.0, const InputVec& y = InputVec::Zero()) const
    { return deriv(x, t, y); }

    /// derivative funciton using a scalar input
    inline OutputVec deriv(const OutputVec& x, f64 t, Scalar y) const
    { static_assert(NUM_NUMER == 1, "only available for single input filters");
      static_assert(NUM_ELTS  == 1, "only available for single input filters");
      return deriv(x, t, InputVec::Constant(y)); }

    /// derivative funciton with internal state using a scalar input
    inline OutputVec deriv(f64 t, Scalar y) const
    { static_assert(NUM_NUMER == 1, "only available for single input filters");
      static_assert(NUM_ELTS  == 1, "only available for single input filters");
      return deriv(x, t, InputVec::Constant(y)); }

    /// Correct the internal state vector for domain restricted values
    inline OutputVec& correctDomain()
    { return correctDomain(x); }

    /***************************************************************************/
    /* End Functions operating on the internal state and/or with scalar inputs */
    /***************************************************************************/

    /***************************************************************************/
    /* Static Funciton Pointer Handles *****************************************/
    /***************************************************************************/

    /// Static derivative handle for use by external parties
    /// Note: unfortunately, variadic template expansion MUST always have the full list
    ///       of arguments, even if the function itself has default args. Thus, when you
    ///       have a situation where multiple function pointers need the same variadic args
    ///       list, they must share EACH argument, regardless of default values. Even
    ///       when calling the function pointer, all arguments must be present regardless
    ///       of default values.
    static inline OutputVec derivHandle(const OutputVec& x, f64 t,
                                        const LinearFilter& filt,
                                        const InputVec& y = InputVec::Zero())
    { return filt.deriv(x, t, y); }

    /***************************************************************************/
    /* End Static Funciton Pointer Handles *************************************/
    /***************************************************************************/

    /// Prints data in CSV form to the given ostream with given leader string,
    ///   deliminator, and line terminator. Format: leader, time, output, input
    /// Note that there WILL be a trailing deliminator on each line.
    virtual void printCsv(std::ostream& os, const OutputVec& x, f64 t,
                                            const InputVec& y,
                                            const std::string& leader = "",
                                            const std::string& delim = ",",
                                            const std::string& term = "\n") const
    {
        Eigen::IOFormat fmt = EigenUtil::linearFormat(delim);

        if (!leader.empty()) os << leader << delim;
        os << 't' << delim << t << delim;
        // Note: the following actually outputs the output matrix by row, i.e. it
        //       will output output(0, 0), output(0, 1), output(0, 2), ... This implies that it
        //       will be outputing groups of {elt(i), delt(i) / dt, d^2elt(i)/dt^2, ...}
        os << 'x' << delim << x.format(fmt) << delim;
        // Note: the following actually outputs the input matrix by row, i.e. it
        //       will output input(0, 0), input(0, 1), input(0, 2), ... This implies that it
        //       will be outputing groups of {elt(i), delt(i) / dt, d^2elt(i)/dt^2, ...}
        os << 'y' << delim << y.format(fmt) << delim;
        if (!term.empty()) os << term;
    }
    /// Prints data in CSV form to the given ostream using the internal state
    inline virtual void printCsv(std::ostream& os, f64 t,
                                                   const InputVec& y,
                                                   const std::string& leader = "",
                                                   const std::string& delim = ",",
                                                   const std::string& term = "\n") const
    { printCsv(os, x, t, y, leader, delim, term); }

    /// Swap
    inline void swap(LinearFilter& o)
    {
        x.swap(o.x);
        i.swap(o.i);
        denomCoeffs.swap(o.denomCoeffs);
        numerCoeffs.swap(o.numerCoeffs);
        std::swap(storeLast, o.storeLast);
        lastInput.swap(o.lastInput);
        lastDeriv.swap(o.lastDeriv);
    }
    /// Copy Ctor
    LinearFilter(const LinearFilter& o) :
        x(o.x), i(o.i), denomCoeffs(o.denomCoeffs), numerCoeffs(o.numerCoeffs),
        storeLast(o.storeLast), lastInput(o.lastInput), lastDeriv(o.lastDeriv) {}
    /// Move Ctor
    LinearFilter(LinearFilter&& o) { swap(o); }
    /// Unifying Assignment
    LinearFilter& operator=(LinearFilter o) { swap(o); return *this; }
};


template <typename Scalar, int NUM_ELTS=1>
LinearFilter<Scalar, 3, 1, NUM_ELTS>
    createSecondOrderLP(Scalar natFreq = 1, Scalar dampRatio = 1, sz order = 1,
                        bool storeLast = false)
{
    // TODO: if there is EVER someone who wants to be able to initialize the filter's initial value
    //       through this creator, feel free to overload the method to take that arg to, but it's
    //       very convenient to just have one that uses Zero() without having to pass it in
    return LinearFilter<Scalar, 3, 1, NUM_ELTS>(
               Eigen::Matrix<Scalar, 3, 1>(natFreq*natFreq, 2*dampRatio*natFreq, 1),
               natFreq*natFreq, Eigen::Matrix<Scalar, NUM_ELTS, 2>::Zero(), order, storeLast);
}


} /* Control */
} /* Kestrel */
