#pragma once

#include <iostream>
#include <string>

#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/EigenUtil.hpp"

#include "kestrel/control/Integrator.hpp"

namespace Kestrel {
namespace Control {

template <typename _Scalar, int _NUM_STATES, int _NUM_INPUTS=0, int _NUM_NOISES=0>
class System {
    // Note: if there is sufficient interest, it would be worth implementing dual numbers
    //       for automatic differentiation of system derivative functions to compute
    //       numerical jacobians. See this page on implementing a new Scalar type for Eigen:
    //       http://eigen.tuxfamily.org/dox/TopicCustomizingEigen.html#CustomScalarType
  public:
    typedef _Scalar Scalar;
    static const int NUM_STATES = _NUM_STATES;
    static const int NUM_INPUTS = _NUM_INPUTS;
    static const int NUM_NOISES = _NUM_NOISES;

    typedef Eigen::Matrix<Scalar, NUM_STATES, 1>          StateVec;
    typedef Eigen::Matrix<Scalar, NUM_INPUTS, 1>          InputVec;
    typedef Eigen::Matrix<Scalar, NUM_NOISES, 1>          NoiseVec;
    typedef Eigen::Matrix<Scalar, NUM_NOISES, NUM_NOISES> NoiseCov;
    typedef Integrator<Scalar, NUM_STATES, 1>             StateVecInt;

  protected:
    StateVec    x;
    StateVecInt i;

  public:
    /// Ctor: from Eigen::Vector and integrator order
    System(const StateVec& x = StateVec::Zero(), sz order = 1) : x(x), i(order)
    {
        static_assert(NUM_STATES != 0, "System cannot have 0 state");
        Assert(i.validOrder(), "need valid integrator order");
    }
    /// Dtor
    virtual ~System() {}

    inline StateVec getState() const            { return x; }
    inline void     setState(const StateVec& x) { this->x = x; }

    /// Integrate a state vector given the state, time, timestep, input, and noise
    StateVec integ(const StateVec& x, f64 t, f64 dt,
                   const InputVec& u = InputVec::Zero(),
                   const NoiseVec& w = NoiseVec::Zero()) const
    {
        StateVec res = i(x, t, dt, derivHandle, *this, u, w);
        return correctDomain(res);
    }

    /// Integrate the internal state vector given the time, timestep, input, and noise
    StateVec integ(f64 t, f64 dt,
                   const InputVec& u = InputVec::Zero(),
                   const NoiseVec& w = NoiseVec::Zero())
    {
        x = i(x, t, dt, derivHandle, *this, u, w);
        return correctDomain(x);
    }

    /***************************************************************************/
    /* User Implemented ********************************************************/
    /***************************************************************************/

    /// The derivative of the state vector given the state, the time, input, and noise
    virtual StateVec deriv(const StateVec& x, f64 t = 0.0,
                           const InputVec& u = InputVec::Zero(),
                           const NoiseVec& w = NoiseVec::Zero()) const
    {
        // Note: though this function is not pure virtual, all children should override it

        // TODO: consider storing the result of this funciton in the class and only
        //       recalculating it if the time/input/noise change (just a thought,
        //       would allow painless reuse of the derivative for things like output
        //       file generation)

        // Treats the system as a simple integrator with the input and noise added to the bottom
        // most states
        StateVec ret = x;
        ret.template block<NUM_STATES-1, 1>(0, 0) = x.template block<NUM_STATES-1, 1>(1, 0);
        ret(NUM_STATES-1) = 0;
        if (NUM_INPUTS > 0) ret.template block<NUM_INPUTS, 1>(NUM_STATES-NUM_INPUTS, 0) += u;
        if (NUM_NOISES > 0) ret.template block<NUM_NOISES, 1>(NUM_STATES-NUM_NOISES, 0) += w;
        return ret;
    }

    /// Correct the state vector for domain restricted values (such as angles that wrap around
    /// or required normalization). This function will be called after every integration.
    /// Note: this is intended to modify the input vector, not copy it.
    virtual StateVec& correctDomain(StateVec& x) const
    {
        // Note: though this function is not pure virtual, all children should override it

        return x;
    }

    /***************************************************************************/
    /* End User Implemented ****************************************************/
    /***************************************************************************/

    /***************************************************************************/
    /* Functions operating on the internal state *******************************/
    /***************************************************************************/

    /* Note: Implementors MUST specify "using ParentType::funcName;" for each of the
     * following so that the redefinitions of the above virtual functions don't shadow
     * these implementations */

    /// derivative of the internal state given the time, input, and noise
    inline StateVec deriv(f64 t = 0.0, const InputVec& u = InputVec::Zero(),
                                       const NoiseVec& w = NoiseVec::Zero()) const
    { return deriv(x, t, u, w); }

    /// Correct the internal state vector for domain restricted values
    inline StateVec& correctDomain()
    { return correctDomain(x); }

    /***************************************************************************/
    /* End Functions operating on the internal state ***************************/
    /***************************************************************************/

    /***************************************************************************/
    /* Static Funciton Pointer Handles *****************************************/
    /***************************************************************************/

    /// Static derivative handle for use by external parties
    /// Note: unfortunately, variadic template expansion MUST always have the full list
    ///       of arguments, even if the function itself has default args. Thus, when you
    ///       have a situation where multiple function pointers need the same variadic args
    ///       list, they must share EACH argument, regardless of default values. Even
    ///       when calling the function pointer, all arguments must be present regardless
    ///       of default values.
    static inline StateVec derivHandle(const StateVec& x, f64 t,
                                       const System& sys,
                                       const InputVec& u = InputVec::Zero(),
                                       const NoiseVec& w = NoiseVec::Zero())
    { return sys.deriv(x, t, u, w); }

    /***************************************************************************/
    /* End Static Funciton Pointer Handles *************************************/
    /***************************************************************************/

    /// Prints data in CSV form to the given ostream with given leader string,
    ///   deliminator, and line terminator. Format: leader, time, state, input, noise,
    /// Note that there WILL be a trailing deliminator on each line.
    //  TODO: should we be outputing the derivative here?
    virtual void printCsv(std::ostream& os, const StateVec& x, f64 t,
                                            const InputVec& u = InputVec::Zero(),
                                            const NoiseVec& w = NoiseVec::Zero(),
                                            const std::string& leader = "",
                                            const std::string& delim = ",",
                                            const std::string& term = "\n") const
    {
        Eigen::IOFormat fmt = EigenUtil::linearFormat(delim);

        if (!leader.empty()) os << leader << delim;
        os << 't' << delim << t << delim;
        os << 'x' << delim << x.format(fmt) << delim;
        os << 'u' << delim << u.format(fmt) << delim;
        os << 'w' << delim << w.format(fmt) << delim;
        if (!term.empty()) os << term;
    }

    /// Prints data in CSV form to the given ostream using the internal state
    inline virtual void printCsv(std::ostream& os, f64 t,
                                                   const InputVec& u = InputVec::Zero(),
                                                   const NoiseVec& w = NoiseVec::Zero(),
                                                   const std::string& leader = "",
                                                   const std::string& delim = ",",
                                                   const std::string& term = "\n") const
    { printCsv(os, x, t, u, w, leader, delim, term); }

    /// Swap
    // Note: Because we actually do the swap here, things will get rough if there is ever
    //       "diamond style" inheritance. Will need to be careful that the grandparent's swap
    //       is not accidentally called twice due to being called by multiple non-leaf children
    //       from a leaf grandchild.
    inline void swap(System& o) { x.swap(o.x); i.swap(o.i); }
    /// Copy Ctor
    System(const System& o) : x(o.x), i(o.i) {}
    /// Move Ctor
    System(System&& o) { swap(o); }
    /// Unifying Assignment
    System& operator=(System o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
