#pragma once

#include <iostream>
#include <string>

#include <Eigen/Dense>

#include "kestrel/util/assert.hpp"
#include "kestrel/util/types.hpp"
#include "kestrel/util/EigenUtil.hpp"

#include "kestrel/control/System.hpp"
#include "kestrel/control/KalmanFilter.hpp"

namespace Kestrel {
namespace Control {

template <typename _Scalar, int _NUM_STATES, int _NUM_INPUTS=0, int _NUM_NOISES=0>
class ObserverSystem: public System<_Scalar, _NUM_STATES, _NUM_INPUTS, _NUM_NOISES> {
  public:
    typedef _Scalar Scalar;
    static const int NUM_STATES = _NUM_STATES;
    static const int NUM_INPUTS = _NUM_INPUTS;
    static const int NUM_NOISES = _NUM_NOISES;

    typedef System<Scalar, NUM_STATES, NUM_INPUTS, NUM_NOISES>       ParentType;

    typedef Eigen::Matrix<Scalar, NUM_STATES, 1>                     StateVec;
    typedef Eigen::Matrix<Scalar, NUM_INPUTS, 1>                     InputVec;
    typedef Eigen::Matrix<Scalar, NUM_NOISES, 1>                     NoiseVec;
    typedef Eigen::Matrix<Scalar, NUM_STATES, NUM_STATES>            StateCov;
    typedef Eigen::Matrix<Scalar, NUM_STATES, NUM_STATES>            StateJac;
    typedef Eigen::Matrix<Scalar, NUM_NOISES, NUM_NOISES>            NoiseCov;
    typedef Eigen::Matrix<Scalar, NUM_STATES, NUM_NOISES>            NoiseJac;

    typedef std::pair<StateVec, StateCov>                            StateDist;
    typedef KalmanFilter<Scalar, NUM_STATES, NUM_INPUTS, NUM_NOISES> StateEkf;

    template<int NUM_OBS> using ObsVec = Eigen::Matrix<Scalar, NUM_OBS, 1>;
    template<int NUM_OBS> using ObsCov = Eigen::Matrix<Scalar, NUM_OBS, NUM_OBS>;
    template<int NUM_OBS> using ObsJac = Eigen::Matrix<Scalar, NUM_OBS, NUM_STATES>;

  protected:
    using ParentType::x;
    StateCov P;
    StateEkf kf;

  public:
    /// Ctor
    ObserverSystem(const StateVec& x = StateVec::Zero(), const StateCov& P = StateCov::Identity(),
                   sz order = 1) : ParentType(x, order), P(P), kf(order) {}
    /// Dtor
    virtual ~ObserverSystem() {}

    inline StateCov getCovar() const            { return P; }
    inline void     setCovar(const StateCov& P) { this->P = P; }

    /// Predict a state vector & covariance forward given the time, timestep, input, and noise
    StateDist predict(const StateVec& x, const StateCov& P, f64 t, f64 dt,
                      const InputVec& u = InputVec::Zero(),
                      const NoiseVec& w = NoiseVec::Zero()) const
    {
        StateDist res = kf.predict(x, P, t, dt, derivHandle, derivJacHandle, noiseCovHandle,
                                   noiseJacHandle, *this, u, w);
        correctDomain(res.first);
        return res;
    }

    /// Predict using the the internal state vector & covariance and store output
    StateDist predict(f64 t, f64 dt, const InputVec& u = InputVec::Zero(),
                                     const NoiseVec& w = NoiseVec::Zero())
    {
        StateDist res = kf.predict(x, P, t, dt, derivHandle, derivJacHandle, noiseCovHandle,
                                   noiseJacHandle, *this, u, w);
        x = correctDomain(res.first);
        P = res.second;
        return res;
    }

    /// Correct a state vector & covariance given an observation, observation covariance,
    /// predicted observation, and observation prediction function
    template <int NUM_OBS>
    StateDist correct(const StateVec&        x, const StateCov&        P,
                      const ObsVec<NUM_OBS>& z, const ObsCov<NUM_OBS>& R,
                      const ObsVec<NUM_OBS>& h, const ObsJac<NUM_OBS>& H) const
    {
        StateDist res = kf.correct(x, P, z, R, h, H);
        correctDomain(res.first);
        return res;
    }

    /// Correct using the internal state vector & covariance and store output
    template <int NUM_OBS>
    StateDist correct(const ObsVec<NUM_OBS>& z, const ObsCov<NUM_OBS>& R,
                      const ObsVec<NUM_OBS>& h, const ObsJac<NUM_OBS>& H)
    {
        StateDist res = kf.correct(x, P, z, R, h, H);
        x = correctDomain(res.first);
        P = res.second;
        return res;
    }

    /***************************************************************************/
    /* User Implemented ********************************************************/
    /***************************************************************************/

    /// The derivative of the state vector given the state, the time, input, and noise
    virtual StateVec deriv(const StateVec& x, f64 t = 0.0,
                           const InputVec& u = InputVec::Zero(),
                           const NoiseVec& w = NoiseVec::Zero()) const
    {
        // Note: though this function is not pure virtual, all children should override it

        // Treats the system as a simple integrator
        return ParentType::deriv(x, t, u, w);
    }

    /// The jacobian of the derivative function with respect to the state vector
    virtual StateJac derivJac(const StateVec& x, f64 t = 0.0,
                              const InputVec& u = InputVec::Zero(),
                              const NoiseVec& w = NoiseVec::Zero()) const
    {
        // Note: though this function is not pure virtual, all children should override it

        // Treats the system as a simple integrator
        StateJac F;
        for (int i = 0; i < NUM_STATES-1; ++i) { F(i, i+1) = 1; }
        return F;
    }

    /// The noise covariance matrix
    virtual NoiseCov noiseCov(const StateVec& x, f64 t = 0.0,
                              const InputVec& u = InputVec::Zero(),
                              const NoiseVec& w = NoiseVec::Zero()) const
    {
        // Note: though this function is not pure virtual, all children should override it

        return 0.5 * NoiseCov::Identity();
    }

    /// The jacobian of the derivative function with respect to the noise vector
    virtual NoiseJac noiseJac(const StateVec& x, f64 t = 0.0,
                              const InputVec& u = InputVec::Zero(),
                              const NoiseVec& w = NoiseVec::Zero()) const
    {
        // Note: though this function is not pure virtual, all children should override it

        return NoiseJac::Identity();
    }

    /// Correct the state vector for domain restricted values (such as angles that wrap around
    /// or required normalization). This function will be called after every prediction and
    /// correction. Note: this is intended to modify the input vector, not copy it.
    virtual StateVec& correctDomain(StateVec& x) const
    {
        // Note: though this function is not pure virtual, all children should override it

        return ParentType::correctDomain(x);
    }

    /* Implementers may also find it useful to define correction related functions based on
     * the observation dimensions they expect to have. The necessary matrices to do a Kalman
     * correction with observation dimension NUM_OBS are:
     *
     * z = type:ObsVec, the observation vector (this is the actual data, don't make a function
     *     to generate it)
     * R = type:ObsCov, the covariance of the observation vector, this could be a constant, or
     *     the user may wish it to be a function of the state or even the observation itself.
     *     Possible forms include R = const, R = R(x), R = R(z), R = R(x, z), ...
     * h = type:ObsVec, the expected observation, typically this will be a function of the form
     *     h = h(x), h = h(x, t), h = h(x, t, u, w), h = h(x, z)
     *     Note: h = h(x, z) is often used for the special case where your measurement has a
     *     domain restriction / wraparound (such as angles) and the h function must project x
     *     into the measurement est that has the best diff with the measurement (i.e. it is the
     *     user's responsibility to make the h function accounts for wraparound so that z - h
     *     has the proper sign)
     * H = type:ObsJac, the jacobian of hx with respect to the state vector. This could be a
     *     constant or could also be a function. Possible forms incluse:
     *     H = const, H = H(x), H = H(x, t), H = H(x, t, u, w), ...
     */

    /***************************************************************************/
    /* End User Implemented ****************************************************/
    /***************************************************************************/

    /***************************************************************************/
    /* Functions operating on the internal state *******************************/
    /***************************************************************************/

    /* Note: Implementors MUST specify "using ParentType::funcName;" for each of the
     * following so that the redefinitions of the above virtual functions don't shadow
     * these implementations */

    /// derivative of the internal state given the time, input, and noise
    inline StateVec deriv(f64 t = 0.0, const InputVec& u = InputVec::Zero(),
                                       const NoiseVec& w = NoiseVec::Zero()) const
    { return ParentType::deriv(t, u, w); }

    /// The jacobian of the derivative function with respect to the internal state vector
    inline StateJac derivJac(f64 t = 0.0, const InputVec& u = InputVec::Zero(),
                                          const NoiseVec& w = NoiseVec::Zero()) const
    { return derivJac(x, t, u, w); }

    /// The noise covariance matrix with respect to the internal state vector
    inline NoiseCov noiseCov(f64 t = 0.0, const InputVec& u = InputVec::Zero(),
                                          const NoiseVec& w = NoiseVec::Zero()) const
    { return noiseCov(x, t, u, w); }

    /// The jacobian of the derivative function with respect to the noise vector evaluated at
    /// internal state vector
    inline NoiseJac noiseJac(f64 t = 0.0, const InputVec& u = InputVec::Zero(),
                                          const NoiseVec& w = NoiseVec::Zero()) const
    { return noiseJac(x, t, u, w); }

    /// Correct the internal state vector for domain restricted values
    inline StateVec& correctDomain()
    { return ParentType::correctDomain(); }

    /* Implementers may also want to create similar functions for the R, h, and H functions
     * based on expected NUM_OBS. See comment in "User Implemented" section for details
     */

    /***************************************************************************/
    /* End Functions operating on the internal state ***************************/
    /***************************************************************************/

    /***************************************************************************/
    /* Static Funciton Pointer Handles *****************************************/
    /***************************************************************************/

    /// Static derivative handle for use by external parties
    static inline StateVec derivHandle(const StateVec& x, f64 t,
                                       const ObserverSystem& sys,
                                       const InputVec& u = InputVec::Zero(),
                                       const NoiseVec& w = NoiseVec::Zero())
    { return sys.deriv(x, t, u, w); }

    /// Static derivative jacobian handle for use by external parties
    static inline StateJac derivJacHandle(const StateVec& x, f64 t,
                                          const ObserverSystem& sys,
                                          const InputVec& u = InputVec::Zero(),
                                          const NoiseVec& w = NoiseVec::Zero())
    { return sys.derivJac(x, t, u, w); }

    /// Static noise covariance handle for use by external parties
    static inline NoiseCov noiseCovHandle(const StateVec& x, f64 t,
                                          const ObserverSystem& sys,
                                          const InputVec& u = InputVec::Zero(),
                                          const NoiseVec& w = NoiseVec::Zero())
    { return sys.noiseCov(x, t, u, w); }

    /// Static noise jacobian handle for use by external parties
    static inline NoiseJac noiseJacHandle(const StateVec& x, f64 t,
                                          const ObserverSystem& sys,
                                          const InputVec& u = InputVec::Zero(),
                                          const NoiseVec& w = NoiseVec::Zero())
    { return sys.noiseJac(x, t, u, w); }

    /* Implementers may also want to create similar functions for the R, h, and H functions
     * based on expected NUM_OBS. See comment in "User Implemented" section for details
     */

    /***************************************************************************/
    /* End Static Funciton Pointer Handles *************************************/
    /***************************************************************************/

    /// Prints data in CSV form to the given ostream with given leading string,
    ///   deliminator, and line terminator. Format: leader, time, state, input, noise, stateCov
    /// Note that there WILL be a trailing deliminator on each line.
    virtual void printCsv(std::ostream& os, const StateVec& x,
                                            const StateCov& P, f64 t,
                                            const InputVec& u = InputVec::Zero(),
                                            const NoiseVec& w = NoiseVec::Zero(),
                                            const std::string& leader = "",
                                            const std::string& delim = ",",
                                            const std::string& term = "\n") const
    {
        ParentType::printCsv(os, x, t, u, w, leader, delim, "");

        Eigen::IOFormat fmt = EigenUtil::linearFormat(delim);

        os << 'P' << delim << P.format(fmt) << delim;
        if (!term.empty()) os << term;
    }

    /// Prints data in CSV form to the given ostream using the internal state and covariance
    inline virtual void printCsv(std::ostream& os, f64 t,
                                                   const InputVec& u = InputVec::Zero(),
                                                   const NoiseVec& w = NoiseVec::Zero(),
                                                   const std::string& leader = "",
                                                   const std::string& delim = ",",
                                                   const std::string& term = "\n") const
    { printCsv(os, x, P, t, u, w, leader, delim, term); }

    /// Prints data in CSV form to the given ostream but omitting the covariance
    using ParentType::printCsv; // printCsv(os, x, t, u, w, leader, delim, term)

    /// Prints an observation in CSV form to the given ostream with the given leading string,
    ///   deliminator, and line terminator. Format: leader, time, observation
    /// Note that there WILL be a trailing deliminator on each line.
    template<int NUM_OBS>
    void printObsCsv(std::ostream& os, const ObsVec<NUM_OBS>& z, f64 t,
                                       const std::string& leader = "",
                                       const std::string& delim = ",",
                                       const std::string& term = "\n") const
    {
        Eigen::IOFormat fmt = EigenUtil::linearFormat(delim);

        if (!leader.empty()) os << leader << delim;
        os << 't' << delim << t << delim;
        os << 'z' << delim << z.format(fmt) << delim;
        if (!term.empty()) os << term;
    }

    /// Swap
    inline void swap(ObserverSystem& o) { ParentType::swap(o); P.swap(o.P); kf.swap(o.kf); }
    /// Copy Ctor
    ObserverSystem(const ObserverSystem& o) : ParentType(o), P(o.P), kf(o.kf) {}
    /// Move Ctor
    ObserverSystem(ObserverSystem&& o) { swap(o); }
    /// Unifying Assignment
    ObserverSystem& operator=(ObserverSystem o) { swap(o); return *this; }
};

} /* Control */
} /* Kestrel */
