#pragma once

#include "kestrel/util/types.hpp"

namespace Kestrel {
namespace PhysicsUtil
{
    static const f64 GRAVITY = 9.80665;
    static const f64 GRAVITY_SQ = GRAVITY*GRAVITY;

} /* PhysicsUtil */
} /* Kestrel */
