#pragma once

#include <type_traits>
#include <Eigen/Dense>
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/EigenUtil.hpp"

namespace Kestrel {
namespace Rotations {

// XXX: can't I just say "using Eigen::MatrixBase" ?
template <typename Derived> using MatrixBase       = Eigen::MatrixBase<Derived>;
template <typename Derived> using QuaternionBase   = Eigen::QuaternionBase<Derived>;

template <typename Scalar> using RotationMatrix    = Eigen::Matrix<Scalar, 3, 3>;
template <typename Scalar> using AngleAxis         = Eigen::AngleAxis<Scalar>;
template <typename Scalar> using Quaternion        = Eigen::Quaternion<Scalar>;
template <typename Scalar> using AngVel            = Eigen::Matrix<Scalar, 3, 1>;
template <typename Scalar> using Axis              = Eigen::Matrix<Scalar, 3, 1>;
template <typename Scalar> using EulerAngles       = Eigen::Matrix<Scalar, 3, 1>; // r, p, y

// Note: vectorized rotation matrix assumed to be ordered columnwise (because eigen is col major)
template <typename Scalar> using RmatVectorized    = Eigen::Matrix<Scalar, 9, 1>;
template <typename Scalar> using RmatDotJacRmat    = Eigen::Matrix<Scalar, 9, 9>;
template <typename Scalar> using RmatDotJacAngVel  = Eigen::Matrix<Scalar, 9, 3>;

template <typename Scalar> using QuatVectorized    = Eigen::Matrix<Scalar, 4, 1>;
template <typename Scalar> using QuatCompMat       = Eigen::Matrix<Scalar, 4, 4>;
template <typename Scalar> using QuatDotJacQuat    = Eigen::Matrix<Scalar, 4, 4>;
template <typename Scalar> using QuatDotJacAngVel  = Eigen::Matrix<Scalar, 4, 3>;

template <typename Scalar> using EulerDotJacEuler  = Eigen::Matrix<Scalar, 3, 3>;
template <typename Scalar> using EulerDotJacAngVel = Eigen::Matrix<Scalar, 3, 3>;

template <typename Scalar> using PerturbationCov   = Eigen::Matrix<Scalar, 3, 3>;
template <typename Scalar> using RmatObsCov        = Eigen::Matrix<Scalar, 9, 9>;
template <typename Scalar> using QuatObsCov        = Eigen::Matrix<Scalar, 4, 4>;
template <typename Scalar> using EulerObsCov       = Eigen::Matrix<Scalar, 3, 3>;

#define ROTATIONS_FORWARD_DECL_INCLUDE_OK
#include "kestrel/util/RotationsForwardDeclare.hpp"
#undef ROTATIONS_FORWARD_DECL_INCLUDE_OK

/* Note: Eigen provides a few useful utilities for rotations, namely AngleAxis<Scalar> and
 *       Quaternion<Scalar>. Both of these should be viewed as acting as true rotations, that
 *       is to say they physically rotate an input vector within the same frame. Identically,
 *       they can be viewed as resolving vectors from the resultant frame back into the original
 *       frame.
 *
 *       Example: Frame A = base
 *                Frame B = rotate Frame A by 0.1 (right handedly) about the unit X vector
 *                AngleAxisd Rba(0.1, Vector3d::UnitX())
 *                Rba is the object that takes a vector represented in Frame A and rotates
 *                    it by 0.1 about the x axis.
 *                In orientation terms, Rba = Oab, that is the object that takes a vector
 *                    resolved in Frame B and gives you it's resolved components in Frame A
 *
 *                If the operation you want is in fact the opposite of either of these, you
 *                must invert the object.
 *
 *       This "sense" of rotations will be considered the default EVERYWHERE. For instance, if
 *       you input Euler angles to get a rotation matrix, assume it does the same thing as the
 *       angle axis object described above.
 *
 *       Also note that this means Eigen rotation objects typically compose from left to right.
 *
 *       Finally, all Euler angles in this library are assumed to be 321 Euler decompositions.
 *       However, because Euler angles suffer from gimbal lock and require trig functions for
 *       many of their operations, it is highly recommended to use either matrices or quaternions
 *       for most calculations.
 *
 * Note: when integrating rotation matrices and quaternions, it is important to normalize the
 *       result frequently to account for linearization errors. For a quaternion, literally
 *       normalizing the vector works. For a rotation matrix, a more complex operation must be
 *       done. See the following links for some references:
 *           http://stackoverflow.com/a/15324910
 *           http://www.cs.cmu.edu/~spiff/moedit99/expmap.pdf
 *           http://www.cs.mcgill.ca/~kry/pubs/ekf/ekf.pdf
 *           https://en.wikipedia.org/wiki/Orthogonal_matrix#Numerical_linear_algebra
 *           https://en.wikipedia.org/wiki/Polar_decomposition
 *      It appears that one good way to normalize the rotation matrix is to take it's SVD and
 *      set all singular values to 1 (this is effectively the same as the polar decomposition).
 *      The mcgill paper ends up using conversions between the rotation matrix and a rotation
 *      vector to ensure only rotation operations are actually applied to the orientation matrix.
 *
 * References:
 *      Geometry, Kinematics, Statics, and Dynamics
 *          Dennis S. Bernstein - University of Michigan - dsbaero@umich.edu
 */

////////////////////////////////////////////////////////////////////////////////////////////////
// Rotation Conversions & Utility //////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

// Note: conversion between Eigen objects is simply through constructors or by calling .matrix()

/** Provides the rotation matrix representing a yaw, pitch, roll rotation (in that order)
 *  If you want to convert to an AngleAxis or Quaternion, just pass construct from the matrix
 */
template <typename Scalar>
inline RotationMatrix<Scalar> fromEuler(Scalar roll, Scalar pitch, Scalar yaw)
{
    return (  AngleAxis<Scalar>(yaw,   Axis<Scalar>::UnitZ())
            * AngleAxis<Scalar>(pitch, Axis<Scalar>::UnitY())
            * AngleAxis<Scalar>(roll,  Axis<Scalar>::UnitX())).matrix();
}

template <typename Derived>                            // const EulerAngles<Scalar>& T
inline RotationMatrix<typename Derived::Scalar> fromEuler(const MatrixBase<Derived>& T)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == EulerAngles<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == EulerAngles<Scalar>::ColsAtCompileTime,
                  "Rotations::fromEuler expects argument of EulerAngles equivalent");

    return fromEuler(T(0), T(1), T(2));
}

/** Provides the euler angles that created the rotation matrix. Will fail if R is not a rotation
 *  If in gimbal lock, assumes that all rotation is yaw and pitch
 *  If you want to derive from a quaternion or angle axis, just construct the matrix from one
 */
template <typename Derived>                // const RotationMatrix<Scalar>& R
EulerAngles<typename Derived::Scalar> toEuler(const MatrixBase<Derived>& R)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == RotationMatrix<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == RotationMatrix<Scalar>::ColsAtCompileTime,
                  "Rotations::toEuler expects argument of RotationMatrix equivalent");

    Scalar pitch;
    // precision issues mean we have to be careful calling asin here
    if (fabs(R(2,0) - 1) < MathUtil::TINY) {
        pitch = -MathUtil::PI/2;
    } else if (fabs(R(2,0) + 1) < MathUtil::TINY) {
        pitch =  MathUtil::PI/2;
    } else {
        pitch = -asin(R(2,0));
    }
    if (fabs(cos(pitch)) < MathUtil::TINY) {
        return EulerAngles<Scalar>(0.0, pitch, -atan2(R(0,1), R(0,2)));
    } else {
        return EulerAngles<Scalar>(atan2(R(2,1), R(2,2)), pitch, atan2(R(1,0), R(0,0)));
    }
}

/// Returns the rotation matrix as an Eigen vector object
template <typename Derived>                 // const RotationMatrix<Scalar>& R
RmatVectorized<typename Derived::Scalar> toVec(const MatrixBase<Derived>& R)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == RotationMatrix<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == RotationMatrix<Scalar>::ColsAtCompileTime,
                  "Rotations::toVec expects argument of RotationMatrix equivalent");

    // Note: Used to be doing this with the input operator:
    //       ret << R(0), R(1), R(2), R(3), R(4), R(5), R(6), R(7), R(8);
    //       but I don't think that saves any time over doing an eval
    RmatVectorized<Scalar> ret(R.eval().data());
    return ret;
}

/// Returns the quaternion as an Eigen vector object
template <typename Derived>                 // const Quaternion<Scalar>& q
QuatVectorized<typename Derived::Scalar> toVec(const QuaternionBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;
    return QuatVectorized<Scalar>(q.w(), q.x(), q.y(), q.z());
}

/// Converts a vectorized rotation matrix to a rotation matrix
template <typename Derived>                 // const RmatVectorized<Scalar>& R
RotationMatrix<typename Derived::Scalar> toMat(const MatrixBase<Derived>& R)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == RmatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == RmatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::toMat expects argument of RmatVectorized equivalent");

    // Note: Used to be doing this with the input operator
    //       ret << R(0), R(3), R(6),
    //              R(1), R(4), R(7),
    //              R(2), R(5), R(8);
    //       but I don't think that saves any time over doing an eval
    RotationMatrix<Scalar> ret(R.eval().data());
    return ret;
}

/// Converts a quaternion to a rotation matrix. Really, better to use the built in `.matrix()`
/// method in the quaternion class, but this at least lets us have the math shown here
template <typename Derived>                     // const QuatVectorized<Scalar>& q
RotationMatrix<typename Derived::Scalar> quatToMat(const MatrixBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatToMat expects argument of QuatVectorized equivalent");

    Scalar qw2  = q(0)*q(0);
    Scalar qx2  = q(1)*q(1);
    Scalar qy2  = q(2)*q(2);
    Scalar qz2  = q(3)*q(3);
    Scalar qwqx = q(0)*q(1);
    Scalar qwqy = q(0)*q(2);
    Scalar qwqz = q(0)*q(3);
    Scalar qxqy = q(1)*q(2);
    Scalar qxqz = q(1)*q(3);
    Scalar qyqz = q(2)*q(3);

    RotationMatrix<Scalar> ret;
    ret << qw2+qx2-qy2-qz2,   2*(qxqy-qwqz),   2*(qwqy+qxqz),
             2*(qwqz+qxqy), qw2-qx2+qy2-qz2,   2*(qyqz-qwqx),
             2*(qxqz-qwqy),   2*(qwqx+qyqz), qw2-qx2-qy2+qz2;

    return ret;
}

/// Converts a quaternion to a rotation matrix. Really, better to use the built in `.matrix()`
/// method in the quaternion class, but this at least lets us have the math shown here
template <typename Derived>                     // const Quaternion<Scalar>& q
RotationMatrix<typename Derived::Scalar> quatToMat(const QuaternionBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;

    return quatToMat(toVec(q));
}

/** Converts a vectorized quaternion to an Eigen quaternion.
 *  Unfortunately, Eigen represents it's quaternion vectors as [x, y, z, w] even though its
 *  element wise constructor takes the arguments in w, x, y, z order (which is extremely confusing
 *  and internally inconsistent. Because of this, NEVER simply pass a vectorized quaternion
 *  into the constructor of a quaternion object, ALWAYS use this function.
 *  Note: this does NOT normalize the quaternion for you
 */
template <typename Derived>              // const QuatVectorized<Scalar>& q
Quaternion<typename Derived::Scalar> toQuat(const MatrixBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::toQuat expects argument of QuatVectorized equivalent");

    return Quaternion<Scalar>(q(0), q(1), q(2), q(3));
}

/** Normalizes a rotation matrix in place by using the SVD. Basic priciple is to take the
 *  SVD(R) = U * Sig * V' and then set the singular values to 1, effectively returning
 *  normSvd(R) = U * V'
 */
template <typename Derived>                        // RotationMatrix<Scalar> R
RotationMatrix<typename Derived::Scalar> svdNormalize(MatrixBase<Derived>& R)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == RotationMatrix<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == RotationMatrix<Scalar>::ColsAtCompileTime,
                  "Rotations::svdNormalize expects argument of RotationMatrix equivalent");

    // Note: it's safe to use no preconditioner because we have a square matrix
    Eigen::JacobiSVD<RotationMatrix<Scalar>, Eigen::NoQRPreconditioner>
        svdR(R, Eigen::ComputeFullU | Eigen::ComputeFullV);
    return R = svdR.matrixU() * svdR.matrixV().adjoint();
}

/// Returns the normalized version of a rotation matrix using the SVD.
template <typename Derived>                         // const RotationMatrix<Scalar> R
RotationMatrix<typename Derived::Scalar> svdNormalized(const MatrixBase<Derived>& R)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == RotationMatrix<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == RotationMatrix<Scalar>::ColsAtCompileTime,
                  "Rotations::svdNormalized expects argument of RotationMatrix equivalent");

    RotationMatrix<Scalar> ret(R);
    return svdNormalize(ret);
}

/// Normalizes a vectorized rotation matrix in place by using the SVD.
template <typename Derived>                           // RmatVectorized<Scalar> R
RmatVectorized<typename Derived::Scalar> vecSvdNormalize(MatrixBase<Derived>& R)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == RmatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == RmatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::vecSvdNormalize expects argument of RmatVectorized equivalent");

    return R = vecSvdNormalized(R);
}

/// Returns the normalized version of a vectorized rotation matrix using the SVD.
template <typename Derived>                            // const RmatVectorized<Scalar> R
RmatVectorized<typename Derived::Scalar> vecSvdNormalized(const MatrixBase<Derived>& R)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == RmatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == RmatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::vecSvdNormalized expects argument of RmatVectorized equivalent");

    return toVec(svdNormalized(toMat(R)));
}

/// Return the identity quaternion
template <typename Scalar>
inline Quaternion<Scalar> quatIdentity()
{ return Quaternion<Scalar>(1.0, 0.0, 0.0, 0.0); }

/// Return the vectorized identity quaternion
template <typename Scalar>
inline QuatVectorized<Scalar> quatVecIdentity()
{ return QuatVectorized<Scalar>(1.0, 0.0, 0.0, 0.0); }

/// forces the quaternion to have positive first element
template <typename Derived>                      // Quaternion<Scalar>& q
Quaternion<typename Derived::Scalar> quatToPositive(QuaternionBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;

    if (q.w() < 0) {
        q.w() = -q.w();
        q.x() = -q.x();
        q.y() = -q.y();
        q.z() = -q.z();
    }
    return q;
}

/// forces the vectorized quaternion to have positive first element
template <typename Derived>                        // QuatVectorized<Scalar>& q
QuatVectorized<typename Derived::Scalar> quatToPositive(MatrixBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatToPositive expects argument of QuatVectorized equivalent");

    if (q(0) < 0) {
        return q *= -1;
    }
    return q;
}

/// Returns the equivalent quaternion with a positive first element
template <typename Derived>                    // const Quaternion<Scalar>& q
Quaternion<typename Derived::Scalar> quatPositive(const QuaternionBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;

    Quaternion<Scalar> ret(q);
    return quatToPositive(ret);
}

/// Returns the equivalent vectorized quaternion with a positive first element
template <typename Derived>                        // const QuatVectorized<Scalar>& q
QuatVectorized<typename Derived::Scalar> quatPositive(const MatrixBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatPositive expects argument of QuatVectorized equivalent");

    QuatVectorized<Scalar> ret(q);
    return quatToPositive(ret);
}

/// Returns a matrix that performs quaternion pre-multiplication (operates on a vectorized quat)
/// q1 * q2 == quatCompLmat(q1) * q2vec
template <typename Derived>                     // const QuatVectorized<Scalar>& q
QuatCompMat<typename Derived::Scalar> quatCompLmat(const MatrixBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatCompLmat expects argument of QuatVectorized equivalent");

    // Note: It appears that the formula for quaternion composition presented in the Bernstein
    //       reference (formula 2.15.33) is incorrect as of 2015/12/04. The sign on the cross
    //       term needs to be flipped with respect to what is stated in the book to match Eigen's
    //       results as well as match results when converted to rotation matrices.
    QuatCompMat<Scalar> Q;
    Q(0,0) = q(0);
    Q.template block<3, 1>(1, 0) =  q.template block<3, 1>(1, 0);
    Q.template block<1, 3>(0, 1) = -q.template block<3, 1>(1, 0).transpose();
    Q.template block<3, 3>(1, 1) =   q(0) * Eigen::Matrix<Scalar, 3, 3>::Identity()
                                   + EigenUtil::crossMat(q.template block<3, 1>(1, 0));
    return Q;
}

/// Returns a matrix that performs quaternion pre-multiplication (operators on a quat)
/// q1 * q2 == quatCompLmat(q1) * q2vec
template <typename Derived>                     // const Quaternion<Scalar>& q
QuatCompMat<typename Derived::Scalar> quatCompLmat(const QuaternionBase<Derived>& q)
{
    return quatCompLmat(toVec(q));
}

/// Returns a matrix that performs quaternion post-multiplication (operates on a vectorized quat)
/// q1 * q2 == quatCompRmat(q1) * q2vec
template <typename Derived>                     // const QuatVectorized<Scalar>& q
QuatCompMat<typename Derived::Scalar> quatCompRmat(const MatrixBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatCompRmat expects argument of QuatVectorized equivalent");

    // Note: It appears that the formula for quaternion composition presented in the Bernstein
    //       reference (formula 2.15.33) is incorrect as of 2015/12/04. The sign on the cross
    //       term needs to be flipped with respect to what is stated in the book to match Eigen's
    //       results as well as match results when converted to rotation matrices.
    QuatCompMat<Scalar> Q;
    Q(0,0) = q(0);
    Q.template block<3, 1>(1, 0) =  q.template block<3, 1>(1, 0);
    Q.template block<1, 3>(0, 1) = -q.template block<3, 1>(1, 0).transpose();
    Q.template block<3, 3>(1, 1) =   q(0) * Eigen::Matrix<Scalar, 3, 3>::Identity()
                                   - EigenUtil::crossMat(q.template block<3, 1>(1, 0));
    return Q;
}

/// Returns a matrix that performs quaternion post-multiplication (operators on a quat)
/// q1 * q2 == quatCompRmat(q1) * q2vec
template <typename Derived>                     // const Quaternion<Scalar>& q
QuatCompMat<typename Derived::Scalar> quatCompRmat(const QuaternionBase<Derived>& q)
{
    return quatCompRmat(toVec(q));
}

/// Returns a quaternion representing the opposite rotation (operates on a vectorized quat)
/// To do this on an actual quaternion object, simply use the quaternion method "inverse()"
template <typename Derived>                     // const QuatVectorized<Scalar>& q
QuatVectorized<typename Derived::Scalar> quatInv(const MatrixBase<Derived>& q)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatInv expects argument of QuatVectorized equivalent");
    return QuatVectorized<Scalar>(q(0), -q(1), -q(2), -q(3));
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Rotation Derivatives ////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------------------------ //
// Rotation Matrices - Rotation Derivatives                                                   //
// ------------------------------------------------------------------------------------------ //

/** Returns the derivative of the rotation matrix given the matrix and the angular velocity
 *  Note that we need to be very precise about which rotation matrix and angular velocity we
 *  mean here:
 *
 *  Rba = the rotation matrix from frame A to frame B as discussed at the top of this file. It
 *        is the matrix that physically rotates a vector through the given motion. It is the one
 *        that Eigen gives you by default when you construct rotation objects.
 *  wba = the angular velocity of frame B relative to frame A, resolved in frame B. This is what
 *        a sensor would be measuring, if it were, say, on the vehicle. It is extremely common
 *        for angular velocity to be expressed in the body frame because all the useful integration
 *        you do with it requires body frame.
 *
 *  return = RbaDot = time derivative of Rba
 */                                             // const RotationMatrix<Scalar>& Rba
template <typename Derived1, typename Derived2> // const AngVel<Scalar>& wba
RotationMatrix<typename Derived1::Scalar> rotDeriv(const MatrixBase<Derived1>& Rba,
                                                   const MatrixBase<Derived2>& wba)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::rotDeriv expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == RotationMatrix<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == RotationMatrix<Scalar>::ColsAtCompileTime,
                  "Rotations::rotDeriv expects argument of RotationMatrix equivalent");
    static_assert(   Derived2::RowsAtCompileTime == AngVel<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == AngVel<Scalar>::ColsAtCompileTime,
                  "Rotations::rotDeriv expects argument of AngVel equivalent");

    // Poisson's Equation
    return Rba * EigenUtil::crossMat(wba);
}

/** Returns the angular velocity given the rotation matrix and the derivative of the matrix
 *  Rotation conventions are the same as discussed for angular velocity -> rotation matrix deriv
 */                                             // const RotationMatrix<Scalar>& Rba
template <typename Derived1, typename Derived2> // const RotationMatrix<Scalar>& RbaDot
AngVel<typename Derived1::Scalar> angVel(const MatrixBase<Derived1>& Rba,
                                         const MatrixBase<Derived2>& RbaDot)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::angVel expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == RotationMatrix<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == RotationMatrix<Scalar>::ColsAtCompileTime,
                  "Rotations::angVel expects argument of RotationMatrix equivalent");
    static_assert(   Derived2::RowsAtCompileTime == RotationMatrix<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == RotationMatrix<Scalar>::ColsAtCompileTime,
                  "Rotations::angVel expects argument of RotationMatrix equivalent");

    return EigenUtil::crossVec(Rba.transpose() * RbaDot);
}

/** Returns the vectorized derivative of the rotation matrix given the vectorized matrix and the
 * angular velocity.
 *  Rotation conventions are the same as discussed for angular velocity -> rotation matrix deriv
 */                                                 // const RmatVectorized<Scalar>& Rba
template <typename Derived1, typename Derived2>     // const AngVel<Scalar>& wba
RmatVectorized<typename Derived1::Scalar> rmatRotDeriv(const MatrixBase<Derived1>& Rba,
                                                       const MatrixBase<Derived2>& wba)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::rmatRotDeriv expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == RmatVectorized<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == RmatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::rmatRotDeriv expects argument of RmatVectorized equivalent");
    static_assert(   Derived2::RowsAtCompileTime == AngVel<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == AngVel<Scalar>::ColsAtCompileTime,
                  "Rotations::rmatRotDeriv expects argument of AngVel equivalent");

    return toVec(rotDeriv(toMat(Rba), wba));
}

/** Returns the angular velocity given the vectorized rotation matrix and the
 *  vectorized derivative derivative of the matrix.
 *  Rotation conventions are the same as discussed for angular velocity -> rotation matrix deriv
 */                                             // const RmatVectorized<Scalar>& Rba
template <typename Derived1, typename Derived2> // const RmatVectorized<Scalar>& RbaDot
AngVel<typename Derived1::Scalar> rmatAngVel(const MatrixBase<Derived1>& Rba,
                                             const MatrixBase<Derived2>& RbaDot)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::rmatAngVel expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == RmatVectorized<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == RmatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::rmatAngVel expects argument of RmatVectorized equivalent");
    static_assert(   Derived2::RowsAtCompileTime == RmatVectorized<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == RmatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::rmatAngVel expects argument of RmatVectorized equivalent");

    return angVel(toMat(Rba), toMat(RbaDot));
}

// ------------------------------------------------------------------------------------------ //
// Quaternions - Rotation Derivatives                                                         //
// ------------------------------------------------------------------------------------------ //

/** Returns the derivative of the quaternion given the quaternion and the angular velocity
 *  Rotation conventions are the same as discussed for angular velocity -> rotation matrix deriv
 */                                             // const Quaternion<Scalar>& qba
template <typename Derived1, typename Derived2> // const AngVel<Scalar>& wba
QuatVectorized<typename Derived1::Scalar> rotDeriv(const QuaternionBase<Derived1>& qba,
                                                   const MatrixBase<Derived2>& wba)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::rotDeriv expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived2::RowsAtCompileTime == AngVel<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == AngVel<Scalar>::ColsAtCompileTime,
                  "Rotations::rotDeriv expects argument of AngVel equivalent");

    // Note: Leaving this as the 4x4 * 4x1 seems to benchmark slightly faster. I believe this
    //       is because of vectorization
    return rotDerivJacRot(qba, wba) * toVec(qba);
    //return rotDerivJacAngVel(qba) * wba;
}

/** Returns the angular velocity given the quaternion and its derivative
 *  Rotation conventions are the same as discussed for angular velocity -> rotation matrix deriv
 */                                             // const Quaternion<Scalar>& qba
template <typename Derived1, typename Derived2> // const QuatVectorized<Scalar>& qbaDot
AngVel<typename Derived1::Scalar> angVel(const QuaternionBase<Derived1>& qba,
                                         const MatrixBase<Derived2>& qbaDot)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::angVel expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived2::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::angVel expects argument of QuatVectorized equivalent");

    return 4 * rotDerivJacAngVel(qba).transpose() * qbaDot;
}

/** Returns the derivative of the quaternion given the vectorized quaternion and the angular vel
 *  Rotation conventions are the same as discussed for angular velocity -> rotation matrix deriv
 */                                                 // const QuatVectorized<Scalar>& qba
template <typename Derived1, typename Derived2>     // const AngVel<Scalar>& wba
QuatVectorized<typename Derived1::Scalar> quatRotDeriv(const MatrixBase<Derived1>& qba,
                                                       const MatrixBase<Derived2>& wba)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::quatRotDeriv expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatRotDeriv expects argument of QuatVectorized equivalent");
    static_assert(   Derived2::RowsAtCompileTime == AngVel<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == AngVel<Scalar>::ColsAtCompileTime,
                  "Rotations::quatRotDeriv expects argument of AngVel equivalent");

    return rotDeriv(toQuat(qba), wba);
}

/** Returns the angular velocity given the vectorized quaternion and its derivative
 *  Rotation conventions are the same as discussed for angular velocity -> rotation matrix deriv
 */                                             // const QuatVectorized<Scalar>& qba
template <typename Derived1, typename Derived2> // const QuatVectorized<Scalar>& qbaDot
AngVel<typename Derived1::Scalar> quatAngVel(const MatrixBase<Derived1>& qba,
                                             const MatrixBase<Derived2>& qbaDot)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::quatAngVel expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatAngVel expects argument of QuatVectorized equivalent");
    static_assert(   Derived2::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatAngVel expects argument of QuatVectorized equivalent");

    return angVel(toQuat(qba), qbaDot);
}

// ------------------------------------------------------------------------------------------ //
// Euler Angles - Rotation Derivatives                                                        //
// ------------------------------------------------------------------------------------------ //

/* Note: Due to type collisions with the rotation matrix functions and a nasty c++ requirements
 *       that both sides of a static branch compile for all template args even though only one
 *       branch is ever taken, we cannot have the euler angles overload the same function as
 *       rotation matrices. See this post for more details:
 * http://stackoverflow.com/questions/23970532/what-do-compilers-do-with-compile-time-branching
 */

/** Returns the derivative of the Euler angles given the Euler angles and the angular velocity
 *  Rotation conventions are the same as discussed for angular velocity -> rotation matrix deriv
 */                                             // const EulerAngles<Scalar>& Tba
template <typename Derived1, typename Derived2> // const AngVel<Scalar>& wba
EulerAngles<typename Derived1::Scalar> eulerDeriv(const MatrixBase<Derived1>& Tba,
                                                  const MatrixBase<Derived2>& wba)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::eulerDeriv expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == EulerAngles<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == EulerAngles<Scalar>::ColsAtCompileTime,
                  "Rotations::eulerDeriv expects argument of EulerAngles equivalent");
    static_assert(   Derived2::RowsAtCompileTime == AngVel<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == AngVel<Scalar>::ColsAtCompileTime,
                  "Rotations::eulerDeriv expects argument of AngVel equivalent");

    return eulerDerivJacAngVel(Tba) * wba;
}

/** Returns the angular velocity given the Euler angles and their derivatives
 *  Rotation conventions are the same as discussed for angular velocity -> rotation matrix deriv
 */                                             // const EulerAngles<Scalar>& Tba
template <typename Derived1, typename Derived2> // const EulerAngles<Scalar>& TbaDot
AngVel<typename Derived1::Scalar> eulerAngVel(const MatrixBase<Derived1>& Tba,
                                              const MatrixBase<Derived2>& TbaDot)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::eulerAngVel expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == EulerAngles<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == EulerAngles<Scalar>::ColsAtCompileTime,
                  "Rotations::eulerAngVel expects argument of EulerAngles equivalent");
    static_assert(   Derived2::RowsAtCompileTime == EulerAngles<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == EulerAngles<Scalar>::ColsAtCompileTime,
                  "Rotations::eulerAngVel expects argument of EulerAngles equivalent");

    // Note: could break this out into another jacobian if there was ever a need for it
    Scalar sr = sin(Tba(0));
    Scalar cr = cos(Tba(0));
    Scalar sp = sin(Tba(1));
    Scalar cp = cos(Tba(1));
    Eigen::Matrix<Scalar, 3, 3> S;
    S << 1,   0,   -sp,
         0,  cr, sr*cp,
         0, -sr, cr*cp;
    return S * TbaDot;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Rotation Derivative Jacobians ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

// ------------------------------------------------------------------------------------------ //
// Rotation Matrices - Rotation Derivative Jacobians                                          //
// ------------------------------------------------------------------------------------------ //

/** Returns the jacobian of the rotation matrix derivative with respect to the rotation matrix
 */                                                   // const RotationMatrix<Scalar>& Rba
template <typename Derived1, typename Derived2>       // const AngVel<Scalar>& wba
RmatDotJacRmat<typename Derived1::Scalar> rotDerivJacRot(const MatrixBase<Derived1>& Rba,
                                                         const MatrixBase<Derived2>& wba)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::rotDerivJacRot expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == RotationMatrix<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == RotationMatrix<Scalar>::ColsAtCompileTime,
                  "Rotations::rotDerivJacRot expects argument of RotationMatrix equivalent");
    static_assert(   Derived2::RowsAtCompileTime == AngVel<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == AngVel<Scalar>::ColsAtCompileTime,
                  "Rotations::rotDerivJacRot expects argument of AngVel equivalent");

    Scalar wx = wba(0);
    Scalar wy = wba(1);
    Scalar wz = wba(2);

    RmatDotJacRmat<Scalar> ret;
    //      00,  10,  20,  01,  11,  21,  02,  12,  22
    ret <<   0,   0,   0,  wz,   0,   0, -wy,   0,   0,  //  R(0,1)*wz + -R(0,2)*wy,
             0,   0,   0,   0,  wz,   0,   0, -wy,   0,  //  R(1,1)*wz + -R(1,2)*wy,
             0,   0,   0,   0,   0,  wz,   0,   0, -wy,  //  R(2,1)*wz + -R(2,2)*wy,
           -wz,   0,   0,   0,   0,   0,  wx,   0,   0,  // -R(0,0)*wz +  R(0,2)*wx,
             0, -wz,   0,   0,   0,   0,   0,  wx,   0,  // -R(1,0)*wz +  R(1,2)*wx,
             0,   0, -wz,   0,   0,   0,   0,   0,  wx,  // -R(2,0)*wz +  R(2,2)*wx,
            wy,   0,   0, -wx,   0,   0,   0,   0,   0,  //  R(0,0)*wy + -R(0,1)*wx,
             0,  wy,   0,   0, -wx,   0,   0,   0,   0,  //  R(1,0)*wy + -R(1,1)*wx,
             0,   0,  wy,   0,   0, -wx,   0,   0,   0;  //  R(2,0)*wy + -R(2,1)*wx,

    return ret;
}

/// Returns the jacobian of the rotation matrix derivative with respect to the angular velocity
template <typename Derived>                               // const RotationMatrix<Scalar>& Rba
RmatDotJacAngVel<typename Derived::Scalar> rotDerivJacAngVel(const MatrixBase<Derived>& Rba)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == RotationMatrix<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == RotationMatrix<Scalar>::ColsAtCompileTime,
                  "Rotations::rotDerivJacAngVel expects argument of RotationMatrix equivalent");

    RmatDotJacAngVel<Scalar> ret;
    ret <<         0, -Rba(0,2),  Rba(0,1),
                   0, -Rba(1,2),  Rba(1,1),
                   0, -Rba(2,2),  Rba(2,1),
            Rba(0,2),         0, -Rba(0,0),
            Rba(1,2),         0, -Rba(1,0),
            Rba(2,2),         0, -Rba(2,0),
           -Rba(0,1),  Rba(0,0),         0,
           -Rba(1,1),  Rba(1,0),         0,
           -Rba(2,1),  Rba(2,0),         0;
    return ret;
}

/** Returns the jacobian of the rotation matrix derivative with respect to the vectorized matrix
 */                                                       // const RmatVectorized<Scalar>& Rba
template <typename Derived1, typename Derived2>           // const AngVel<Scalar>& wba
RmatDotJacRmat<typename Derived1::Scalar> rmatRotDerivJacRot(const MatrixBase<Derived1>& Rba,
                                                             const MatrixBase<Derived2>& wba)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::rmatRotDerivJacRot expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == RmatVectorized<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == RmatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::rmatRotDerivJacRot expects argument of RmatVectorized equivalent");
    static_assert(   Derived2::RowsAtCompileTime == AngVel<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == AngVel<Scalar>::ColsAtCompileTime,
                  "Rotations::rmatRotDerivJacRot expects argument of AngVel equivalent");

    return rotDerivJacRot(toMat(Rba), wba);
}

/// Returns the jacobian of the rotation matrix derivative with respect to the angular velocity
template <typename Derived>                                   // const RmatVectorized<Scalar>& Rba
RmatDotJacAngVel<typename Derived::Scalar> rmatRotDerivJacAngVel(const MatrixBase<Derived>& Rba)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == RmatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == RmatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::rmatRotDerivJacAngVel expects argument of RmatVectorized equivalent");

    return rotDerivJacAngVel(toMat(Rba));
}

// ------------------------------------------------------------------------------------------ //
// Quaternions - Rotation Derivative Jacobians                                                //
// ------------------------------------------------------------------------------------------ //

/** Returns the jacobian of the quaternion derivative with respect to the quaternion
 */                                                   // const Quaternion<Scalar>& qba
template <typename Derived1, typename Derived2>       // const AngVel<Scalar>& wba
QuatDotJacQuat<typename Derived1::Scalar> rotDerivJacRot(const QuaternionBase<Derived1>& qba,
                                                         const MatrixBase<Derived2>& wba)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::rotDerivJacRot expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived2::RowsAtCompileTime == AngVel<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == AngVel<Scalar>::ColsAtCompileTime,
                  "Rotations::rotDerivJacRot expects argument of AngVel equivalent");

    QuatDotJacQuat<Scalar> ret;
    ret(0, 0) = 0;
    // This weird bit of syntax is because ret is a templated type, and the template for .block
    // needs to be able to resolve the type of it's caller. There is a small note in the .block
    // docs here:
    // http://eigen.tuxfamily.org/dox/classEigen_1_1DenseBase.html#a3e433315822db2811a65e88c70672743
    ret.template block<3, 1>(1, 0) =  wba;
    ret.template block<1, 3>(0, 1) = -wba.transpose();
    ret.template block<3, 3>(1, 1) =  EigenUtil::crossMat(-wba);
    return 0.5 * ret;
}

/// Returns the jacobian of the quaternion derivative with respect to the angular velocity
template <typename Derived>                               // const Quaternion<Scalar>& qba
QuatDotJacAngVel<typename Derived::Scalar> rotDerivJacAngVel(const QuaternionBase<Derived>& qba)
{
    typedef typename Derived::Scalar Scalar;

    QuatDotJacAngVel<Scalar> ret;
    QuatVectorized<Scalar> q = toVec(qba);

    // See note above on strange ".template" syntax
    ret.template block<1, 3>(0, 0) =  -q.template block<3, 1>(1, 0).transpose();
    ret.template block<3, 3>(1, 0) =   q(0) * RotationMatrix<Scalar>::Identity()
                                     + EigenUtil::crossMat(q.template block<3, 1>(1, 0));
    return 0.5 * ret;
}

/** Returns the jacobian of the quaternion derivative with respect to the vectorized quaternion
 */                                                       // const QuatVectorized<Scalar>& qba
template <typename Derived1, typename Derived2>           // const AngVel<Scalar>& wba
QuatDotJacQuat<typename Derived1::Scalar> quatRotDerivJacRot(const MatrixBase<Derived1>& qba,
                                                             const MatrixBase<Derived2>& wba)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::quatRotDerivJacRot expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatRotDerivJacRot expects argument of QuatVectorized equivalent");
    static_assert(   Derived2::RowsAtCompileTime == AngVel<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == AngVel<Scalar>::ColsAtCompileTime,
                  "Rotations::quatRotDerivJacRot expects argument of AngVel equivalent");

    return rotDerivJacRot(toQuat(qba), wba);
}

/// Returns the jacobian of the quaternion derivative with respect to the angular velocity
template <typename Derived>                                   // const Quaternion<Scalar>& qba
QuatDotJacAngVel<typename Derived::Scalar> quatRotDerivJacAngVel(const MatrixBase<Derived>& qba)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatRotDerivJacAngVel expects argument of QuatVectorized equivalent");

    return rotDerivJacAngVel(toQuat(qba));
}

// ------------------------------------------------------------------------------------------ //
// Euler Angles - Rotation Derivative Jacobians                                               //
// ------------------------------------------------------------------------------------------ //

/** Returns the jacobian of the Euler angles derivatives with respect to the Euler angles
 *  Note: this cannot give a valid anwswer if the system is in gimbal lock (p = +/- pi/2)
 */                                                         // const EulerAngles<Scalar>& Tba
template <typename Derived1, typename Derived2>             // const AngVel<Scalar>& wba
EulerDotJacEuler<typename Derived1::Scalar> eulerDerivJacEuler(const MatrixBase<Derived1>& Tba,
                                                               const MatrixBase<Derived2>& wba)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::eulerDerivJacEuler expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == EulerAngles<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == EulerAngles<Scalar>::ColsAtCompileTime,
                  "Rotations::eulerDerivJacEuler expects argument of EulerAngles equivalent");
    static_assert(   Derived2::RowsAtCompileTime == AngVel<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == AngVel<Scalar>::ColsAtCompileTime,
                  "Rotations::eulerDerivJacEuler expects argument of AngVel equivalent");

    EulerDotJacEuler<Scalar> ret;
    Scalar sr = sin(Tba(0));
    Scalar cr = cos(Tba(0));
    Scalar sp = sin(Tba(1));
    Scalar cp = cos(Tba(1));
    Scalar tp = sp / cp;

    Scalar L =  wba(1)*cr - wba(2)*sr;
    Scalar M =  wba(2)*cr + wba(1)*sr;
    Scalar N = -wba(2)*cr - wba(1)*sr;

    // Verified this using Matlab sym-math
    ret << L*tp, M/(cp*cp), 0,
              N,         0, 0,
           L/cp,   M*tp/cp, 0;

    if (fabs(cp) < MathUtil::TINY) {
        // Not really sure what the best thing to set these values to in gimbal lock
        ret(0,0) = 0;
        ret(2,0) = 0;
        ret(0,1) = 0;
        ret(2,1) = 0;
    }

    return ret;
}

/** Returns the jacobian of the Euler angles derivatives with respect to the angular velocity
 *  Note: this cannot give a valid anwswer if the system is in gimbal lock (p = +/- pi/2)
 */
template <typename Derived>                                  // const EulerAngles<Scalar>& Tba
EulerDotJacAngVel<typename Derived::Scalar> eulerDerivJacAngVel(const MatrixBase<Derived>& Tba)
{
    typedef typename Derived::Scalar Scalar;
    static_assert(   Derived::RowsAtCompileTime == EulerAngles<Scalar>::RowsAtCompileTime
                  && Derived::ColsAtCompileTime == EulerAngles<Scalar>::ColsAtCompileTime,
                  "Rotations::eulerDerivJacAngVel expects argument of EulerAngles equivalent");

    EulerDotJacAngVel<Scalar> ret;
    Scalar sr = sin(Tba(0));
    Scalar cr = cos(Tba(0));
    Scalar sp = sin(Tba(1));
    Scalar cp = cos(Tba(1));
    Scalar tp = sp / cp;

    ret << 1, sr*tp, cr*tp,
           0,    cr,   -sr,
           0, sr/cp, cr/cp;

    if (fabs(cp) < MathUtil::TINY) {
        // Not really sure what the best thing to set these values to in gimbal lock
        ret(1, 0) = 0;
        ret(1, 2) = 0;
        ret(2, 0) = 0;
        ret(2, 2) = 0;
    }

    return ret;
}

////////////////////////////////////////////////////////////////////////////////////////////////
// Rotation Observation Covariances ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

/* Note: This family of functions are used to produce an observation covariance matrix for use
 *       in a Kalman filter when performing a correction with the given rotation object. The
 *       user must input a 3x3 covariance matrix that represents the covariance of the observation
 *       with respect to perturbation rotations about the global x, y, and z axes
 *       (so the diagonal of the input matrix is the variance in each of those directions,
 *       respectively). Only valid for reasonably small covariances (because it uses a jacobian).
 */

// ------------------------------------------------------------------------------------------ //
// Rotation Matrices - Rotation Observation Covariances                                       //
// ------------------------------------------------------------------------------------------ //

/** Returns the covariance of the rotation matrix observation given estimated rotation matrix and
 *  the observation noise covariance as described above.
 */                                             // const RotationMatrix<Scalar>& Rba
template <typename Derived1, typename Derived2> // const PerturbationCov<Scalar>& Q
RmatObsCov<typename Derived1::Scalar> obsCov(const MatrixBase<Derived1>& Rba,
                                             const MatrixBase<Derived2>& Q)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::obsCov expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == RotationMatrix<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == RotationMatrix<Scalar>::ColsAtCompileTime,
                  "Rotations::obsCov expects argument of RotationMatrix equivalent");
    static_assert(   Derived2::RowsAtCompileTime == PerturbationCov<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == PerturbationCov<Scalar>::ColsAtCompileTime,
                  "Rotations::obsCov expects argument of PerturbationCov equivalent");

    RmatDotJacAngVel<Scalar> F = rotDerivJacAngVel(Rba);
    // need orientation matrix to convert noise from global to body frame: Oba = Rba.transpose()
    return F * Rba.transpose() * Q * Rba * F.transpose();
}

/** Returns the covariance of the rotation matrix observation given vectorized estimated rotation
 *  matrix and the observation noise covariance as described above.
 */                                             // const RmatVectorized<Scalar>& Rba
template <typename Derived1, typename Derived2> // const PerturbationCov<Scalar>& Q
RmatObsCov<typename Derived1::Scalar> rmatObsCov(const MatrixBase<Derived1>& Rba,
                                                 const MatrixBase<Derived2>& Q)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::rmatObsCov expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == RmatVectorized<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == RmatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::rmatObsCov expects argument of RmatVectorized equivalent");
    static_assert(   Derived2::RowsAtCompileTime == PerturbationCov<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == PerturbationCov<Scalar>::ColsAtCompileTime,
                  "Rotations::rmatObsCov expects argument of PerturbationCov equivalent");

    return obsCov(toMat(Rba), Q);
}

// ------------------------------------------------------------------------------------------ //
// Quaternions - Rotation Observation Covariances                                             //
// ------------------------------------------------------------------------------------------ //

/** Returns the covariance of the quaternion observation given estimated quaternion and the
 *  observation noise covariance as described above.
 */                                             // const Quaternion<Scalar>& qba
template <typename Derived1, typename Derived2> // const PerturbationCov<Scalar>& Q
QuatObsCov<typename Derived1::Scalar> obsCov(const QuaternionBase<Derived1>& qba,
                                             const MatrixBase<Derived2>& Q)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::obsCov expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived2::RowsAtCompileTime == PerturbationCov<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == PerturbationCov<Scalar>::ColsAtCompileTime,
                  "Rotations::obsCov expects argument of PerturbationCov equivalent");

    QuatDotJacAngVel<Scalar> F = rotDerivJacAngVel(qba);
    // need orientation matrix to convert noise from global to body frame: Oba = Rba.transpose()
    RotationMatrix<Scalar> Rba = qba.matrix();
    return F * Rba.transpose() * Q * Rba * F.transpose();
}

/** Returns the covariance of the quaternion observation given vectorized estimated quaternion
 *  and the observation noise covariance as described above.
 */                                             // const QuatVectorized<Scalar>& qba
template <typename Derived1, typename Derived2> // const PerturbationCov<Scalar>& Q
QuatObsCov<typename Derived1::Scalar> quatObsCov(const MatrixBase<Derived1>& qba,
                                                 const MatrixBase<Derived2>& Q)
{
    static_assert(std::is_same<typename Derived1::Scalar, typename Derived2::Scalar>::value,
                  "Rotations::quatObsCov expects objects with the same Scalar types");

    typedef typename Derived1::Scalar Scalar;
    static_assert(   Derived1::RowsAtCompileTime == QuatVectorized<Scalar>::RowsAtCompileTime
                  && Derived1::ColsAtCompileTime == QuatVectorized<Scalar>::ColsAtCompileTime,
                  "Rotations::quatObsCov expects argument of QuatVectorized equivalent");
    static_assert(   Derived2::RowsAtCompileTime == PerturbationCov<Scalar>::RowsAtCompileTime
                  && Derived2::ColsAtCompileTime == PerturbationCov<Scalar>::ColsAtCompileTime,
                  "Rotations::quatObsCov expects argument of PerturbationCov equivalent");

    return obsCov(toQuat(qba), Q);
}

} /* Rotations */
} /* Kestrel */
