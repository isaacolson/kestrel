#include "kestrel/util/TimeUtil.hpp"

using namespace Kestrel;

static bool useOverride = false;
static u64  useStamp    = 0;

u64 TimeUtil::stamp()
{
    return useOverride ? useStamp : utime();
}

void TimeUtil::overrideStamp(u64 stamp)
{
    useOverride = true;
    useStamp    = stamp;
}

void TimeUtil::resetStamp()
{
    useOverride = false;
}
