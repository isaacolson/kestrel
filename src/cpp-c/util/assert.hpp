#pragma once
#include <iostream>
#include <cassert>

#define Assert(cond, msg) assert((cond) && msg)

#define AssertMsg(cond, ...) \
    do {\
        if (!(cond)) { \
            getAssertPrinterLock(); \
            __kestrel_print_recursive__(std::cerr, "\n\n\n\033[1m\033[31mAssert", \
                                        " in \033[39m", __PRETTY_FUNCTION__, \
                                        "\033[31m from \033[39m", __FILE__, \
                                        "\033[31m on line \033[39m", __LINE__, \
                                        "\n\n\033[1m\033[31mmsg:\n\033[0m", __VA_ARGS__, \
                                        "\n\n\033[1m\033[31mfailed cond:\n\033[0m", #cond); \
            std::cerr << "\n\n" << std::endl; \
            assert(false); \
            exit(1); \
        } \
    } while (0)

void getAssertPrinterLock();

static void __kestrel_print_recursive__(std::ostream& os) { }

template<typename First, typename... Rest>
static void __kestrel_print_recursive__(std::ostream& os, First&& first, Rest&&... rest)
{
    os << std::forward<First>(first);
    __kestrel_print_recursive__(os, std::forward<Rest>(rest)...);
}
