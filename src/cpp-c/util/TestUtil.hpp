#pragma once

#include <iostream>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

namespace Kestrel {
namespace TestUtil {

// Note: this has to be "int" because the template of eigen matrices is "int"
template<typename Derived1, typename Derived2>
static bool matEquals(const Eigen::MatrixBase<Derived1>& x,
                      const Eigen::MatrixBase<Derived2>& y,
                      f64 delta = MathUtil::TINY,
                      bool print = true)
{
    // TODO: this currently prints BEFORE the cxxtest error line that it is actually attributed
    //       too, figure out a way to make it come after
    bool firstPrint = true;
    bool retval = true;

    if (x.rows() != y.rows() || x.cols() != y.cols()) {
        if (print) std::cout << "\nSize mismatch: x is ["
                             << x.rows() << "," << x.cols() << "],  y is ["
                             << y.rows() << "," << y.cols() << "]\n" << std::endl;
        return false;
    }

    for (int j = 0; j < x.cols(); ++j) { // Eigen matrices are stored column major
        for (int i = 0; i < x.rows(); ++i) {
            if (!(fabs(x(i,j) - y(i,j)) < delta)) { // took the negation to catch NAN
                retval = false;

                if (print) {
                    if (firstPrint) {
                        std::cout << "\n";
                        firstPrint = false;
                    }

                    // TODO: change this print to use printf and format better
                    //       (maybe horizontal vectors just above each other)
                    std::cout << "elt " << i << ", " << j << ": " << x(i,j) << " != " << y(i,j)
                              << " within " << delta << "\n";
                }
            }
        }
    }
    if (print && !firstPrint) std::cout << std::endl;

    return retval;
}

} /* TestUtil */
} /* Kestrel */
