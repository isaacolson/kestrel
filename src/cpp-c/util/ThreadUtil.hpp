#pragma once

#include <utility>
#include <thread>
#include <atomic>

#include <kestrel/util/assert.hpp>

namespace Kestrel {
namespace ThreadUtil {

class Runnable {
  protected:
    std::atomic_bool done {true};
    std::atomic_bool running {false};

  public:
    /// Ctor
    Runnable() {}
    /// Dtor
    virtual ~Runnable() {}

  protected: // Children should override to inject desired functionality

    /// Function that will be run. Should return `if (isDone())`
    virtual void runFunc() {}

    /// Function that will be called on stop. Use for extra signalling if necessary
    virtual void stopFunc() {}

  public: // the actual interface to a user

    /// Run in this thread.
    void run()
    {
        done = false;
        running = true;
        runFunc();
        done = true;
        running = false;
    };

    /// Stop thread execution.
    void stop()
    {
        done = true;
        stopFunc();
    }

    /// Returns if the runnable should return from run()
    bool isDone() const { return done; }

    /// Returns if the runnable is still in the run() call
    bool isRunning() const { return running; }

  public:

    // Note: it is not safe for inheritors to copy or swap when the runnable is running,
    //       this class prevents the unsafe behavior with assertions. To get the protection,
    //       children need to call the parent swap / copy

    /// Swap
    inline void swap(Runnable& o) { Assert(!running && !o.running, "cannot swap while running"); }
    /// Copy Ctor
    Runnable(const Runnable& o)   { Assert(!running && !o.running, "cannot copy while running"); }
    /// Move Ctor
    Runnable(Runnable&& o) { swap(o); }
    /// Unifying Assignment
    Runnable& operator=(Runnable o) { swap(o); return *this; }
};

class Thread: public Runnable {
  public:
    typedef Runnable ParentType;

  private:
    std::thread t;

  public:
    /// Ctor
    Thread() {}
    /// Dtor
    virtual ~Thread() {}

    /// Run in a different thread. Children should inject function by overriding runFunc()
    void start()
    {
        if (isRunning()) return;
        running = true; // avoid race condition with join
        t = std::thread(&Thread::run, this);
    }

    /// Wait for thread to join(). Note this does not call stop() (often that should be done first)
    void join()
    {
        if (t.joinable()) t.join();
    }

    /// Detatch thread. User will never be able to reconnect with it.
    void detach()
    {
        if (!isRunning()) return;
        t.detach();
    }

    // Note: it is not safe for inheritors to copy or swap when the thread is running,
    //       this class prevents the unsafe behavior with assertions. To get the protection,
    //       children need to call the parent swap / copy

    /// Swap
    inline void swap(Thread& o) { ParentType::swap(o); }
    /// Copy Ctor
    Thread(const Thread& o) : ParentType(o) {}
    /// Move Ctor
    Thread(Thread&& o) { swap(o); }
    /// Unifying Assignment
    Thread& operator=(Thread o) { swap(o); return *this; }
};

} /* ThreadUtil */
} /* Kestrel */
