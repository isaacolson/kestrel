#pragma once

#include <string>

#include <Eigen/Dense>

/* Note: I have not plumbed the depths of the best way to make functions on Eigen matrices ...
 *       it turns out it can be MUCH more complicated than I'd imagined to make functions that
 *       work on their lazy-eval expressions. I've implemented a couple that do take lazy-evals,
 *       but from what I can tell, they could be more optimal if I left everything as lazy-eval
 *       more often. See the following links for more details:
 *
 *       http://eigen.tuxfamily.org/dox/TopicFunctionTakingEigenTypes.html
 *       http://eigen.tuxfamily.org/dox/TopicLazyEvaluation.html
 *       http://eigen.tuxfamily.org/dox/TopicCustomizingEigen.html
 *       http://eigen.tuxfamily.org/dox/classEigen_1_1MatrixBase.html
 *
 * TODO: For now, all of these functions only work for statically sized objects (and static
 *       assert on the size). If I start using dynamic sized objects, just add something like
 *       "or size is dynamic" to the asserts and then check the size at runtime.
 */

namespace Kestrel {
namespace EigenUtil {

/// Returns the symmetric component of the input matrix (which must be square)
template <typename Derived>
inline typename Eigen::MatrixBase<Derived>::PlainObject
    forceSym(const Eigen::MatrixBase<Derived>& M)
{
    static_assert(Derived::RowsAtCompileTime == Derived::ColsAtCompileTime,
                  "EigenUtil::forceSym expects a square matrix");

    return (0.5*(M + M.transpose())).eval();
}

/// Returns the skew symmetric component of the input matrix (which must be square)
template <typename Derived>
inline typename Eigen::MatrixBase<Derived>::PlainObject
    forceSkew(const Eigen::MatrixBase<Derived>& M)
{
    static_assert(Derived::RowsAtCompileTime == Derived::ColsAtCompileTime,
                  "EigenUtil::forceSkew expects a square matrix");

    return (0.5*(M - M.transpose())).eval();
}

/// Returns the matrix that performs the cross product when it operates on a vector. v must be 3x1
template <typename Derived>                                // const Eigen::Matrix<Scalar, 3, 1>
inline Eigen::Matrix<typename Derived::Scalar, 3, 3> crossMat(const Eigen::MatrixBase<Derived>& v)
{
    static_assert(Derived::RowsAtCompileTime == 3 && Derived::ColsAtCompileTime == 1,
                  "EigenUtil::crossMat expects argument of Matrix<Scalar, 3, 1> equivalent");

    Eigen::Matrix<typename Derived::Scalar, 3, 3> ret;
    ret <<     0, -v(2),  v(1),
            v(2),     0, -v(0),
           -v(1),  v(0),     0;
    return ret;
}

/// Inverts the crossMat operation, M must be a skew sym 3x3
template <typename Derived>                                // const Eigen::Matrix<Scalar, 3, 3>
inline Eigen::Matrix<typename Derived::Scalar, 3, 1> crossVec(const Eigen::MatrixBase<Derived>& M)
{
    static_assert(Derived::RowsAtCompileTime == 3 && Derived::ColsAtCompileTime == 3,
                  "EigenUtil::crossVec expects argument of Matrix<Scalar, 3, 3> equivalent");

    // Note: this function will do it's best to account for precision discrepencies
    //       in the cross matrix, but if you pass in a matrix that is not skew symmetric,
    //       the output of this will be garbage (garbage in, garbage out)
    return Eigen::Matrix<typename Derived::Scalar, 3, 1>(0.5*(M(2,1)-M(1,2)),
                                                         0.5*(M(0,2)-M(2,0)),
                                                         0.5*(M(1,0)-M(0,1)));
}

inline Eigen::IOFormat linearFormat(const std::string& delim)
{ return Eigen::IOFormat(Eigen::StreamPrecision, Eigen::DontAlignCols, delim, delim); }

} /* EigenUtil */
} /* Kestrel */
