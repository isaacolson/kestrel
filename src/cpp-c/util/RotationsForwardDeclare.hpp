// Do not include this file manually, it is used solely by Rotations.hpp
#ifdef ROTATIONS_FORWARD_DECL_INCLUDE_OK

// Only dumping function prototypes in here to forward declare them while preserving
// organization within the main file

template <typename Derived1, typename Derived2>
QuatDotJacQuat<typename Derived1::Scalar> rotDerivJacRot(const QuaternionBase<Derived1>& qba,
                                                         const MatrixBase<Derived2>& wba);

template <typename Derived>
QuatDotJacAngVel<typename Derived::Scalar> rotDerivJacAngVel(const QuaternionBase<Derived>& qba);

template <typename Derived1, typename Derived2>
EulerDotJacEuler<typename Derived1::Scalar> eulerDerivJacEuler(const MatrixBase<Derived1>& Tba,
                                                               const MatrixBase<Derived2>& wba);

template <typename Derived>
EulerDotJacAngVel<typename Derived::Scalar> eulerDerivJacAngVel(const MatrixBase<Derived>& Tba);

#endif
