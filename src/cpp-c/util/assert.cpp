#include "kestrel/util/assert.hpp"

#include <mutex>

static std::mutex assertPrintLock;

void getAssertPrinterLock() { assertPrintLock.lock(); }

