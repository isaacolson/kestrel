#pragma once

#include <cxxtest/TestSuite.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/TimeUtil.hpp"

using namespace std;
using namespace Kestrel;
using namespace TimeUtil;

class TimeUtilTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { TimeUtil::resetStamp(); }
    void tearDown() override {}

    void testStamp()
    {
        u64 t = utime();
        u64 s = stamp();

        TS_ASSERT_DELTA(t, s, 1000); // less than 1 ms should be plenty

        overrideStamp(1337);

        s = stamp();

        TS_ASSERT_EQUALS(s, 1337);
    }
};

