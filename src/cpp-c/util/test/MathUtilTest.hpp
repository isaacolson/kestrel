#pragma once

#include <cxxtest/TestSuite.h>

#include <iostream>
#include <vector>

#include <Eigen/Dense>
#include "kestrel/util/types.hpp"
#include "kestrel/util/MathUtil.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace MathUtil;

class MathUtilTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testRadianConversions()
    {
        TS_ASSERT_DELTA(radToPiPi( 4),   4-  TWOPI, 0.001);
        TS_ASSERT_DELTA(radToPiPi( 40),  40-6*TWOPI, 0.001);
        TS_ASSERT_DELTA(radToPiPi(-4),  -4+  TWOPI, 0.001);
        TS_ASSERT_DELTA(radToPiPi(-40), -40+6*TWOPI, 0.001);

        TS_ASSERT_DELTA(radDiffToPiPi( 0,  2), 2, 0.001);
        TS_ASSERT_DELTA(radDiffToPiPi(-2,  2), 2-TWOPI, 0.001);
        TS_ASSERT_DELTA(radDiffToPiPi( 2,  0), 0, 0.001);
        TS_ASSERT_DELTA(radDiffToPiPi( 2, -2), TWOPI-2, 0.001);
    }

    void testScalarGausNoise()
    {
        f64 mean = 2;
        f64 var  = 4;

        sz count = 1000;
        vector<f64> set(count);
        for (sz i = 0; i < count; ++i) set[i] = gausNoise(mean, var);

        MathUtil::NormDist res1 = MathUtil::normDistQuick(set);
        MathUtil::NormDist res2 = MathUtil::normDist(set);

        MathUtil::NormDist res3{0,0,0};
        for (sz i = 0; i < count; ++i) res3 = MathUtil::normDist(res3, set[i]);

        // These are super wide bounds, but I'm not really worried about this failing
        // and don't want to throw false negatives
        TS_ASSERT_DELTA(res1.mean, mean, 0.5);
        TS_ASSERT_DELTA(res1.var,  var,  0.9);
        TS_ASSERT_DELTA(res2.mean, res1.mean, MathUtil::TINY);
        TS_ASSERT_DELTA(res2.var,  res1.var,  MathUtil::TINY);
        TS_ASSERT_DELTA(res3.mean, res1.mean, MathUtil::TINY);
        TS_ASSERT_DELTA(res3.var,  res1.var,  MathUtil::TINY);
    }

    void testGausNoise()
    {
        Matrix<f64, 2, 2> Q; Q << 2, 0,
                                  0, 4;

        sz count = 1000;
        vector<f64> set1(count);
        vector<f64> set2(count);
        for (sz i = 0; i < count; ++i) {
            Matrix<f64, 2, 1> w = gausNoise(Q, true);
            set1[i] = w[0];
            set2[i] = w[1];
        }

        MathUtil::NormDist res1 = MathUtil::normDistQuick(set1);
        MathUtil::NormDist res2 = MathUtil::normDistQuick(set2);
        MathUtil::NormDist res3 = MathUtil::normDist(set1);
        MathUtil::NormDist res4 = MathUtil::normDist(set2);

        MathUtil::NormDist res5{0,0,0};
        MathUtil::NormDist res6{0,0,0};
        for (sz i = 0; i < count; ++i) {
            res5 = MathUtil::normDist(res5, set1[i]);
            res6 = MathUtil::normDist(res6, set2[i]);
        }

        // These are super wide bounds, but I'm not really worried about this failing
        // and don't want to throw false negatives
        TS_ASSERT_DELTA(res1.mean, 0,      0.5);
        TS_ASSERT_DELTA(res1.var,  Q(0,0), 0.9);
        TS_ASSERT_DELTA(res2.mean, 0,      0.5);
        TS_ASSERT_DELTA(res2.var,  Q(1,1), 0.9);
        TS_ASSERT_DELTA(res3.mean, res1.mean, MathUtil::TINY);
        TS_ASSERT_DELTA(res3.var,  res1.var,  MathUtil::TINY);
        TS_ASSERT_DELTA(res4.mean, res2.mean, MathUtil::TINY);
        TS_ASSERT_DELTA(res4.var,  res2.var,  MathUtil::TINY);
        TS_ASSERT_DELTA(res5.mean, res1.mean, MathUtil::TINY);
        TS_ASSERT_DELTA(res5.var,  res1.var,  MathUtil::TINY);
        TS_ASSERT_DELTA(res6.mean, res2.mean, MathUtil::TINY);
        TS_ASSERT_DELTA(res6.var,  res2.var,  MathUtil::TINY);
    }

    void testNonDiagGausNoise()
    {
        Matrix<f64, 2, 2> Q; Q << 1, 0.9,
                                  0.9, 1;

        sz count = 1000;
        vector<Matrix<f64, 2, 1>> set(count);
        for (sz i = 0; i < count; ++i) {
            set[i] = gausNoise(Q);
        }

        MathUtil::MultiNormDist<2> res = MathUtil::normDist(set);

        // These are super wide bounds, but I'm not really worried about this failing
        // and don't want to throw false negatives
        TS_ASSERT_DELTA(res.mean(0),  0, 0.5);
        TS_ASSERT_DELTA(res.mean(1),  0, 0.5);
        TS_ASSERT_DELTA(res.cov(0,0), Q(0,0), 0.9);
        TS_ASSERT_DELTA(res.cov(0,1), Q(0,1), 0.5);
        TS_ASSERT_DELTA(res.cov(1,0), Q(1,0), 0.5);
        TS_ASSERT_DELTA(res.cov(1,1), Q(1,1), 0.9);
    }

    void testUnifNoise()
    {
        Matrix<f64, 3, 1> min(-1.0, -2.0, -3.0);
        Matrix<f64, 3, 1> max( 1.0,  2.0,  3.0);

        Matrix<f64, 3, 1> expMean = (max + min) / 2;
        Matrix<f64, 3, 1> expVar  = (max - min).array().square() / 12;

        sz count = 1000;
        vector<Matrix<f64, 3, 1>> set(count);
        for (sz i = 0; i < count; ++i) {
            set[i] = unifNoise(min, max);
        }

        MathUtil::MultiNormDist<3> res = MathUtil::normDist(set);

        // These are super wide bounds, but I'm not really worried about this failing
        // and don't want to throw false negatives
        TS_ASSERT_DELTA(res.mean(0),  expMean(0), 0.5);
        TS_ASSERT_DELTA(res.mean(1),  expMean(1), 0.5);
        TS_ASSERT_DELTA(res.mean(2),  expMean(2), 0.5);
        TS_ASSERT_DELTA(res.cov(0,0), expVar(0),  0.9);
        TS_ASSERT_DELTA(res.cov(1,1), expVar(1),  0.9);
        TS_ASSERT_DELTA(res.cov(2,2), expVar(2),  0.9);
    }

    void testEigenNegLLT()
    {
        Matrix<f64, 2, 2> P; P << -1, 0, 0, 1;
        LLT<Matrix<f64, 2, 2>> llt(P);

        ComputationInfo i = llt.info();
        TS_ASSERT(i != ComputationInfo::Success);
    }
};
