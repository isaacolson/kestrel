#pragma once

#include <cxxtest/TestSuite.h>

#include <iostream>
#include <sstream>

#include <Eigen/Dense>

#include "kestrel/util/TestUtil.hpp"
#include "kestrel/util/EigenUtil.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace TestUtil;
using namespace EigenUtil;

class EigenUtilTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testForceSym()
    {
        Matrix3d P = Matrix3d::Identity();
        P(0, 2) = 0.1;
        Matrix3d sym = forceSym(P);
        Matrix3d exp = P;
        exp(0, 2) = exp(2, 0) = 0.05;
        TS_ASSERT(matEquals(sym, exp));
    }

    void testForceSkew()
    {
        Matrix3d P = Matrix3d::Identity();
        P(0, 2) = 0.1;
        Matrix3d skew = forceSkew(P);
        Matrix3d exp = Matrix3d::Zero();
        exp(0, 2) = 0.05;
        exp(2, 0) = -exp(0, 2);
        TS_ASSERT(matEquals(skew, exp));
    }

    void testSymSkewDecomp()
    {
        Matrix3d P = Matrix3d::Random();
        Matrix3d sym  = forceSym(P);
        Matrix3d skew = forceSkew(P);
        TS_ASSERT(matEquals(P, (Matrix3d)(sym + skew)));
    }

    void testCrossMat()
    {
        Vector3d v(1.0, 2.0, 3.0);
        Matrix3d vx = crossMat(v);
        Matrix3d ex; ex <<  0.0, -3.0,  2.0,
                            3.0,  0.0, -1.0,
                           -2.0,  1.0,  0.0;
        TS_ASSERT(matEquals(vx, ex));
    }

    void testCrossVec()
    {
        Matrix3d vx; vx <<  0.0, -3.0,  2.0,
                            3.0,  0.0, -1.0,
                           -2.0,  1.0,  0.0;
        Vector3d v = crossVec(vx);
        Vector3d ex(1.0, 2.0, 3.0);
        TS_ASSERT(matEquals(v, ex));
    }

    void testEigenMatrixBaseOperation()
    {
        Vector3d w(1, 2, 3);

        Matrix3d W1 =  crossMat(-w);
        Matrix3d W2 = -crossMat( w);
        TS_ASSERT(matEquals(W1, W2));

        Vector3d w1 =  crossVec(-W1);
        Vector3d w2 = -crossVec( W2);
        TS_ASSERT(matEquals(w1, w2));
    }

    void testEigenBlocks()
    {
        Matrix3d M = Matrix3d::Ones();
        M.block<2, 2>(0, 0) = Matrix2d::Zero();

        Vector3d v = Vector3d::Constant(2);
        M.block<3, 1>(0, 2) = v;

        Matrix3d N;
        N << 0, 0, 2,
             0, 0, 2,
             1, 1, 2;

        TS_ASSERT(matEquals(M, N));
    }

    void testEigenSvdOrder()
    {
        Matrix<f64, 5, 5> M; M.setRandom();
        Eigen::JacobiSVD<decltype(M)> svd(M, Eigen::ComputeFullU | Eigen::ComputeFullV);
        Matrix<f64, 5, 1> sv = svd.singularValues();

        for (int i = 1; i < 5; ++i) {
            TS_ASSERT_LESS_THAN_EQUALS(sv(i), sv(i-1));
        }
    }

    void testFormat()
    {
        // Note: for whatever reason the "matrix suffix" term seems to add a mandatory
        //       "row suffix" of an extra space as well as a newline at the end
        IOFormat CsvFormatSpace(Eigen::StreamPrecision, 0, ",", ",", "", "", "", ",");
        IOFormat CsvFormat(Eigen::StreamPrecision, 0, ",", ",");

        Matrix3d x; x <<  1,  2,  3,
                          4,  5,  6,
                          7,  8,  9;
        stringstream str1;
        str1 << x.format(CsvFormat);
        TS_ASSERT(str1.str() == "1,2,3,4,5,6,7,8,9");

        stringstream str2;
        str2 << x.format(CsvFormatSpace) << endl;
        TS_ASSERT(str2.str() == "1,2,3, 4,5,6, 7,8,9,\n");
    }

    //void testStaticAssert()
    //{
        //Matrix<f64, 2, 3> wrongSize = Matrix<f64, 2, 3>::Zero();
        //Matrix<f64, 2, 3> shouldAssert = forceSym(wrongSize);
                             //shouldAssert = forceSkew(wrongSize);
        //TS_ASSERT(shouldAssert.rows() == shouldAssert.cols());
    //}
};
