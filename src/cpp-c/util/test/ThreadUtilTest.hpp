#pragma once

#include <cxxtest/TestSuite.h>

#include <unistd.h>
#include <iostream>
#include <mutex>
#include <condition_variable>

#include "kestrel/util/types.hpp"
#include "kestrel/util/ThreadUtil.hpp"

using namespace std;
using namespace Kestrel;
using namespace ThreadUtil;

class Counter: public Thread {
  public:
    typedef Thread ParentType;

  protected:
    u32 max;
    u32 cur = 0;

  public:
    /// Ctor
    Counter(u32 max = 10) : max(max) {}
    /// Dtor
    virtual ~Counter() {}

    virtual bool cond()
    { return !isDone() && (cur < max); }

    virtual void op()
    { usleep(1); ++cur; }

    void runFunc() override
    { while (cond()) { op(); } }

    u32 get() const { return cur; }
};

class CounterSignal : public Counter
{
  public:
    typedef Counter ParentType;
    mutex lock1; // Locked used for condition signaling
    mutex lock2; // Lock used to prevent counter for continuing after signaling until signaled
                 // thread has a chance to wake up and do something
    condition_variable cv;

  private:
    u32 sig;

  public:
    /// Ctor
    CounterSignal (u32 max = 10, u32 sig = 5) : Counter(max), sig(sig) {}
    /// Dtor
    virtual ~CounterSignal () {}

    void op() override
    {
        usleep(1);
        unique_lock<mutex> lk(lock1);
        ++cur;
        if (get() == sig) {
            cv.notify_all();
            lk.unlock();
            lock2.lock();
        }
    }
};

class ThreadUtilTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void testInit()
    {
        Counter c;
        TS_ASSERT(c.isDone());
        TS_ASSERT(!c.isRunning());
        TS_ASSERT_EQUALS(c.get(), 0);
    }

    void testRun()
    {
        Counter c(1000);
        c.run();
        TS_ASSERT(c.isDone());
        TS_ASSERT(!c.isRunning());
        TS_ASSERT_EQUALS(c.get(), 1000);
    }

    void testStop()
    {
        CounterSignal c(1000, 500);

        std::thread t([&]{
            unique_lock<mutex> lk2(c.lock2);
            unique_lock<mutex> lk1(c.lock1);
            c.cv.wait(lk1, [&](){ return c.get() == 500; });
            c.stop();
        });

        c.run();
        TS_ASSERT(c.isDone());
        TS_ASSERT(!c.isRunning());
        TS_ASSERT_EQUALS(c.get(), 500);

        t.join();
    }

    void testStart()
    {
        Counter c(1000);
        c.start();
        c.join();
        TS_ASSERT(c.isDone());
        TS_ASSERT(!c.isRunning());
        TS_ASSERT_EQUALS(c.get(), 1000);
    }

    void testInterrupt()
    {
        CounterSignal c(1000, 500);

        std::thread t([&]{
            unique_lock<mutex> lk2(c.lock2);
            unique_lock<mutex> lk1(c.lock1);
            c.cv.wait(lk1, [&](){ return c.get() == 500; });
            c.stop();
        });

        c.start();
        c.stop();

        c.join();
        TS_ASSERT(c.isDone());
        TS_ASSERT(!c.isRunning());
        TS_ASSERT_EQUALS(c.get(), 500);

        t.join();
    }

    void testJoin()
    {
        Counter c(1000);
        c.join();

        c.start();
        c.stop();
        while (c.isRunning()) usleep(100);
        c.join();
    }
};
