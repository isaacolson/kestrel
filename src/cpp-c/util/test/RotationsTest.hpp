#pragma once

#include <cxxtest/TestSuite.h>

#include <iostream>

#include <Eigen/Dense>

#include "kestrel/util/TestUtil.hpp"
#include "kestrel/util/MathUtil.hpp"
#include "kestrel/util/EigenUtil.hpp"
#include "kestrel/util/Rotations.hpp"

// XXX: bit of a circular dependency here, tests that need this should move
//#include "kestrel/control/KalmanFilter.hpp"

using namespace std;
using namespace Eigen;
using namespace Kestrel;
using namespace MathUtil;
using namespace EigenUtil;
using namespace TestUtil;
using namespace Rotations;

class RotationsTest: public CxxTest::TestSuite {
  private:
    Vector3d    wba;
    Matrix3d    Rba;
    Quaterniond qba;
    Vector3d    Tba;

    f64         dt;
    AngleAxisd  dAngle;

  public:
    void setUp() override
    {
        wba = Vector3d(0.1, 0.2, 0.3);
        Tba = Vector3d(0.3, 0.2, 0.1);
        Rba = fromEuler(Tba);
        qba = Quaterniond(Rba);

        dt     = 0.1;
        dAngle = AngleAxisd((dt*wba).norm(), (dt*wba).normalized());
    }

    void tearDown() override {}

    void testEigenQuatIdentity()
    {
        Vector4d q = toVec(quatIdentity<f64>());
        Vector4d qvec = quatVecIdentity<f64>();
        Vector4d exp(1, 0, 0, 0);
        TS_ASSERT(matEquals(q, exp));
        TS_ASSERT(matEquals(qvec, exp));
    }

    void testEigenAngleAxis()
    {
        f64 theta = 0.1;
        Vector3d axis(1.0, 2.0, 3.0);
        axis.normalize();

        AngleAxisd eigRes(theta, axis);
        Matrix3d rodriguezFormula =   cos(theta)*Matrix3d::Identity()
                                    + (1-cos(theta))*axis*axis.transpose()
                                    + sin(theta)*crossMat(axis);
        TS_ASSERT(matEquals(eigRes.matrix(), rodriguezFormula));
    }

    void testEigenCompose()
    {
        f64 theta = 0.1;
        Vector3d axis1(1.0, 2.0, 3.0);
        axis1.normalize();
        Vector3d axis2(3.0, 2.0, 1.0);
        axis2.normalize();

        AngleAxisd aa1(theta, axis1);
        AngleAxisd aa2(theta, axis2);
        Matrix3d    R1(aa1);
        Matrix3d    R2(aa2);
        Quaterniond q1(aa1);
        Quaterniond q2(aa2);
        TS_ASSERT(matEquals(q1.matrix(), R1));
        TS_ASSERT(matEquals(q2.matrix(), R2));

        Matrix3d    mRes(R1 * R2);
        AngleAxisd aaRes(aa1 * aa2);
        Quaterniond qRes(q1 * q2);
        Matrix3d  mixRes(R1 * aa2);
        TS_ASSERT(matEquals( aaRes.matrix(), mRes));
        TS_ASSERT(matEquals(  qRes.matrix(), mRes));
        TS_ASSERT(matEquals(mixRes, mRes));

        // Testing that quaternion multiplication works as expected
        QuatVectorized<f64> q1v(toVec(q1));
        QuatVectorized<f64> q2v(toVec(q2));
        QuatVectorized<f64> qResv(toVec(qRes));

        Matrix4d q1mat = quatCompLmat(q1);
        QuatVectorized<f64> qResMatv1 = q1mat * q2v;
        Matrix4d q2mat = quatCompRmat(q2);
        QuatVectorized<f64> qResMatv2 = q2mat * q1v;

        TS_ASSERT(matEquals(qResMatv1, qResv));
        TS_ASSERT(matEquals(qResMatv2, qResv));
    }

    void testEigenRotate()
    {
        f64 theta = 0.1;
        Vector3d axis(0, 0, 1);

        AngleAxisd aa(theta, axis);
        Quaterniond q(aa);
        Matrix3d    R(aa);

        Vector3d v(1, 0, 0);
        Vector3d vt(cos(theta), sin(theta), 0);

        Vector3d aaRes(aa*v);
        Vector3d  qRes(q*v);
        Vector3d  RRes(R*v);

        TS_ASSERT(matEquals(aaRes, vt));
        TS_ASSERT(matEquals( qRes, vt));
        TS_ASSERT(matEquals( RRes, vt));

        // Testing that quaternion rotation works as expected
        QuatVectorized<f64> qv(toVec(q));
        QuatVectorized<f64> vq(0, v(0), v(1), v(2));
        QuatVectorized<f64> qInvv(toVec(q.inverse()));

        // We expect this operation to work like: q1 * v * q1inv in quaternion multiplication
        Matrix4d vMat    = quatCompRmat(vq);
        Matrix4d qInvMat = quatCompRmat(qInvv);

        QuatVectorized<f64> vtRawAug = qInvMat * vMat * qv;
        Vector3d vtRaw(vtRawAug.block<3, 1>(1, 0));

        TS_ASSERT(matEquals(vtRaw, vt));
    }

    void testEigenAngularDistance()
    {
        Quaterniond q1( 1, 0, 0, 0);
        Quaterniond q2(-1, 0, 0, 0);
        Quaterniond q3( 0, 1, 0, 0);

        // Note: this implies that eigen's angular separation function accounts for quaternion
        //       unwinding (ie comparson between quaternions with positive first term and
        //       negative first term).
        // TODO: test conversion back to angle axis and ensure that angle is always the small angle
        //       (that will be important for controllers)
        TS_ASSERT_DELTA(q1.angularDistance(q2), 0, TINY);
        TS_ASSERT_DELTA(q1.angularDistance(q3), PI, TINY);

        f64 theta = 0.1;
        Vector3d axis1(1.0, 2.0, 3.0);
        axis1.normalize();
        Vector3d axis2(3.0, 2.0, 1.0);
        axis2.normalize();

        // Ensure the separating quaternion has the same angle component as the angularDistance
        AngleAxisd aa1(theta, axis1);
        AngleAxisd aa2(theta, axis2);
        q1 = aa1;
        q2 = aa2;
        Quaterniond qe = q1.inverse() * q2; // q2 = q1 * qe
        TS_ASSERT_DELTA(q1.angularDistance(q2), 2*acos(qe.w()), TINY);
    }

    void testEigenSlerp()
    {
        f64 theta = 0.1;
        Vector3d axis1(1.0, 2.0, 3.0);
        axis1.normalize();
        Vector3d axis2(3.0, 2.0, 1.0);
        axis2.normalize();

        Quaterniond q1(AngleAxisd(theta, axis1));
        Quaterniond q2(AngleAxisd(theta, axis2));

        f64 interp = 0.2;
        Quaterniond qSlerp = q1.slerp(interp, q2);
        TS_ASSERT_LESS_THAN(qSlerp.angularDistance(q1), qSlerp.angularDistance(q2));

        Quaterniond qe = q1.inverse() * q2; // q2 = q1 * qe
        f64 te = 2*acos(qe.w());
        f64 tInterp = interp*te;
        QuatVectorized<f64> qInterp;
        qInterp(0) = cos(tInterp/2);
        qInterp(1) = qe.x() * sin(tInterp/2) / sin(te/2);
        qInterp(2) = qe.y() * sin(tInterp/2) / sin(te/2);
        qInterp(3) = qe.z() * sin(tInterp/2) / sin(te/2);
        TS_ASSERT(matEquals(toVec(qSlerp), toVec(q1 * toQuat(qInterp))));

        Quaterniond q3( 1, 0, 0, 0);
        Quaterniond q4(-1, 0, 0, 0);
        Quaterniond qSlerp2 = q3.slerp(0.5, q4);
        TS_ASSERT_DELTA(qSlerp2.angularDistance(q3), 0, TINY);
        TS_ASSERT_DELTA(qSlerp2.angularDistance(q4), 0, TINY);
    }

    void testEigenFromTwoVectors()
    {
        f64 theta = 0.2;
        f64 phi   = 0.3;

        Vector3d v1(0, 0, 1);
        Vector3d v2(sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta));
        v1 *= 1.337;
        v2 *= 1.337;

        {
            Quaterniond qEigen; qEigen.setFromTwoVectors(v1, v2);

            f64 axisAngle = std::atan2(v2(0), -v2(1));
            f64 rotAngle  = std::asin(v2.block<2,1>(0,0).norm() / v2.norm());

            f64 sAxis = std::sin(axisAngle);
            f64 cAxis = std::cos(axisAngle);
            f64 sRot2 = std::sin(0.5*rotAngle);
            f64 cRot2 = std::cos(0.5*rotAngle);
            QuatVectorized<f64> qTrig(cRot2, cAxis*sRot2, sAxis*sRot2, 0);

            TS_ASSERT(matEquals(toVec(qEigen), qTrig));

            // Ensure we recovered the proper rotation
            TS_ASSERT(matEquals(v2, qEigen*v1));
            TS_ASSERT(matEquals(v2, toQuat(qTrig)*v1));
        }

        // Now test with the addition of an initial yaw
        {
            f64 yaw = 0.4;

            Quaterniond qYaw(AngleAxisd(yaw, Vector3d::UnitZ()));
            Quaterniond qPhi; qPhi.setFromTwoVectors(v1, qYaw.inverse() * v2);
            Quaterniond qEigen = qYaw * qPhi;

            // Note: we can recover a simpler operation by applying the rotations in the opposite
            //       order (has to do with composing quaternions in either local or global frame)
            Quaterniond qPhi2; qPhi2.setFromTwoVectors(v1, v2);
            Quaterniond qEigen2 = qPhi2 * qYaw;

            f64 axisAngle = std::atan2(v2(0), -v2(1)) - yaw;
            f64 rotAngle  = std::asin(v2.block<2,1>(0,0).norm() / v2.norm());

            f64 sYaw2     = std::sin(0.5*yaw);
            f64 cYaw2     = std::cos(0.5*yaw);
            f64 sYaw2Axis = std::sin(0.5*yaw + axisAngle);
            f64 cYaw2Axis = std::cos(0.5*yaw + axisAngle);
            f64 sRot2     = std::sin(0.5*rotAngle);
            f64 cRot2     = std::cos(0.5*rotAngle);

            QuatVectorized<f64> qTrig(cYaw2*cRot2, cYaw2Axis*sRot2, sYaw2Axis*sRot2, sYaw2*cRot2);

            TS_ASSERT(matEquals(toVec(qEigen), toVec(qEigen2)));
            TS_ASSERT(matEquals(toVec(qEigen), qTrig));

            // Ensure we recovered the proper rotation
            TS_ASSERT(matEquals(v2, qEigen*v1));
            TS_ASSERT(matEquals(v2, qEigen2*v1));
            TS_ASSERT(matEquals(v2, toQuat(qTrig)*v1));
        }
    }

    void testEuler()
    {
        f64 r =  0.1;
        f64 p = -0.1;
        f64 y =  0.2;
        Vector3d angles(r, p, y);

        // F_A --yaw-> F_B --pitch-> F_C --roll-> F_D
        Matrix3d R_BA(AngleAxisd(y, Vector3d::UnitZ()));
        Matrix3d R_CB(AngleAxisd(p, Vector3d::UnitY()));
        Matrix3d R_DC(AngleAxisd(r, Vector3d::UnitX()));

        // Rotation matrices compose from left to right (orientation matrices go right to left)
        // O_DA  = O_DC  * O_CB  * O_BA
        // O_DA' = O_BA' * O_CB' * O_DC'
        // R_DA  = R_BA  * R_CB  * R_DC
        Matrix3d R_DA = R_BA * R_CB * R_DC;

        Matrix3d res = fromEuler(r, p, y);
        TS_ASSERT(matEquals(res, R_DA));
        res = fromEuler(angles);
        TS_ASSERT(matEquals(res, R_DA));

        Vector3d angleRes = toEuler(res);
        TS_ASSERT(matEquals(angleRes, angles));
    }

    void testMatrix()
    {
        Matrix3d rot = quatToMat(qba);
        Matrix3d rotV = quatToMat(toVec(qba));

        TS_ASSERT(matEquals(qba.matrix(), Rba));
        TS_ASSERT(matEquals(rot, Rba));
        TS_ASSERT(matEquals(rotV, Rba));
    }

    void testGimbalLock()
    {
        Vector3d angles(0.1, PI/2, 0.2);

        Matrix3d R = fromEuler(angles);
        Vector3d res = toEuler(R);

        Vector3d exp(0.0, PI/2, 0.2 - 0.1);
        TS_ASSERT(matEquals(res, exp));
    }

    void testRmatDeriv()
    {
        Matrix3d RbaDot   = rotDeriv(Rba, wba);
        Matrix3d RbaInteg = Rba + RbaDot * dt;
        Matrix3d RbaDelta = Rba * dAngle;
        Vector3d wbaEst   = angVel(Rba, RbaDot);

        // Note: that the tolerance here is a function of the actual angular velocity and
        //       prediction time. The linearization can only be so accurate.
        TS_ASSERT(matEquals(RbaInteg, RbaDelta, 0.001));
        TS_ASSERT(matEquals(wbaEst, wba));
    }

    void testQuatDeriv()
    {
        Vector4d    qbaDot   = rotDeriv(qba, wba);
        Quaterniond qbaInteg = toQuat(toVec(qba) + qbaDot * dt);
        Quaterniond dq       = Quaterniond(dAngle);
        Quaterniond qbaDelta = qba * dq;
        Vector3d    wbaEst   = angVel(qba, qbaDot);

        // Note: that the tolerance here is a function of the actual angular velocity and
        //       prediction time. The linearization can only be so accurate.
        TS_ASSERT(matEquals(toVec(qbaInteg), toVec(qbaDelta), 0.001));
        TS_ASSERT(matEquals(wbaEst, wba));
    }

    void testEulerDeriv()
    {
        Vector3d TbaDot   = eulerDeriv(Tba, wba);
        Vector3d TbaInteg = Tba + TbaDot * dt;
        Vector3d TbaDelta = toEuler(fromEuler(Tba) * dAngle);
        Vector3d wbaEst   = eulerAngVel(Tba, TbaDot);

        // Note: that the tolerance here is a function of the actual angular velocity and
        //       prediction time. The linearization can only be so accurate.
        TS_ASSERT(matEquals(TbaInteg, TbaDelta, 0.001));
        TS_ASSERT(matEquals(wbaEst, wba));
    }

    void testRmatDotJac()
    {
        Matrix3d RbaDot = rotDeriv(Rba, wba);

        Matrix<f64, 9, 1> RbaVec          = toVec(Rba);
        Matrix<f64, 9, 1> RbaDotVec       = toVec(RbaDot);
        Matrix<f64, 9, 9> RbaDotJacRot    = rotDerivJacRot(Rba, wba);
        Matrix<f64, 9, 3> RbaDotJacAngVel = rotDerivJacAngVel(Rba);

        f64 delta = 0.1;
        for (int i = 0; i < 9; ++i) {
            Matrix<f64, 9, 1> RbaVecDelta = Matrix<f64, 9, 1>::Zero();
            RbaVecDelta(i) = delta;

            // Note: shouldn't normalize the rotation matrix after the delta, because
            //       that would actually change what the delta was, resulting in
            //       a changed jacobian estimate
            Matrix<f64, 9, 1> RbaDotVecDelta = toVec(rotDeriv(toMat(RbaVec + RbaVecDelta), wba));
            Matrix<f64, 9, 1> RbaDotVecDeltaEst = RbaDotVec + RbaDotJacRot * RbaVecDelta;

            TS_ASSERT(matEquals(RbaDotVecDeltaEst, RbaDotVecDelta, 0.001));
        }
        for (int i = 0; i < 3; ++i) {
            Vector3d wbaDelta = Vector3d::Zero();
            wbaDelta(i) = delta;

            Matrix<f64, 9, 1> RbaDotVecDelta = toVec(rotDeriv(Rba, Vector3d(wba + wbaDelta)));
            Matrix<f64, 9, 1> RbaDotVecDeltaEst = RbaDotVec + RbaDotJacAngVel * wbaDelta;

            TS_ASSERT(matEquals(RbaDotVecDeltaEst, RbaDotVecDelta, 0.001));
        }
    }

    void testQuatDotJac()
    {
        Vector4d qbaDot = rotDeriv(qba, wba);

        Vector4d qbaVec = toVec(qba);

        Matrix<f64, 4, 4> qbaDotJacRot    = rotDerivJacRot(qba, wba);
        Matrix<f64, 4, 3> qbaDotJacAngVel = rotDerivJacAngVel(qba);

        f64 delta = 0.1;
        for (int i = 0; i < 4; ++i) {
            Vector4d qbaDelta = Vector4d::Zero();
            qbaDelta(i) = delta;

            // Note: shouldn't normalize the quaternion after the delta, because
            //       that would actually change what the delta was, resulting in
            //       a changed jacobian estimate
            Vector4d qbaDotDelta = rotDeriv(toQuat(qbaVec + qbaDelta), wba);
            Vector4d qbaDotDeltaEst = qbaDot + qbaDotJacRot * qbaDelta;

            TS_ASSERT(matEquals(qbaDotDeltaEst, qbaDotDelta, 0.001));
        }
        for (int i = 0; i < 3; ++i) {
            Vector3d wbaDelta = Vector3d::Zero();
            wbaDelta(i) = delta;

            Vector4d qbaDotDelta = rotDeriv(qba, Vector3d(wba + wbaDelta));
            Vector4d qbaDotDeltaEst = qbaDot + qbaDotJacAngVel * wbaDelta;

            TS_ASSERT(matEquals(qbaDotDeltaEst, qbaDotDelta, 0.001));
        }
    }

    void testEulerDotJac()
    {
        Vector3d TbaDot = eulerDeriv(Tba, wba);

        Matrix3d TbaDotJacRot    = eulerDerivJacEuler(Tba, wba);
        Matrix3d TbaDotJacAngVel = eulerDerivJacAngVel(Tba);

        f64 delta = 0.1;
        for (int i = 0; i < 3; ++i) {
            Vector3d TbaDelta = Vector3d::Zero();
            TbaDelta(i) = delta;

            Vector3d TbaDotDelta = eulerDeriv(Vector3d(Tba + TbaDelta), wba);
            Vector3d TbaDotDeltaEst = TbaDot + TbaDotJacRot * TbaDelta;

            // Note: had to use a slightly wider tolerance here because this jacobian is
            //       very nonlinear
            TS_ASSERT(matEquals(TbaDotDeltaEst, TbaDotDelta, 0.003));
        }
        for (int i = 0; i < 3; ++i) {
            Vector3d wbaDelta = Vector3d::Zero();
            wbaDelta(i) = delta;

            Vector3d TbaDotDelta = eulerDeriv(Tba, Vector3d(wba + wbaDelta));
            Vector3d TbaDotDeltaEst = TbaDot + TbaDotJacAngVel * wbaDelta;

            TS_ASSERT(matEquals(TbaDotDeltaEst, TbaDotDelta, 0.001));
        }
    }

    void testRmatSvdNormalize()
    {
        Matrix3d normalizedRba = svdNormalized(Rba);
        TS_ASSERT(matEquals(normalizedRba, Rba)); // no change in a well formed rotaiton mat

        f64 dt = 0.1;
        // this will cause Rba2 to not be perfectly orthogonal
        Matrix3d Rba2 = Rba + rotDeriv(Rba, wba) * dt;

        TS_ASSERT(!matEquals(Rba2 * Rba2.transpose(), Matrix3d::Identity(), TINY, false));

        svdNormalize(Rba2);
        TS_ASSERT(matEquals(Rba2 * Rba2.transpose(), Matrix3d::Identity()));

        // Try to do the integration much smoother and ensure the result is similar
        int steps = 50;
        Matrix3d Rba3 = Rba;
        for (int i = 0; i < steps; ++i) {
            Rba3 = Rba3 + rotDeriv(Rba3, wba) * (dt / steps);
        }

        // Even infinitely smooth integration will differ slightly from the re-normalized
        // answer, but we have to settle for "pretty" accurate
        TS_ASSERT(matEquals(Rba2, Rba3, 100*TINY));
    }

    void testQuatPositive()
    {
        // we don't actually care if these are normalized here
        Quaterniond     q(-1,  1,  1,  1);
        Vector4d       qv(-1,  1,  1,  1);
        Quaterniond  qExp( 1, -1, -1, -1);
        Vector4d    qvExp( 1, -1, -1, -1);

        Quaterniond  qRes = quatPositive(q);
        Vector4d    qvRes = quatPositive(qv);

        TS_ASSERT(matEquals(toVec(qRes), toVec(qExp)));
        TS_ASSERT(matEquals(qvRes, qvExp));

        TS_ASSERT_EQUALS(q.w(), -1);
        TS_ASSERT_EQUALS(qv(0), -1);

        quatToPositive(q);
        quatToPositive(qv);

        TS_ASSERT(matEquals(toVec(q), toVec(qExp)));
        TS_ASSERT(matEquals(qv, qvExp));
    }

    // XXX: these need to move to somewhere else
    //void testQuatCorrectionSphereCov()
    //{
        //Control::KalmanFilter<f64, 4, 3, 3> kf;
//
        //Vector4d    estVec = unifNoise(Vector4d(-1, -1, -1, -1), Vector4d(1, 1, 1, 1));
        //Quaterniond est = toQuat(estVec).normalized();
        //quatToPositive(est);
        //Matrix3d    estNoise = 0.25 * (0.1 * 0.1) * Matrix3d::Identity();
        //Matrix4d    estCov =   Rotations::obsCov(est, estNoise)
                             //+ 0.25 * (0.001 * 0.001) * Matrix4d::Identity();
        //Matrix4d    obsJac = Matrix4d::Identity();
//
        //Matrix3d    obsNoise = 0.25 * (0.05 * 0.05) * Matrix3d::Identity();
        //Vector3d    rotVec = gausNoise(estNoise, true);
        //AngleAxisd  dAngle(rotVec.norm(), rotVec.normalized());
        //Quaterniond obs(est*dAngle);
        //quatToPositive(obs);
        //Matrix4d    obsCov = Rotations::obsCov(est, obsNoise);
//
        //decltype(kf)::StateVecCov kfRes = kf.correct(toVec(est), estCov, toVec(obs), obsCov,
                                                   //toVec(est), obsJac);
        //Quaterniond res = toQuat(kfRes.first).normalized();
//
        //f64 estToObs = est.angularDistance(obs);
        //f64 estToRes = est.angularDistance(res);
        //f64 resToObs = obs.angularDistance(res);
//
        //// correction should move in the correct direction
        //TS_ASSERT_DELTA(estToObs, estToRes + resToObs, 0.001);
        //// correction weight should be approxmately the same as the covariance weights
        //TS_ASSERT_DELTA(estToRes / resToObs, estNoise(0,0) / obsNoise(0,0), 0.1);
    //}
//
    //void testQuatCorrectionEllipCov()
    //{
        //Control::KalmanFilter<f64, 4, 3, 3> kf;
//
        //// Start rotated about the x axis
        //Quaterniond est(cos(0.5*PI/3), sin(0.5*PI/3), 0, 0);
        //quatToPositive(est);
        //Matrix3d    estNoise = 0.25 * (0.1 * 0.1) * Matrix3d::Identity();
        //Matrix4d    estCov =   Rotations::obsCov(est, estNoise)
                             //+ 0.25 * (0.001 * 0.001) * Matrix4d::Identity();
        //Matrix4d    obsJac = Matrix4d::Identity();
//
        //// bad noise about z, small noise about xy
        //Matrix3d    obsNoise = 0.25 * (0.05 * 0.05) * Matrix3d::Identity();
        //obsNoise(2,2)        = 0.25 * (0.3 * 0.3); // very uncertain about z axis
        //Quaterniond zRot(cos(0.5*0.1), 0, 0, sin(0.5*0.1));
        //Quaterniond xyRot(cos(0.5*0.02), 0.5*sin(0.5*0.02), 0.5*sin(0.5*0.02), 0);
        //Quaterniond obs(zRot * est * xyRot);
        //quatToPositive(obs);
        //Quaterniond obsxy(est * xyRot);
        //quatToPositive(obsxy);
        //Matrix4d    obsCov = Rotations::obsCov(est, obsNoise);
//
        //decltype(kf)::StateVecCov kfRes = kf.correct(toVec(est), estCov, toVec(obs), obsCov,
                                                   //toVec(est), obsJac);
        //Quaterniond res = toQuat(kfRes.first).normalized();
//
        ////f64 estToObs   = est.angularDistance(obs);
        ////f64 estToRes   = est.angularDistance(res);
        //f64 resToObs   = obs.angularDistance(res);
        //f64 resToObsxy = obsxy.angularDistance(res);
//
        ////cout << endl << toVec(est)   << endl
             ////<< endl << toVec(obs)   << endl
             ////<< endl << toVec(obsxy) << endl
             ////<< endl << toVec(res)   << endl
             ////<< endl << "est -> obs:   " << estToObs
             ////<< endl << "est -> res:   " << estToRes
             ////<< endl << "res -> obs:   " << resToObs
             ////<< endl << "res -> obsXY: " << resToObsxy
             ////<< endl << "est -> res + res ->  obs: " << estToRes + resToObs
             ////<< endl << "est -> res / res ->  obs: " << estToRes / resToObs
             ////<< endl;
//
        //// correction should be a lot closer to Obsxy than the obs
        //TS_ASSERT_LESS_THAN(resToObsxy, resToObs);
        //// correction of the z term should be minimal
        //TS_ASSERT_DELTA(res.z(), 0, 0.01);
    //}

    // Not implemented right now because it would be pedantic, but could do it on a rainy day in
    // the future
    //void testRmatCorrectionSphereCov() { }
    //void testQuatCorrectionEllipCov() { }

    void testVectorizedOps()
    {
        Matrix<f64, 3, 3> RbaDot          = rotDeriv(Rba, wba);
        Matrix<f64, 3, 1> RbaWba          = angVel(Rba, RbaDot);
        Matrix<f64, 9, 9> RbaDotJacRot    = rotDerivJacRot(Rba, wba);
        Matrix<f64, 9, 3> RbaDotJacAngVel = rotDerivJacAngVel(Rba);

        Matrix<f64, 9, 1> vecRbaDot          = rmatRotDeriv(toVec(Rba), wba);
        Matrix<f64, 3, 1> vecRbaWba          = rmatAngVel(toVec(Rba), vecRbaDot);
        Matrix<f64, 9, 9> vecRbaDotJacRot    = rmatRotDerivJacRot(toVec(Rba), wba);
        Matrix<f64, 9, 3> vecRbaDotJacAngVel = rmatRotDerivJacAngVel(toVec(Rba));

        TS_ASSERT(matEquals(vecRbaDot,          toVec(RbaDot)));
        TS_ASSERT(matEquals(vecRbaWba,          RbaWba));
        TS_ASSERT(matEquals(vecRbaDotJacRot,    RbaDotJacRot));
        TS_ASSERT(matEquals(vecRbaDotJacAngVel, RbaDotJacAngVel));

        Matrix<f64, 4, 1> qbaDot          = rotDeriv(qba, wba);
        Matrix<f64, 3, 1> qbaWba          = angVel(qba, qbaDot);
        Matrix<f64, 4, 4> qbaDotJacRot    = rotDerivJacRot(qba, wba);
        Matrix<f64, 4, 3> qbaDotJacAngVel = rotDerivJacAngVel(qba);

        Matrix<f64, 4, 1> vecQbaDot          = quatRotDeriv(toVec(qba), wba);
        Matrix<f64, 3, 1> vecQbaWba          = quatAngVel(toVec(qba), vecQbaDot);
        Matrix<f64, 4, 4> vecQbaDotJacRot    = quatRotDerivJacRot(toVec(qba), wba);
        Matrix<f64, 4, 3> vecQbaDotJacAngVel = quatRotDerivJacAngVel(toVec(qba));

        TS_ASSERT(matEquals(vecQbaDot,          qbaDot));
        TS_ASSERT(matEquals(vecQbaWba,          qbaWba));
        TS_ASSERT(matEquals(vecQbaDotJacRot,    qbaDotJacRot));
        TS_ASSERT(matEquals(vecQbaDotJacAngVel, qbaDotJacAngVel));
    }

    // If uncommented, the following test should be expected to throw static asserts
    //void testStaticAssertSize()
    //{
        //Matrix<f64, 8, 1> wrongSize = Matrix<f64, 8, 1>::Zero();
        //Matrix3d shouldAssert = toMat(wrongSize);
        //TS_ASSERT_EQUALS(wrongSize.size(), shouldAssert.size());
        //Quaterniond willAssert = toQuat(wrongSize);
        //TS_ASSERT_EQUALS(wrongSize.norm(), willAssert.norm());
    //}
};
