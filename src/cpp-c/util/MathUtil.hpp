#pragma once

#include <cmath>
#include <utility>
#include <iostream>
#include <vector>
#include <random>
#include <Eigen/Dense>

#include "kestrel/util/types.hpp"
#include "kestrel/util/assert.hpp"

#ifndef M_PI
#define M_PI 3.14159265359
#endif

namespace Kestrel {
namespace MathUtil {

template <int N> using nVec = Eigen::Matrix<f64, N, 1>;
template <int N> using nMat = Eigen::Matrix<f64, N, N>;

struct NormDist {
    f64 mean;
    f64 var;
    sz  n;

    inline void shift(f64 val) { mean += val; }

    inline void scale(f64 val) { mean *= val; var *= val * val; }

    inline void print(std::ostream& os = std::cout, char term = '\0', bool stddev = false) const
    {
        os << n << " samples in N{" << mean << ", " << var;
        if (stddev) os << " = (" << std::sqrt(var) << ")^2";
        os << "}" << term;
    }

    friend inline std::ostream& operator<<(std::ostream& os, const NormDist& nd)
    {
        nd.print(os, '\0', true);
        return os;
    }
};

template <int N>
struct MultiNormDist {
    nVec<N> mean;
    nMat<N> cov;
    sz      n;
};

constexpr f64 PI    = M_PI;
constexpr f64 TWOPI = 2*PI;
constexpr f64 TINY  = 1e-6;
constexpr f64 TINY2 = 1e-12;

// TODO: change these to arbitrary types
/// Compile time evaluated max, access via Max<a, b>::val
template<int n1, int n2> struct Max {
    enum { val = n1 > n2 ? n1 : n2 };
};
/// Compile time evaluated min, access via Min<a, b>::val
template<int n1, int n2> struct Min {
    enum { val = n1 > n2 ? n2 : n1 };
};

static inline f64 toRad(f64 theta)
{
    return theta * (PI / 180.0);
}

static inline f64 toDeg(f64 theta)
{
    return theta * (180.0 / PI);
}

/// This is an efficient way to bound theta provided that theta is not allowed to grow massively
/// outside the bounds of (-PI, PI]
static inline f64 radToPiPi(f64 theta)
{
    while (theta <= -PI) theta += TWOPI;
    while (theta >   PI) theta -= TWOPI;
    return theta;
}

/// This is an efficient way to condition the angle b so that b-a in (-PI, PI]
/// Note that this does not give you the actual difference b-a.
/// Both a and b must already be in (-PI, PI]
static inline f64 radDiffToPiPi(f64 a, f64 b)
{
    if      (b-a <= -PI) return b + TWOPI;
    else if (b-a >   PI) return b - TWOPI;
    else                 return b;
}

// TODO: consider changing the noise generators to take eigen base classes
// XXX: noise generators cause race conditions if you use them in threads. Probably better
//      to shift them to classes that you instantiate individually in threads where you'd
//      use them.

static inline f64 unifNoise(f64 min, f64 max)
{
    // C++11 RNG library is very new to me and I'm not sure if I'm doing this optimally
    static std::random_device rd;
    static std::minstd_rand gen(rd());
    static std::uniform_real_distribution<f64> dist(0.0, 1.0);

    return dist(gen) * (max - min) + min;
}

/// Generates a uniform random vector with values between the provided minima and maxima
template <int N_DIM>
static inline nVec<N_DIM> unifNoise(const nVec<N_DIM>& min, const nVec<N_DIM>& max)
{
    // C++11 RNG library is very new to me and I'm not sure if I'm doing this optimally
    static std::random_device rd;
    static std::minstd_rand gen(rd());
    static std::uniform_real_distribution<f64> dist(0.0, 1.0);

    nVec<N_DIM> w;
    for (int i = 0; i < N_DIM; ++i) {
        w(i) = dist(gen);
    }

    w = w.array() * (max - min).array() + min.array();

    return w;
}

static inline f64 gausNoise(f64 mean, f64 var)
{
    // C++11 RNG library is very new to me and I'm not sure if I'm doing this optimally
    static std::random_device rd;
    static std::minstd_rand gen(rd());
    static std::normal_distribution<f64> dist(0.0, 1.0);

    return std::sqrt(var) * dist(gen) + mean;
}

/// Generates a Gaussian random vector with mean 0 and input covariance. Can support any
/// symmetric, positive semi-definite covariance. If diag is set to true, assumes a diagonal
/// covariance (faster).
// TODO: not sure if noise generated from this actually satisfies the noise model of
//       a continuous Kalman filter. Look into the noise model assumptions made by the
//       filter and ensure this is valid (I remember reading that the noise was assumed
//       to be Brownian somewhere).
template <int N_DIM>
static inline nVec<N_DIM> gausNoise(const nMat<N_DIM>& Cov, bool diag = false)
{
    // C++11 RNG library is very new to me and I'm not sure if I'm doing this optimally
    static std::random_device rd;
    static std::minstd_rand gen(rd());
    static std::normal_distribution<f64> dist(0.0, 1.0);

    nVec<N_DIM> w;
    for (int i = 0; i < N_DIM; ++i) {
        w(i) = dist(gen);
    }

    if (diag) {
        for (int i = 0; i < N_DIM; ++i) {
            Assert(Cov(i,i) >= 0, "Cannot have a negative covariance");
            // Noise is distributed on N[0, sigma] where Q(i,i) = sigma^2
            w(i) *= sqrt(Cov(i,i));
        }
    } else {
        // Use Cholesky Decomp to produce noise as shown in:
        // https://en.wikipedia.org/wiki/Cholesky_decomposition#Monte_Carlo_simulation
        Eigen::LLT<nMat<N_DIM>> lltOfCov(Cov);
        Eigen::ComputationInfo i = lltOfCov.info();
        Assert(i == Eigen::ComputationInfo::Success, "Cannot convert LLT decomp of Covariance");
        w = lltOfCov.matrixL() * w;
    }

    return w;
}

/// Computes the mean and variance in one pass, unstable for very large sets or
/// sets with large means
static inline NormDist normDistQuick(const std::vector<f64>& set)
{
    NormDist ret{0, 0, set.size()};
    if (set.empty()) return ret;

    f64 sum    = 0;
    f64 sq_sum = 0;
    for (sz i = 0; i < ret.n; ++i) {
       sum    += set[i];
       sq_sum += set[i] * set[i];
    }
    ret.mean = sum / ret.n;
    // Apply Bessel's Correction: https://en.wikipedia.org/wiki/Bessel%27s_correction
    ret.var  = ret.n / (ret.n-1.0) * (sq_sum / ret.n - ret.mean * ret.mean);
    return ret;;
}

/// Computes the mean and variance in two passes, stable except for very large sets
static inline NormDist normDist(const std::vector<f64>& set)
{
    NormDist ret{0, 0, set.size()};
    if (set.empty()) return ret;

    for (sz i = 0; i < ret.n; ++i) ret.mean += set[i];
    ret.mean /= ret.n;

    for (sz i = 0; i < ret.n; ++i) ret.var += (set[i] - ret.mean) * (set[i] - ret.mean);
    ret.var  /= ret.n-1;

    return ret;
}

/// Updates a normal distribution given one more observation, resultant dist only valid if n >= 2
/// It is still valid to create the dist iteratively as long as all values of the initial (invalid)
/// distribution are initialized to 0
static inline NormDist normDist(const NormDist& dist, f64 obs)
{
    // Based on http://www.johndcook.com/blog/standard_deviation/
    NormDist ret;
    ret.n    = dist.n + 1;
    ret.mean = dist.mean + (obs - dist.mean) / ret.n;
    ret.var  = (ret.n == 1) ? 0
                            : (dist.var * (dist.n - 1) + (obs - dist.mean) * (obs - ret.mean))
                              / (ret.n - 1);
    return ret;
}

/// Computes the mean and variance in two passes, stable except for very large sets
template <int N>
static inline MultiNormDist<N> normDist(const std::vector<nVec<N>>& set)
{
    MultiNormDist<N> ret{nVec<N>::Zero(), nMat<N>::Identity(), set.size()};
    if (set.empty()) return ret;

    for (sz i = 0; i < ret.n; ++i) ret.mean += set[i];
    ret.mean /= ret.n;

    for (sz i = 0; i < ret.n; ++i) ret.cov += (set[i] - ret.mean) * (set[i] - ret.mean).transpose();
    ret.cov  /= ret.n-1;

    return ret;
}

} /* MathUtil */
} /* Kestrel */
