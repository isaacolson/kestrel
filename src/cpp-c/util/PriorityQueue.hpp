#include <queue>
#include <vector>
#include <functional>

namespace Kestrel {

template<class T, class Compare = std::less<T>>
class PriorityQueue : public std::priority_queue<T, std::vector<T>, Compare>
{
  public:
    PriorityQueue() : std::priority_queue<T, std::vector<T>, Compare>() {}

    PriorityQueue(const std::vector<T>& c) :
        std::priority_queue<T, std::vector<T>, Compare>(Compare(), c) {}

    void reheapify()
    {
        std::make_heap(this->c.begin(), this->c.end(), this->comp);
    }

    std::vector<T> clear()
    {
        std::vector<T> ret(this->c.begin(), this->c.end());
        this->c.clear();
        return ret;
    }
};

} /* Kestrel */

