#pragma once

// Note: assuming a linux system
#include <iostream>
#include <iomanip>
#include <vector>
#include <utility>
#include <sys/time.h>
#include <unistd.h>

#include "kestrel/util/types.hpp"
#include "kestrel/util/assert.hpp"
#include "kestrel/util/MathUtil.hpp"

// TODO: probably worth switching this over to use <chrono> and <thread> as a more
//       standard and c++11 like interface

namespace Kestrel {
namespace TimeUtil {

static inline u64 utime()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    u64 tmp = (u64)tv.tv_sec * 1000000 + tv.tv_usec;
    Assert(tmp < 2e16, "unreasonable utime");
    return (u64)tv.tv_sec * 1000000 + tv.tv_usec;
}

// Deterministic timestamp, value can be overridden for faster than realtime operation
u64  stamp();
// Override the value of stamp()
void overrideStamp(u64 stamp);
// Reset stamp to be the utime
void resetStamp();

static inline void msleep(int ms)
{
    ::usleep(1000 * ms);
}

static inline void usleep(int us)
{
    ::usleep(us);
}

static inline f64 toSec(u64 us)
{
    return (f64)us/1000000;
}

static inline u64 toUsec(f64 s)
{
    return (u64)s*1000000;
}

/// All times are given in microseconds
class Timer {
  private:
    bool enabled;
    u64 begin;
    u64 accum;

  public:
    /// Ctor
    Timer(bool enabled=true) : enabled(enabled), accum(0) { if (enabled) begin = utime(); }
    /// Dtor
    virtual ~Timer() {}

    virtual inline void start()
    {
        if (!enabled) {
            enabled = true;
            begin = utime();
        }
    }

    virtual inline u64 toc()
    {
        if (enabled) {
            u64 now  = utime();
            u64 diff = now - begin;
            begin    = now;
            accum   += diff;
            return diff;
        } else {
            return 0;
        }
    }

    virtual inline u64 stop()
    {
        u64 ret = toc();
        enabled = false;
        return ret;
    }

    virtual inline u64 total() const
    {
        return accum;
    }

    inline bool running() const { return enabled; }
    inline f64  tocSec()        { return toSec(toc()); }
    inline f64  stopSec()       { return toSec(stop()); }
    inline f64  totalSec()      { return toSec(total()); }

    /// Swap
    inline void swap(Timer& o)
    { std::swap(enabled, o.enabled); std::swap(begin, o.begin); std::swap(accum, o.accum); }
    /// Copy Ctor
    Timer(const Timer& o) : enabled(o.enabled), begin(o.begin), accum(o.accum) {}
    /// Move Ctor
    Timer(Timer&& o) { swap(o); }
    /// Unifying Assignment
    Timer& operator=(Timer o) { swap(o); return *this; }
};

class StatTimer: public Timer {
  private:
    f64                x{0};
    MathUtil::NormDist dist{0, 0, 0};
    // TODO: consider adding an option to make this record samples in case the user
    //       wants to do more complex statistics on it.

  public:
    /// Ctor
    StatTimer(bool enabled=true) : Timer(enabled) {}
    /// Dtor
    virtual ~StatTimer() {}

    virtual inline u64 toc()
    {
        if (running()) {
            u64 ret = Timer::toc();
            x = toSec(ret);
            dist = MathUtil::normDist(dist, x);
            return ret;
        } else {
            return 0;
        }
    }

    inline u64 stopNoRecord()
    {
        // XXX: this isn't necessarily the most optimal way to do this, but it does comply
        //      with the Timer API. Perhaps we want to move some Timer things to protected
        //      and make this more efficient.
        f64                tmpX    = x;
        MathUtil::NormDist tmpDist = dist;
        u64 ret = stop();
        x    = tmpX;
        dist = tmpDist;
        return ret;
    }

    inline f64 lastSample() const { return x; }
    inline MathUtil::NormDist stats() const { return dist; }

    inline void resetStats() { dist = MathUtil::NormDist{0, 0, 0}; }

    inline void printStats(std::ostream& os = std::cout, char term = '\n',
                           bool printLastSample = true) const
    {
        if (printLastSample) os << std::setw(8) << x << "s   ";
        dist.print(os, term, true);
    }

    friend inline std::ostream& operator<<(std::ostream& os, const StatTimer& timer)
    {
        timer.printStats(os, '\0', true);
        return os;
    }

    /// Swap
    void swap(StatTimer& o) { Timer::swap(o); std::swap(x, o.x); std::swap(dist, o.dist); }
    /// Copy Ctor
    StatTimer(const StatTimer& o) : Timer(o), x(o.x), dist(o.dist) {}
    /// Move Ctor
    StatTimer(StatTimer&& o) { swap(o); }
    /// Unifying Assignment
    StatTimer& operator=(StatTimer o) { swap(o); return *this; }
};

} /* TimeUtil */
} /* Kestrel */
