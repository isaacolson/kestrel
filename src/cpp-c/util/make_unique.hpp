#pragma once

#include <memory>

// make_unique: should be part of C++11 by default, exclusion was basically oversight
namespace std
{

// XXX: is this going to be a problem when Kestrel becomes submoduled if the parent
//      repo also implements make_unique?
template <typename T, typename... Args>
static std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

} /* std */
