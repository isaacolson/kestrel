#pragma once

#include <iostream>
#include <string>

namespace example {
    void greeting(std::ostream& os, const std::string& target);
    void farewell(std::ostream& os, const std::string& target);
    int addition(int a, int b);
}
