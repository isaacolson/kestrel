#include "greet.h"
#include "greet.hpp"

#include <cstdio>
#include <iostream>

using namespace std;

int main(int argc, const char *argv[])
{
    greeting(stdout, "World");
    farewell(stdout, "World");

    example::greeting(cout, "World");
    example::farewell(cout, "World");

    return 0;
}
