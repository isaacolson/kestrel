#pragma once

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

void greeting(FILE* fd, const char* target);
void farewell(FILE* fd, const char* target);
int addition(int a, int b);

#ifdef __cplusplus
}
#endif
