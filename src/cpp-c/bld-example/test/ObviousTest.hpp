#pragma once

#include "greet.h"
#include "greet.hpp"

#include <cxxtest/TestSuite.h>

class ObviousTest: public CxxTest::TestSuite {
  private:
    /* data */

  public:
    void setUp() override { /* Fixture Init */ }
    void tearDown() override {}

    void test2p2()
    {
        TS_ASSERT_EQUALS(2 + 2, 4);
    }

    void testAdding()
    {
        TS_ASSERT_EQUALS(addition(2, 2), 4);
        TS_ASSERT_EQUALS(example::addition(2, 2), 4);
    }
};
