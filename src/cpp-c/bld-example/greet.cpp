#include "greet.hpp"

#include <iostream>
#include <string>

void example::greeting(std::ostream& os, const std::string& target)
{
    os << "Hello from cpp, " << target << std::endl;
}

void example::farewell(std::ostream& os, const std::string& target)
{
    os << "Goodbye from cpp, " << target << std::endl;
}

int example::addition(int a, int b)
{
    return a + b;
}
