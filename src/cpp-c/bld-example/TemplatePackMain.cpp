#include <utility>



template <typename T, typename R, typename... Args1, typename... Args2>
R call(R (T::*fn)(Args1...), T *t, Args2&&... args) {
    return ((*t).*fn)(std::forward<Args2>(args)...);
}

class Calculator {
    public:
    int add(const int& a, const int& b) {
        return a + b;
    }
};

int main() {
    Calculator *calculator = new Calculator();
    int* a = new int(2);
    int* b = new int(4);

    // compiles
    calculator->add(*a, *b);

    // does not compile!
    call<Calculator>(&Calculator::add,calculator, *a, *b);

    delete calculator;
    delete a;
    delete b;

    return 0;
}
