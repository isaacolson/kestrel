#include "greet.h"

#include <stdio.h>

void greeting(FILE* fd, const char* target)
{
    fprintf(fd, "Hello from c, %s\n", target);
}

void farewell(FILE* fd, const char* target)
{
    fprintf(fd, "Goodbye from c, %s\n", target);
}

int addition(int a, int b)
{
    return a + b;
}
