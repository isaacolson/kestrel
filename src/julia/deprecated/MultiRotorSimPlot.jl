function plotMultiRotorSim(filepath::AbstractString, fignum::Int64 = 1)
    data = loadCsv(filepath);

    figure(fignum);
    clf();

    ax = [subplot(2, 1, 1)];
    shareId = [length(ax)];
    title("MultiRotor Simulation SMC: Z Control");
    plotVals(data["SMC"]["t"], data["SMC"]["zd"], [ "--k"], ["zd"]);
    plotVals(data["SYS"]["t"], data["SYS"]["x"][:,3], ["-k"], ["z"]);
    plotVals(data["SYS"]["t"], data["SYS"]["x"][:,6], ["-r"], ["zdot"]);
    plotVals(data["SMC"]["t"], data["SMC"]["zdFilt"], [ "-.k"], ["zdFilt"]);
    plotVals(data["SMC"]["t"], data["SMC"]["zddotFilt"], [ "-.r"], ["zddotFilt"]);
    xlabel("Time (s)");
    ylabel("Z (m), Zdot (m/s)");
    legend(loc=1);

    # #=
    ax = [ax subplot(2, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["SMC"]["t"], data["SMC"]["zd"], [ "--k"], ["zd"]);
    plotVals(data["SMC"]["t"], data["SMC"]["zdFilt"], [ "-k"], ["zdFilt"]);
    plotVals(data["SMC"]["t"], data["SMC"]["zddotFilt"], [ "-.k"], ["zddotFilt"]);
    #plotVals(data["SMC"]["t"], data["SMC"]["zddot2Filt"], [ ":k"], ["zddot2Filt"]);
    xlabel("Time (s)");
    ylabel("Z Setpt Filter");
    legend(loc=1);
    # =#



    figure(fignum+1);
    clf();

    ax = [ax subplot(2, 1, 1, sharex=ax[shareId[1]])];
    title("MultiRotor Simulation SMC: XY Control");
    plotVals(data["SMC"]["t"], data["SMC"]["pd"], ["--k", "--b"], ["xd", "yd"]);
    plotVals(data["SYS"]["t"], data["SYS"]["x"][:,1:2], ["-k", "-b"], ["x", "y"]);
    plotVals(data["SYS"]["t"], data["SYS"]["x"][:,4:5], ["-r", "-g"], ["xdot", "ydot"]);
    plotVals(data["SMC"]["t"], data["SMC"]["pdFilt"], ["-.k", "-.b"], ["xdFilt", "ydFilt"]);
    plotVals(data["SMC"]["t"], data["SMC"]["pddotFilt"], ["-.r", "-.g"],
              ["xddotFilt", "yddotFilt"]);
    xlabel("Time (s)");
    ylabel("XY (m), XYdot (m/s)");
    legend(loc=1);

    # #=
    ax = [ax subplot(2, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["SMC"]["t"], data["SMC"]["pd"], ["--k", "--b"], ["xd", "yd"]);
    plotVals(data["SMC"]["t"], data["SMC"]["pdFilt"], ["-k", "-b"], ["xdFilt", "ydFilt"]);
    plotVals(data["SMC"]["t"], data["SMC"]["pddotFilt"], ["-.k", "-.b"],
              ["xddotFilt", "yddotFilt"]);
    #plotVals(data["SMC"]["t"], data["SMC"]["pddot2Filt"], [":k", ":b"],
              #["xddot2Filt", "yddot2Filt"]);
    xlabel("Time (s)");
    ylabel("XY Setpt Filter");
    legend(loc=1);

    # =#



    figure(fignum+2);
    clf();

    ax = [ax subplot(2, 1, 1, sharex=ax[shareId[1]])];
    title("MultiRotor Simulation SMC: Quaternion Control");
    plotVals(data["SMC"]["t"], data["SMC"]["qd"], [ "--k", "--r", "--g", "--b"],
        ["qtd", "qxd", "qyd", "qzd"]);
    plotVals(data["SYS"]["t"], data["SYS"]["x"][:,7:10], ["-k", "-r", "-g", "-b"],
                                                         ["qt", "qx", "qy", "qz"]);
    plotVals(data["SMC"]["t"], data["SMC"]["qdFilt"], [ "-.k", "-.r", "-.g", "-.b"],
        ["qtdFilt", "qxdFilt", "qydFilt", "qzdFilt"]);
    xlabel("Time (s)");
    ylabel("Quaternion");
    legend(loc=1);

    # #=
    ax = [ax subplot(2, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["SMC"]["t"], data["SMC"]["qd"], [ "--k", "--r", "--g", "--b"],
        ["qtd", "qxd", "qyd", "qzd"]);
    plotVals(data["SMC"]["t"], data["SMC"]["qdFilt"], [ "-k", "-r", "-g", "-b"],
        ["qtdFilt", "qxdFilt", "qydFilt", "qzdFilt"]);
    plotVals(data["SMC"]["t"], data["SMC"]["qddotFilt"], [ "-.k", "-.r", "-.g", "-.b"],
        ["qtddotFilt", "qxddotFilt", "qyddotFilt", "qzddotFilt"]);
    #plotVals(data["SMC"]["t"], data["SMC"]["qddot2Filt"], [ ":k", ":r", ":g", ":b"],
        #["qtddot2Filt", "qxddot2Filt", "qyddot2Filt", "qzddot2Filt"]);
    xlabel("Time (s)");
    ylabel("Quaternion Setpt Filter");
    legend(loc=1);
    # =#



    figure(fignum+3);
    clf();

    ax = [ax subplot(2, 1, 1, sharex=ax[shareId[1]])];
    title("MultiRotor Simulation SMC: Angular Vel Control");
    plotVals(data["SMC"]["t"], data["SMC"]["wd"], [ "--r", "--g", "--b"],
        ["wxd", "wyd", "wzd"]);
    plotVals(data["SYS"]["t"], data["SYS"]["x"][:,11:13], [ "-r", "-g", "-b"], ["wx", "wy", "wz"]);
    plotVals(data["SMC"]["t"], data["SMC"]["wdFilt"], [ "-.r", "-.g", "-.b"],
        ["wxdFilt", "wydFilt", "wzdFilt"]);
    xlabel("Time (s)");
    ylabel("Angular Vel (rad/s)");
    legend(loc=1);

    # #=
    ax = [ax subplot(2, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["SMC"]["t"], data["SMC"]["wd"], [ "--r", "--g", "--b"],
        ["wxd", "wyd", "wzd"]);
    plotVals(data["SMC"]["t"], data["SMC"]["wdFilt"], [ "-r", "-g", "-b"],
        ["wxdFilt", "wydFilt", "wzdFilt"]);
    plotVals(data["SMC"]["t"], data["SMC"]["wddotFilt"], [ "-.r", "-.g", "-.b"],
        ["wxddotFilt", "wyddotFilt", "wzddotFilt"]);
    #plotVals(data["SMC"]["t"], data["SMC"]["wddot2Filt"], [ ":r", ":g", ":b"],
        #["wxddot2Filt", "wyddot2Filt", "wzddot2Filt"]);
    xlabel("Time (s)");
    ylabel("Angular Vel Setpt Filter");
    legend(loc=1);
    # =#


    figure(fignum+4);
    clf();

    ax = [ax subplot(2, 1, 1, sharex=ax[shareId[1]])];
    title("MultiRotor Simulation SMC: Inputs");
    plotVals(data["SYS"]["t"], data["SYS"]["u"][:,1], ["-k"], ["Lift"]);
    plotVals(data["SYS"]["t"], data["SYS"]["w"][:,3], [":k"], ["Fz Disturbance"]);
    xlabel("Time (s)");
    ylabel("Lift (N)");
    legend(loc=1);

    ax = [ax subplot(2, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["SYS"]["t"], data["SYS"]["u"][:,2:4], ["-r", "-g", "-b"], ["Mx", "My", "Mz"]);
    plotVals(data["SYS"]["t"], data["SYS"]["w"][:,4:6], [":r", ":g", ":b"],
        ["Mx Disturbance", "My Disturbance", "Mz Disturbance"]);
    xlabel("Time (s)");
    ylabel("Torques (Nm)");
    legend(loc=1);

    return data;
end
