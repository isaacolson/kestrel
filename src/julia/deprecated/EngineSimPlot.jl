function plotEngineSim(filepath::AbstractString, fignum::Int64 = 1)
    data = loadCsv(filepath);

    figure(fignum);
    clf();

    ax = [subplot(4, 1, 1)];
    shareId = [length(ax)];
    title("Engine Simulation SMC");

    plotVals(data["SMCSYS"]["t"], data["SMCSYS"]["x"][:,1], ["-k"], ["Actual"]);
    plotVals(data["SMCSET"]["t"], data["SMCSET"]["yd"][:,3], ["--b"], ["Setpt"]);
    xlabel("Time (s)");
    ylabel("Air Mass (kg)");
    legend(loc=0);

    ax = [ax subplot(4, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["SMCSYS"]["t"], data["SMCSYS"]["x"][:,2], ["-k"], ["Actual"]);
    plotVals(data["SMCSET"]["t"], data["SMCSET"]["yd"], ["--g", "--b"], ["Setpt", "Smooth Setpt"]);
    xlabel("Time (s)");
    ylabel("Engine Speed (rad/s)");
    legend(loc=0);

    ax = [ax subplot(4, 1, 3, sharex=ax[shareId[1]])];
    plotVals(data["SMCSYS"]["t"], data["SMCSYS"]["x"][:,3], ["-k"], ["Actual"]);
    plotVals(data["SMCSET"]["t"], data["SMCSET"]["c4"], ["-r"], ["Estimate"]);
    xlabel("Time (s)");
    ylabel("Drag Coefficient (Nms^2)");
    legend(loc=0);

    ax = [ax subplot(4, 1, 4, sharex=ax[shareId[1]])];
    plotVals(data["SMCSYS"]["t"], data["SMCSYS"]["u"], ["-k"]);
    xlabel("Time (s)");
    ylabel("Throttle Input (deg)");



    figure(fignum+1);
    clf();

    ax = [ax subplot(4, 1, 1, sharex=ax[shareId[1]])];
    title("Engine Simulation FLC");
    plotVals(data["FLCSYS"]["t"], data["FLCSYS"]["x"][:,1], ["-k"]);
    xlabel("Time (s)");
    ylabel("Air Mass (kg)");

    ax = [ax subplot(4, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["FLCSYS"]["t"], data["FLCSYS"]["x"][:,2], ["-k"], ["Actual"]);
    plotVals(data["FLCSET"]["t"], data["FLCSET"]["yd"], ["--g"], ["Setpt"]);
    xlabel("Time (s)");
    ylabel("Engine Speed (rad/s)");
    legend(loc=0);

    ax = [ax subplot(4, 1, 3, sharex=ax[shareId[1]])];
    plotVals(data["FLCSYS"]["t"], data["FLCSYS"]["x"][:,3], ["-k"]);
    xlabel("Time (s)");
    ylabel("Drag Coefficient (Nms^2)");

    ax = [ax subplot(4, 1, 4, sharex=ax[shareId[1]])];
    plotVals(data["FLCSYS"]["t"], data["FLCSYS"]["u"], ["-k"]);
    xlabel("Time (s)");
    ylabel("Throttle Input (deg)");



    figure(fignum+2);
    clf();

    ax = [ax subplot(4, 1, 1, sharex=ax[shareId[1]])];
    title("Engine Simulation PID");
    plotVals(data["PIDSYS"]["t"], data["PIDSYS"]["x"][:,1], ["-k"]);
    xlabel("Time (s)");
    ylabel("Air Mass (kg)");

    ax = [ax subplot(4, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["PIDSYS"]["t"], data["PIDSYS"]["x"][:,2], ["-k"], ["Actual"]);
    plotVals(data["PIDSET"]["t"], data["PIDSET"]["yd"], ["--g"], ["Setpt"]);
    plotVals(data["PIDSET"]["t"], data["PIDSET"]["ydFilt"], ["--b"], ["Smooth Setpt"]);
    xlabel("Time (s)");
    ylabel("Engine Speed (rad/s)");
    legend(loc=0);

    ax = [ax subplot(4, 1, 3, sharex=ax[shareId[1]])];
    plotVals(data["PIDSYS"]["t"], data["PIDSYS"]["x"][:,3], ["-k"]);
    xlabel("Time (s)");
    ylabel("Drag Coefficient (Nms^2)");

    ax = [ax subplot(4, 1, 4, sharex=ax[shareId[1]])];
    plotVals(data["PIDSYS"]["t"], data["PIDSYS"]["u"], ["-k"]);
    xlabel("Time (s)");
    ylabel("Throttle Input (deg)");



    figure(fignum+3);
    clf();

    ax = [ax subplot(3, 1, 1, sharex=ax[shareId[1]])];
    title("Engine Simulation Control Method Comparisons (max noise)");
    plotVals(data["SMCSET"]["t"], data["SMCSET"]["yd"], ["--g", "--b"], ["Setpt", "Smooth Setpt"]);
    plotVals(data["SMCSYS"]["t"], data["SMCSYS"]["x"][:,2], ["-k"], ["SMC"]);
    plotVals(data["FLCSYS"]["t"], data["FLCSYS"]["x"][:,2], ["-r"], ["FLC"]);
    plotVals(data["PIDSYS"]["t"], data["PIDSYS"]["x"][:,2], ["-c"], ["PID"]);
    xlabel("Time (s)");
    ylabel("Engine Speed (rad/s)");
    legend(loc=0);

    ax = [ax subplot(3, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["SMCSYS"]["t"], data["SMCSYS"]["u"], ["-k"], ["SMC"]);
    plotVals(data["FLCSYS"]["t"], data["FLCSYS"]["u"], ["-r"], ["FLC"]);
    plotVals(data["PIDSYS"]["t"], data["PIDSYS"]["u"], ["-c"], ["PID"]);
    xlabel("Time (s)");
    ylabel("Throttle Input (deg)");
    legend(loc=0);

    ax = [ax subplot(3, 1, 3, sharex=ax[shareId[1]])];
    plotVals(data["SMCSYS"]["t"], data["SMCSYS"]["x"][:,3], ["-k"]);
    xlabel("Time (s)");
    ylabel("Drag Coefficient (Nms^2)");

    return data;
end
