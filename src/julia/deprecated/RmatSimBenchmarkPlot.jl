function plotRmatSimBenchmark(filepath::AbstractString, fignum::Int64 = 1)
    data = loadCsv(filepath);

    figure(fignum);
    clf();
    ax = axes();
    plotVals(data["SIM"]["t"], data["SIM"]["x"][:,1:9],
             [ "-k",  "-b",  "-r", "--k", "--b", "--r", "-.k", "-.b", "-.r"],
             ["R00", "R10", "R20", "R01", "R11", "R21", "R02", "R12", "R22"]);
    legend();

    figure(fignum+1);
    clf();
    axes(sharex=ax);
    plotVals(data["SIM"]["t"], data["SIM"]["x"][:,10:12], ["-k", "-b", "-r"],
             ["wx", "wy", "wz"]);
    legend();

    return data;
end
