function plotQuatSimBenchmark(filepath::AbstractString, fignum::Int64 = 1)
    data = loadCsv(filepath);

    figure(fignum);
    clf();
    ax = axes();
    plotVals(data["SIM"]["t"], data["SIM"]["x"][:,1:4], ["-k", "-b", "-r", "-g"],
             ["qw", "qx", "qy", "qz"]);
    legend();

    figure(fignum+1);
    clf();
    axes(sharex=ax);
    plotVals(data["SIM"]["t"], data["SIM"]["x"][:,5:7], ["-k", "-b", "-r"],
             ["wx", "wy", "wz"]);
    legend();

    return data;
end
