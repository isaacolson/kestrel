function plotZOnlySim(filepath::AbstractString, fignum::Int64 = 1)
    data = loadCsv(filepath);

    figure(fignum);
    clf();

    ax = [subplot(2, 1, 1)];
    shareId = [length(ax)];
    title("Z Simulation SMC");

    plotVals(data["SYS"]["t"], data["SYS"]["x"], [ "-k", "-b"], ["ZPos (m)", "ZVel (m/s)"]);
    plotVals(data["SMC"]["t"], data["SMC"]["yd"], ["--k"], ["Zd (m)"]);
    xlabel("Time (s)");
    ylabel("Z, Zdot");
    legend(loc=1);

    ax = [ax subplot(2, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["SYS"]["t"], data["SYS"]["u"], ["-k"], ["FL"]);
    plotVals(data["SYS"]["t"], data["SYS"]["w"], ["-.k"], ["d"]);
    xlabel("Time (s)");
    ylabel("Lift Force (N)");
    legend(loc=1);

    figure(fignum+1);
    clf();

    ax = [ax axes(sharex=ax[shareId[1]])];
    plotVals(data["SMC"]["t"], data["SMC"]["yd"], ["--k"], ["zd"]);
    plotVals(data["SMC"]["t"], data["SMC"]["ydFilt"], ["-k"], ["zdFilt"]);
    plotVals(data["SMC"]["t"], data["SMC"]["yddotFilt"], ["-.k"], ["zddotFilt"]);
    plotVals(data["SMC"]["t"], data["SMC"]["yddotdotFilt"], [":k"], ["zddotdotFilt"]);
    xlabel("Time (s)");
    ylabel("Z Setpt Filter");
    legend(loc=1);

    return data;
end
