function plotQuatOnlySim(filepath::AbstractString, fignum::Int64 = 1)
    data = loadCsv(filepath);

    figure(fignum);
    clf();

    ax = [subplot(2, 1, 1)];
    shareId = [length(ax)];
    title("Quaternion Simulation SMC");

    plotVals(data["SYS"]["t"], data["SYS"]["x"], ["-k", "-r", "-g", "-b"],
             ["qt", "qx", "qy", "qz"]);
    plotVals(data["SMC"]["t"], data["SMC"]["yd"], ["--k", "--r", "--g", "--b"],
             ["qtd", "qxd", "qyd", "qzd"]);
    xlabel("Time (s)");
    ylabel("Quaternion");
    legend(loc=1);

    ax = [ax subplot(2, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["SYS"]["t"], data["SYS"]["u"], ["-r", "-g", "-b"], ["wx", "wy", "wz"]);
    plotVals(data["SYS"]["t"], data["SYS"]["w"], ["-.r", "-.g", "-.b"], ["nx", "ny", "nz"]);
    xlabel("Time (s)");
    ylabel("Angular Vel (rad/s)");
    legend(loc=1);

    figure(fignum+1);
    clf();

    ax = [ax axes(sharex=ax[shareId[1]])];
    plotVals(data["SMC"]["t"], data["SMC"]["yd"], ["--k", "--r", "--g", "--b"],
             ["qtd", "qxd", "qyd", "qzd"]);
    plotVals(data["SMC"]["t"], data["SMC"]["ydFilt"], ["-k", "-r", "-g", "-b"],
             ["qtdFilt", "qxdFilt", "qydFilt", "qzdFilt"]);
    plotVals(data["SMC"]["t"], data["SMC"]["yddotFilt"], ["-.k", "-.r", "-.g", "-.b"],
             ["qtddotFilt", "qxddotFilt", "qyddotFilt", "qzddotFilt"]);
    xlabel("Time (s)");
    ylabel("Quaternion Setpt Filter");
    legend(loc=1);


    return data;
end
