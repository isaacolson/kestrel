function plotExampleSystemImpl(filepath::AbstractString, fignum::Int64 = 1)
    data = loadCsv(filepath, noLeader=true);

    expected = [ sin(data["t"]) cos(data["t"]) sin(data["t"])-cos(data["t"]) ];

    figure(fignum);
    clf();
    plotVals(data["t"], expected,  ["--k", "--b", "--g"]);
    plotVals(data["t"], data["x"], [ "-k",  "-b",  "-g"]);

    return data;
end
