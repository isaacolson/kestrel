function plotExampleObserverSim(filepath::AbstractString, fignum::Int64 = 1)
    data = loadCsv(filepath);

    figure(fignum);
    clf();
    plotVals(data["SIM"]["t"], data["SIM"]["x"], ["--k", "--b", "--r", "--g"],
             ["sim_x", "sim_y", "sim_yaw", "sim_steer"]);
    plotVals(data["EST"]["t"], data["EST"]["x"], [ "-k",  "-b",  "-r",  "-g"],
             ["est_x", "est_y", "est_yaw", "est_steer"]);
    plot95Bounds(data["EST"]["t"], data["EST"]["x"], data["EST"]["P"],
                 ["-.k", "-.b", "-.r", "-.g"], ["2sig_x", "2sig_y", "2sig_yaw", "2sig_steer"]);
    plotVals(data["OBS_XY"]["t"], data["OBS_XY"]["z"], [".k", ".b"], ["obs_x", "obs_y"]);
    plotVals(data["OBS_Yaw"]["t"], data["OBS_Yaw"]["z"], [".g"], ["obs_yaw"]);
    legend();

    figure(fignum+1);
    clf();
    plotPhase(data["SIM"]["x"], [1 2], ["--k"], ["sim x vs y"]);
    plotPhase(data["EST"]["x"], [1 2], [ "-k"], ["est x vs y"]);
    legend();

    return data;
end
