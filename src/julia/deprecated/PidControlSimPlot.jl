function plotExamplePidControlSim(filepath::AbstractString, fignum::Int64 = 1)
    data = loadCsv(filepath);

    figure(fignum);
    clf();
    ax = axes();
    #plotVals(data["SYS"]["t"], data["SYS"]["x"], ["-k", "-b"], ["x", "xdot"]);
    plotVals(data["SET"]["t"], data["SET"]["yd"], ["--g"], ["yd"]);
    plotVals(data["SET"]["t"], data["SET"]["y"], ["-g"], ["y"]);
    plotVals(data["SET"]["t"], data["SET"]["ydot"], ["-c"], ["ydot"]);
    plotVals(data["SYS"]["t"], data["SYS"]["u"], ["-r"], ["u"]);
    legend();

    figure(fignum+1);
    clf();
    axes(sharex=ax);
    plotVals(data["SET"]["t"], data["SET"]["error"], ["-g", "-k", "-c"], ["E_p", "E_i", "E_d"]);
    legend();

    return data;
end
