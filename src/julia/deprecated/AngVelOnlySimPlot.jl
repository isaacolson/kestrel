function plotAngVelOnlySim(filepath::AbstractString, fignum::Int64 = 1)
    data = loadCsv(filepath);

    figure(fignum);
    clf();

    ax = [subplot(2, 1, 1)];
    shareId = [length(ax)];
    title("Angular Velocity Simulation SMC");

    plotVals(data["SYS"]["t"], data["SYS"]["x"], [ "-r", "-g", "-b"], ["wx", "wy", "wz"]);
    plotVals(data["SMC"]["t"], data["SMC"]["yd"], ["--r", "--g", "--b"], ["wxd", "wyd", "wzd"]);
    xlabel("Time (s)");
    ylabel("Angular Vel (rad/s)");
    legend(loc=1);

    ax = [ax subplot(2, 1, 2, sharex=ax[shareId[1]])];
    plotVals(data["SYS"]["t"], data["SYS"]["u"], ["-r", "-g", "-b"], ["ax", "ay", "az"]);
    plotVals(data["SYS"]["t"], data["SYS"]["w"], ["-.r", "-.g", "-.b"], ["nx", "ny", "nz"]);
    xlabel("Time (s)");
    ylabel("Applied Torque (Nm)");
    legend(loc=1);

    figure(fignum+1);
    clf();

    ax = [ax axes(sharex=ax[shareId[1]])];
    plotVals(data["SMC"]["t"], data["SMC"]["yd"], ["--r", "--g", "--b"], ["wxd", "wyd", "wzd"]);
    plotVals(data["SMC"]["t"], data["SMC"]["ydFilt"], ["-r", "-g", "-b"],
             ["wxdFilt", "wydFilt", "wzdFilt"]);
    plotVals(data["SMC"]["t"], data["SMC"]["yddotFilt"], ["-.r", "-.g", "-.b"],
             ["wxddotFilt", "wyddotFilt", "wzddotFilt"]);
    xlabel("Time (s)");
    ylabel("Angular Vel Setpt Filter");
    legend(loc=1);

    return data;
end
