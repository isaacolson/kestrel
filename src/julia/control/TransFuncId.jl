importall PyPlot;

"""
Plots the FFT and the Bode plot of a transfer function id. Can optionally be
used to plot a guess at the transfer function. Note that specifying multiple
forms of estimate will result in the multiplication of the three.

keywords:
 titleStr  : set part of the title of the figure
 est2ord   : [natFreq, damping] the second order estimated response
 estZpk    : ([z0, z1, z2, ...], [p0, p1, p2, ...], k) zeros, poles, and gain
             of the transfer function
 estNumDen : ([n0, n1, n2, ...], [d0, d1, d2, ...]) numerator and denominator
             polynomials of estimate transfer function
 omegalim  : [min, max] limits on frequencies displayed in plots (x axis)
 gainLim   : [min, max] limits on gain displayed in Bode plot (y axis)
 phaseLim  : [min, max] limits on phase displayed in Bode plot (y axis)
 infoMin   : minimum input magnitude to be considered a "good datapoint"
 log       : plot in units of decibels / decade

`fftdata = transFuncId([tStart; tEnd], tInput, xInput, tOutput, xOutput);`
`plotTransFuncId(fftdata..., fignum);`
`plotTransFuncId(fftdata..., fignum, titlestr="my system", est2ord = [2 0.7]);`
"""
function plotTransFuncId( omega::Array{Float64},
                           gain::Array{Float64,1},
                          phase::Array{Float64,1},
                          input::Array{Complex{Float64},1},
                         output::Array{Complex{Float64},1},
                         fignum::Int64 = 1;
                         kwargs...)

    d = Dict(val[1] => val[2] for val in kwargs);

    omegaLim = get(d, :omegaLim, []);
    gainLim  = get(d, :gainLim,  []);
    phaseLim = get(d, :phaseLim, []);
    infoMin  = get(d, :infoMin,   0);

    estGiven = [false, false, false]
    est = ones(size(omega));
    est2ord = get(d, :est2ord, []);
    if (typeof(est2ord)<:Array && length(est2ord) == 2 &&
        typeof(est2ord[1])<:Real && typeof(est2ord[2])<:Real)

        estGiven[1] = true;

        numer = est2ord[1]^2;
        denom = -omega.*omega + 2*est2ord[1]*est2ord[2]*omega*im + est2ord[1]^2;

        est = est .* numer ./ denom;
    end

    estZpk = get(d, :estZpk, ());
    if (typeof(estZpk)<:Tuple && length(estZpk) == 3 &&
        typeof(estZpk[1])<:Array && !isempty(estZpk[1]) &&
        typeof(estZpk[2])<:Array && !isempty(estZpk[2]) &&
        typeof(estZpk[1][1])<:Real && typeof(estZpk[2][1])<:Real &&
        typeof(estZpk[3])<:Real)

        estGiven[2] = true;

        z = estZpk[1];
        p = estZpk[2];
        k = estZpk[3];

        numer = ones(size(omega));
        denom = ones(size(omega));

        for i = 1:length(z)
            numer = numer .* (omega*im - z[i]);
        end
        for i = 1:length(p)
            denom = denom .* (omega*im - p[i]);
        end

        est = est .* k .* numer ./ denom;
    end

    estNumDen = get(d, :estNumDen, ());
    if (typeof(estNumDen)<:Tuple && length(estNumDen) == 2 &&
        typeof(estNumDen[1])<:Array && !isempty(estNumDen[1]) &&
        typeof(estNumDen[2])<:Array && !isempty(estNumDen[2]) &&
        typeof(estNumDen[1][1])<:Real && typeof(estNumDen[2][1])<:Real)

        estGiven[3] = true;

        numC = estNumDen[1];
        denC = estNumDen[2];

        numer = zeros(size(omega));
        denom = zeros(size(omega));

        expo = 0;
        for i = 1:length(numC)
            numer += numC[i] * (omega*im).^expo;
            expo += 1;
        end

        expo = 0;
        for i = 1:length(denC)
            denom += denC[i] * (omega*im).^expo;
            expo += 1;
        end

        est = est .* numer ./ denom;
    end

    idx = find(x -> abs(x) > infoMin, input);
     omegaGood =  omega[idx];
      gainGood =   gain[idx];
     phaseGood =  phase[idx];
     inputGood =  input[idx];
    outputGood = output[idx];


    figure(fignum); clf();
    ax = [subplot(3, 1, 1)];
    shareId = [length(ax)];
    ts = string("FFT and Bode Plot: ", get(d, :titleStr, ""));
    if (estGiven[1])
        ts = string(ts, "\n est2ord = ", est2ord);
    end
    if (estGiven[2])
        ts = string(ts, ",\nestZ = ", z, "    estP = ", p, "    estK = ", k);
    end
    if (estGiven[3])
        ts = string(ts, ",\nestNum = ", numC, "    estDen = ", denC);
    end
    title(ts);

    val     = [abs(input) abs(output)];
    valGood = [abs(inputGood) abs(outputGood)];
    if (get(d, :log, false))
        val     = 20*log10(val);
        valGood = 20*log10(valGood);
        ax[end][:set_xscale]("log");
    end

    plotVals(omega, val, ["-k", "-b"], ["input FFT abs", "output FFT abs"], linewidth=0.5);
    plotVals(omegaGood, valGood, ["-k", "-b"], linewidth=2);
    xlabel("Freq (rad/s)");
    legend(loc=1);
    if (!isempty(omegaLim))
        xlim(omegaLim);
    end

    ax = [ax subplot(3, 1, 2, sharex=ax[shareId[1]])];
    val     = gain;
    valGood = gainGood;
    if (get(d, :log, false))
        val     = 20*log10(val);
        valGood = 20*log10(valGood);
        ax[end][:set_xscale]("log");
    end
    plotVals(omega, val, ["-k"], ["data"], linewidth=0.5);
    plotVals(omegaGood, valGood, ["-k"], linewidth=2);
    if (any(estGiven))
        val = abs(est);
        if (get(d, :log, false))
            val = 20*log10(val);
        end
        plotVals(omega, val, ["-b"], ["est"], linewidth=1);
    end
    xlabel("Freq (rad/s)");

    if (get(d, :log, false))
        ylabel("Gain (dB)");
    else
        ylabel("Gain (abs)");
    end
    legend(loc=1);
    if (!isempty(omegaLim))
        xlim(omegaLim);
    end
    if (!isempty(gainLim))
        ylim(gainLim);
    end

    ax = [ax subplot(3, 1, 3, sharex=ax[shareId[1]])];
    if (get(d, :log, false))
        ax[end][:set_xscale]("log");
    end
    plotVals(omega, phase, ["-k"], ["data"], linewidth=0.5);
    plotVals(omegaGood, phaseGood, ["-k"], linewidth=2);
    if (any(estGiven))
        plotVals(omega, angle(est), ["-b"], ["est"], linewidth=1);
    end
    xlabel("Freq (rad/s)");
    ylabel("phase (rad)");
    legend(loc=1);
    if (!isempty(omegaLim))
        xlim(omegaLim);
    end
    if (!isempty(phaseLim))
        ylim(phaseLim);
    end
end


"""
Computes the Bode info of the transfer from an input signal to an output signal.
Signals should have constant frequency samples (TODO: switch to nfft to remove
this requirement), though the frequency of the input may be different than that
of the output. Evaluates the signals on the range tRange[1] < t < tRange[2].

`(omega, gain, phase, input, output) = transFuncId( [tStart; tEnd],
                                                    tInput,  xInput,
                                                   tOutput, xOutput);`
"""
function transFuncId{T1<:Real, T2<:Real}( tRange::Array{T1},
                                          tInput::Array{T2},
                                          xInput::Array{T2},
                                         tOutput::Array{T2},
                                         xOutput::Array{T2})

     inOmega,  inFft = fftInfo(tRange,  tInput,  xInput);
    outOmega, outFft = fftInfo(tRange, tOutput, xOutput);

    # Get the smaller frequency range and strip the fft values
     omega::Array{Float64}            = size(inOmega)[1] < size(outOmega)[1] ? inOmega : outOmega;
     input::Array{Complex{Float64},1} =  inFft[1:size(omega)[1]];
    output::Array{Complex{Float64},1} = outFft[1:size(omega)[1]];

    response::Array{Complex{Float64},1} = output ./ input;
        gain::Array{Float64,1}          = abs(response);
       phase::Array{Float64,1}          = angle(response);

    return omega, gain, phase, input, output;
end

"""
Computes the fft in frequency space of a signal on the range
tRange[1] < t < tRange[2]. The magnitude of the fft is normalized by the number
of samples given (so they may be compared with other fft data generated by this
function).

`(omega, fft) = fftInfo([tStart; tEnd], t, x);`
"""
function fftInfo{T1<:Real, T2<:Real}(tRange::Array{T1},
                                          t::Array{T2},
                                          x::Array{T2})
    comp(a) = tRange[1] < a < tRange[2];

    idx::Array{Int64,1}   = find(comp, t);
     tr::Array{Float64,1} = t[idx];
     xr::Array{Float64,1} = x[idx];

    assert(length(size(t)) == 1 || (length(size(t)) == 2 && size(t)[2] == 1));
    assert(length(size(x)) == 1 || (length(size(x)) == 2 && size(x)[2] == 1));

         n::Int64   = size(tr)[1];
      nSig::Int64   = floor(n / 2.0) + 1;
    period::Float64 = mean(diff(t));
     omega::Float64 = 2*pi / period;

      xFft::Array{Complex{Float64},1} = fft(xr)[1:nSig];
    xOmega::Array{Float64}            = collect((0:nSig-1) * omega / n);

    # normalize the output by number of samples used:
    # http://www.mechanicalvibration.com/Making_matlab_s_fft_functio.html
    xFft[1]       /= n;
    xFft[end]     /= n;
    xFft[2:end-1] /= n / 2;

    return xOmega, xFft;
end
