function loadCsv{T<:AbstractString}(filepath::T; noLeader::Bool = false, delim::Char = ',')
    csv = readdlm(filepath, delim);

    if (noLeader)
        return buildDataDict(csv, rowIdsPresent=false);
    else
        ids = getRowIds(csv);
        data::Dict{AbstractString, Dict{AbstractString, Array{Float64}}} =
            Dict(id => buildDataDict(extractIdRows(csv, id)) for id in ids);
        return data;
    end
end

function buildDataDict(rowSimilarData::Array{Any,2}; rowIdsPresent=true)
    if (rowIdsPresent)
        ids = getColIds(rowSimilarData[:,2:end]);
    else
        ids = getColIds(rowSimilarData);
    end
    data::Dict{AbstractString, Array{Float64}} =
        Dict(id => extractIdCols(rowSimilarData, id) for id in ids);
    return data;
end

function getRowIds(data::Array{Any,2})
    ids::Array{AbstractString} = map(strip, unique(data[:,1]));
    return ids;
end

function extractIdRows(data::Array{Any,2}, id::AbstractString)
    return data[map(strip, data[:,1]) .== id, :];
end

function getColIds(rowSimilarData::Array{Any,2})
    n = size(rowSimilarData)[2];
    idLoc = fill(false, n);
    for i = 1:n
        idLoc[i] = (typeof(rowSimilarData[1,i]) <: AbstractString) & (rowSimilarData[1,i] != "");
    end
    ids::Array{AbstractString} = map(strip, rowSimilarData[1,idLoc]);
    return ids;
end

function extractIdCols(rowSimilarData::Array{Any,2}, id::AbstractString)
    n = size(rowSimilarData)[2];
    dataLoc = fill(false,n);
    found = false;
    for i = 1:n
        if found
            if typeof(rowSimilarData[1,i]) <: Number
                dataLoc[i] = true;
            else
                break;
            end
        elseif typeof(rowSimilarData[1,i]) <: AbstractString
            if strip(rowSimilarData[1,i]) == id
                found = true;
            end
        end
    end
    data::Array{Float64} = rowSimilarData[:,dataLoc];
    return data;
end
