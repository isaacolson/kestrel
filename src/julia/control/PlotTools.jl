importall PyPlot;
using PyCall;

function plotVals{T1<:Real, T2<:Real, T3<:AbstractString}(time::Array{T1},
                                                          vals::Array{T2},
                                                          linespecs::Array{T3};
                                                          kwargs...)
    plotVals(time, vals, linespecs, fill("", length(linespecs)); kwargs...);
end
function plotVals{T1<:Real, T2<:Real, T3<:AbstractString, T4<:AbstractString}(time::Array{T1},
                                                                              vals::Array{T2},
                                                                              linespecs::Array{T3},
                                                                              labels::Array{T4};
                                                                              kwargs...)
    # Note: number of plots controlled by the number of cols in vals, not by the size of linespecs
    #       or labels. The user is responsible for passing sufficient linespecs & labels to
    #       cover the data, but it is not an error to have extra linespecs & labels.
    # TODO: though it it relatively easy to make this plot different columns of vals by passing in
    #       a modified matrix, for api consistency with the changes mentioned for the 95 bounds
    #       function, should make the same input index array changes here
    for i = 1:size(vals, 2)
        plot(time, vals[:,i], linespecs[i], hold=true, label=labels[i]; kwargs...);
    end
end

function plot95Bounds{T<:AbstractString}(time::Array{Float64},
                                         vals::Array{Float64},
                                         cov::Array{Float64},
                                         linespecs::Array{T};
                                         kwargs...)
    plot95Bounds(time, vals, cov, linespecs, fill("", length(linespecs)); kwargs...);
end
function plot95Bounds{T1<:AbstractString, T2<:AbstractString}(time::Array{Float64},
                                                              vals::Array{Float64},
                                                              cov::Array{Float64},
                                                              linespecs::Array{T1},
                                                              labels::Array{T2};
                                                              kwargs...)
    # TODO: this function is only really good at plotting cov bounds when you pass in the whole
    #       vals and cov vectors, it would be hard to get part of the vals vector (say the third
    #       and fourth col) by themselves because you would have to chop out the right parts of the
    #       cov vector. Consider adding an index array input that tells the function which cols
    #       of vals it should plot
    n = size(vals, 2);
    for i = 1:n
        # Note: this is a univariable representation of the 2 sigma bounds, which is not accurate
        #       for multivariable statistics. Check out a few of these relevant pages to improve
        #       the bounds if necessary:
        #       http://jonathantemplin.com/files/multivariate/mv11icpsr/mv11icpsr_lecture04.pdf
        #       https://en.wikipedia.org/wiki/Chi-squared_distribution
        #       https://en.wikipedia.org/wiki/Particular_values_of_the_Gamma_function
        plot(time, vals[:,i] - 2*sqrt(cov[:,i+(i-1)*n]), linespecs[i], hold=true, label=labels[i];
             kwargs...);
        plot(time, vals[:,i] + 2*sqrt(cov[:,i+(i-1)*n]), linespecs[i], hold=true; kwargs...);
    end
end

function plotPhase{T<:AbstractString}(vals::Array{Float64},
                                      pairs::Array{Int64,2},
                                      linespecs::Array{T};
                                      kwargs...)
    plotPhase(vals, pairs, linespecs, fill("", length(linespecs)); kwargs...);
end
function plotPhase{T1<:AbstractString, T2<:AbstractString}(vals::Array{Float64},
                                                           pairs::Array{Int64,2},
                                                           linespecs::Array{T1},
                                                           labels::Array{T2};
                                                           kwargs...)
    for i = 1:size(pairs, 1)
        plot(vals[:,pairs[i,1]], vals[:,pairs[i,2]], linespecs[i], hold=true, label=labels[i];
             kwargs...);
    end
end

__smlg = nothing;
function setupSmartLegend{T<:AbstractString}(kestrelHome::T = ENV["KESTREL_HOME"])
    if (!("$kestrelHome/src/python" in PyVector(pyimport("sys")["path"])))
        unshift!(PyVector(pyimport("sys")["path"]), "$kestrelHome/src/python");
    end
    global __smlg = pyimport("smartLegend");
end

function smartLegend(;kwargs...)
    if (!isa(__smlg, Void))
        leg = legend(;kwargs...);
        __smlg[:interactive_legend]();
    else
        warn("failed to create interactive legend, have you run setupSmartLegend yet?");
    end
end

