module Control
include("CsvTools.jl");
include("PlotTools.jl");

include("Rotations.jl");
include("TransFuncId.jl");
include("StateSpace.jl");
end

import Control;
importall PyPlot;
