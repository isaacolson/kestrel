

"""
Force an object to be a 2 dim array of the specified size (works on scalars and arrays) \n
`a = array([1 2], 2, 1)`
"""
array(x, r, c) = reshape(collect(x), r, c);

"""
Force an object to be a 2 dim array of the specified size and type \n
`a = array([1 2], 2, 1, Float64)`
"""
array(x, r, c, t) = convert(Array{t,2}, array(x, r, c));


"""
State Space class \n
`sys = StateSpace(A, B, C, D)`
"""
type StateSpace
    n::Int64;
    m::Int64;
    p::Int64;
    A::Array{Float64, 2};
    B::Array{Float64, 2};
    C::Array{Float64, 2};
    D::Array{Float64, 2};

    function StateSpace(A::Any, B::Any, C::Any, D::Any)
        n = size(A, 1);
        m = size(B, 2);
        p = size(C, 1);
        @assert    n == size(A, 2)    "A must be square"
        @assert    n == size(B, 1)    "B must have same rows as A"
        @assert    n == size(C, 2)    "C must have same cols as A"
        @assert    m == size(D, 2)    "D must have same cols as B"
        @assert    p == size(D, 1)    "D must have same rows as C"

        return new(n, m, p,
                   array(A, n, n, Float64),
                   array(B, n, m, Float64),
                   array(C, p, n, Float64),
                   array(D, p, m, Float64));
    end
end


"""
Constructs a 2nd order filter with the specified natural frequency and damping ratio.
The outputs will be ordered [ lowpass; bandpass; highpass ]. \n
`filt = SecondOrderFilt(natFreq, damping)`
"""
function SecondOrderFilter(natFreq::Float64, damping::Float64)
    @assert    0 < natFreq    "Natural Frequency must be positive"
    @assert    0 < damping    "Damping Ratio must be positive"

     w2::Float64 = natFreq*natFreq;
    zw2::Float64 = 2*damping*natFreq;

    return StateSpace([0 1; -w2 -zw2], [0; 1], [ w2 0; 0 zw2; -w2 -zw2 ], [0; 0; 1]);
end
function SecondOrderFilter(natFreq::Any, damping::Any)
    return SecondOrderFilter(convert(Float64, natFreq), convert(Float64, damping));
end


"""
Convenient size checks on the state and input vectors \n
`checkVectorDims(sys, x, u)`
"""
function checkVectorDims(sys::StateSpace, x::Array{Float64,2}, u::Array{Float64,2})
    @assert    sys.n == size(x, 1)    "x must be $(sys.n) long. x = $x"
    @assert        1 == size(x, 2)    "x must be a column vector. x = $x"
    @assert    sys.m == size(u, 1)    "u must be $(sys.m) long. u = $u"
    @assert        1 == size(u, 2)    "u must be a column vector. u = $u"
end


"""
Computes the derivative of the state given the state and input vectors \n
`dx = deriv(sys, x, u)`
"""
function deriv(sys::StateSpace, x::Array{Float64,2}, u::Array{Float64,2})
    checkVectorDims(sys, x, u);
    ret::Array{Float64,2} = sys.A*x + sys.B*u;
    return ret;
end
function deriv(sys::StateSpace, x::Any, u::Any)
    return deriv(sys, array(x, length(x), 1, Float64), array(u, length(u), 1, Float64));
end


"""
Computes the output of the system given the state and input vectors \n
`y = output(sys, x, u)`
"""
function output(sys::StateSpace, x::Array{Float64,2}, u::Array{Float64,2})
    checkVectorDims(sys, x, u);
    ret::Array{Float64,2} = sys.C*x + sys.D*u;
    return ret;
end
function output(sys::StateSpace, x::Any, u::Any)
    return output(sys, array(x, length(x), 1, Float64), array(u, length(u), 1, Float64));
end


"""
Integrates a state over the given timestep using the given state and input vectors \n
`x = integ(sys, dt, x, u)`
"""
function integ(sys::StateSpace, dt::Float64, x::Array{Float64,2}, u::Array{Float64,2})
    checkVectorDims(sys, x, u);

    # Note: using 4th order RK integration
    k1::Array{Float64,2} = deriv(sys, x            , u);
    k2::Array{Float64,2} = deriv(sys, x + dt/2 * k1, u);
    k3::Array{Float64,2} = deriv(sys, x + dt/2 * k2, u);
    k4::Array{Float64,2} = deriv(sys, x + dt   * k3, u);

    ret::Array{Float64,2} = x + dt/6 * (k1 + 2*k2 + 2*k3 + k4);
    return ret;
end
function integ(sys::StateSpace, dt::Any, x::Any, u::Any)
    return integ(sys, convert(Float64, dt),
                 array(x, length(x), 1, Float64), array(u, length(u), 1, Float64));
end


# TODO: Should make this also optionally initialize the system using the 0 freq transfer func equiv
"""
Simulates a system over the provided time series of inputs. Returns a length(t) by p array
representing the output of the system over the given time range

### Keyword Args
```
  initX    : n by 1 array representing the initial state of the system
```

"""
function sim(sys::StateSpace, t::Array{Float64,2}, u::Array{Float64,2}; kwargs...)

    len = length(t);

    kw = Dict(val[1] => val[2] for val in kwargs);
    x::Array{Float64,2} = array(get(kw, :initX, zeros(sys.n, 1)), sys.n, 1, Float64);

    @assert    len   >= 1             "Cannot operate on an empty time vector"
    @assert    len   == size(t, 1)    "Expecting t as a column vector"
    @assert    1     == size(t, 2)    "Expecting t as a column vector"
    @assert    len   == size(u, 1)    "Expecting length(t) rows of u"
    @assert    sys.m == size(u, 2)    "Expecting $(sys.m) cols of u"
    @assert    sys.n == size(x, 1)    "Expecting $(sys.n) rows of x"
    @assert    1     == size(x, 2)    "Expecting x as a column vector"

    ret::Array{Float64,2} = zeros(len, sys.p);

    ret[1,:] = output(sys, x, u[1,:]')';
    for i = 2:len
        x = integ(sys, t[i] - t[i-1], x, u[i,:]');
        ret[i,:] = output(sys, x, u[i,:]')';
    end

    return ret;
end
function sim(sys::StateSpace, t::Any, u::Any; kwargs...)
    return sim(sys, array(t, length(t),     1, Float64),
                    array(u, length(t), sys.m, Float64); kwargs...);
end
