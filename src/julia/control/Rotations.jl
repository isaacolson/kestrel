TINY = 0.000001;

"""
Computes the cross multiply operator:

`cross(r, v) = crossMat(r) * v`
"""
function crossMat{T<:Real}(r::Array{T})
    return [    0 -r[3]  r[2]
             r[3]     0 -r[1]
            -r[2]  r[1]     0];
end

"""
Constructs a Rotation Matrix from a given angle and axis of rotation

`R = angAxToRotMat(angle, axis)`
"""
function angAxToRotMat{T1<:Real, T2<:Real}(theta::T1, u::Array{T2})
    c = cos(theta);
    s = sin(theta);
    uhat = u[1:3] / norm(u[1:3]);
    return c * eye(3) + (1 - c) * uhat * uhat' + s * crossMat(uhat);
end

"""
Constructs a quaternion from a given angle and axis of rotation

`q = angAxToQuat(angle, axis)`
"""
function angAxToQuat{T1<:Real, T2<:Real}(theta::T1, u::Array{T2})
    return [ cos(theta/2); u[1:3]/norm(u) * sin(theta/2) ];
end

"""
Converts the inverse rotation from a quaternion

`qinv = quatInv(q)`
"""
function quatInv{T<:Real}(q::Array{T})
    return [ q[1]; -q[2:4] ];
end

"""
Normalizes a quaternion and ensures the first element is positive (for easy comparisons)
"""
function quatNormalize{T<:Real}(q::Array{T})
    if (q[1] >= 0)
        return q[1:4] / norm(q[1:4]);;
    else
        return -q[1:4] / norm(q[1:4]);
    end
end

"""
Returns a matrix that performs quaternion pre-multiplication:

`quatMult(q1, q2) == quatCompLmat(q1) * q2`
"""
function quatCompLmat{T<:Real}(q::Array{T})
    return [ q[1]                       -q[2:4]'
             q[2:4] q[1]*eye(3)+crossMat(q[2:4]) ];
end

"""
Returns a matrix that performs quaternion post-multiplication:

`quatMult(q1, q2) == quatCompRmat(q2) * q1`
"""
function quatCompRmat{T<:Real}(q::Array{T})
    return [ q[1]                       -q[2:4]'
             q[2:4] q[1]*eye(3)-crossMat(q[2:4]) ];
end

"""
Quaternion multiply: composes rotations represented by the quaternions

`q1q2 = quatMult(q1, q2)`
"""
function quatMult{T1<:Real, T2<:Real}(q1::Array{T1}, q2::Array{T2})
    return quatCompLmat(q1[1:4]) * q2[1:4];
end

"""
Converts a quaternion to a rotation matrix

`R = quatToRotMat(q)`
"""
function quatToRotMat{T<:Real}(q::Array{T})
    qw2  = q[1]*q[1];
    qx2  = q[2]*q[2];
    qy2  = q[3]*q[3];
    qz2  = q[4]*q[4];
    qwqx = q[1]*q[2];
    qwqy = q[1]*q[3];
    qwqz = q[1]*q[4];
    qxqy = q[2]*q[3];
    qxqz = q[2]*q[4];
    qyqz = q[3]*q[4];
    return [ qw2+qx2-qy2-qz2    2*(qxqy-qwqz)    2*(qwqy+qxqz)
               2*(qwqz+qxqy)  qw2-qx2+qy2-qz2    2*(qyqz-qwqx)
               2*(qxqz-qwqy)    2*(qwqx+qyqz)  qw2-qx2-qy2+qz2 ];
end

"""
Converts a rotation matrix to a quaterion

`q = rotMatToQuat(R)`
"""
function rotMatToQuat{T<:Real}(R::Array{T,2})
    den::Float64 = 1;
    if (1 + R[1,1] + R[2,2] + R[3,3] > TINY)
        den = 2.0 * sqrt(1.0 + R[1,1] + R[2,2] + R[3,3]);
        return [ 0.25 * den
                 (R[3,2] - R[2,3]) / den
                 (R[1,3] - R[3,1]) / den
                 (R[2,1] - R[1,2]) / den];
    elseif (R[1,1] > R[2,2] && R[1,1] > R[3,3])
        den = 2.0 * sqrt(1.0 + R[1,1] - R[2,2] - R[3,3]);
        return [ (R[3,2] - R[2,3]) / den
                 0.25 * den
                 (R[2,1] + R[1,2]) / den
                 (R[3,1] + R[1,3]) / den];
    elseif (R[2,2] > R[1,1] && R[2,2] > R[3,3])
        den = 2.0 * sqrt(1.0 - R[1,1] + R[2,2] - R[3,3]);
        return [ (R[1,3] - R[3,1]) / den
                 (R[2,1] + R[1,2]) / den
                 0.25 * den
                 (R[3,1] + R[1,3]) / den];
    else  # R[3,3] > R[1,1] && R[3,3] > R[2,2])
        den = 2.0 * sqrt(1.0 - R[1,1] - R[2,2] + R[3,3]);
        return [ (R[2,1] - R[1,2]) / den
                 (R[3,1] + R[1,3]) / den
                 (R[3,2] + R[2,3]) / den
                 0.25 * den ];
    end
end

"""
Convert Euler angles to a Rotation matrix

`R = eulerToRotMat(roll, pitch, yaw)`
"""
function eulerToRotMat{T1<:Real, T2<:Real, T3<:Real}(r::T1, p::T2, y::T3)
    # need the extra parenthesis to get the multi line expression to work properly
    return angAxToRotMat(y, [0; 0; 1]) * angAxToRotMat(p, [0; 1; 0]) * angAxToRotMat(r, [1; 0; 0]);
end
function eulerToRotMat{T<:Real}(rpy::Array{T})
    return eulerToRotMat(rpy...)
end

"""
Converts a Rotation matrix to Euler angls

`rpy = rotMatToEuler(R)`
"""
function rotMatToEuler{T<:Real}(R::Array{T,2})
    p::Float64 = 0;
    if     (abs(R[3,1] - 1) < TINY)
        p = -pi/2;
    elseif (abs(R[3,1] + 1) < TINY)
        p =  pi/2;
    else
        p = -asin(R[3,1]);
    end

    if (abs(cos(p)) < TINY)
        return [0; p; -atan2(R[1,2], R[1,3])];
    else
        return [atan2(R[3,2], R[3,3]); p; atan2(R[2,1], R[1,1])];
    end
end

"""
Convert angular velocity to Euler derivatives

`drpy = angVelToEuler(rpy, omega)`
"""
function angVelToEuler{T1<:Real, T2<:Real}(rpy::Array{T1}, omega::Array{T2})
    # Note: if we want jacobeans, could easily rip this out to use
    sr = sin(rpy[1]);
    cr = cos(rpy[1]);
    sp = sin(rpy[2]);
    cp = cos(rpy[2]);
    jac = [ 1  0  -sp;  0  cr  sr*cp;  0  -sr  cr*cp ];
    return jac * omega;
end

"""
Convert quaternion derivative to angular velocity

`omega = quatToAngVel(q, qdot)`
"""
function quatToAngVel{T1<:Real, T2<:Real}(q::Array{T1}, qdot::Array{T2})
    # Note: can rip this matrix out into a helper function if we do more quat / ang vel conversions
    jac = 2 * [ -q[2:4] (q[1] * eye(3) + crossMat(q[2:4])') ];
    return jac * qdot;
end

"""
Converts angular velocity to quaternion derivative

`qdot = angVelToQuat(q, omega)`
"""
function angVelToQuat{T1<:Real, T2<:Real}(q::Array{T1}, omega::Array{T2})
    # Note: can rip this matrix out into a helper function if we do more quat / ang vel conversions
    jac = 0.5 * [ 0 -omega'; omega crossMat(-omega) ];
    return jac * q;
end
